'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Mention = function (_Entity) {
    _inherits(Mention, _Entity);

    function Mention() {
        _classCallCheck(this, Mention);

        return _possibleConstructorReturn(this, (Mention.__proto__ || Object.getPrototypeOf(Mention)).apply(this, arguments));
    }

    _createClass(Mention, [{
        key: 'url',

        // URL of user's profile (can be remote)
        value: function url() {
            return this.json.url;
        }
        // The username of the account

    }, {
        key: 'username',
        value: function username() {
            return this.json.username;
        }
        // Equals username for local users, includes @domain for remote ones

    }, {
        key: 'acct',
        value: function acct() {
            return this.json.acct;
        }
        // Account ID

    }, {
        key: 'id',
        value: function id() {
            return this.json.id;
        }
    }, {
        key: 'hasUrl',
        value: function hasUrl(url) {
            return this.url() === url;
        }
    }]);

    return Mention;
}(_entity2.default);

exports.default = Mention;
//# sourceMappingURL=mention.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1lbnRpb24uanMiXSwibmFtZXMiOlsiTWVudGlvbiIsImpzb24iLCJ1cmwiLCJ1c2VybmFtZSIsImFjY3QiLCJpZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQTs7Ozs7Ozs7OzsrZUFIQTs7Ozs7SUFJTUEsTzs7Ozs7Ozs7Ozs7O0FBQ0Y7OEJBQ007QUFDRixtQkFBTyxLQUFLQyxJQUFMLENBQVVDLEdBQWpCO0FBQ0g7QUFDRDs7OzttQ0FDVztBQUNQLG1CQUFPLEtBQUtELElBQUwsQ0FBVUUsUUFBakI7QUFDSDtBQUNEOzs7OytCQUNPO0FBQ0gsbUJBQU8sS0FBS0YsSUFBTCxDQUFVRyxJQUFqQjtBQUNIO0FBQ0Q7Ozs7NkJBQ0s7QUFDRCxtQkFBTyxLQUFLSCxJQUFMLENBQVVJLEVBQWpCO0FBQ0g7OzsrQkFDTUgsRyxFQUFLO0FBQ1IsbUJBQU8sS0FBS0EsR0FBTCxPQUFlQSxHQUF0QjtBQUNIOzs7Ozs7a0JBRVVGLE87QUFDZiIsImZpbGUiOiJtZW50aW9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjQuXG4gKi9cbmltcG9ydCBFbnRpdHkgZnJvbSAnLi4vZW50aXR5JztcbmNsYXNzIE1lbnRpb24gZXh0ZW5kcyBFbnRpdHkge1xuICAgIC8vIFVSTCBvZiB1c2VyJ3MgcHJvZmlsZSAoY2FuIGJlIHJlbW90ZSlcbiAgICB1cmwoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24udXJsO1xuICAgIH1cbiAgICAvLyBUaGUgdXNlcm5hbWUgb2YgdGhlIGFjY291bnRcbiAgICB1c2VybmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi51c2VybmFtZTtcbiAgICB9XG4gICAgLy8gRXF1YWxzIHVzZXJuYW1lIGZvciBsb2NhbCB1c2VycywgaW5jbHVkZXMgQGRvbWFpbiBmb3IgcmVtb3RlIG9uZXNcbiAgICBhY2N0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLmFjY3Q7XG4gICAgfVxuICAgIC8vIEFjY291bnQgSURcbiAgICBpZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5pZDtcbiAgICB9XG4gICAgaGFzVXJsKHVybCkge1xuICAgICAgICByZXR1cm4gdGhpcy51cmwoKSA9PT0gdXJsO1xuICAgIH1cbn1cbmV4cG9ydCBkZWZhdWx0IE1lbnRpb247XG4vLyMgc291cmNlTWFwcGluZ1VSTD1tZW50aW9uLmpzLm1hcCJdfQ==