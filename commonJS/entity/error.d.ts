/**
 * Created by tottokotkd on 17/04/24.
 */
import Entity from '../entity';
declare class Error extends Entity {
    error(): any;
}
export default Error;
