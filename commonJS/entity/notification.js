'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _account = require('./account');

var _account2 = _interopRequireDefault(_account);

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

var _status = require('./status');

var _status2 = _interopRequireDefault(_status);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Notification = function (_Entity) {
    _inherits(Notification, _Entity);

    function Notification() {
        _classCallCheck(this, Notification);

        return _possibleConstructorReturn(this, (Notification.__proto__ || Object.getPrototypeOf(Notification)).apply(this, arguments));
    }

    _createClass(Notification, [{
        key: 'id',

        // The notification ID
        value: function id() {
            return this.json.id;
        }
        // One of: "mention", "reblog", "favourite", "follow"

    }, {
        key: 'type',
        value: function type() {
            return this.json.type;
        }
        // The time the notification was created

    }, {
        key: 'createdAt',
        value: function createdAt() {
            return this.date(this.json.created_at);
        }
        // The Account sending the notification to the user

    }, {
        key: 'account',
        value: function account() {
            return new _account2.default(this.json.account);
        }
        // The Status associated with the notification, if applicable

    }, {
        key: 'status',
        value: function status() {
            return new _status2.default(this.json.statuses);
        }
    }]);

    return Notification;
}(_entity2.default);

exports.default = Notification;
//# sourceMappingURL=notification.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vdGlmaWNhdGlvbi5qcyJdLCJuYW1lcyI6WyJOb3RpZmljYXRpb24iLCJqc29uIiwiaWQiLCJ0eXBlIiwiZGF0ZSIsImNyZWF0ZWRfYXQiLCJhY2NvdW50Iiwic3RhdHVzZXMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBR0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7Ozs7OytlQUxBOzs7OztJQU1NQSxZOzs7Ozs7Ozs7Ozs7QUFDRjs2QkFDSztBQUNELG1CQUFPLEtBQUtDLElBQUwsQ0FBVUMsRUFBakI7QUFDSDtBQUNEOzs7OytCQUNPO0FBQ0gsbUJBQU8sS0FBS0QsSUFBTCxDQUFVRSxJQUFqQjtBQUNIO0FBQ0Q7Ozs7b0NBQ1k7QUFDUixtQkFBTyxLQUFLQyxJQUFMLENBQVUsS0FBS0gsSUFBTCxDQUFVSSxVQUFwQixDQUFQO0FBQ0g7QUFDRDs7OztrQ0FDVTtBQUNOLG1CQUFPLHNCQUFZLEtBQUtKLElBQUwsQ0FBVUssT0FBdEIsQ0FBUDtBQUNIO0FBQ0Q7Ozs7aUNBQ1M7QUFDTCxtQkFBTyxxQkFBVyxLQUFLTCxJQUFMLENBQVVNLFFBQXJCLENBQVA7QUFDSDs7Ozs7O2tCQUVVUCxZO0FBQ2YiLCJmaWxlIjoibm90aWZpY2F0aW9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjQuXG4gKi9cbmltcG9ydCBBY2NvdW50IGZyb20gJy4vYWNjb3VudCc7XG5pbXBvcnQgRW50aXR5IGZyb20gJy4uL2VudGl0eSc7XG5pbXBvcnQgU3RhdHVzIGZyb20gJy4vc3RhdHVzJztcbmNsYXNzIE5vdGlmaWNhdGlvbiBleHRlbmRzIEVudGl0eSB7XG4gICAgLy8gVGhlIG5vdGlmaWNhdGlvbiBJRFxuICAgIGlkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLmlkO1xuICAgIH1cbiAgICAvLyBPbmUgb2Y6IFwibWVudGlvblwiLCBcInJlYmxvZ1wiLCBcImZhdm91cml0ZVwiLCBcImZvbGxvd1wiXG4gICAgdHlwZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi50eXBlO1xuICAgIH1cbiAgICAvLyBUaGUgdGltZSB0aGUgbm90aWZpY2F0aW9uIHdhcyBjcmVhdGVkXG4gICAgY3JlYXRlZEF0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5kYXRlKHRoaXMuanNvbi5jcmVhdGVkX2F0KTtcbiAgICB9XG4gICAgLy8gVGhlIEFjY291bnQgc2VuZGluZyB0aGUgbm90aWZpY2F0aW9uIHRvIHRoZSB1c2VyXG4gICAgYWNjb3VudCgpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBBY2NvdW50KHRoaXMuanNvbi5hY2NvdW50KTtcbiAgICB9XG4gICAgLy8gVGhlIFN0YXR1cyBhc3NvY2lhdGVkIHdpdGggdGhlIG5vdGlmaWNhdGlvbiwgaWYgYXBwbGljYWJsZVxuICAgIHN0YXR1cygpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBTdGF0dXModGhpcy5qc29uLnN0YXR1c2VzKTtcbiAgICB9XG59XG5leHBvcnQgZGVmYXVsdCBOb3RpZmljYXRpb247XG4vLyMgc291cmNlTWFwcGluZ1VSTD1ub3RpZmljYXRpb24uanMubWFwIl19