'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Tag = function (_Entity) {
    _inherits(Tag, _Entity);

    function Tag() {
        _classCallCheck(this, Tag);

        return _possibleConstructorReturn(this, (Tag.__proto__ || Object.getPrototypeOf(Tag)).apply(this, arguments));
    }

    _createClass(Tag, [{
        key: 'name',

        // The hashtag, not including the preceding
        value: function name() {
            return this.json.name;
        }
        // The URL of the hashtag

    }, {
        key: 'url',
        value: function url() {
            return this.json.url;
        }
    }, {
        key: 'hasUrl',
        value: function hasUrl(url) {
            return this.url() === url;
        }
    }]);

    return Tag;
}(_entity2.default);

exports.default = Tag;
//# sourceMappingURL=tag.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhZy5qcyJdLCJuYW1lcyI6WyJUYWciLCJqc29uIiwibmFtZSIsInVybCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQTs7Ozs7Ozs7OzsrZUFIQTs7Ozs7SUFJTUEsRzs7Ozs7Ozs7Ozs7O0FBQ0Y7K0JBQ087QUFDSCxtQkFBTyxLQUFLQyxJQUFMLENBQVVDLElBQWpCO0FBQ0g7QUFDRDs7Ozs4QkFDTTtBQUNGLG1CQUFPLEtBQUtELElBQUwsQ0FBVUUsR0FBakI7QUFDSDs7OytCQUNNQSxHLEVBQUs7QUFDUixtQkFBTyxLQUFLQSxHQUFMLE9BQWVBLEdBQXRCO0FBQ0g7Ozs7OztrQkFFVUgsRztBQUNmIiwiZmlsZSI6InRhZy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB0b3R0b2tvdGtkIG9uIDE3LzA0LzI0LlxuICovXG5pbXBvcnQgRW50aXR5IGZyb20gJy4uL2VudGl0eSc7XG5jbGFzcyBUYWcgZXh0ZW5kcyBFbnRpdHkge1xuICAgIC8vIFRoZSBoYXNodGFnLCBub3QgaW5jbHVkaW5nIHRoZSBwcmVjZWRpbmdcbiAgICBuYW1lKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLm5hbWU7XG4gICAgfVxuICAgIC8vIFRoZSBVUkwgb2YgdGhlIGhhc2h0YWdcbiAgICB1cmwoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24udXJsO1xuICAgIH1cbiAgICBoYXNVcmwodXJsKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnVybCgpID09PSB1cmw7XG4gICAgfVxufVxuZXhwb3J0IGRlZmF1bHQgVGFnO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9dGFnLmpzLm1hcCJdfQ==