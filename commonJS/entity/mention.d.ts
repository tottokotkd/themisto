/**
 * Created by tottokotkd on 17/04/24.
 */
import Entity from '../entity';
declare class Mention extends Entity {
    url(): any;
    username(): any;
    acct(): any;
    id(): any;
    hasUrl(url: any): boolean;
}
export default Mention;
