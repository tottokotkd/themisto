/**
 * Created by tottokotkd on 17/04/24.
 */
import Entity from '../entity';
declare class Account extends Entity {
    id(): any;
    username(): any;
    acct(): any;
    displayName(): any;
    locked(): boolean;
    createdAt(): number;
    followersCount(): any;
    followingCount(): any;
    statusesCount(): any;
    note(): any;
    url(): any;
    avatar(): any;
    staticAvatar(): any;
    header(): any;
    staticHeader(): any;
}
export default Account;
