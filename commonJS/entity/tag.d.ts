/**
 * Created by tottokotkd on 17/04/24.
 */
import Entity from '../entity';
declare class Tag extends Entity {
    name(): any;
    url(): any;
    hasUrl(url: any): boolean;
}
export default Tag;
