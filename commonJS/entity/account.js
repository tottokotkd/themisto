'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Account = function (_Entity) {
    _inherits(Account, _Entity);

    function Account() {
        _classCallCheck(this, Account);

        return _possibleConstructorReturn(this, (Account.__proto__ || Object.getPrototypeOf(Account)).apply(this, arguments));
    }

    _createClass(Account, [{
        key: 'id',

        // ID of the account
        value: function id() {
            return this.json.id;
        }
        // The username of the account

    }, {
        key: 'username',
        value: function username() {
            return this.json.username;
        }
        // Equals username for local users, includes @domain for remote ones

    }, {
        key: 'acct',
        value: function acct() {
            return this.json.acct;
        }
        // The account's display name

    }, {
        key: 'displayName',
        value: function displayName() {
            return this.json.display_name;
        }
        // Boolean for when the account cannot be followed without waiting for approval first

    }, {
        key: 'locked',
        value: function locked() {
            return !!this.json.locked;
        }
        // The time the account was created

    }, {
        key: 'createdAt',
        value: function createdAt() {
            return Date.parse(this.json.created_at);
        }
        // The number of followers for the account

    }, {
        key: 'followersCount',
        value: function followersCount() {
            return this.json.followers_count;
        }
        // The number of accounts the given account is following

    }, {
        key: 'followingCount',
        value: function followingCount() {
            return this.json.following_count;
        }
        // The number of statuses the account has made

    }, {
        key: 'statusesCount',
        value: function statusesCount() {
            return this.json.statuses_count;
        }
        // Biography of user

    }, {
        key: 'note',
        value: function note() {
            return this.json.note;
        }
        // URL of the user's profile page (can be remote)

    }, {
        key: 'url',
        value: function url() {
            return this.json.url;
        }
        // URL to the avatar image

    }, {
        key: 'avatar',
        value: function avatar() {
            if (!this.json.avatar) return null;
            var url = this.json.avatar || '';
            return url.match(new RegExp('^/')) ? '' : url;
        }
        // URL to the avatar static image (gif)

    }, {
        key: 'staticAvatar',
        value: function staticAvatar() {
            return this.json.avatar_static;
        }
        // URL to the header image

    }, {
        key: 'header',
        value: function header() {
            return this.json.header;
        }
        // URL to the header static image (gif)

    }, {
        key: 'staticHeader',
        value: function staticHeader() {
            return this.json.header_static;
        }
    }]);

    return Account;
}(_entity2.default);

exports.default = Account;
//# sourceMappingURL=account.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjY291bnQuanMiXSwibmFtZXMiOlsiQWNjb3VudCIsImpzb24iLCJpZCIsInVzZXJuYW1lIiwiYWNjdCIsImRpc3BsYXlfbmFtZSIsImxvY2tlZCIsIkRhdGUiLCJwYXJzZSIsImNyZWF0ZWRfYXQiLCJmb2xsb3dlcnNfY291bnQiLCJmb2xsb3dpbmdfY291bnQiLCJzdGF0dXNlc19jb3VudCIsIm5vdGUiLCJ1cmwiLCJhdmF0YXIiLCJtYXRjaCIsIlJlZ0V4cCIsImF2YXRhcl9zdGF0aWMiLCJoZWFkZXIiLCJoZWFkZXJfc3RhdGljIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBOzs7Ozs7Ozs7OytlQUhBOzs7OztJQUlNQSxPOzs7Ozs7Ozs7Ozs7QUFDRjs2QkFDSztBQUNELG1CQUFPLEtBQUtDLElBQUwsQ0FBVUMsRUFBakI7QUFDSDtBQUNEOzs7O21DQUNXO0FBQ1AsbUJBQU8sS0FBS0QsSUFBTCxDQUFVRSxRQUFqQjtBQUNIO0FBQ0Q7Ozs7K0JBQ087QUFDSCxtQkFBTyxLQUFLRixJQUFMLENBQVVHLElBQWpCO0FBQ0g7QUFDRDs7OztzQ0FDYztBQUNWLG1CQUFPLEtBQUtILElBQUwsQ0FBVUksWUFBakI7QUFDSDtBQUNEOzs7O2lDQUNTO0FBQ0wsbUJBQU8sQ0FBQyxDQUFDLEtBQUtKLElBQUwsQ0FBVUssTUFBbkI7QUFDSDtBQUNEOzs7O29DQUNZO0FBQ1IsbUJBQU9DLEtBQUtDLEtBQUwsQ0FBVyxLQUFLUCxJQUFMLENBQVVRLFVBQXJCLENBQVA7QUFDSDtBQUNEOzs7O3lDQUNpQjtBQUNiLG1CQUFPLEtBQUtSLElBQUwsQ0FBVVMsZUFBakI7QUFDSDtBQUNEOzs7O3lDQUNpQjtBQUNiLG1CQUFPLEtBQUtULElBQUwsQ0FBVVUsZUFBakI7QUFDSDtBQUNEOzs7O3dDQUNnQjtBQUNaLG1CQUFPLEtBQUtWLElBQUwsQ0FBVVcsY0FBakI7QUFDSDtBQUNEOzs7OytCQUNPO0FBQ0gsbUJBQU8sS0FBS1gsSUFBTCxDQUFVWSxJQUFqQjtBQUNIO0FBQ0Q7Ozs7OEJBQ007QUFDRixtQkFBTyxLQUFLWixJQUFMLENBQVVhLEdBQWpCO0FBQ0g7QUFDRDs7OztpQ0FDUztBQUNMLGdCQUFJLENBQUMsS0FBS2IsSUFBTCxDQUFVYyxNQUFmLEVBQ0ksT0FBTyxJQUFQO0FBQ0osZ0JBQU1ELE1BQU0sS0FBS2IsSUFBTCxDQUFVYyxNQUFWLElBQW9CLEVBQWhDO0FBQ0EsbUJBQU9ELElBQUlFLEtBQUosQ0FBVSxJQUFJQyxNQUFKLENBQVcsSUFBWCxDQUFWLElBQThCLEVBQTlCLEdBQW1DSCxHQUExQztBQUNIO0FBQ0Q7Ozs7dUNBQ2U7QUFDWCxtQkFBTyxLQUFLYixJQUFMLENBQVVpQixhQUFqQjtBQUNIO0FBQ0Q7Ozs7aUNBQ1M7QUFDTCxtQkFBTyxLQUFLakIsSUFBTCxDQUFVa0IsTUFBakI7QUFDSDtBQUNEOzs7O3VDQUNlO0FBQ1gsbUJBQU8sS0FBS2xCLElBQUwsQ0FBVW1CLGFBQWpCO0FBQ0g7Ozs7OztrQkFFVXBCLE87QUFDZiIsImZpbGUiOiJhY2NvdW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjQuXG4gKi9cbmltcG9ydCBFbnRpdHkgZnJvbSAnLi4vZW50aXR5JztcbmNsYXNzIEFjY291bnQgZXh0ZW5kcyBFbnRpdHkge1xuICAgIC8vIElEIG9mIHRoZSBhY2NvdW50XG4gICAgaWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uaWQ7XG4gICAgfVxuICAgIC8vIFRoZSB1c2VybmFtZSBvZiB0aGUgYWNjb3VudFxuICAgIHVzZXJuYW1lKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLnVzZXJuYW1lO1xuICAgIH1cbiAgICAvLyBFcXVhbHMgdXNlcm5hbWUgZm9yIGxvY2FsIHVzZXJzLCBpbmNsdWRlcyBAZG9tYWluIGZvciByZW1vdGUgb25lc1xuICAgIGFjY3QoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uYWNjdDtcbiAgICB9XG4gICAgLy8gVGhlIGFjY291bnQncyBkaXNwbGF5IG5hbWVcbiAgICBkaXNwbGF5TmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5kaXNwbGF5X25hbWU7XG4gICAgfVxuICAgIC8vIEJvb2xlYW4gZm9yIHdoZW4gdGhlIGFjY291bnQgY2Fubm90IGJlIGZvbGxvd2VkIHdpdGhvdXQgd2FpdGluZyBmb3IgYXBwcm92YWwgZmlyc3RcbiAgICBsb2NrZWQoKSB7XG4gICAgICAgIHJldHVybiAhIXRoaXMuanNvbi5sb2NrZWQ7XG4gICAgfVxuICAgIC8vIFRoZSB0aW1lIHRoZSBhY2NvdW50IHdhcyBjcmVhdGVkXG4gICAgY3JlYXRlZEF0KCkge1xuICAgICAgICByZXR1cm4gRGF0ZS5wYXJzZSh0aGlzLmpzb24uY3JlYXRlZF9hdCk7XG4gICAgfVxuICAgIC8vIFRoZSBudW1iZXIgb2YgZm9sbG93ZXJzIGZvciB0aGUgYWNjb3VudFxuICAgIGZvbGxvd2Vyc0NvdW50KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLmZvbGxvd2Vyc19jb3VudDtcbiAgICB9XG4gICAgLy8gVGhlIG51bWJlciBvZiBhY2NvdW50cyB0aGUgZ2l2ZW4gYWNjb3VudCBpcyBmb2xsb3dpbmdcbiAgICBmb2xsb3dpbmdDb3VudCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5mb2xsb3dpbmdfY291bnQ7XG4gICAgfVxuICAgIC8vIFRoZSBudW1iZXIgb2Ygc3RhdHVzZXMgdGhlIGFjY291bnQgaGFzIG1hZGVcbiAgICBzdGF0dXNlc0NvdW50KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLnN0YXR1c2VzX2NvdW50O1xuICAgIH1cbiAgICAvLyBCaW9ncmFwaHkgb2YgdXNlclxuICAgIG5vdGUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24ubm90ZTtcbiAgICB9XG4gICAgLy8gVVJMIG9mIHRoZSB1c2VyJ3MgcHJvZmlsZSBwYWdlIChjYW4gYmUgcmVtb3RlKVxuICAgIHVybCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi51cmw7XG4gICAgfVxuICAgIC8vIFVSTCB0byB0aGUgYXZhdGFyIGltYWdlXG4gICAgYXZhdGFyKCkge1xuICAgICAgICBpZiAoIXRoaXMuanNvbi5hdmF0YXIpXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5qc29uLmF2YXRhciB8fCAnJztcbiAgICAgICAgcmV0dXJuIHVybC5tYXRjaChuZXcgUmVnRXhwKCdeLycpKSA/ICcnIDogdXJsO1xuICAgIH1cbiAgICAvLyBVUkwgdG8gdGhlIGF2YXRhciBzdGF0aWMgaW1hZ2UgKGdpZilcbiAgICBzdGF0aWNBdmF0YXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uYXZhdGFyX3N0YXRpYztcbiAgICB9XG4gICAgLy8gVVJMIHRvIHRoZSBoZWFkZXIgaW1hZ2VcbiAgICBoZWFkZXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uaGVhZGVyO1xuICAgIH1cbiAgICAvLyBVUkwgdG8gdGhlIGhlYWRlciBzdGF0aWMgaW1hZ2UgKGdpZilcbiAgICBzdGF0aWNIZWFkZXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uaGVhZGVyX3N0YXRpYztcbiAgICB9XG59XG5leHBvcnQgZGVmYXVsdCBBY2NvdW50O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9YWNjb3VudC5qcy5tYXAiXX0=