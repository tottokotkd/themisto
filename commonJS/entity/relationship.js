'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Relationship = function (_Entity) {
    _inherits(Relationship, _Entity);

    function Relationship() {
        _classCallCheck(this, Relationship);

        return _possibleConstructorReturn(this, (Relationship.__proto__ || Object.getPrototypeOf(Relationship)).apply(this, arguments));
    }

    _createClass(Relationship, [{
        key: 'id',

        // Target account id
        value: function id() {
            return this.json.id;
        }
        // Whether the user is currently following the account

    }, {
        key: 'following',
        value: function following() {
            return !!this.json.following;
        }
        // Whether the user is currently being followed by the account

    }, {
        key: 'followedBy',
        value: function followedBy() {
            return !!this.json.followed_by;
        }
        // Whether the user is currently blocking the account

    }, {
        key: 'blocking',
        value: function blocking() {
            return !!this.json.blocking;
        }
        // Whether the user is currently muting the account

    }, {
        key: 'muting',
        value: function muting() {
            return !!this.json.muting;
        }
        // Whether the user has requested to follow the account

    }, {
        key: 'requested',
        value: function requested() {
            return !!this.json.requested;
        }
    }]);

    return Relationship;
}(_entity2.default);

exports.default = Relationship;
//# sourceMappingURL=relationship.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlbGF0aW9uc2hpcC5qcyJdLCJuYW1lcyI6WyJSZWxhdGlvbnNoaXAiLCJqc29uIiwiaWQiLCJmb2xsb3dpbmciLCJmb2xsb3dlZF9ieSIsImJsb2NraW5nIiwibXV0aW5nIiwicmVxdWVzdGVkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBOzs7Ozs7Ozs7OytlQUhBOzs7OztJQUlNQSxZOzs7Ozs7Ozs7Ozs7QUFDRjs2QkFDSztBQUNELG1CQUFPLEtBQUtDLElBQUwsQ0FBVUMsRUFBakI7QUFDSDtBQUNEOzs7O29DQUNZO0FBQ1IsbUJBQU8sQ0FBQyxDQUFDLEtBQUtELElBQUwsQ0FBVUUsU0FBbkI7QUFDSDtBQUNEOzs7O3FDQUNhO0FBQ1QsbUJBQU8sQ0FBQyxDQUFDLEtBQUtGLElBQUwsQ0FBVUcsV0FBbkI7QUFDSDtBQUNEOzs7O21DQUNXO0FBQ1AsbUJBQU8sQ0FBQyxDQUFDLEtBQUtILElBQUwsQ0FBVUksUUFBbkI7QUFDSDtBQUNEOzs7O2lDQUNTO0FBQ0wsbUJBQU8sQ0FBQyxDQUFDLEtBQUtKLElBQUwsQ0FBVUssTUFBbkI7QUFDSDtBQUNEOzs7O29DQUNZO0FBQ1IsbUJBQU8sQ0FBQyxDQUFDLEtBQUtMLElBQUwsQ0FBVU0sU0FBbkI7QUFDSDs7Ozs7O2tCQUVVUCxZO0FBQ2YiLCJmaWxlIjoicmVsYXRpb25zaGlwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjQuXG4gKi9cbmltcG9ydCBFbnRpdHkgZnJvbSAnLi4vZW50aXR5JztcbmNsYXNzIFJlbGF0aW9uc2hpcCBleHRlbmRzIEVudGl0eSB7XG4gICAgLy8gVGFyZ2V0IGFjY291bnQgaWRcbiAgICBpZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5pZDtcbiAgICB9XG4gICAgLy8gV2hldGhlciB0aGUgdXNlciBpcyBjdXJyZW50bHkgZm9sbG93aW5nIHRoZSBhY2NvdW50XG4gICAgZm9sbG93aW5nKCkge1xuICAgICAgICByZXR1cm4gISF0aGlzLmpzb24uZm9sbG93aW5nO1xuICAgIH1cbiAgICAvLyBXaGV0aGVyIHRoZSB1c2VyIGlzIGN1cnJlbnRseSBiZWluZyBmb2xsb3dlZCBieSB0aGUgYWNjb3VudFxuICAgIGZvbGxvd2VkQnkoKSB7XG4gICAgICAgIHJldHVybiAhIXRoaXMuanNvbi5mb2xsb3dlZF9ieTtcbiAgICB9XG4gICAgLy8gV2hldGhlciB0aGUgdXNlciBpcyBjdXJyZW50bHkgYmxvY2tpbmcgdGhlIGFjY291bnRcbiAgICBibG9ja2luZygpIHtcbiAgICAgICAgcmV0dXJuICEhdGhpcy5qc29uLmJsb2NraW5nO1xuICAgIH1cbiAgICAvLyBXaGV0aGVyIHRoZSB1c2VyIGlzIGN1cnJlbnRseSBtdXRpbmcgdGhlIGFjY291bnRcbiAgICBtdXRpbmcoKSB7XG4gICAgICAgIHJldHVybiAhIXRoaXMuanNvbi5tdXRpbmc7XG4gICAgfVxuICAgIC8vIFdoZXRoZXIgdGhlIHVzZXIgaGFzIHJlcXVlc3RlZCB0byBmb2xsb3cgdGhlIGFjY291bnRcbiAgICByZXF1ZXN0ZWQoKSB7XG4gICAgICAgIHJldHVybiAhIXRoaXMuanNvbi5yZXF1ZXN0ZWQ7XG4gICAgfVxufVxuZXhwb3J0IGRlZmF1bHQgUmVsYXRpb25zaGlwO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9cmVsYXRpb25zaGlwLmpzLm1hcCJdfQ==