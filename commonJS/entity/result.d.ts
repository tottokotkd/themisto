import Entity from '../entity';
declare class Result extends Entity {
    accounts(): any;
    statuses(): any;
    hashtags(): any;
}
export default Result;
