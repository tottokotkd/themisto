/**
 * Created by tottokotkd on 17/04/24.
 */
import Entity from '../entity';
declare class Relationship extends Entity {
    id(): any;
    following(): boolean;
    followedBy(): boolean;
    blocking(): boolean;
    muting(): boolean;
    requested(): boolean;
}
export default Relationship;
