'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Instance = function (_Entity) {
    _inherits(Instance, _Entity);

    function Instance() {
        _classCallCheck(this, Instance);

        return _possibleConstructorReturn(this, (Instance.__proto__ || Object.getPrototypeOf(Instance)).apply(this, arguments));
    }

    _createClass(Instance, [{
        key: 'uri',

        // URI of the current instance
        value: function uri() {
            return this.json.uri;
        }
        // The instance's title

    }, {
        key: 'title',
        value: function title() {
            return this.json.title;
        }
        // A description for the instance

    }, {
        key: 'description',
        value: function description() {
            return this.json.description;
        }
        // An email address which can be used to contact the instance administrator

    }, {
        key: 'email',
        value: function email() {
            return this.json.email;
        }
    }]);

    return Instance;
}(_entity2.default);

exports.default = Instance;
//# sourceMappingURL=instance.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluc3RhbmNlLmpzIl0sIm5hbWVzIjpbIkluc3RhbmNlIiwianNvbiIsInVyaSIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJlbWFpbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQTs7Ozs7Ozs7OzsrZUFIQTs7Ozs7SUFJTUEsUTs7Ozs7Ozs7Ozs7O0FBQ0Y7OEJBQ007QUFDRixtQkFBTyxLQUFLQyxJQUFMLENBQVVDLEdBQWpCO0FBQ0g7QUFDRDs7OztnQ0FDUTtBQUNKLG1CQUFPLEtBQUtELElBQUwsQ0FBVUUsS0FBakI7QUFDSDtBQUNEOzs7O3NDQUNjO0FBQ1YsbUJBQU8sS0FBS0YsSUFBTCxDQUFVRyxXQUFqQjtBQUNIO0FBQ0Q7Ozs7Z0NBQ1E7QUFDSixtQkFBTyxLQUFLSCxJQUFMLENBQVVJLEtBQWpCO0FBQ0g7Ozs7OztrQkFFVUwsUTtBQUNmIiwiZmlsZSI6Imluc3RhbmNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjQuXG4gKi9cbmltcG9ydCBFbnRpdHkgZnJvbSAnLi4vZW50aXR5JztcbmNsYXNzIEluc3RhbmNlIGV4dGVuZHMgRW50aXR5IHtcbiAgICAvLyBVUkkgb2YgdGhlIGN1cnJlbnQgaW5zdGFuY2VcbiAgICB1cmkoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24udXJpO1xuICAgIH1cbiAgICAvLyBUaGUgaW5zdGFuY2UncyB0aXRsZVxuICAgIHRpdGxlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLnRpdGxlO1xuICAgIH1cbiAgICAvLyBBIGRlc2NyaXB0aW9uIGZvciB0aGUgaW5zdGFuY2VcbiAgICBkZXNjcmlwdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5kZXNjcmlwdGlvbjtcbiAgICB9XG4gICAgLy8gQW4gZW1haWwgYWRkcmVzcyB3aGljaCBjYW4gYmUgdXNlZCB0byBjb250YWN0IHRoZSBpbnN0YW5jZSBhZG1pbmlzdHJhdG9yXG4gICAgZW1haWwoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uZW1haWw7XG4gICAgfVxufVxuZXhwb3J0IGRlZmF1bHQgSW5zdGFuY2U7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbnN0YW5jZS5qcy5tYXAiXX0=