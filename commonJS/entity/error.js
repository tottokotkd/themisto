'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Error = function (_Entity) {
    _inherits(Error, _Entity);

    function Error() {
        _classCallCheck(this, Error);

        return _possibleConstructorReturn(this, (Error.__proto__ || Object.getPrototypeOf(Error)).apply(this, arguments));
    }

    _createClass(Error, [{
        key: 'error',

        // A textual description of the error
        value: function error() {
            return this.json.error;
        }
    }]);

    return Error;
}(_entity2.default);

exports.default = Error;
//# sourceMappingURL=error.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVycm9yLmpzIl0sIm5hbWVzIjpbIkVycm9yIiwianNvbiIsImVycm9yIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBOzs7Ozs7Ozs7OytlQUhBOzs7OztJQUlNQSxLOzs7Ozs7Ozs7Ozs7QUFDRjtnQ0FDUTtBQUNKLG1CQUFPLEtBQUtDLElBQUwsQ0FBVUMsS0FBakI7QUFDSDs7Ozs7O2tCQUVVRixLO0FBQ2YiLCJmaWxlIjoiZXJyb3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdG90dG9rb3RrZCBvbiAxNy8wNC8yNC5cbiAqL1xuaW1wb3J0IEVudGl0eSBmcm9tICcuLi9lbnRpdHknO1xuY2xhc3MgRXJyb3IgZXh0ZW5kcyBFbnRpdHkge1xuICAgIC8vIEEgdGV4dHVhbCBkZXNjcmlwdGlvbiBvZiB0aGUgZXJyb3JcbiAgICBlcnJvcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5lcnJvcjtcbiAgICB9XG59XG5leHBvcnQgZGVmYXVsdCBFcnJvcjtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWVycm9yLmpzLm1hcCJdfQ==