'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _find = require('lodash/find');

var _find2 = _interopRequireDefault(_find);

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

var _account = require('./account');

var _account2 = _interopRequireDefault(_account);

var _application = require('./application');

var _application2 = _interopRequireDefault(_application);

var _attachment = require('./attachment');

var _attachment2 = _interopRequireDefault(_attachment);

var _mention = require('./mention');

var _mention2 = _interopRequireDefault(_mention);

var _tag = require('./tag');

var _tag2 = _interopRequireDefault(_tag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Status = function (_Entity) {
    _inherits(Status, _Entity);

    function Status(json) {
        _classCallCheck(this, Status);

        var _this = _possibleConstructorReturn(this, (Status.__proto__ || Object.getPrototypeOf(Status)).call(this, json));

        _this.cache.created_at = json.created_at ? Date.parse(json.created_at) : null;
        return _this;
    }
    // The ID of the statuses


    _createClass(Status, [{
        key: 'id',
        value: function id() {
            return this.json.id;
        }
        // A Fediverse-unique resource ID

    }, {
        key: 'uri',
        value: function uri() {
            return this.json.uri;
        }
        // URL to the statuses page (can be remote)

    }, {
        key: 'url',
        value: function url() {
            return this.json.url;
        }
        // The Account which posted the statuses

    }, {
        key: 'account',
        value: function account() {
            if (this.json.account) {
                return new _account2.default(this.json.account);
            }
            return null;
        }
        // null or the ID of the statuses it replies to

    }, {
        key: 'inReplyToId',
        value: function inReplyToId() {
            return this.json.in_reply_to_id;
        }
        // null or the ID of the account it replies to

    }, {
        key: 'inReplyToAccountId',
        value: function inReplyToAccountId() {
            return this.json.in_reply_to_account_id;
        }
        // null or the reblogged Status

    }, {
        key: 'reblog',
        value: function reblog() {
            if (this.json.reblog) {
                return new Status(this.json.reblog);
            }
            return null;
        }
        // Body of the statuses; this will contain HTML (remote HTML already sanitized)

    }, {
        key: 'content',
        value: function content() {
            if (this.cache.content) return this.cache.content;
            var arr = _jquery2.default.parseHTML(this.json.content);
            var content = (0, _jquery2.default)(arr);
            var attachments = this.mediaAttachments();
            var mentions = this.mentions();
            var tags = this.tags();
            (0, _jquery2.default)('a', content).each(function (index, element) {
                var href = element.href;
                if (!href) return;
                var attachment = (0, _find2.default)(attachments, function (a) {
                    return a.hasUrl(href);
                });
                if (attachment) {
                    element.innerHTML = ':)';
                    element.className = 'attachment_link ' + attachment.type();
                    return;
                }
                var tag = (0, _find2.default)(tags, function (t) {
                    return t.hasUrl(href);
                });
                var frontLetter = element.innerHTML[0];
                var parent = (0, _jquery2.default)(element).parent('.tag');
                if (tag || frontLetter === '#' || parent.length !== 0) {
                    element.className = 'tag_link';
                    return;
                }
                var mention = (0, _find2.default)(mentions, function (m) {
                    return m.hasUrl(href);
                });
                if (mention) {
                    element.className = 'mention_link';
                    return;
                }
                element.className = 'external_link';
            });
            var container = (0, _jquery2.default)('<div/>');
            content.each(function (index, element) {
                container.append(element);
            });
            this.cache.content = container.html();
            return this.cache.content;
        }
        // The time the statuses was created

    }, {
        key: 'createdAt',
        value: function createdAt() {
            if (this.cache.created_at) {
                return new Date(this.cache.created_at);
            }
            return null;
        }
    }, {
        key: 'createdBefore',
        value: function createdBefore(current) {
            return this.getTimeRange(this.cache.created_at, current);
        }
        // The number of reblogs for the statuses

    }, {
        key: 'reblogsCount',
        value: function reblogsCount() {
            return this.json.reblogs_count;
        }
        // The number of favourites for the statuses

    }, {
        key: 'favouritesCount',
        value: function favouritesCount() {
            return this.json.favourites_count;
        }
        // Whether the authenticated user has reblogged the statuses

    }, {
        key: 'reblogged',
        value: function reblogged() {
            return !!this.json.reblogged;
        }
        // Whether the authenticated user has favorited the statuses

    }, {
        key: 'favorited',
        value: function favorited() {
            return !!this.json.favourited;
        }
        // Whether media attachments should be hidden by default

    }, {
        key: 'sensitive',
        value: function sensitive() {
            return !!this.json.sensitive;
        }
        // If not empty, warning text that should be displayed before the actual content

    }, {
        key: 'spoilerText',
        value: function spoilerText() {
            return this.json.spoiler_text;
        }
        // One of: public, unlisted, private, direct

    }, {
        key: 'visibility',
        value: function visibility() {
            switch (this.json.visibility) {
                case 'public':
                    return Status.Visibility.public;
                case 'unlisted':
                    return Status.Visibility.unlisted;
                case 'private':
                    return Status.Visibility.private;
                case 'direct':
                    return Status.Visibility.direct;
                default:
                    return null;
            }
        }
        // An array of Attachments

    }, {
        key: 'mediaAttachments',
        value: function mediaAttachments() {
            return this.array(this.json.media_attachments).map(function (json) {
                return new _attachment2.default(json);
            });
        }
        // Whether Attachments exist or not

    }, {
        key: 'hasMediaAttachments',
        value: function hasMediaAttachments() {
            return !!this.json.media_attachments;
        }
        // An array of Mentions

    }, {
        key: 'mentions',
        value: function mentions() {
            return this.array(this.json.mentions).map(function (json) {
                return new _mention2.default(json);
            });
        }
        // An array of Tags

    }, {
        key: 'tags',
        value: function tags() {
            return this.array(this.json.tags).map(function (json) {
                return new _tag2.default(json);
            });
        }
        // Application from which the statuses was posted

    }, {
        key: 'application',
        value: function application() {
            if (this.json.application) {
                return new _application2.default(this.json.application);
            }
            return null;
        }
    }]);

    return Status;
}(_entity2.default);

(function (Status) {
    var Visibility;
    (function (Visibility) {
        Visibility[Visibility["public"] = 0] = "public";
        Visibility[Visibility["unlisted"] = 1] = "unlisted";
        Visibility[Visibility["private"] = 2] = "private";
        Visibility[Visibility["direct"] = 3] = "direct";
    })(Visibility = Status.Visibility || (Status.Visibility = {}));
})(Status || (Status = {}));
exports.default = Status;
//# sourceMappingURL=status.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0YXR1cy5qcyJdLCJuYW1lcyI6WyJTdGF0dXMiLCJqc29uIiwiY2FjaGUiLCJjcmVhdGVkX2F0IiwiRGF0ZSIsInBhcnNlIiwiaWQiLCJ1cmkiLCJ1cmwiLCJhY2NvdW50IiwiaW5fcmVwbHlfdG9faWQiLCJpbl9yZXBseV90b19hY2NvdW50X2lkIiwicmVibG9nIiwiY29udGVudCIsImFyciIsInBhcnNlSFRNTCIsImF0dGFjaG1lbnRzIiwibWVkaWFBdHRhY2htZW50cyIsIm1lbnRpb25zIiwidGFncyIsImVhY2giLCJpbmRleCIsImVsZW1lbnQiLCJocmVmIiwiYXR0YWNobWVudCIsImEiLCJoYXNVcmwiLCJpbm5lckhUTUwiLCJjbGFzc05hbWUiLCJ0eXBlIiwidGFnIiwidCIsImZyb250TGV0dGVyIiwicGFyZW50IiwibGVuZ3RoIiwibWVudGlvbiIsIm0iLCJjb250YWluZXIiLCJhcHBlbmQiLCJodG1sIiwiY3VycmVudCIsImdldFRpbWVSYW5nZSIsInJlYmxvZ3NfY291bnQiLCJmYXZvdXJpdGVzX2NvdW50IiwicmVibG9nZ2VkIiwiZmF2b3VyaXRlZCIsInNlbnNpdGl2ZSIsInNwb2lsZXJfdGV4dCIsInZpc2liaWxpdHkiLCJWaXNpYmlsaXR5IiwicHVibGljIiwidW5saXN0ZWQiLCJwcml2YXRlIiwiZGlyZWN0IiwiYXJyYXkiLCJtZWRpYV9hdHRhY2htZW50cyIsIm1hcCIsImFwcGxpY2F0aW9uIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7Ozs7K2VBVkE7Ozs7O0lBV01BLE07OztBQUNGLG9CQUFZQyxJQUFaLEVBQWtCO0FBQUE7O0FBQUEsb0hBQ1JBLElBRFE7O0FBRWQsY0FBS0MsS0FBTCxDQUFXQyxVQUFYLEdBQXdCRixLQUFLRSxVQUFMLEdBQWtCQyxLQUFLQyxLQUFMLENBQVdKLEtBQUtFLFVBQWhCLENBQWxCLEdBQWdELElBQXhFO0FBRmM7QUFHakI7QUFDRDs7Ozs7NkJBQ0s7QUFDRCxtQkFBTyxLQUFLRixJQUFMLENBQVVLLEVBQWpCO0FBQ0g7QUFDRDs7Ozs4QkFDTTtBQUNGLG1CQUFPLEtBQUtMLElBQUwsQ0FBVU0sR0FBakI7QUFDSDtBQUNEOzs7OzhCQUNNO0FBQ0YsbUJBQU8sS0FBS04sSUFBTCxDQUFVTyxHQUFqQjtBQUNIO0FBQ0Q7Ozs7a0NBQ1U7QUFDTixnQkFBSSxLQUFLUCxJQUFMLENBQVVRLE9BQWQsRUFBdUI7QUFDbkIsdUJBQU8sc0JBQVksS0FBS1IsSUFBTCxDQUFVUSxPQUF0QixDQUFQO0FBQ0g7QUFDRCxtQkFBTyxJQUFQO0FBQ0g7QUFDRDs7OztzQ0FDYztBQUNWLG1CQUFPLEtBQUtSLElBQUwsQ0FBVVMsY0FBakI7QUFDSDtBQUNEOzs7OzZDQUNxQjtBQUNqQixtQkFBTyxLQUFLVCxJQUFMLENBQVVVLHNCQUFqQjtBQUNIO0FBQ0Q7Ozs7aUNBQ1M7QUFDTCxnQkFBSSxLQUFLVixJQUFMLENBQVVXLE1BQWQsRUFBc0I7QUFDbEIsdUJBQU8sSUFBSVosTUFBSixDQUFXLEtBQUtDLElBQUwsQ0FBVVcsTUFBckIsQ0FBUDtBQUNIO0FBQ0QsbUJBQU8sSUFBUDtBQUNIO0FBQ0Q7Ozs7a0NBQ1U7QUFDTixnQkFBSSxLQUFLVixLQUFMLENBQVdXLE9BQWYsRUFDSSxPQUFPLEtBQUtYLEtBQUwsQ0FBV1csT0FBbEI7QUFDSixnQkFBTUMsTUFBTSxpQkFBT0MsU0FBUCxDQUFpQixLQUFLZCxJQUFMLENBQVVZLE9BQTNCLENBQVo7QUFDQSxnQkFBTUEsVUFBVSxzQkFBT0MsR0FBUCxDQUFoQjtBQUNBLGdCQUFNRSxjQUFjLEtBQUtDLGdCQUFMLEVBQXBCO0FBQ0EsZ0JBQU1DLFdBQVcsS0FBS0EsUUFBTCxFQUFqQjtBQUNBLGdCQUFNQyxPQUFPLEtBQUtBLElBQUwsRUFBYjtBQUNBLGtDQUFPLEdBQVAsRUFBWU4sT0FBWixFQUFxQk8sSUFBckIsQ0FBMEIsVUFBQ0MsS0FBRCxFQUFRQyxPQUFSLEVBQW9CO0FBQzFDLG9CQUFNQyxPQUFPRCxRQUFRQyxJQUFyQjtBQUNBLG9CQUFJLENBQUNBLElBQUwsRUFDSTtBQUNKLG9CQUFNQyxhQUFhLG9CQUFLUixXQUFMLEVBQWtCO0FBQUEsMkJBQUtTLEVBQUVDLE1BQUYsQ0FBU0gsSUFBVCxDQUFMO0FBQUEsaUJBQWxCLENBQW5CO0FBQ0Esb0JBQUlDLFVBQUosRUFBZ0I7QUFDWkYsNEJBQVFLLFNBQVIsR0FBb0IsSUFBcEI7QUFDQUwsNEJBQVFNLFNBQVIsd0JBQXVDSixXQUFXSyxJQUFYLEVBQXZDO0FBQ0E7QUFDSDtBQUNELG9CQUFNQyxNQUFNLG9CQUFLWCxJQUFMLEVBQVc7QUFBQSwyQkFBS1ksRUFBRUwsTUFBRixDQUFTSCxJQUFULENBQUw7QUFBQSxpQkFBWCxDQUFaO0FBQ0Esb0JBQU1TLGNBQWNWLFFBQVFLLFNBQVIsQ0FBa0IsQ0FBbEIsQ0FBcEI7QUFDQSxvQkFBTU0sU0FBUyxzQkFBT1gsT0FBUCxFQUFnQlcsTUFBaEIsQ0FBdUIsTUFBdkIsQ0FBZjtBQUNBLG9CQUFJSCxPQUFRRSxnQkFBZ0IsR0FBeEIsSUFBZ0NDLE9BQU9DLE1BQVAsS0FBa0IsQ0FBdEQsRUFBeUQ7QUFDckRaLDRCQUFRTSxTQUFSLEdBQW9CLFVBQXBCO0FBQ0E7QUFDSDtBQUNELG9CQUFNTyxVQUFVLG9CQUFLakIsUUFBTCxFQUFlO0FBQUEsMkJBQUtrQixFQUFFVixNQUFGLENBQVNILElBQVQsQ0FBTDtBQUFBLGlCQUFmLENBQWhCO0FBQ0Esb0JBQUlZLE9BQUosRUFBYTtBQUNUYiw0QkFBUU0sU0FBUixHQUFvQixjQUFwQjtBQUNBO0FBQ0g7QUFDRE4sd0JBQVFNLFNBQVIsR0FBb0IsZUFBcEI7QUFDSCxhQXZCRDtBQXdCQSxnQkFBTVMsWUFBWSxzQkFBTyxRQUFQLENBQWxCO0FBQ0F4QixvQkFBUU8sSUFBUixDQUFhLFVBQUNDLEtBQUQsRUFBUUMsT0FBUixFQUFvQjtBQUM3QmUsMEJBQVVDLE1BQVYsQ0FBaUJoQixPQUFqQjtBQUNILGFBRkQ7QUFHQSxpQkFBS3BCLEtBQUwsQ0FBV1csT0FBWCxHQUFxQndCLFVBQVVFLElBQVYsRUFBckI7QUFDQSxtQkFBTyxLQUFLckMsS0FBTCxDQUFXVyxPQUFsQjtBQUNIO0FBQ0Q7Ozs7b0NBQ1k7QUFDUixnQkFBSSxLQUFLWCxLQUFMLENBQVdDLFVBQWYsRUFBMkI7QUFDdkIsdUJBQU8sSUFBSUMsSUFBSixDQUFTLEtBQUtGLEtBQUwsQ0FBV0MsVUFBcEIsQ0FBUDtBQUNIO0FBQ0QsbUJBQU8sSUFBUDtBQUNIOzs7c0NBQ2FxQyxPLEVBQVM7QUFDbkIsbUJBQU8sS0FBS0MsWUFBTCxDQUFrQixLQUFLdkMsS0FBTCxDQUFXQyxVQUE3QixFQUF5Q3FDLE9BQXpDLENBQVA7QUFDSDtBQUNEOzs7O3VDQUNlO0FBQ1gsbUJBQU8sS0FBS3ZDLElBQUwsQ0FBVXlDLGFBQWpCO0FBQ0g7QUFDRDs7OzswQ0FDa0I7QUFDZCxtQkFBTyxLQUFLekMsSUFBTCxDQUFVMEMsZ0JBQWpCO0FBQ0g7QUFDRDs7OztvQ0FDWTtBQUNSLG1CQUFPLENBQUMsQ0FBQyxLQUFLMUMsSUFBTCxDQUFVMkMsU0FBbkI7QUFDSDtBQUNEOzs7O29DQUNZO0FBQ1IsbUJBQU8sQ0FBQyxDQUFDLEtBQUszQyxJQUFMLENBQVU0QyxVQUFuQjtBQUNIO0FBQ0Q7Ozs7b0NBQ1k7QUFDUixtQkFBTyxDQUFDLENBQUMsS0FBSzVDLElBQUwsQ0FBVTZDLFNBQW5CO0FBQ0g7QUFDRDs7OztzQ0FDYztBQUNWLG1CQUFPLEtBQUs3QyxJQUFMLENBQVU4QyxZQUFqQjtBQUNIO0FBQ0Q7Ozs7cUNBQ2E7QUFDVCxvQkFBUSxLQUFLOUMsSUFBTCxDQUFVK0MsVUFBbEI7QUFDSSxxQkFBSyxRQUFMO0FBQ0ksMkJBQU9oRCxPQUFPaUQsVUFBUCxDQUFrQkMsTUFBekI7QUFDSixxQkFBSyxVQUFMO0FBQ0ksMkJBQU9sRCxPQUFPaUQsVUFBUCxDQUFrQkUsUUFBekI7QUFDSixxQkFBSyxTQUFMO0FBQ0ksMkJBQU9uRCxPQUFPaUQsVUFBUCxDQUFrQkcsT0FBekI7QUFDSixxQkFBSyxRQUFMO0FBQ0ksMkJBQU9wRCxPQUFPaUQsVUFBUCxDQUFrQkksTUFBekI7QUFDSjtBQUNJLDJCQUFPLElBQVA7QUFWUjtBQVlIO0FBQ0Q7Ozs7MkNBQ21CO0FBQ2YsbUJBQU8sS0FBS0MsS0FBTCxDQUFXLEtBQUtyRCxJQUFMLENBQVVzRCxpQkFBckIsRUFBd0NDLEdBQXhDLENBQTRDO0FBQUEsdUJBQVEseUJBQWV2RCxJQUFmLENBQVI7QUFBQSxhQUE1QyxDQUFQO0FBQ0g7QUFDRDs7Ozs4Q0FDc0I7QUFDbEIsbUJBQU8sQ0FBQyxDQUFDLEtBQUtBLElBQUwsQ0FBVXNELGlCQUFuQjtBQUNIO0FBQ0Q7Ozs7bUNBQ1c7QUFDUCxtQkFBTyxLQUFLRCxLQUFMLENBQVcsS0FBS3JELElBQUwsQ0FBVWlCLFFBQXJCLEVBQStCc0MsR0FBL0IsQ0FBbUM7QUFBQSx1QkFBUSxzQkFBWXZELElBQVosQ0FBUjtBQUFBLGFBQW5DLENBQVA7QUFDSDtBQUNEOzs7OytCQUNPO0FBQ0gsbUJBQU8sS0FBS3FELEtBQUwsQ0FBVyxLQUFLckQsSUFBTCxDQUFVa0IsSUFBckIsRUFBMkJxQyxHQUEzQixDQUErQjtBQUFBLHVCQUFRLGtCQUFRdkQsSUFBUixDQUFSO0FBQUEsYUFBL0IsQ0FBUDtBQUNIO0FBQ0Q7Ozs7c0NBQ2M7QUFDVixnQkFBSSxLQUFLQSxJQUFMLENBQVV3RCxXQUFkLEVBQTJCO0FBQ3ZCLHVCQUFPLDBCQUFnQixLQUFLeEQsSUFBTCxDQUFVd0QsV0FBMUIsQ0FBUDtBQUNIO0FBQ0QsbUJBQU8sSUFBUDtBQUNIOzs7Ozs7QUFFTCxDQUFDLFVBQVV6RCxNQUFWLEVBQWtCO0FBQ2YsUUFBSWlELFVBQUo7QUFDQSxLQUFDLFVBQVVBLFVBQVYsRUFBc0I7QUFDbkJBLG1CQUFXQSxXQUFXLFFBQVgsSUFBdUIsQ0FBbEMsSUFBdUMsUUFBdkM7QUFDQUEsbUJBQVdBLFdBQVcsVUFBWCxJQUF5QixDQUFwQyxJQUF5QyxVQUF6QztBQUNBQSxtQkFBV0EsV0FBVyxTQUFYLElBQXdCLENBQW5DLElBQXdDLFNBQXhDO0FBQ0FBLG1CQUFXQSxXQUFXLFFBQVgsSUFBdUIsQ0FBbEMsSUFBdUMsUUFBdkM7QUFDSCxLQUxELEVBS0dBLGFBQWFqRCxPQUFPaUQsVUFBUCxLQUFzQmpELE9BQU9pRCxVQUFQLEdBQW9CLEVBQTFDLENBTGhCO0FBTUgsQ0FSRCxFQVFHakQsV0FBV0EsU0FBUyxFQUFwQixDQVJIO2tCQVNlQSxNO0FBQ2YiLCJmaWxlIjoic3RhdHVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjQuXG4gKi9cbmltcG9ydCBqUXVlcnkgZnJvbSAnanF1ZXJ5JztcbmltcG9ydCBmaW5kIGZyb20gJ2xvZGFzaC9maW5kJztcbmltcG9ydCBFbnRpdHkgZnJvbSAnLi4vZW50aXR5JztcbmltcG9ydCBBY2NvdW50IGZyb20gJy4vYWNjb3VudCc7XG5pbXBvcnQgQXBwbGljYXRpb24gZnJvbSAnLi9hcHBsaWNhdGlvbic7XG5pbXBvcnQgQXR0YWNobWVudCBmcm9tICcuL2F0dGFjaG1lbnQnO1xuaW1wb3J0IE1lbnRpb24gZnJvbSAnLi9tZW50aW9uJztcbmltcG9ydCBUYWcgZnJvbSAnLi90YWcnO1xuY2xhc3MgU3RhdHVzIGV4dGVuZHMgRW50aXR5IHtcbiAgICBjb25zdHJ1Y3Rvcihqc29uKSB7XG4gICAgICAgIHN1cGVyKGpzb24pO1xuICAgICAgICB0aGlzLmNhY2hlLmNyZWF0ZWRfYXQgPSBqc29uLmNyZWF0ZWRfYXQgPyBEYXRlLnBhcnNlKGpzb24uY3JlYXRlZF9hdCkgOiBudWxsO1xuICAgIH1cbiAgICAvLyBUaGUgSUQgb2YgdGhlIHN0YXR1c2VzXG4gICAgaWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uaWQ7XG4gICAgfVxuICAgIC8vIEEgRmVkaXZlcnNlLXVuaXF1ZSByZXNvdXJjZSBJRFxuICAgIHVyaSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi51cmk7XG4gICAgfVxuICAgIC8vIFVSTCB0byB0aGUgc3RhdHVzZXMgcGFnZSAoY2FuIGJlIHJlbW90ZSlcbiAgICB1cmwoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24udXJsO1xuICAgIH1cbiAgICAvLyBUaGUgQWNjb3VudCB3aGljaCBwb3N0ZWQgdGhlIHN0YXR1c2VzXG4gICAgYWNjb3VudCgpIHtcbiAgICAgICAgaWYgKHRoaXMuanNvbi5hY2NvdW50KSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IEFjY291bnQodGhpcy5qc29uLmFjY291bnQpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICAvLyBudWxsIG9yIHRoZSBJRCBvZiB0aGUgc3RhdHVzZXMgaXQgcmVwbGllcyB0b1xuICAgIGluUmVwbHlUb0lkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLmluX3JlcGx5X3RvX2lkO1xuICAgIH1cbiAgICAvLyBudWxsIG9yIHRoZSBJRCBvZiB0aGUgYWNjb3VudCBpdCByZXBsaWVzIHRvXG4gICAgaW5SZXBseVRvQWNjb3VudElkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLmluX3JlcGx5X3RvX2FjY291bnRfaWQ7XG4gICAgfVxuICAgIC8vIG51bGwgb3IgdGhlIHJlYmxvZ2dlZCBTdGF0dXNcbiAgICByZWJsb2coKSB7XG4gICAgICAgIGlmICh0aGlzLmpzb24ucmVibG9nKSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IFN0YXR1cyh0aGlzLmpzb24ucmVibG9nKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgLy8gQm9keSBvZiB0aGUgc3RhdHVzZXM7IHRoaXMgd2lsbCBjb250YWluIEhUTUwgKHJlbW90ZSBIVE1MIGFscmVhZHkgc2FuaXRpemVkKVxuICAgIGNvbnRlbnQoKSB7XG4gICAgICAgIGlmICh0aGlzLmNhY2hlLmNvbnRlbnQpXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jYWNoZS5jb250ZW50O1xuICAgICAgICBjb25zdCBhcnIgPSBqUXVlcnkucGFyc2VIVE1MKHRoaXMuanNvbi5jb250ZW50KTtcbiAgICAgICAgY29uc3QgY29udGVudCA9IGpRdWVyeShhcnIpO1xuICAgICAgICBjb25zdCBhdHRhY2htZW50cyA9IHRoaXMubWVkaWFBdHRhY2htZW50cygpO1xuICAgICAgICBjb25zdCBtZW50aW9ucyA9IHRoaXMubWVudGlvbnMoKTtcbiAgICAgICAgY29uc3QgdGFncyA9IHRoaXMudGFncygpO1xuICAgICAgICBqUXVlcnkoJ2EnLCBjb250ZW50KS5lYWNoKChpbmRleCwgZWxlbWVudCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgaHJlZiA9IGVsZW1lbnQuaHJlZjtcbiAgICAgICAgICAgIGlmICghaHJlZilcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICBjb25zdCBhdHRhY2htZW50ID0gZmluZChhdHRhY2htZW50cywgYSA9PiBhLmhhc1VybChocmVmKSk7XG4gICAgICAgICAgICBpZiAoYXR0YWNobWVudCkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuaW5uZXJIVE1MID0gJzopJztcbiAgICAgICAgICAgICAgICBlbGVtZW50LmNsYXNzTmFtZSA9IGBhdHRhY2htZW50X2xpbmsgJHthdHRhY2htZW50LnR5cGUoKX1gO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IHRhZyA9IGZpbmQodGFncywgdCA9PiB0Lmhhc1VybChocmVmKSk7XG4gICAgICAgICAgICBjb25zdCBmcm9udExldHRlciA9IGVsZW1lbnQuaW5uZXJIVE1MWzBdO1xuICAgICAgICAgICAgY29uc3QgcGFyZW50ID0galF1ZXJ5KGVsZW1lbnQpLnBhcmVudCgnLnRhZycpO1xuICAgICAgICAgICAgaWYgKHRhZyB8fCAoZnJvbnRMZXR0ZXIgPT09ICcjJykgfHwgcGFyZW50Lmxlbmd0aCAhPT0gMCkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuY2xhc3NOYW1lID0gJ3RhZ19saW5rJztcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBtZW50aW9uID0gZmluZChtZW50aW9ucywgbSA9PiBtLmhhc1VybChocmVmKSk7XG4gICAgICAgICAgICBpZiAobWVudGlvbikge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuY2xhc3NOYW1lID0gJ21lbnRpb25fbGluayc7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxlbWVudC5jbGFzc05hbWUgPSAnZXh0ZXJuYWxfbGluayc7XG4gICAgICAgIH0pO1xuICAgICAgICBjb25zdCBjb250YWluZXIgPSBqUXVlcnkoJzxkaXYvPicpO1xuICAgICAgICBjb250ZW50LmVhY2goKGluZGV4LCBlbGVtZW50KSA9PiB7XG4gICAgICAgICAgICBjb250YWluZXIuYXBwZW5kKGVsZW1lbnQpO1xuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5jYWNoZS5jb250ZW50ID0gY29udGFpbmVyLmh0bWwoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FjaGUuY29udGVudDtcbiAgICB9XG4gICAgLy8gVGhlIHRpbWUgdGhlIHN0YXR1c2VzIHdhcyBjcmVhdGVkXG4gICAgY3JlYXRlZEF0KCkge1xuICAgICAgICBpZiAodGhpcy5jYWNoZS5jcmVhdGVkX2F0KSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IERhdGUodGhpcy5jYWNoZS5jcmVhdGVkX2F0KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgY3JlYXRlZEJlZm9yZShjdXJyZW50KSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldFRpbWVSYW5nZSh0aGlzLmNhY2hlLmNyZWF0ZWRfYXQsIGN1cnJlbnQpO1xuICAgIH1cbiAgICAvLyBUaGUgbnVtYmVyIG9mIHJlYmxvZ3MgZm9yIHRoZSBzdGF0dXNlc1xuICAgIHJlYmxvZ3NDb3VudCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5yZWJsb2dzX2NvdW50O1xuICAgIH1cbiAgICAvLyBUaGUgbnVtYmVyIG9mIGZhdm91cml0ZXMgZm9yIHRoZSBzdGF0dXNlc1xuICAgIGZhdm91cml0ZXNDb3VudCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5mYXZvdXJpdGVzX2NvdW50O1xuICAgIH1cbiAgICAvLyBXaGV0aGVyIHRoZSBhdXRoZW50aWNhdGVkIHVzZXIgaGFzIHJlYmxvZ2dlZCB0aGUgc3RhdHVzZXNcbiAgICByZWJsb2dnZWQoKSB7XG4gICAgICAgIHJldHVybiAhIXRoaXMuanNvbi5yZWJsb2dnZWQ7XG4gICAgfVxuICAgIC8vIFdoZXRoZXIgdGhlIGF1dGhlbnRpY2F0ZWQgdXNlciBoYXMgZmF2b3JpdGVkIHRoZSBzdGF0dXNlc1xuICAgIGZhdm9yaXRlZCgpIHtcbiAgICAgICAgcmV0dXJuICEhdGhpcy5qc29uLmZhdm91cml0ZWQ7XG4gICAgfVxuICAgIC8vIFdoZXRoZXIgbWVkaWEgYXR0YWNobWVudHMgc2hvdWxkIGJlIGhpZGRlbiBieSBkZWZhdWx0XG4gICAgc2Vuc2l0aXZlKCkge1xuICAgICAgICByZXR1cm4gISF0aGlzLmpzb24uc2Vuc2l0aXZlO1xuICAgIH1cbiAgICAvLyBJZiBub3QgZW1wdHksIHdhcm5pbmcgdGV4dCB0aGF0IHNob3VsZCBiZSBkaXNwbGF5ZWQgYmVmb3JlIHRoZSBhY3R1YWwgY29udGVudFxuICAgIHNwb2lsZXJUZXh0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLnNwb2lsZXJfdGV4dDtcbiAgICB9XG4gICAgLy8gT25lIG9mOiBwdWJsaWMsIHVubGlzdGVkLCBwcml2YXRlLCBkaXJlY3RcbiAgICB2aXNpYmlsaXR5KCkge1xuICAgICAgICBzd2l0Y2ggKHRoaXMuanNvbi52aXNpYmlsaXR5KSB7XG4gICAgICAgICAgICBjYXNlICdwdWJsaWMnOlxuICAgICAgICAgICAgICAgIHJldHVybiBTdGF0dXMuVmlzaWJpbGl0eS5wdWJsaWM7XG4gICAgICAgICAgICBjYXNlICd1bmxpc3RlZCc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIFN0YXR1cy5WaXNpYmlsaXR5LnVubGlzdGVkO1xuICAgICAgICAgICAgY2FzZSAncHJpdmF0ZSc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIFN0YXR1cy5WaXNpYmlsaXR5LnByaXZhdGU7XG4gICAgICAgICAgICBjYXNlICdkaXJlY3QnOlxuICAgICAgICAgICAgICAgIHJldHVybiBTdGF0dXMuVmlzaWJpbGl0eS5kaXJlY3Q7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgfVxuICAgIC8vIEFuIGFycmF5IG9mIEF0dGFjaG1lbnRzXG4gICAgbWVkaWFBdHRhY2htZW50cygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYXJyYXkodGhpcy5qc29uLm1lZGlhX2F0dGFjaG1lbnRzKS5tYXAoanNvbiA9PiBuZXcgQXR0YWNobWVudChqc29uKSk7XG4gICAgfVxuICAgIC8vIFdoZXRoZXIgQXR0YWNobWVudHMgZXhpc3Qgb3Igbm90XG4gICAgaGFzTWVkaWFBdHRhY2htZW50cygpIHtcbiAgICAgICAgcmV0dXJuICEhdGhpcy5qc29uLm1lZGlhX2F0dGFjaG1lbnRzO1xuICAgIH1cbiAgICAvLyBBbiBhcnJheSBvZiBNZW50aW9uc1xuICAgIG1lbnRpb25zKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5hcnJheSh0aGlzLmpzb24ubWVudGlvbnMpLm1hcChqc29uID0+IG5ldyBNZW50aW9uKGpzb24pKTtcbiAgICB9XG4gICAgLy8gQW4gYXJyYXkgb2YgVGFnc1xuICAgIHRhZ3MoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFycmF5KHRoaXMuanNvbi50YWdzKS5tYXAoanNvbiA9PiBuZXcgVGFnKGpzb24pKTtcbiAgICB9XG4gICAgLy8gQXBwbGljYXRpb24gZnJvbSB3aGljaCB0aGUgc3RhdHVzZXMgd2FzIHBvc3RlZFxuICAgIGFwcGxpY2F0aW9uKCkge1xuICAgICAgICBpZiAodGhpcy5qc29uLmFwcGxpY2F0aW9uKSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IEFwcGxpY2F0aW9uKHRoaXMuanNvbi5hcHBsaWNhdGlvbik7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxufVxuKGZ1bmN0aW9uIChTdGF0dXMpIHtcbiAgICB2YXIgVmlzaWJpbGl0eTtcbiAgICAoZnVuY3Rpb24gKFZpc2liaWxpdHkpIHtcbiAgICAgICAgVmlzaWJpbGl0eVtWaXNpYmlsaXR5W1wicHVibGljXCJdID0gMF0gPSBcInB1YmxpY1wiO1xuICAgICAgICBWaXNpYmlsaXR5W1Zpc2liaWxpdHlbXCJ1bmxpc3RlZFwiXSA9IDFdID0gXCJ1bmxpc3RlZFwiO1xuICAgICAgICBWaXNpYmlsaXR5W1Zpc2liaWxpdHlbXCJwcml2YXRlXCJdID0gMl0gPSBcInByaXZhdGVcIjtcbiAgICAgICAgVmlzaWJpbGl0eVtWaXNpYmlsaXR5W1wiZGlyZWN0XCJdID0gM10gPSBcImRpcmVjdFwiO1xuICAgIH0pKFZpc2liaWxpdHkgPSBTdGF0dXMuVmlzaWJpbGl0eSB8fCAoU3RhdHVzLlZpc2liaWxpdHkgPSB7fSkpO1xufSkoU3RhdHVzIHx8IChTdGF0dXMgPSB7fSkpO1xuZXhwb3J0IGRlZmF1bHQgU3RhdHVzO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9c3RhdHVzLmpzLm1hcCJdfQ==