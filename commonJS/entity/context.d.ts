/**
 * Created by tottokotkd on 17/04/24.
 */
import Entity from '../entity';
declare class Context extends Entity {
    ancestors(): any;
    descendants(): any;
}
export default Context;
