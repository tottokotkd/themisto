import Entity from '../entity';
declare class Attachment extends Entity {
    id(): any;
    type(): any;
    url(): any;
    remoteUrl(): any;
    previewUrl(): any;
    textUrl(): any;
    hasUrl(url: any): any;
}
export default Attachment;
