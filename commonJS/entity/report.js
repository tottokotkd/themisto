'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Report = function (_Entity) {
    _inherits(Report, _Entity);

    function Report() {
        _classCallCheck(this, Report);

        return _possibleConstructorReturn(this, (Report.__proto__ || Object.getPrototypeOf(Report)).apply(this, arguments));
    }

    _createClass(Report, [{
        key: 'id',

        // The ID of the report
        value: function id() {
            return this.json.id;
        }
        // The action taken in response to the report

    }, {
        key: 'actionTaken',
        value: function actionTaken() {
            return this.json.action_taken;
        }
    }]);

    return Report;
}(_entity2.default);

exports.default = Report;
//# sourceMappingURL=report.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlcG9ydC5qcyJdLCJuYW1lcyI6WyJSZXBvcnQiLCJqc29uIiwiaWQiLCJhY3Rpb25fdGFrZW4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBR0E7Ozs7Ozs7Ozs7K2VBSEE7Ozs7O0lBSU1BLE07Ozs7Ozs7Ozs7OztBQUNGOzZCQUNLO0FBQ0QsbUJBQU8sS0FBS0MsSUFBTCxDQUFVQyxFQUFqQjtBQUNIO0FBQ0Q7Ozs7c0NBQ2M7QUFDVixtQkFBTyxLQUFLRCxJQUFMLENBQVVFLFlBQWpCO0FBQ0g7Ozs7OztrQkFFVUgsTTtBQUNmIiwiZmlsZSI6InJlcG9ydC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB0b3R0b2tvdGtkIG9uIDE3LzA0LzI0LlxuICovXG5pbXBvcnQgRW50aXR5IGZyb20gJy4uL2VudGl0eSc7XG5jbGFzcyBSZXBvcnQgZXh0ZW5kcyBFbnRpdHkge1xuICAgIC8vIFRoZSBJRCBvZiB0aGUgcmVwb3J0XG4gICAgaWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uaWQ7XG4gICAgfVxuICAgIC8vIFRoZSBhY3Rpb24gdGFrZW4gaW4gcmVzcG9uc2UgdG8gdGhlIHJlcG9ydFxuICAgIGFjdGlvblRha2VuKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLmFjdGlvbl90YWtlbjtcbiAgICB9XG59XG5leHBvcnQgZGVmYXVsdCBSZXBvcnQ7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1yZXBvcnQuanMubWFwIl19