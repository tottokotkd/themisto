/**
 * Created by tottokotkd on 17/04/24.
 */
import Entity from '../entity';
declare class Card extends Entity {
    url(): any;
    title(): any;
    description(): any;
    image(): any;
}
export default Card;
