'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Application = function (_Entity) {
    _inherits(Application, _Entity);

    function Application() {
        _classCallCheck(this, Application);

        return _possibleConstructorReturn(this, (Application.__proto__ || Object.getPrototypeOf(Application)).apply(this, arguments));
    }

    _createClass(Application, [{
        key: 'name',

        // Name of the app
        value: function name() {
            return this.json.name;
        }
        // Homepage URL of the app

    }, {
        key: 'website',
        value: function website() {
            return this.json.name;
        }
    }]);

    return Application;
}(_entity2.default);

exports.default = Application;
//# sourceMappingURL=application.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcGxpY2F0aW9uLmpzIl0sIm5hbWVzIjpbIkFwcGxpY2F0aW9uIiwianNvbiIsIm5hbWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBR0E7Ozs7Ozs7Ozs7K2VBSEE7Ozs7O0lBSU1BLFc7Ozs7Ozs7Ozs7OztBQUNGOytCQUNPO0FBQ0gsbUJBQU8sS0FBS0MsSUFBTCxDQUFVQyxJQUFqQjtBQUNIO0FBQ0Q7Ozs7a0NBQ1U7QUFDTixtQkFBTyxLQUFLRCxJQUFMLENBQVVDLElBQWpCO0FBQ0g7Ozs7OztrQkFFVUYsVztBQUNmIiwiZmlsZSI6ImFwcGxpY2F0aW9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjQuXG4gKi9cbmltcG9ydCBFbnRpdHkgZnJvbSAnLi4vZW50aXR5JztcbmNsYXNzIEFwcGxpY2F0aW9uIGV4dGVuZHMgRW50aXR5IHtcbiAgICAvLyBOYW1lIG9mIHRoZSBhcHBcbiAgICBuYW1lKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLm5hbWU7XG4gICAgfVxuICAgIC8vIEhvbWVwYWdlIFVSTCBvZiB0aGUgYXBwXG4gICAgd2Vic2l0ZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5uYW1lO1xuICAgIH1cbn1cbmV4cG9ydCBkZWZhdWx0IEFwcGxpY2F0aW9uO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9YXBwbGljYXRpb24uanMubWFwIl19