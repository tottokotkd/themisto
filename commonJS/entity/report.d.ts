/**
 * Created by tottokotkd on 17/04/24.
 */
import Entity from '../entity';
declare class Report extends Entity {
    id(): any;
    actionTaken(): any;
}
export default Report;
