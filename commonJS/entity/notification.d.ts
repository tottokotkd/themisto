/**
 * Created by tottokotkd on 17/04/24.
 */
import Account from './account';
import Entity from '../entity';
import Status from './status';
declare class Notification extends Entity {
    id(): any;
    type(): any;
    createdAt(): Date;
    account(): Account;
    status(): Status;
}
export default Notification;
