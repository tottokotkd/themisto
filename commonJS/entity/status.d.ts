import Entity from '../entity';
import Account from './account';
import Application from './application';
import Attachment from './attachment';
import Mention from './mention';
import Tag from './tag';
declare class Status extends Entity {
    constructor(json: any);
    id(): any;
    uri(): any;
    url(): any;
    account(): Account;
    inReplyToId(): any;
    inReplyToAccountId(): any;
    reblog(): Status;
    content(): any;
    createdAt(): Date;
    createdBefore(current: any): {
        days: number;
        hours: number;
        minutes: number;
        seconds: number;
    };
    reblogsCount(): any;
    favouritesCount(): any;
    reblogged(): boolean;
    favorited(): boolean;
    sensitive(): boolean;
    spoilerText(): any;
    visibility(): Status.Visibility;
    mediaAttachments(): Attachment[];
    hasMediaAttachments(): boolean;
    mentions(): Mention[];
    tags(): Tag[];
    application(): Application;
}
declare module Status {
    enum Visibility {
        public = 0,
        unlisted = 1,
        private = 2,
        direct = 3,
    }
}
export default Status;
