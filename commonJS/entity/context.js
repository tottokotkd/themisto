'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

var _status = require('./status');

var _status2 = _interopRequireDefault(_status);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Context = function (_Entity) {
    _inherits(Context, _Entity);

    function Context() {
        _classCallCheck(this, Context);

        return _possibleConstructorReturn(this, (Context.__proto__ || Object.getPrototypeOf(Context)).apply(this, arguments));
    }

    _createClass(Context, [{
        key: 'ancestors',

        // The ancestors of the statuses in the conversation, as a list of Statuses
        value: function ancestors() {
            return this.json.ancestors.map(_status2.default.constructor);
        }
        // The descendants of the statuses in the conversation, as a list of Statuses

    }, {
        key: 'descendants',
        value: function descendants() {
            return this.json.descendants(_status2.default.constructor);
        }
    }]);

    return Context;
}(_entity2.default);

exports.default = Context;
//# sourceMappingURL=context.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRleHQuanMiXSwibmFtZXMiOlsiQ29udGV4dCIsImpzb24iLCJhbmNlc3RvcnMiLCJtYXAiLCJjb25zdHJ1Y3RvciIsImRlc2NlbmRhbnRzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBOzs7O0FBQ0E7Ozs7Ozs7Ozs7K2VBSkE7Ozs7O0lBS01BLE87Ozs7Ozs7Ozs7OztBQUNGO29DQUNZO0FBQ1IsbUJBQU8sS0FBS0MsSUFBTCxDQUFVQyxTQUFWLENBQW9CQyxHQUFwQixDQUF3QixpQkFBT0MsV0FBL0IsQ0FBUDtBQUNIO0FBQ0Q7Ozs7c0NBQ2M7QUFDVixtQkFBTyxLQUFLSCxJQUFMLENBQVVJLFdBQVYsQ0FBc0IsaUJBQU9ELFdBQTdCLENBQVA7QUFDSDs7Ozs7O2tCQUVVSixPO0FBQ2YiLCJmaWxlIjoiY29udGV4dC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB0b3R0b2tvdGtkIG9uIDE3LzA0LzI0LlxuICovXG5pbXBvcnQgRW50aXR5IGZyb20gJy4uL2VudGl0eSc7XG5pbXBvcnQgU3RhdHVzIGZyb20gJy4vc3RhdHVzJztcbmNsYXNzIENvbnRleHQgZXh0ZW5kcyBFbnRpdHkge1xuICAgIC8vIFRoZSBhbmNlc3RvcnMgb2YgdGhlIHN0YXR1c2VzIGluIHRoZSBjb252ZXJzYXRpb24sIGFzIGEgbGlzdCBvZiBTdGF0dXNlc1xuICAgIGFuY2VzdG9ycygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5hbmNlc3RvcnMubWFwKFN0YXR1cy5jb25zdHJ1Y3Rvcik7XG4gICAgfVxuICAgIC8vIFRoZSBkZXNjZW5kYW50cyBvZiB0aGUgc3RhdHVzZXMgaW4gdGhlIGNvbnZlcnNhdGlvbiwgYXMgYSBsaXN0IG9mIFN0YXR1c2VzXG4gICAgZGVzY2VuZGFudHMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uZGVzY2VuZGFudHMoU3RhdHVzLmNvbnN0cnVjdG9yKTtcbiAgICB9XG59XG5leHBvcnQgZGVmYXVsdCBDb250ZXh0O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9Y29udGV4dC5qcy5tYXAiXX0=