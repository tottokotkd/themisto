'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Card = function (_Entity) {
    _inherits(Card, _Entity);

    function Card() {
        _classCallCheck(this, Card);

        return _possibleConstructorReturn(this, (Card.__proto__ || Object.getPrototypeOf(Card)).apply(this, arguments));
    }

    _createClass(Card, [{
        key: 'url',

        // The url associated with the card
        value: function url() {
            return this.json.url;
        }
        // The title of the card

    }, {
        key: 'title',
        value: function title() {
            return this.json.title;
        }
        // The card description

    }, {
        key: 'description',
        value: function description() {
            return this.json.description;
        }
        // The image associated with the card, if any

    }, {
        key: 'image',
        value: function image() {
            return this.json.image;
        }
    }]);

    return Card;
}(_entity2.default);

exports.default = Card;
//# sourceMappingURL=card.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhcmQuanMiXSwibmFtZXMiOlsiQ2FyZCIsImpzb24iLCJ1cmwiLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwiaW1hZ2UiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBR0E7Ozs7Ozs7Ozs7K2VBSEE7Ozs7O0lBSU1BLEk7Ozs7Ozs7Ozs7OztBQUNGOzhCQUNNO0FBQ0YsbUJBQU8sS0FBS0MsSUFBTCxDQUFVQyxHQUFqQjtBQUNIO0FBQ0Q7Ozs7Z0NBQ1E7QUFDSixtQkFBTyxLQUFLRCxJQUFMLENBQVVFLEtBQWpCO0FBQ0g7QUFDRDs7OztzQ0FDYztBQUNWLG1CQUFPLEtBQUtGLElBQUwsQ0FBVUcsV0FBakI7QUFDSDtBQUNEOzs7O2dDQUNRO0FBQ0osbUJBQU8sS0FBS0gsSUFBTCxDQUFVSSxLQUFqQjtBQUNIOzs7Ozs7a0JBRVVMLEk7QUFDZiIsImZpbGUiOiJjYXJkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjQuXG4gKi9cbmltcG9ydCBFbnRpdHkgZnJvbSAnLi4vZW50aXR5JztcbmNsYXNzIENhcmQgZXh0ZW5kcyBFbnRpdHkge1xuICAgIC8vIFRoZSB1cmwgYXNzb2NpYXRlZCB3aXRoIHRoZSBjYXJkXG4gICAgdXJsKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLnVybDtcbiAgICB9XG4gICAgLy8gVGhlIHRpdGxlIG9mIHRoZSBjYXJkXG4gICAgdGl0bGUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24udGl0bGU7XG4gICAgfVxuICAgIC8vIFRoZSBjYXJkIGRlc2NyaXB0aW9uXG4gICAgZGVzY3JpcHRpb24oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24uZGVzY3JpcHRpb247XG4gICAgfVxuICAgIC8vIFRoZSBpbWFnZSBhc3NvY2lhdGVkIHdpdGggdGhlIGNhcmQsIGlmIGFueVxuICAgIGltYWdlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLmltYWdlO1xuICAgIH1cbn1cbmV4cG9ydCBkZWZhdWx0IENhcmQ7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1jYXJkLmpzLm1hcCJdfQ==