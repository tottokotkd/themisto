'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _some = require('lodash/some');

var _some2 = _interopRequireDefault(_some);

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Attachment = function (_Entity) {
    _inherits(Attachment, _Entity);

    function Attachment() {
        _classCallCheck(this, Attachment);

        return _possibleConstructorReturn(this, (Attachment.__proto__ || Object.getPrototypeOf(Attachment)).apply(this, arguments));
    }

    _createClass(Attachment, [{
        key: 'id',

        // ID of the attachment
        value: function id() {
            return this.json.id;
        }
        // One of: "image", "video", "gifv"

    }, {
        key: 'type',
        value: function type() {
            return this.json.type;
        }
        // URL of the locally hosted version of the image

    }, {
        key: 'url',
        value: function url() {
            return this.json.url;
        }
        // For remote images, the remote URL of the original image

    }, {
        key: 'remoteUrl',
        value: function remoteUrl() {
            return this.json.remote_url;
        }
        // URL of the preview image

    }, {
        key: 'previewUrl',
        value: function previewUrl() {
            return this.json.preview_url;
        }
        // Shorter URL for the image, for insertion into text (only present on local images)

    }, {
        key: 'textUrl',
        value: function textUrl() {
            return this.json.text_url;
        }
    }, {
        key: 'hasUrl',
        value: function hasUrl(url) {
            var urls = [this.url(), this.remoteUrl(), this.previewUrl(), this.textUrl()];
            return (0, _some2.default)(urls, function (u) {
                return u === url;
            });
        }
    }]);

    return Attachment;
}(_entity2.default);

exports.default = Attachment;
//# sourceMappingURL=attachment.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImF0dGFjaG1lbnQuanMiXSwibmFtZXMiOlsiQXR0YWNobWVudCIsImpzb24iLCJpZCIsInR5cGUiLCJ1cmwiLCJyZW1vdGVfdXJsIiwicHJldmlld191cmwiLCJ0ZXh0X3VybCIsInVybHMiLCJyZW1vdGVVcmwiLCJwcmV2aWV3VXJsIiwidGV4dFVybCIsInUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBR0E7Ozs7QUFDQTs7Ozs7Ozs7OzsrZUFKQTs7Ozs7SUFLTUEsVTs7Ozs7Ozs7Ozs7O0FBQ0Y7NkJBQ0s7QUFDRCxtQkFBTyxLQUFLQyxJQUFMLENBQVVDLEVBQWpCO0FBQ0g7QUFDRDs7OzsrQkFDTztBQUNILG1CQUFPLEtBQUtELElBQUwsQ0FBVUUsSUFBakI7QUFDSDtBQUNEOzs7OzhCQUNNO0FBQ0YsbUJBQU8sS0FBS0YsSUFBTCxDQUFVRyxHQUFqQjtBQUNIO0FBQ0Q7Ozs7b0NBQ1k7QUFDUixtQkFBTyxLQUFLSCxJQUFMLENBQVVJLFVBQWpCO0FBQ0g7QUFDRDs7OztxQ0FDYTtBQUNULG1CQUFPLEtBQUtKLElBQUwsQ0FBVUssV0FBakI7QUFDSDtBQUNEOzs7O2tDQUNVO0FBQ04sbUJBQU8sS0FBS0wsSUFBTCxDQUFVTSxRQUFqQjtBQUNIOzs7K0JBQ01ILEcsRUFBSztBQUNSLGdCQUFNSSxPQUFPLENBQUMsS0FBS0osR0FBTCxFQUFELEVBQWEsS0FBS0ssU0FBTCxFQUFiLEVBQStCLEtBQUtDLFVBQUwsRUFBL0IsRUFBa0QsS0FBS0MsT0FBTCxFQUFsRCxDQUFiO0FBQ0EsbUJBQU8sb0JBQUtILElBQUwsRUFBVztBQUFBLHVCQUFLSSxNQUFNUixHQUFYO0FBQUEsYUFBWCxDQUFQO0FBQ0g7Ozs7OztrQkFFVUosVTtBQUNmIiwiZmlsZSI6ImF0dGFjaG1lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgdG90dG9rb3RrZCBvbiAxNy8wNC8yNC5cbiAqL1xuaW1wb3J0IHNvbWUgZnJvbSAnbG9kYXNoL3NvbWUnO1xuaW1wb3J0IEVudGl0eSBmcm9tICcuLi9lbnRpdHknO1xuY2xhc3MgQXR0YWNobWVudCBleHRlbmRzIEVudGl0eSB7XG4gICAgLy8gSUQgb2YgdGhlIGF0dGFjaG1lbnRcbiAgICBpZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5pZDtcbiAgICB9XG4gICAgLy8gT25lIG9mOiBcImltYWdlXCIsIFwidmlkZW9cIiwgXCJnaWZ2XCJcbiAgICB0eXBlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLnR5cGU7XG4gICAgfVxuICAgIC8vIFVSTCBvZiB0aGUgbG9jYWxseSBob3N0ZWQgdmVyc2lvbiBvZiB0aGUgaW1hZ2VcbiAgICB1cmwoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24udXJsO1xuICAgIH1cbiAgICAvLyBGb3IgcmVtb3RlIGltYWdlcywgdGhlIHJlbW90ZSBVUkwgb2YgdGhlIG9yaWdpbmFsIGltYWdlXG4gICAgcmVtb3RlVXJsKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5qc29uLnJlbW90ZV91cmw7XG4gICAgfVxuICAgIC8vIFVSTCBvZiB0aGUgcHJldmlldyBpbWFnZVxuICAgIHByZXZpZXdVcmwoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24ucHJldmlld191cmw7XG4gICAgfVxuICAgIC8vIFNob3J0ZXIgVVJMIGZvciB0aGUgaW1hZ2UsIGZvciBpbnNlcnRpb24gaW50byB0ZXh0IChvbmx5IHByZXNlbnQgb24gbG9jYWwgaW1hZ2VzKVxuICAgIHRleHRVcmwoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmpzb24udGV4dF91cmw7XG4gICAgfVxuICAgIGhhc1VybCh1cmwpIHtcbiAgICAgICAgY29uc3QgdXJscyA9IFt0aGlzLnVybCgpLCB0aGlzLnJlbW90ZVVybCgpLCB0aGlzLnByZXZpZXdVcmwoKSwgdGhpcy50ZXh0VXJsKCldO1xuICAgICAgICByZXR1cm4gc29tZSh1cmxzLCB1ID0+IHUgPT09IHVybCk7XG4gICAgfVxufVxuZXhwb3J0IGRlZmF1bHQgQXR0YWNobWVudDtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWF0dGFjaG1lbnQuanMubWFwIl19