'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _account = require('./account');

var _account2 = _interopRequireDefault(_account);

var _entity = require('../entity');

var _entity2 = _interopRequireDefault(_entity);

var _status = require('./status');

var _status2 = _interopRequireDefault(_status);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Result = function (_Entity) {
    _inherits(Result, _Entity);

    function Result() {
        _classCallCheck(this, Result);

        return _possibleConstructorReturn(this, (Result.__proto__ || Object.getPrototypeOf(Result)).apply(this, arguments));
    }

    _createClass(Result, [{
        key: 'accounts',

        // An array of matched Accounts
        value: function accounts() {
            return (this.json.accounts || []).map(function (j) {
                return new _account2.default(j);
            });
        }
        // An array of matchhed Statuses

    }, {
        key: 'statuses',
        value: function statuses() {
            return (this.json.statuses || []).map(function (j) {
                return new _status2.default(j);
            });
        }
        // An array of matched hashtags, as strings

    }, {
        key: 'hashtags',
        value: function hashtags() {
            return this.json.hashtags || [];
        }
    }]);

    return Result;
}(_entity2.default);

exports.default = Result;
//# sourceMappingURL=result.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlc3VsdC5qcyJdLCJuYW1lcyI6WyJSZXN1bHQiLCJqc29uIiwiYWNjb3VudHMiLCJtYXAiLCJqIiwic3RhdHVzZXMiLCJoYXNodGFncyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQTs7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7Ozs7K2VBTEE7Ozs7O0lBTU1BLE07Ozs7Ozs7Ozs7OztBQUNGO21DQUNXO0FBQ1AsbUJBQU8sQ0FBQyxLQUFLQyxJQUFMLENBQVVDLFFBQVYsSUFBc0IsRUFBdkIsRUFBMkJDLEdBQTNCLENBQStCO0FBQUEsdUJBQUssc0JBQVlDLENBQVosQ0FBTDtBQUFBLGFBQS9CLENBQVA7QUFDSDtBQUNEOzs7O21DQUNXO0FBQ1AsbUJBQU8sQ0FBQyxLQUFLSCxJQUFMLENBQVVJLFFBQVYsSUFBc0IsRUFBdkIsRUFBMkJGLEdBQTNCLENBQStCO0FBQUEsdUJBQUsscUJBQVdDLENBQVgsQ0FBTDtBQUFBLGFBQS9CLENBQVA7QUFDSDtBQUNEOzs7O21DQUNXO0FBQ1AsbUJBQVEsS0FBS0gsSUFBTCxDQUFVSyxRQUFWLElBQXNCLEVBQTlCO0FBQ0g7Ozs7OztrQkFFVU4sTTtBQUNmIiwiZmlsZSI6InJlc3VsdC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB0b3R0b2tvdGtkIG9uIDE3LzA0LzI0LlxuICovXG5pbXBvcnQgQWNjb3VudCBmcm9tICcuL2FjY291bnQnO1xuaW1wb3J0IEVudGl0eSBmcm9tICcuLi9lbnRpdHknO1xuaW1wb3J0IFN0YXR1cyBmcm9tICcuL3N0YXR1cyc7XG5jbGFzcyBSZXN1bHQgZXh0ZW5kcyBFbnRpdHkge1xuICAgIC8vIEFuIGFycmF5IG9mIG1hdGNoZWQgQWNjb3VudHNcbiAgICBhY2NvdW50cygpIHtcbiAgICAgICAgcmV0dXJuICh0aGlzLmpzb24uYWNjb3VudHMgfHwgW10pLm1hcChqID0+IG5ldyBBY2NvdW50KGopKTtcbiAgICB9XG4gICAgLy8gQW4gYXJyYXkgb2YgbWF0Y2hoZWQgU3RhdHVzZXNcbiAgICBzdGF0dXNlcygpIHtcbiAgICAgICAgcmV0dXJuICh0aGlzLmpzb24uc3RhdHVzZXMgfHwgW10pLm1hcChqID0+IG5ldyBTdGF0dXMoaikpO1xuICAgIH1cbiAgICAvLyBBbiBhcnJheSBvZiBtYXRjaGVkIGhhc2h0YWdzLCBhcyBzdHJpbmdzXG4gICAgaGFzaHRhZ3MoKSB7XG4gICAgICAgIHJldHVybiAodGhpcy5qc29uLmhhc2h0YWdzIHx8IFtdKTtcbiAgICB9XG59XG5leHBvcnQgZGVmYXVsdCBSZXN1bHQ7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1yZXN1bHQuanMubWFwIl19