/**
 * Created by tottokotkd on 17/04/24.
 */
import Entity from '../entity';
declare class Instance extends Entity {
    uri(): any;
    title(): any;
    description(): any;
    email(): any;
}
export default Instance;
