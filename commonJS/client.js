'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */


var _oauth = require('oauth');

var _link = require('./link');

var _link2 = _interopRequireDefault(_link);

var _response = require('./response');

var _response2 = _interopRequireDefault(_response);

var _account = require('./entity/account');

var _account2 = _interopRequireDefault(_account);

var _attachment = require('./entity/attachment');

var _attachment2 = _interopRequireDefault(_attachment);

var _card = require('./entity/card');

var _card2 = _interopRequireDefault(_card);

var _context53 = require('./entity/context');

var _context54 = _interopRequireDefault(_context53);

var _instance = require('./entity/instance');

var _instance2 = _interopRequireDefault(_instance);

var _notification = require('./entity/notification');

var _notification2 = _interopRequireDefault(_notification);

var _relationship = require('./entity/relationship');

var _relationship2 = _interopRequireDefault(_relationship);

var _status = require('./entity/status');

var _status2 = _interopRequireDefault(_status);

var _result = require('./entity/result');

var _result2 = _interopRequireDefault(_result);

var _report = require('./entity/report');

var _report2 = _interopRequireDefault(_report);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Mastodon = function () {
    /**
     * constructor
     */
    function Mastodon() {
        _classCallCheck(this, Mastodon);

        this.callback = 'urn:ietf:wg:oauth:2.0:oob';
        // client params
        this.name = 'themisto-lib';
        this.site = null;
        this.scopes = ['read', 'write', 'follow'];
        // URLs
        this.url = {
            base: null,
            api: null,
            stream: null
        };
        // credential
        this.credential = {
            clientID: null,
            clientSecret: null,
            accessToken: null,
            refreshToken: null
        };
        // https flag
        this.useSecureProtocol = true;
    }
    /**
     * set client name for app registration
     * @param name client name
     */


    _createClass(Mastodon, [{
        key: 'setClientName',
        value: function setClientName(name) {
            this.name = name;
        }
        /**
         * set site url for app registration
         * @param site client site URL
         */

    }, {
        key: 'setClientSiteUrl',
        value: function setClientSiteUrl(site) {
            this.site = site;
        }
        /**
         * set credentials for OAuth
         * @param credentials
         */

    }, {
        key: 'setCredentials',
        value: function setCredentials(credentials) {
            this.credential = {
                clientID: credentials.clientID,
                clientSecret: credentials.clientSecret,
                accessToken: credentials.accessToken,
                refreshToken: credentials.refreshToken
            };
        }
        /**
         * set scopes for app registration
         * @param scopes
         */

    }, {
        key: 'setScopes',
        value: function setScopes(scopes) {
            this.scopes = scopes;
        }
        /**
         * set URLs for fetch
         * @param base
         * @param api
         * @param stream
         */

    }, {
        key: 'setURLs',
        value: function setURLs(_ref) {
            var base = _ref.base,
                api = _ref.api,
                stream = _ref.stream;

            this.url = {
                base: base,
                api: api || base,
                stream: stream || base
            };
        }
        /**
         * export client config
         * @returns {{name: string, site: string, scopes: [string], url: Mastodon.URLs, credential: Mastodon.Credentials}}
         */

    }, {
        key: 'exportConfig',
        value: function exportConfig() {
            return {
                name: this.name,
                site: this.site,
                scopes: this.scopes,
                url: {
                    base: this.url.base,
                    api: this.url.api,
                    stream: this.url.stream
                },
                credential: {
                    clientID: this.credential.clientID,
                    clientSecret: this.credential.clientSecret,
                    accessToken: this.credential.accessToken,
                    refreshToken: this.credential.refreshToken
                }
            };
        }
        /*
         * Auth
         */
        /**
         * Registering an application:
         *
         * POST /api/v1/apps
         *
         * Form data:
         *
         * client_name:
         *   Name of your application
         * redirect_uris:
         *   Where the user should be redirected after authorization
         *   (for no redirect, use urn:ietf:wg:oauth:2.0:oob)
         * scopes:
         *   This can be a space-separated list of the following items: "read", "write" and "follow"
         *   (see this page for details on what the scopes do)
         *   website: (optional)
         *   URL to the homepage of your app
         *
         * Creates a new OAuth app. Returns id, client_id and client_secret which can be used
         * with OAuth authentication in your 3rd party app.
         *
         * These values should be requested in the app itself from the API for each
         * new app install + mastodon domain combo, and stored in the app for future requests.
         *
         * @returns {Promise<R{}>}
         */

    }, {
        key: 'registerApp',
        value: function () {
            var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
                var api, method, body, response, _response$value$json, client_id, client_secret;

                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                api = this._getApi('apps');
                                method = 'POST';
                                body = new URLSearchParams();

                                body.set('client_name', this.name);
                                body.set('redirect_uris', this.callback);
                                body.set('scopes', this.scopes.join(' '));
                                _context.next = 8;
                                return this._fetchJson(api, { method: method, body: body });

                            case 8:
                                response = _context.sent;

                                if (response.isSuccess()) {
                                    _response$value$json = response.value.json, client_id = _response$value$json.client_id, client_secret = _response$value$json.client_secret;

                                    this.setCredentials({
                                        clientID: client_id,
                                        clientSecret: client_secret
                                    });
                                }
                                return _context.abrupt('return', response);

                            case 11:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function registerApp() {
                return _ref2.apply(this, arguments);
            }

            return registerApp;
        }()
        /**
         * get OAuth authorizing URL
         * @returns {string}
         */

    }, {
        key: 'getAuthorizeURL',
        value: function getAuthorizeURL() {
            var _exportConfig$credent = this.exportConfig().credential,
                clientID = _exportConfig$credent.clientID,
                clientSecret = _exportConfig$credent.clientSecret;

            var oauth = new _oauth.OAuth2(clientID, clientSecret, this._getHTTPS(), null, '/oauth/token');
            return oauth.getAuthorizeUrl({
                redirect_uri: this.callback,
                response_type: 'code',
                scope: this.scopes.join(' ')
            });
        }
        /**
         * send OAuth code
         * @param code
         * @returns {Promise<R{}>}
         */

    }, {
        key: 'authenticate',
        value: function () {
            var _ref3 = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(code) {
                var _this = this;

                var _exportConfig$credent2, clientID, clientSecret, oauth, auth, _ref4, accessToken, refreshToken, response;

                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _exportConfig$credent2 = this.exportConfig().credential, clientID = _exportConfig$credent2.clientID, clientSecret = _exportConfig$credent2.clientSecret;
                                oauth = new _oauth.OAuth2(clientID, clientSecret, this._getHTTPS(), null, '/oauth/token');
                                auth = new Promise(function (resolve, reject) {
                                    oauth.getOAuthAccessToken(code, {
                                        grant_type: 'authorization_code',
                                        redirect_uri: _this.callback
                                    }, function (err, accessToken, refreshToken, response) {
                                        if (err) {
                                            reject(err);
                                        } else {
                                            resolve({ accessToken: accessToken, refreshToken: refreshToken, response: response });
                                        }
                                    });
                                });
                                _context2.prev = 3;
                                _context2.next = 6;
                                return auth;

                            case 6:
                                _ref4 = _context2.sent;
                                accessToken = _ref4.accessToken;
                                refreshToken = _ref4.refreshToken;
                                response = _ref4.response;

                                this.setCredentials({ clientID: clientID, clientSecret: clientSecret, accessToken: accessToken, refreshToken: refreshToken });
                                return _context2.abrupt('return', _response2.default.success({ accessToken: accessToken, refreshToken: refreshToken, response: response }));

                            case 14:
                                _context2.prev = 14;
                                _context2.t0 = _context2['catch'](3);
                                return _context2.abrupt('return', _response2.default.create(_response2.default.Type.networkError, _context2.t0));

                            case 17:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[3, 14]]);
            }));

            function authenticate(_x) {
                return _ref3.apply(this, arguments);
            }

            return authenticate;
        }()
        /*
         * Accounts
         */
        // Fetching an account:
        //
        // GET /api/v1/accounts/:id
        //
        // Returns an Account.

    }, {
        key: 'getAccount',
        value: function () {
            var _ref5 = _asyncToGenerator(regeneratorRuntime.mark(function _callee3(id) {
                var url, response, account;
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                url = this._getApi('accounts', id);
                                _context3.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context3.sent;

                                if (!response.isFailure()) {
                                    _context3.next = 6;
                                    break;
                                }

                                return _context3.abrupt('return', response);

                            case 6:
                                account = new _account2.default(response.value.json);
                                return _context3.abrupt('return', response.copy({ account: account }));

                            case 8:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));

            function getAccount(_x2) {
                return _ref5.apply(this, arguments);
            }

            return getAccount;
        }()
        // Getting the current user:
        //
        // GET /api/v1/accounts/verify_credentials
        //
        // Returns the authenticated user's Account.

    }, {
        key: 'getCurrentAccount',
        value: function () {
            var _ref6 = _asyncToGenerator(regeneratorRuntime.mark(function _callee4() {
                var url, response, account;
                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                url = this._getApi('accounts', null, 'verify_credentials');
                                _context4.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context4.sent;

                                if (!response.isFailure()) {
                                    _context4.next = 6;
                                    break;
                                }

                                return _context4.abrupt('return', response);

                            case 6:
                                account = new _account2.default(response.value.json);
                                return _context4.abrupt('return', response.copy({ account: account }));

                            case 8:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));

            function getCurrentAccount() {
                return _ref6.apply(this, arguments);
            }

            return getCurrentAccount;
        }()
        //   Updating the current user:
        //
        //   PATCH /api/v1/accounts/update_credentials
        //
        //   Form data:
        //
        //   display_name: The name to display in the user's profile
        //   note: A new biography for the user
        //   avatar: A base64 encoded image to display as the user's avatar (e.g. data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUoAAADrCAYAAAA...)
        //   header: A base64 encoded image to display as the user's header image (e.g. data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUoAAADrCAYAAAA...)

    }, {
        key: 'updateCurrentAccount',
        value: function () {
            var _ref7 = _asyncToGenerator(regeneratorRuntime.mark(function _callee5() {
                var _ref8 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
                    _ref8$displayName = _ref8.displayName,
                    displayName = _ref8$displayName === undefined ? null : _ref8$displayName,
                    _ref8$note = _ref8.note,
                    note = _ref8$note === undefined ? null : _ref8$note,
                    _ref8$avatar = _ref8.avatar,
                    avatar = _ref8$avatar === undefined ? null : _ref8$avatar,
                    _ref8$header = _ref8.header,
                    header = _ref8$header === undefined ? null : _ref8$header;

                var url, method, body, response, account;
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                url = this._getApi('accounts', null, 'update_credentials');
                                method = 'PATCH';
                                body = new URLSearchParams();

                                if (displayName) body.set('display_name', displayName);
                                if (note) body.set('note', note);
                                if (avatar) body.set('avatar', avatar);
                                if (header) body.set('header', header);
                                _context5.next = 9;
                                return this._authFetchJson(url, { method: method, body: body });

                            case 9:
                                response = _context5.sent;

                                if (!response.isFailure()) {
                                    _context5.next = 12;
                                    break;
                                }

                                return _context5.abrupt('return', response);

                            case 12:
                                account = new _account2.default(response.value.json);
                                return _context5.abrupt('return', response.copy({ account: account }));

                            case 14:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));

            function updateCurrentAccount() {
                return _ref7.apply(this, arguments);
            }

            return updateCurrentAccount;
        }()
        //   Getting an account's followers:
        //
        //   GET /api/v1/accounts/:id/followers
        //
        //   Returns an array of Accounts.

    }, {
        key: 'getFollowers',
        value: function () {
            var _ref9 = _asyncToGenerator(regeneratorRuntime.mark(function _callee6(id) {
                var url, response, accounts, link;
                return regeneratorRuntime.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                url = this._getApi('accounts', id, 'followers');
                                _context6.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context6.sent;

                                if (!response.isFailure()) {
                                    _context6.next = 6;
                                    break;
                                }

                                return _context6.abrupt('return', response);

                            case 6:
                                accounts = response.value.json.map(function (v) {
                                    return new _account2.default(v);
                                });
                                link = response.link(_link2.default.Type.accounts);
                                return _context6.abrupt('return', response.copy({ link: link, accounts: accounts }));

                            case 9:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this);
            }));

            function getFollowers(_x4) {
                return _ref9.apply(this, arguments);
            }

            return getFollowers;
        }()
        //   Getting who account is following:
        //
        //   GET /api/v1/accounts/:id/following
        //
        //   Returns an array of Accounts.

    }, {
        key: 'getFollowings',
        value: function () {
            var _ref10 = _asyncToGenerator(regeneratorRuntime.mark(function _callee7(id) {
                var url, response, accounts, link;
                return regeneratorRuntime.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                url = this._getApi('accounts', id, 'following');
                                _context7.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context7.sent;

                                if (!response.isFailure()) {
                                    _context7.next = 6;
                                    break;
                                }

                                return _context7.abrupt('return', response);

                            case 6:
                                accounts = response.value.json.map(function (v) {
                                    return new _account2.default(v);
                                });
                                link = response.link(_link2.default.Type.accounts);
                                return _context7.abrupt('return', response.copy({ link: link, accounts: accounts }));

                            case 9:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, this);
            }));

            function getFollowings(_x5) {
                return _ref10.apply(this, arguments);
            }

            return getFollowings;
        }()
        //   Getting an account's statuses:
        //
        //   GET /api/v1/accounts/:id/statuses
        //
        //   Query parameters:
        //
        //   only_media (optional): Only return statuses that have media attachments
        //   exclude_replies (optional): Skip statuses that reply to other statuses
        //
        //   Returns an array of Statuses.

    }, {
        key: 'getStatusesOfAccount',
        value: function () {
            var _ref11 = _asyncToGenerator(regeneratorRuntime.mark(function _callee8(id) {
                var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
                    onlyMedia: false,
                    excludeReplies: true
                };
                var url, response, statuses, link;
                return regeneratorRuntime.wrap(function _callee8$(_context8) {
                    while (1) {
                        switch (_context8.prev = _context8.next) {
                            case 0:
                                url = this._getApi('accounts', id, 'statuses', {
                                    only_media: params.onlyMedia,
                                    exclude_replies: params.excludeReplies
                                });
                                _context8.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context8.sent;

                                if (!response.isFailure()) {
                                    _context8.next = 6;
                                    break;
                                }

                                return _context8.abrupt('return', response);

                            case 6:
                                statuses = response.value.json.map(function (v) {
                                    return new _status2.default(v);
                                });
                                link = response.link(_link2.default.Type.statuses);
                                return _context8.abrupt('return', response.copy({ link: link, statuses: statuses }));

                            case 9:
                            case 'end':
                                return _context8.stop();
                        }
                    }
                }, _callee8, this);
            }));

            function getStatusesOfAccount(_x6) {
                return _ref11.apply(this, arguments);
            }

            return getStatusesOfAccount;
        }()
        //   Following an account:
        //
        //   POST /api/v1/accounts/:id/follow
        //
        //   Returns the target account's Relationship.

    }, {
        key: 'followAccount',
        value: function () {
            var _ref12 = _asyncToGenerator(regeneratorRuntime.mark(function _callee9(id) {
                var url, method, response, relationship;
                return regeneratorRuntime.wrap(function _callee9$(_context9) {
                    while (1) {
                        switch (_context9.prev = _context9.next) {
                            case 0:
                                url = this._getApi('accounts', id, 'follow');
                                method = 'POST';
                                _context9.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context9.sent;

                                if (!response.isFailure()) {
                                    _context9.next = 7;
                                    break;
                                }

                                return _context9.abrupt('return', response);

                            case 7:
                                relationship = new _relationship2.default(response.value.json);
                                return _context9.abrupt('return', response.copy({ relationship: relationship }));

                            case 9:
                            case 'end':
                                return _context9.stop();
                        }
                    }
                }, _callee9, this);
            }));

            function followAccount(_x8) {
                return _ref12.apply(this, arguments);
            }

            return followAccount;
        }()
        //   Unfollowing an account:
        //
        //   POST /api/v1/accounts/:id/unfollow
        //
        //   Returns the target account's Relationship.

    }, {
        key: 'unfollowAccount',
        value: function () {
            var _ref13 = _asyncToGenerator(regeneratorRuntime.mark(function _callee10(id) {
                var url, method, response, relationship;
                return regeneratorRuntime.wrap(function _callee10$(_context10) {
                    while (1) {
                        switch (_context10.prev = _context10.next) {
                            case 0:
                                url = this._getApi('accounts', id, 'unfollow');
                                method = 'POST';
                                _context10.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context10.sent;

                                if (!response.isFailure()) {
                                    _context10.next = 7;
                                    break;
                                }

                                return _context10.abrupt('return', response);

                            case 7:
                                relationship = new _relationship2.default(response.value.json);
                                return _context10.abrupt('return', response.copy({ relationship: relationship }));

                            case 9:
                            case 'end':
                                return _context10.stop();
                        }
                    }
                }, _callee10, this);
            }));

            function unfollowAccount(_x9) {
                return _ref13.apply(this, arguments);
            }

            return unfollowAccount;
        }()
        //   Blocking an account:
        //
        //   POST /api/v1/accounts/:id/block
        //
        //   Returns the target account's Relationship.

    }, {
        key: 'blockAccount',
        value: function () {
            var _ref14 = _asyncToGenerator(regeneratorRuntime.mark(function _callee11(id) {
                var url, method, response, relationship;
                return regeneratorRuntime.wrap(function _callee11$(_context11) {
                    while (1) {
                        switch (_context11.prev = _context11.next) {
                            case 0:
                                url = this._getApi('accounts', id, 'block');
                                method = 'POST';
                                _context11.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context11.sent;

                                if (!response.isFailure()) {
                                    _context11.next = 7;
                                    break;
                                }

                                return _context11.abrupt('return', response);

                            case 7:
                                relationship = new _relationship2.default(response.value.json);
                                return _context11.abrupt('return', response.copy({ relationship: relationship }));

                            case 9:
                            case 'end':
                                return _context11.stop();
                        }
                    }
                }, _callee11, this);
            }));

            function blockAccount(_x10) {
                return _ref14.apply(this, arguments);
            }

            return blockAccount;
        }()
        //   Unblocking an account:
        //
        //   POST /api/v1/accounts/:id/unblock
        //
        //   Returns the target account's Relationship.

    }, {
        key: 'unblockAccount',
        value: function () {
            var _ref15 = _asyncToGenerator(regeneratorRuntime.mark(function _callee12(id) {
                var url, method, response, relationship;
                return regeneratorRuntime.wrap(function _callee12$(_context12) {
                    while (1) {
                        switch (_context12.prev = _context12.next) {
                            case 0:
                                url = this._getApi('accounts', id, 'unblock');
                                method = 'POST';
                                _context12.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context12.sent;

                                if (!response.isFailure()) {
                                    _context12.next = 7;
                                    break;
                                }

                                return _context12.abrupt('return', response);

                            case 7:
                                relationship = new _relationship2.default(response.value.json);
                                return _context12.abrupt('return', response.copy({ relationship: relationship }));

                            case 9:
                            case 'end':
                                return _context12.stop();
                        }
                    }
                }, _callee12, this);
            }));

            function unblockAccount(_x11) {
                return _ref15.apply(this, arguments);
            }

            return unblockAccount;
        }()
        //   Muting an account:
        //
        //   POST /api/v1/accounts/:id/mute
        //
        //   Returns the target account's Relationship.

    }, {
        key: 'muteAccount',
        value: function () {
            var _ref16 = _asyncToGenerator(regeneratorRuntime.mark(function _callee13(id) {
                var url, method, response, relationship;
                return regeneratorRuntime.wrap(function _callee13$(_context13) {
                    while (1) {
                        switch (_context13.prev = _context13.next) {
                            case 0:
                                url = this._getApi('accounts', id, 'mute');
                                method = 'POST';
                                _context13.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context13.sent;

                                if (!response.isFailure()) {
                                    _context13.next = 7;
                                    break;
                                }

                                return _context13.abrupt('return', response);

                            case 7:
                                relationship = new _relationship2.default(response.value.json);
                                return _context13.abrupt('return', response.copy({ relationship: relationship }));

                            case 9:
                            case 'end':
                                return _context13.stop();
                        }
                    }
                }, _callee13, this);
            }));

            function muteAccount(_x12) {
                return _ref16.apply(this, arguments);
            }

            return muteAccount;
        }()
        //   Unmuting an account:
        //
        //   POST /api/v1/accounts/:id/unmute
        //
        //   Returns the target account's Relationship.

    }, {
        key: 'unmuteAccount',
        value: function () {
            var _ref17 = _asyncToGenerator(regeneratorRuntime.mark(function _callee14(id) {
                var url, method, response, relationship;
                return regeneratorRuntime.wrap(function _callee14$(_context14) {
                    while (1) {
                        switch (_context14.prev = _context14.next) {
                            case 0:
                                url = this._getApi('accounts', id, 'unmute');
                                method = 'POST';
                                _context14.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context14.sent;

                                if (!response.isFailure()) {
                                    _context14.next = 7;
                                    break;
                                }

                                return _context14.abrupt('return', response);

                            case 7:
                                relationship = new _relationship2.default(response.value.json);
                                return _context14.abrupt('return', response.copy({ relationship: relationship }));

                            case 9:
                            case 'end':
                                return _context14.stop();
                        }
                    }
                }, _callee14, this);
            }));

            function unmuteAccount(_x13) {
                return _ref17.apply(this, arguments);
            }

            return unmuteAccount;
        }()
        //   Getting an account's relationships:
        //
        //   GET /api/v1/accounts/relationships
        //
        //   Query parameters:
        //
        //   id (can be array): Account IDs
        //
        //   Returns an array of Relationships of the current user to a list of given accounts.

    }, {
        key: 'getRelationships',
        value: function () {
            var _ref18 = _asyncToGenerator(regeneratorRuntime.mark(function _callee15(ids) {
                var targets, url, response, relationships;
                return regeneratorRuntime.wrap(function _callee15$(_context15) {
                    while (1) {
                        switch (_context15.prev = _context15.next) {
                            case 0:
                                targets = Array.isArray(ids) ? ids : [ids];
                                url = this._getApi('accounts', null, 'relationships', { id: targets });
                                // const url = this._getApi('accounts', null, `relationships?id[]=${ids[0]}&id[]=${ids[1]}`, {id: targets})

                                _context15.next = 4;
                                return this._authFetchJson(url);

                            case 4:
                                response = _context15.sent;

                                if (!response.isFailure()) {
                                    _context15.next = 7;
                                    break;
                                }

                                return _context15.abrupt('return', response);

                            case 7:
                                relationships = response.value.json.map(function (v) {
                                    return new _relationship2.default(v);
                                });
                                return _context15.abrupt('return', response.copy({ relationships: relationships }));

                            case 9:
                            case 'end':
                                return _context15.stop();
                        }
                    }
                }, _callee15, this);
            }));

            function getRelationships(_x14) {
                return _ref18.apply(this, arguments);
            }

            return getRelationships;
        }()
        //   Searching for accounts:
        //
        //   GET /api/v1/accounts/search
        //
        //   Query parameters:
        //
        //   q: What to search for
        //   limit: Maximum number of matching accounts to return (default: 40)
        //
        //   Returns an array of matching Accounts.
        //   Will lookup an account remotely if the search term is
        //   in the username@domain format and not yet in the database.

    }, {
        key: 'searchAccounts',
        value: function () {
            var _ref19 = _asyncToGenerator(regeneratorRuntime.mark(function _callee16(query) {
                var limit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 40;
                var url, response, accounts;
                return regeneratorRuntime.wrap(function _callee16$(_context16) {
                    while (1) {
                        switch (_context16.prev = _context16.next) {
                            case 0:
                                url = this._getApi('accounts', null, 'search', { q: query, limit: limit });
                                _context16.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context16.sent;

                                if (!response.isFailure()) {
                                    _context16.next = 6;
                                    break;
                                }

                                return _context16.abrupt('return', response);

                            case 6:
                                accounts = response.value.json.map(function (v) {
                                    return new _account2.default(v);
                                });
                                return _context16.abrupt('return', response.copy({ accounts: accounts }));

                            case 8:
                            case 'end':
                                return _context16.stop();
                        }
                    }
                }, _callee16, this);
            }));

            function searchAccounts(_x15) {
                return _ref19.apply(this, arguments);
            }

            return searchAccounts;
        }()
        /*
        * Blocks
        */
        //   Fetching a user's blocks:
        //
        //   GET /api/v1/blocks
        //
        //   Returns an array of Accounts blocked by the authenticated user.

    }, {
        key: 'getBlocks',
        value: function () {
            var _ref20 = _asyncToGenerator(regeneratorRuntime.mark(function _callee17() {
                var url, response, accounts, link;
                return regeneratorRuntime.wrap(function _callee17$(_context17) {
                    while (1) {
                        switch (_context17.prev = _context17.next) {
                            case 0:
                                url = this._getApi('blocks');
                                _context17.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context17.sent;

                                if (!response.isFailure()) {
                                    _context17.next = 6;
                                    break;
                                }

                                return _context17.abrupt('return', response);

                            case 6:
                                accounts = response.value.json.map(function (v) {
                                    return new _account2.default(v);
                                });
                                link = response.link(_link2.default.Type.accounts);
                                return _context17.abrupt('return', response.copy({ link: link, accounts: accounts }));

                            case 9:
                            case 'end':
                                return _context17.stop();
                        }
                    }
                }, _callee17, this);
            }));

            function getBlocks() {
                return _ref20.apply(this, arguments);
            }

            return getBlocks;
        }()
        /*
         * Favourites
         */
        //   Fetching a user's favourites:
        //
        //   GET /api/v1/favourites
        //
        //   Returns an array of Statuses favourited by the authenticated user.

    }, {
        key: 'getFavorites',
        value: function () {
            var _ref21 = _asyncToGenerator(regeneratorRuntime.mark(function _callee18() {
                var url, response, statuses, link;
                return regeneratorRuntime.wrap(function _callee18$(_context18) {
                    while (1) {
                        switch (_context18.prev = _context18.next) {
                            case 0:
                                url = this._getApi('favourites');
                                _context18.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context18.sent;

                                if (!response.isFailure()) {
                                    _context18.next = 6;
                                    break;
                                }

                                return _context18.abrupt('return', response);

                            case 6:
                                statuses = response.value.json.map(function (v) {
                                    return new _status2.default(v);
                                });
                                link = response.link(_link2.default.Type.statuses);
                                return _context18.abrupt('return', response.copy({ link: link, statuses: statuses }));

                            case 9:
                            case 'end':
                                return _context18.stop();
                        }
                    }
                }, _callee18, this);
            }));

            function getFavorites() {
                return _ref21.apply(this, arguments);
            }

            return getFavorites;
        }()
        /*
         * Follow Requests
         */
        //   Fetching a list of follow requests:
        //
        //   GET /api/v1/follow_requests
        //
        //   Returns an array of Accounts which have requested to follow the authenticated user.

    }, {
        key: 'getFollowRequests',
        value: function () {
            var _ref22 = _asyncToGenerator(regeneratorRuntime.mark(function _callee19() {
                var url, response, accounts, link;
                return regeneratorRuntime.wrap(function _callee19$(_context19) {
                    while (1) {
                        switch (_context19.prev = _context19.next) {
                            case 0:
                                url = this._getApi('follow_requests');
                                _context19.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context19.sent;

                                if (!response.isFailure()) {
                                    _context19.next = 6;
                                    break;
                                }

                                return _context19.abrupt('return', response);

                            case 6:
                                accounts = response.value.json.map(function (v) {
                                    return new _account2.default(v);
                                });
                                link = response.link(_link2.default.Type.accounts);
                                return _context19.abrupt('return', response.copy({ link: link, accounts: accounts }));

                            case 9:
                            case 'end':
                                return _context19.stop();
                        }
                    }
                }, _callee19, this);
            }));

            function getFollowRequests() {
                return _ref22.apply(this, arguments);
            }

            return getFollowRequests;
        }()
        //   Authorizing follow requests:
        //
        //   POST /api/v1/follow_requests/:id/authorize
        //
        //   Form data:
        //
        //   id: The id of the account to authorize or reject
        //
        //   Returns an empty object.

    }, {
        key: 'authorizeFollowRequest',
        value: function () {
            var _ref23 = _asyncToGenerator(regeneratorRuntime.mark(function _callee20(id) {
                var url, method, body;
                return regeneratorRuntime.wrap(function _callee20$(_context20) {
                    while (1) {
                        switch (_context20.prev = _context20.next) {
                            case 0:
                                url = this._getApi('follow_requests', id, 'authorize');
                                method = 'POST';
                                body = new FormData();

                                body.append('id', String(id));
                                _context20.next = 6;
                                return this._authFetch(url, { method: method, body: body });

                            case 6:
                                return _context20.abrupt('return', _context20.sent);

                            case 7:
                            case 'end':
                                return _context20.stop();
                        }
                    }
                }, _callee20, this);
            }));

            function authorizeFollowRequest(_x17) {
                return _ref23.apply(this, arguments);
            }

            return authorizeFollowRequest;
        }()
        //   Rejecting follow requests:
        //
        //   POST /api/v1/follow_requests/:id/reject
        //
        //   Form data:
        //
        //   id: The id of the account to authorize or reject
        //
        //   Returns an empty object.

    }, {
        key: 'rejectFollowRequest',
        value: function () {
            var _ref24 = _asyncToGenerator(regeneratorRuntime.mark(function _callee21(id) {
                var url, method, body;
                return regeneratorRuntime.wrap(function _callee21$(_context21) {
                    while (1) {
                        switch (_context21.prev = _context21.next) {
                            case 0:
                                url = this._getApi('follow_requests', id, 'reject');
                                method = 'POST';
                                body = new FormData();

                                body.append('id', String(id));
                                _context21.next = 6;
                                return this._authFetch(url, { method: method, body: body });

                            case 6:
                                return _context21.abrupt('return', _context21.sent);

                            case 7:
                            case 'end':
                                return _context21.stop();
                        }
                    }
                }, _callee21, this);
            }));

            function rejectFollowRequest(_x18) {
                return _ref24.apply(this, arguments);
            }

            return rejectFollowRequest;
        }()
        /*
         * Follows
         */
        //   Following a remote user:
        //
        //   POST /api/v1/follows
        //
        //   Form data:
        //
        //   uri: username@domain of the person you want to follow
        //
        //   Returns the local representation of the followed account, as an Account.

    }, {
        key: 'followRemoteUser',
        value: function () {
            var _ref25 = _asyncToGenerator(regeneratorRuntime.mark(function _callee22(uri) {
                var url, method, body, response, account;
                return regeneratorRuntime.wrap(function _callee22$(_context22) {
                    while (1) {
                        switch (_context22.prev = _context22.next) {
                            case 0:
                                url = this._getApi('follows');
                                method = 'POST';
                                body = new FormData();

                                body.append('uri', uri);
                                _context22.next = 6;
                                return this._authFetchJson(url, { method: method, body: body });

                            case 6:
                                response = _context22.sent;

                                if (!response.isFailure()) {
                                    _context22.next = 9;
                                    break;
                                }

                                return _context22.abrupt('return', response);

                            case 9:
                                account = new _account2.default(response.value.json);
                                return _context22.abrupt('return', response.copy({ account: account }));

                            case 11:
                            case 'end':
                                return _context22.stop();
                        }
                    }
                }, _callee22, this);
            }));

            function followRemoteUser(_x19) {
                return _ref25.apply(this, arguments);
            }

            return followRemoteUser;
        }()
        /*
         * Instances
         */
        //   Getting instance information:
        //
        //   GET /api/v1/instance
        //
        //   Returns the current Instance. Does not require authentication.

    }, {
        key: 'getInstanceInfo',
        value: function () {
            var _ref26 = _asyncToGenerator(regeneratorRuntime.mark(function _callee23() {
                var url, response, instance;
                return regeneratorRuntime.wrap(function _callee23$(_context23) {
                    while (1) {
                        switch (_context23.prev = _context23.next) {
                            case 0:
                                url = this._getApi('instance');
                                _context23.next = 3;
                                return this._fetchJson(url);

                            case 3:
                                response = _context23.sent;

                                if (!response.isFailure()) {
                                    _context23.next = 6;
                                    break;
                                }

                                return _context23.abrupt('return', response);

                            case 6:
                                instance = new _instance2.default(response.value.json);
                                return _context23.abrupt('return', response.copy({ instance: instance }));

                            case 8:
                            case 'end':
                                return _context23.stop();
                        }
                    }
                }, _callee23, this);
            }));

            function getInstanceInfo() {
                return _ref26.apply(this, arguments);
            }

            return getInstanceInfo;
        }()
        /*
        * Media
        */
        //   Uploading a media attachment:
        //
        //   POST /api/v1/media
        //
        //   Form data:
        //
        //   file: Media to be uploaded
        //
        //   Returns an Attachment that can be used when creating a statuses.

    }, {
        key: 'uploadMediaAttachment',
        value: function () {
            var _ref27 = _asyncToGenerator(regeneratorRuntime.mark(function _callee24(file) {
                var url, method, body, response, attachment;
                return regeneratorRuntime.wrap(function _callee24$(_context24) {
                    while (1) {
                        switch (_context24.prev = _context24.next) {
                            case 0:
                                url = this._getApi('media');
                                method = 'POST';
                                body = new FormData();

                                body.append('file', file);
                                _context24.next = 6;
                                return this._authFetchJson(url, { method: method, body: body });

                            case 6:
                                response = _context24.sent;

                                if (!response.isFailure()) {
                                    _context24.next = 9;
                                    break;
                                }

                                return _context24.abrupt('return', response);

                            case 9:
                                attachment = new _attachment2.default(response.value.json);
                                return _context24.abrupt('return', response.copy({ attachment: attachment }));

                            case 11:
                            case 'end':
                                return _context24.stop();
                        }
                    }
                }, _callee24, this);
            }));

            function uploadMediaAttachment(_x20) {
                return _ref27.apply(this, arguments);
            }

            return uploadMediaAttachment;
        }()
        /*
         * Mutes
         */
        //   Fetching a user's mutes:
        //
        //   GET /api/v1/mutes
        //
        //   Returns an array of Accounts muted by the authenticated user.

    }, {
        key: 'getMutes',
        value: function () {
            var _ref28 = _asyncToGenerator(regeneratorRuntime.mark(function _callee25() {
                var url, response, accounts;
                return regeneratorRuntime.wrap(function _callee25$(_context25) {
                    while (1) {
                        switch (_context25.prev = _context25.next) {
                            case 0:
                                url = this._getApi('mutes');
                                _context25.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context25.sent;

                                if (!response.isFailure()) {
                                    _context25.next = 6;
                                    break;
                                }

                                return _context25.abrupt('return', response);

                            case 6:
                                accounts = response.value.json.map(function (v) {
                                    return new _account2.default(v);
                                });
                                return _context25.abrupt('return', response.copy({ accounts: accounts }));

                            case 8:
                            case 'end':
                                return _context25.stop();
                        }
                    }
                }, _callee25, this);
            }));

            function getMutes() {
                return _ref28.apply(this, arguments);
            }

            return getMutes;
        }()
        /*
         * Notifications
         */
        //   Fetching a user's notifications:
        //
        //   GET /api/v1/notifications
        //
        //   Returns a list of Notifications for the authenticated user.

    }, {
        key: 'getNotifications',
        value: function () {
            var _ref29 = _asyncToGenerator(regeneratorRuntime.mark(function _callee26() {
                var url, response, notifications, link;
                return regeneratorRuntime.wrap(function _callee26$(_context26) {
                    while (1) {
                        switch (_context26.prev = _context26.next) {
                            case 0:
                                url = this._getApi('notifications');
                                _context26.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context26.sent;

                                if (!response.isFailure()) {
                                    _context26.next = 6;
                                    break;
                                }

                                return _context26.abrupt('return', response);

                            case 6:
                                notifications = response.value.json.map(function (v) {
                                    return new _notification2.default(v);
                                });
                                link = response.link(_link2.default.Type.notifications);
                                return _context26.abrupt('return', response.copy({ link: link, notifications: notifications }));

                            case 9:
                            case 'end':
                                return _context26.stop();
                        }
                    }
                }, _callee26, this);
            }));

            function getNotifications() {
                return _ref29.apply(this, arguments);
            }

            return getNotifications;
        }()
        //   Getting a single notification:
        //
        //   GET /api/v1/notifications/:id
        //
        //   Returns the Notification.

    }, {
        key: 'getNotification',
        value: function () {
            var _ref30 = _asyncToGenerator(regeneratorRuntime.mark(function _callee27(id) {
                var url, response, notification;
                return regeneratorRuntime.wrap(function _callee27$(_context27) {
                    while (1) {
                        switch (_context27.prev = _context27.next) {
                            case 0:
                                url = this._getApi('notifications', id);
                                _context27.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context27.sent;

                                if (!response.isFailure()) {
                                    _context27.next = 6;
                                    break;
                                }

                                return _context27.abrupt('return', response);

                            case 6:
                                notification = new _notification2.default(response.value.json);
                                return _context27.abrupt('return', response.copy({ notification: notification }));

                            case 8:
                            case 'end':
                                return _context27.stop();
                        }
                    }
                }, _callee27, this);
            }));

            function getNotification(_x21) {
                return _ref30.apply(this, arguments);
            }

            return getNotification;
        }()
        //   Clearing notifications:
        //
        //   POST /api/v1/notifications/clear
        //
        //   Deletes all notifications from the Mastodon server for the authenticated user. Returns an empty object.

    }, {
        key: 'clearNotifications',
        value: function () {
            var _ref31 = _asyncToGenerator(regeneratorRuntime.mark(function _callee28() {
                var url, method;
                return regeneratorRuntime.wrap(function _callee28$(_context28) {
                    while (1) {
                        switch (_context28.prev = _context28.next) {
                            case 0:
                                url = this._getApi('notifications', 'clear');
                                method = 'POST';
                                _context28.next = 4;
                                return this._authFetch(url, { method: method });

                            case 4:
                                return _context28.abrupt('return', _context28.sent);

                            case 5:
                            case 'end':
                                return _context28.stop();
                        }
                    }
                }, _callee28, this);
            }));

            function clearNotifications() {
                return _ref31.apply(this, arguments);
            }

            return clearNotifications;
        }()
        /*
         * Reports
         */
        //   Fetching a user's reports:
        //
        //   GET /api/v1/reports
        //
        //   Returns a list of Reports made by the authenticated user.

    }, {
        key: 'fetchReports',
        value: function () {
            var _ref32 = _asyncToGenerator(regeneratorRuntime.mark(function _callee29() {
                var url, response, reports;
                return regeneratorRuntime.wrap(function _callee29$(_context29) {
                    while (1) {
                        switch (_context29.prev = _context29.next) {
                            case 0:
                                url = this._getApi('reports');
                                _context29.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context29.sent;

                                if (!response.isFailure()) {
                                    _context29.next = 6;
                                    break;
                                }

                                return _context29.abrupt('return', response);

                            case 6:
                                reports = response.value.json.map(function (j) {
                                    return new _report2.default(j);
                                });
                                return _context29.abrupt('return', response.copy({ reports: reports }));

                            case 8:
                            case 'end':
                                return _context29.stop();
                        }
                    }
                }, _callee29, this);
            }));

            function fetchReports() {
                return _ref32.apply(this, arguments);
            }

            return fetchReports;
        }()
        //   Reporting a user:
        //
        //   POST /api/v1/reports
        //
        //   Form data:
        //
        //   account_id: The ID of the account to report
        //   status_ids: The IDs of statuses to report (can be an array)
        //   comment: A comment to associate with the report.
        //
        //   Returns the finished Report.

    }, {
        key: 'reportUser',
        value: function () {
            var _ref33 = _asyncToGenerator(regeneratorRuntime.mark(function _callee30() {
                var _ref34 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
                    _ref34$accountId = _ref34.accountId,
                    accountId = _ref34$accountId === undefined ? null : _ref34$accountId,
                    _ref34$statusIds = _ref34.statusIds,
                    statusIds = _ref34$statusIds === undefined ? [] : _ref34$statusIds,
                    _ref34$comment = _ref34.comment,
                    comment = _ref34$comment === undefined ? null : _ref34$comment;

                var url, method, body, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, id, response, report;

                return regeneratorRuntime.wrap(function _callee30$(_context30) {
                    while (1) {
                        switch (_context30.prev = _context30.next) {
                            case 0:
                                url = this._getApi('reports');
                                method = 'POST';
                                body = new FormData();

                                body.append('account_id', accountId);
                                _iteratorNormalCompletion = true;
                                _didIteratorError = false;
                                _iteratorError = undefined;
                                _context30.prev = 7;
                                for (_iterator = statusIds[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                                    id = _step.value;

                                    body.append('status_ids[]', id);
                                }
                                _context30.next = 15;
                                break;

                            case 11:
                                _context30.prev = 11;
                                _context30.t0 = _context30['catch'](7);
                                _didIteratorError = true;
                                _iteratorError = _context30.t0;

                            case 15:
                                _context30.prev = 15;
                                _context30.prev = 16;

                                if (!_iteratorNormalCompletion && _iterator.return) {
                                    _iterator.return();
                                }

                            case 18:
                                _context30.prev = 18;

                                if (!_didIteratorError) {
                                    _context30.next = 21;
                                    break;
                                }

                                throw _iteratorError;

                            case 21:
                                return _context30.finish(18);

                            case 22:
                                return _context30.finish(15);

                            case 23:
                                body.append('comment', comment);
                                _context30.next = 26;
                                return this._authFetchJson(url, { method: method, body: body });

                            case 26:
                                response = _context30.sent;

                                if (!response.isFailure()) {
                                    _context30.next = 29;
                                    break;
                                }

                                return _context30.abrupt('return', response);

                            case 29:
                                report = new _report2.default(response.value.json);
                                return _context30.abrupt('return', response.copy({ report: report }));

                            case 31:
                            case 'end':
                                return _context30.stop();
                        }
                    }
                }, _callee30, this, [[7, 11, 15, 23], [16,, 18, 22]]);
            }));

            function reportUser() {
                return _ref33.apply(this, arguments);
            }

            return reportUser;
        }()
        /*
        * Search
        */
        //   Searching for content:
        //
        //   GET /api/v1/search
        //
        //   Form data:
        //
        //   q: The search query
        //   resolve: Whether to resolve non-local accounts
        //
        //   Returns Results. If q is a URL, Mastodon will attempt to fetch the provided account
        // or statuses. Otherwise, it will do a local account and hashtag search.

    }, {
        key: 'search',
        value: function () {
            var _ref35 = _asyncToGenerator(regeneratorRuntime.mark(function _callee31(query) {
                var resolveRemoteAccounts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
                var url, response, result;
                return regeneratorRuntime.wrap(function _callee31$(_context31) {
                    while (1) {
                        switch (_context31.prev = _context31.next) {
                            case 0:
                                url = this._getApi('search', null, null, { q: query, resolve: resolveRemoteAccounts });
                                _context31.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context31.sent;

                                if (!response.isFailure()) {
                                    _context31.next = 6;
                                    break;
                                }

                                return _context31.abrupt('return', response);

                            case 6:
                                result = new _result2.default(response.value.json);
                                return _context31.abrupt('return', response.copy({ result: result }));

                            case 8:
                            case 'end':
                                return _context31.stop();
                        }
                    }
                }, _callee31, this);
            }));

            function search(_x23) {
                return _ref35.apply(this, arguments);
            }

            return search;
        }()
        /*
        * Statuses
        */
        //   Fetching a status:
        //
        //   GET /api/v1/statuses/:id
        //
        //   Returns a Status.

    }, {
        key: 'fetchStatus',
        value: function () {
            var _ref36 = _asyncToGenerator(regeneratorRuntime.mark(function _callee32(id) {
                var url, response, status;
                return regeneratorRuntime.wrap(function _callee32$(_context32) {
                    while (1) {
                        switch (_context32.prev = _context32.next) {
                            case 0:
                                url = this._getApi('statuses', id);
                                _context32.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context32.sent;

                                if (!response.isFailure()) {
                                    _context32.next = 6;
                                    break;
                                }

                                return _context32.abrupt('return', response);

                            case 6:
                                status = new _status2.default(response.value.json);
                                return _context32.abrupt('return', response.copy({ status: status }));

                            case 8:
                            case 'end':
                                return _context32.stop();
                        }
                    }
                }, _callee32, this);
            }));

            function fetchStatus(_x25) {
                return _ref36.apply(this, arguments);
            }

            return fetchStatus;
        }()
        //   Getting statuses context:
        //
        //   GET /api/v1/statuses/:id/context
        //
        //   Returns a Context.

    }, {
        key: 'getStatusContext',
        value: function () {
            var _ref37 = _asyncToGenerator(regeneratorRuntime.mark(function _callee33(id) {
                var url, response, context;
                return regeneratorRuntime.wrap(function _callee33$(_context33) {
                    while (1) {
                        switch (_context33.prev = _context33.next) {
                            case 0:
                                url = this._getApi('statuses', id, 'context');
                                _context33.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context33.sent;

                                if (!response.isFailure()) {
                                    _context33.next = 6;
                                    break;
                                }

                                return _context33.abrupt('return', response);

                            case 6:
                                context = new _context54.default(response.value.json);
                                return _context33.abrupt('return', response.copy({ context: context }));

                            case 8:
                            case 'end':
                                return _context33.stop();
                        }
                    }
                }, _callee33, this);
            }));

            function getStatusContext(_x26) {
                return _ref37.apply(this, arguments);
            }

            return getStatusContext;
        }()
        //   Getting a card associated with a statuses:
        //
        //   GET /api/v1/statuses/:id/card
        //
        //   Returns a Card.

    }, {
        key: 'getStatusCard',
        value: function () {
            var _ref38 = _asyncToGenerator(regeneratorRuntime.mark(function _callee34(id) {
                var url, response, card;
                return regeneratorRuntime.wrap(function _callee34$(_context34) {
                    while (1) {
                        switch (_context34.prev = _context34.next) {
                            case 0:
                                url = this._getApi('statuses', id, 'card');
                                _context34.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context34.sent;

                                if (!response.isFailure()) {
                                    _context34.next = 6;
                                    break;
                                }

                                return _context34.abrupt('return', response);

                            case 6:
                                card = new _card2.default(response.value.json);
                                return _context34.abrupt('return', response.copy({ card: card }));

                            case 8:
                            case 'end':
                                return _context34.stop();
                        }
                    }
                }, _callee34, this);
            }));

            function getStatusCard(_x27) {
                return _ref38.apply(this, arguments);
            }

            return getStatusCard;
        }()
        //   Getting who reblogged a statuses:
        //
        //   GET /api/v1/statuses/:id/reblogged_by
        //
        //   Returns an array of Accounts.

    }, {
        key: 'getRebloggingAccounts',
        value: function () {
            var _ref39 = _asyncToGenerator(regeneratorRuntime.mark(function _callee35(id) {
                var url, response, accounts;
                return regeneratorRuntime.wrap(function _callee35$(_context35) {
                    while (1) {
                        switch (_context35.prev = _context35.next) {
                            case 0:
                                url = this._getApi('statuses', id, 'reblogged_by');
                                _context35.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context35.sent;

                                if (!response.isFailure()) {
                                    _context35.next = 6;
                                    break;
                                }

                                return _context35.abrupt('return', response);

                            case 6:
                                accounts = response.value.json.map(function (j) {
                                    return new _account2.default(j);
                                });
                                return _context35.abrupt('return', response.copy({ accounts: accounts }));

                            case 8:
                            case 'end':
                                return _context35.stop();
                        }
                    }
                }, _callee35, this);
            }));

            function getRebloggingAccounts(_x28) {
                return _ref39.apply(this, arguments);
            }

            return getRebloggingAccounts;
        }()
        //   Getting who favourited a statuses:
        //
        //   GET /api/v1/statuses/:id/favourited_by
        //
        //   Returns an array of Accounts.

    }, {
        key: 'getFavoritingAccounts',
        value: function () {
            var _ref40 = _asyncToGenerator(regeneratorRuntime.mark(function _callee36(id) {
                var url, response, accounts;
                return regeneratorRuntime.wrap(function _callee36$(_context36) {
                    while (1) {
                        switch (_context36.prev = _context36.next) {
                            case 0:
                                url = this._getApi('statuses', id, 'favourited_by');
                                _context36.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context36.sent;

                                if (!response.isFailure()) {
                                    _context36.next = 6;
                                    break;
                                }

                                return _context36.abrupt('return', response);

                            case 6:
                                accounts = response.value.json.map(function (j) {
                                    return new _account2.default(j);
                                });
                                return _context36.abrupt('return', response.copy({ accounts: accounts }));

                            case 8:
                            case 'end':
                                return _context36.stop();
                        }
                    }
                }, _callee36, this);
            }));

            function getFavoritingAccounts(_x29) {
                return _ref40.apply(this, arguments);
            }

            return getFavoritingAccounts;
        }()
        //   Posting a new statuses:
        //
        //   POST /api/v1/statuses
        //
        //   Form data:
        //
        //   status: The text of the statuses
        //   in_reply_to_id (optional): local ID of the statuses you want to reply to
        //   media_ids (optional): array of media IDs to attach to the statuses (maximum 4)
        //   sensitive (optional): set this to mark the media of the statuses as NSFW
        //   spoiler_text (optional): text to be shown as a warning before the actual content
        //   visibility (optional): either "direct", "private", "unlisted" or "public"
        //
        //   Returns the new Status.

    }, {
        key: 'postStatus',
        value: function () {
            var _ref41 = _asyncToGenerator(regeneratorRuntime.mark(function _callee37(text) {
                var _ref42 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
                    _ref42$replyTo = _ref42.replyTo,
                    replyTo = _ref42$replyTo === undefined ? null : _ref42$replyTo,
                    _ref42$mediaIds = _ref42.mediaIds,
                    mediaIds = _ref42$mediaIds === undefined ? [] : _ref42$mediaIds,
                    _ref42$sensitive = _ref42.sensitive,
                    sensitive = _ref42$sensitive === undefined ? false : _ref42$sensitive,
                    _ref42$spoilerText = _ref42.spoilerText,
                    spoilerText = _ref42$spoilerText === undefined ? null : _ref42$spoilerText,
                    _ref42$visibility = _ref42.visibility,
                    visibility = _ref42$visibility === undefined ? 'public' : _ref42$visibility;

                var url, method, body, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, id, response, status;

                return regeneratorRuntime.wrap(function _callee37$(_context37) {
                    while (1) {
                        switch (_context37.prev = _context37.next) {
                            case 0:
                                url = this._getApi('statuses', null, null);
                                method = 'POST';
                                body = new FormData();

                                body.append('status', text);
                                if (replyTo) body.append('in_reply_to_id', replyTo);
                                _iteratorNormalCompletion2 = true;
                                _didIteratorError2 = false;
                                _iteratorError2 = undefined;
                                _context37.prev = 8;
                                for (_iterator2 = mediaIds[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                                    id = _step2.value;

                                    body.append('media_ids[]', id);
                                }
                                _context37.next = 16;
                                break;

                            case 12:
                                _context37.prev = 12;
                                _context37.t0 = _context37['catch'](8);
                                _didIteratorError2 = true;
                                _iteratorError2 = _context37.t0;

                            case 16:
                                _context37.prev = 16;
                                _context37.prev = 17;

                                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                    _iterator2.return();
                                }

                            case 19:
                                _context37.prev = 19;

                                if (!_didIteratorError2) {
                                    _context37.next = 22;
                                    break;
                                }

                                throw _iteratorError2;

                            case 22:
                                return _context37.finish(19);

                            case 23:
                                return _context37.finish(16);

                            case 24:
                                if (sensitive) body.append('sensitive', 'true');
                                if (spoilerText) body.append('spoiler_text', spoilerText);
                                body.append('visibility', visibility);
                                _context37.next = 29;
                                return this._authFetchJson(url, { method: method, body: body });

                            case 29:
                                response = _context37.sent;

                                if (!response.isFailure()) {
                                    _context37.next = 32;
                                    break;
                                }

                                return _context37.abrupt('return', response);

                            case 32:
                                status = new _status2.default(response.value.json);
                                return _context37.abrupt('return', response.copy({ status: status }));

                            case 34:
                            case 'end':
                                return _context37.stop();
                        }
                    }
                }, _callee37, this, [[8, 12, 16, 24], [17,, 19, 23]]);
            }));

            function postStatus(_x30) {
                return _ref41.apply(this, arguments);
            }

            return postStatus;
        }()
        //   Deleting a statuses:
        //
        //   DELETE /api/v1/statuses/:id
        //
        //   Returns an empty object.

    }, {
        key: 'deleteStatus',
        value: function () {
            var _ref43 = _asyncToGenerator(regeneratorRuntime.mark(function _callee38(id) {
                var url, method;
                return regeneratorRuntime.wrap(function _callee38$(_context38) {
                    while (1) {
                        switch (_context38.prev = _context38.next) {
                            case 0:
                                url = this._getApi('statuses', id);
                                method = 'DELETE';
                                _context38.next = 4;
                                return this._authFetch(url, { method: method });

                            case 4:
                                return _context38.abrupt('return', _context38.sent);

                            case 5:
                            case 'end':
                                return _context38.stop();
                        }
                    }
                }, _callee38, this);
            }));

            function deleteStatus(_x32) {
                return _ref43.apply(this, arguments);
            }

            return deleteStatus;
        }()
        //   Reblogging a statuses:
        //
        //   POST /api/v1/statuses/:id/reblog
        //
        //   Returns the target Status.

    }, {
        key: 'reblogStatus',
        value: function () {
            var _ref44 = _asyncToGenerator(regeneratorRuntime.mark(function _callee39(id) {
                var url, method, response, status;
                return regeneratorRuntime.wrap(function _callee39$(_context39) {
                    while (1) {
                        switch (_context39.prev = _context39.next) {
                            case 0:
                                url = this._getApi('statuses', id, 'reblog');
                                method = 'POST';
                                _context39.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context39.sent;

                                if (!response.isFailure()) {
                                    _context39.next = 7;
                                    break;
                                }

                                return _context39.abrupt('return', response);

                            case 7:
                                status = new _status2.default(response.value.json);
                                return _context39.abrupt('return', response.copy({ status: status }));

                            case 9:
                            case 'end':
                                return _context39.stop();
                        }
                    }
                }, _callee39, this);
            }));

            function reblogStatus(_x33) {
                return _ref44.apply(this, arguments);
            }

            return reblogStatus;
        }()
        //   Unreblogging a statuses:
        //
        //   POST /api/v1/statuses/:id/unreblog
        //
        //   Returns the target Status.

    }, {
        key: 'unreblogStatus',
        value: function () {
            var _ref45 = _asyncToGenerator(regeneratorRuntime.mark(function _callee40(id) {
                var url, method, response, status;
                return regeneratorRuntime.wrap(function _callee40$(_context40) {
                    while (1) {
                        switch (_context40.prev = _context40.next) {
                            case 0:
                                url = this._getApi('statuses', id, 'unreblog');
                                method = 'POST';
                                _context40.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context40.sent;

                                if (!response.isFailure()) {
                                    _context40.next = 7;
                                    break;
                                }

                                return _context40.abrupt('return', response);

                            case 7:
                                status = new _status2.default(response.value.json);
                                return _context40.abrupt('return', response.copy({ status: status }));

                            case 9:
                            case 'end':
                                return _context40.stop();
                        }
                    }
                }, _callee40, this);
            }));

            function unreblogStatus(_x34) {
                return _ref45.apply(this, arguments);
            }

            return unreblogStatus;
        }()
        //   Favouriting a statuses:
        //
        //   POST /api/v1/statuses/:id/favourite
        //
        //   Returns the target Status.

    }, {
        key: 'favoriteStatus',
        value: function () {
            var _ref46 = _asyncToGenerator(regeneratorRuntime.mark(function _callee41(id) {
                var url, method, response, status;
                return regeneratorRuntime.wrap(function _callee41$(_context41) {
                    while (1) {
                        switch (_context41.prev = _context41.next) {
                            case 0:
                                url = this._getApi('statuses', id, 'favourite');
                                method = 'POST';
                                _context41.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context41.sent;

                                if (!response.isFailure()) {
                                    _context41.next = 7;
                                    break;
                                }

                                return _context41.abrupt('return', response);

                            case 7:
                                status = new _status2.default(response.value.json);
                                return _context41.abrupt('return', response.copy({ status: status }));

                            case 9:
                            case 'end':
                                return _context41.stop();
                        }
                    }
                }, _callee41, this);
            }));

            function favoriteStatus(_x35) {
                return _ref46.apply(this, arguments);
            }

            return favoriteStatus;
        }()
        //   Unfavouriting a statuses:
        //
        //   POST /api/v1/statuses/:id/unfavourite
        //
        //   Returns the target Status.

    }, {
        key: 'unfavoriteStatus',
        value: function () {
            var _ref47 = _asyncToGenerator(regeneratorRuntime.mark(function _callee42(id) {
                var url, method, response, status;
                return regeneratorRuntime.wrap(function _callee42$(_context42) {
                    while (1) {
                        switch (_context42.prev = _context42.next) {
                            case 0:
                                url = this._getApi('statuses', id, 'unfavourite');
                                method = 'POST';
                                _context42.next = 4;
                                return this._authFetchJson(url, { method: method });

                            case 4:
                                response = _context42.sent;

                                if (!response.isFailure()) {
                                    _context42.next = 7;
                                    break;
                                }

                                return _context42.abrupt('return', response);

                            case 7:
                                status = new _status2.default(response.value.json);
                                return _context42.abrupt('return', response.copy({ status: status }));

                            case 9:
                            case 'end':
                                return _context42.stop();
                        }
                    }
                }, _callee42, this);
            }));

            function unfavoriteStatus(_x36) {
                return _ref47.apply(this, arguments);
            }

            return unfavoriteStatus;
        }()
        /*
         * Timelines
         */
        //   Retrieving a home timeline:
        //
        //   GET /api/v1/timelines/home

    }, {
        key: 'getHomeTimeLine',
        value: function () {
            var _ref48 = _asyncToGenerator(regeneratorRuntime.mark(function _callee43() {
                var url, response, statuses, link;
                return regeneratorRuntime.wrap(function _callee43$(_context43) {
                    while (1) {
                        switch (_context43.prev = _context43.next) {
                            case 0:
                                url = this._getApi('timelines', null, 'home');
                                _context43.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context43.sent;

                                if (!response.isFailure()) {
                                    _context43.next = 6;
                                    break;
                                }

                                return _context43.abrupt('return', response);

                            case 6:
                                statuses = response.value.json.map(function (v) {
                                    return new _status2.default(v);
                                });
                                link = response.link(_link2.default.Type.statuses);
                                return _context43.abrupt('return', response.copy({ link: link, statuses: statuses }));

                            case 9:
                            case 'end':
                                return _context43.stop();
                        }
                    }
                }, _callee43, this);
            }));

            function getHomeTimeLine() {
                return _ref48.apply(this, arguments);
            }

            return getHomeTimeLine;
        }()
        //   Retrieving a public timeline:
        //
        //   GET /api/v1/timelines/public
        //
        //   Query parameters:
        //   local: Only return statuses originating from this instance

    }, {
        key: 'getTimeLine',
        value: function () {
            var _ref49 = _asyncToGenerator(regeneratorRuntime.mark(function _callee44() {
                var local = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
                var url, response, statuses, link;
                return regeneratorRuntime.wrap(function _callee44$(_context44) {
                    while (1) {
                        switch (_context44.prev = _context44.next) {
                            case 0:
                                url = this._getApi('timelines', null, 'public', { local: local });
                                _context44.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context44.sent;

                                if (!response.isFailure()) {
                                    _context44.next = 6;
                                    break;
                                }

                                return _context44.abrupt('return', response);

                            case 6:
                                statuses = response.value.json.map(function (v) {
                                    return new _status2.default(v);
                                });
                                link = response.link(_link2.default.Type.statuses);
                                return _context44.abrupt('return', response.copy({ link: link, statuses: statuses }));

                            case 9:
                            case 'end':
                                return _context44.stop();
                        }
                    }
                }, _callee44, this);
            }));

            function getTimeLine() {
                return _ref49.apply(this, arguments);
            }

            return getTimeLine;
        }()
        //   Retrieving a hashtag timeline:
        //
        //   GET /api/v1/timelines/tag/:hashtag
        //
        //   Query parameters:
        //   local: Only return statuses originating from this instance

    }, {
        key: 'getHashTagTimeLine',
        value: function () {
            var _ref50 = _asyncToGenerator(regeneratorRuntime.mark(function _callee45(hashtag) {
                var local = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
                var url, response, statuses, link;
                return regeneratorRuntime.wrap(function _callee45$(_context45) {
                    while (1) {
                        switch (_context45.prev = _context45.next) {
                            case 0:
                                url = this._getApi('timelines', 'tag', hashtag, { local: local });
                                _context45.next = 3;
                                return this._authFetchJson(url);

                            case 3:
                                response = _context45.sent;

                                if (!response.isFailure()) {
                                    _context45.next = 6;
                                    break;
                                }

                                return _context45.abrupt('return', response);

                            case 6:
                                statuses = response.value.json.map(function (v) {
                                    return new _status2.default(v);
                                });
                                link = response.link(_link2.default.Type.statuses);
                                return _context45.abrupt('return', response.copy({ link: link, statuses: statuses }));

                            case 9:
                            case 'end':
                                return _context45.stop();
                        }
                    }
                }, _callee45, this);
            }));

            function getHashTagTimeLine(_x38) {
                return _ref50.apply(this, arguments);
            }

            return getHashTagTimeLine;
        }()
        /*
        * Link
        */

    }, {
        key: 'getNext',
        value: function () {
            var _ref51 = _asyncToGenerator(regeneratorRuntime.mark(function _callee46(link) {
                return regeneratorRuntime.wrap(function _callee46$(_context46) {
                    while (1) {
                        switch (_context46.prev = _context46.next) {
                            case 0:
                                if (!(!link || !link.next || !link.next.url)) {
                                    _context46.next = 2;
                                    break;
                                }

                                return _context46.abrupt('return', _response2.default.create(_response2.default.Type.nullLink));

                            case 2:
                                _context46.next = 4;
                                return this._loadLinkUrl(link.next.url, link);

                            case 4:
                                return _context46.abrupt('return', _context46.sent);

                            case 5:
                            case 'end':
                                return _context46.stop();
                        }
                    }
                }, _callee46, this);
            }));

            function getNext(_x40) {
                return _ref51.apply(this, arguments);
            }

            return getNext;
        }()
    }, {
        key: 'getPrev',
        value: function () {
            var _ref52 = _asyncToGenerator(regeneratorRuntime.mark(function _callee47(link) {
                return regeneratorRuntime.wrap(function _callee47$(_context47) {
                    while (1) {
                        switch (_context47.prev = _context47.next) {
                            case 0:
                                if (!(!link || !link.prev || !link.prev.url)) {
                                    _context47.next = 2;
                                    break;
                                }

                                return _context47.abrupt('return', _response2.default.create(_response2.default.Type.nullLink));

                            case 2:
                                _context47.next = 4;
                                return this._loadLinkUrl(link.prev.url, link);

                            case 4:
                                return _context47.abrupt('return', _context47.sent);

                            case 5:
                            case 'end':
                                return _context47.stop();
                        }
                    }
                }, _callee47, this);
            }));

            function getPrev(_x41) {
                return _ref52.apply(this, arguments);
            }

            return getPrev;
        }()
        /*
         * Stream
         */

    }, {
        key: 'createUserStream',
        value: function createUserStream(handler) {
            var stream = 'user';
            return this.createStream({ stream: stream }, handler);
        }
    }, {
        key: 'createPublicStream',
        value: function createPublicStream(local, handler) {
            var stream = local ? 'public:local' : 'public';
            return this.createStream({ stream: stream }, handler);
        }
    }, {
        key: 'createHashTagStream',
        value: function createHashTagStream(tag, local, handler) {
            var stream = local ? 'hashtag:local' : 'public';
            return this.createStream({ stream: stream, tag: tag }, handler);
        }
    }, {
        key: 'createStream',
        value: function createStream(target, handler) {
            target.access_token = this.credential.accessToken;
            var socket = new WebSocket(this._getStream(target));
            socket.onopen = function () {
                return handler('open');
            };
            socket.onclose = function () {
                return handler('close');
            };
            socket.onmessage = function (msg) {
                var data = JSON.parse(msg.data);
                if (!data.payload) return;
                switch (data.event) {
                    case 'update':
                        {
                            var status = new _status2.default(JSON.parse(data.payload));
                            handler('message', 'update', status);
                            break;
                        }
                    case 'notification':
                        {
                            var notification = new _notification2.default(JSON.parse(data.payload));
                            handler('message', 'notification', notification);
                            break;
                        }
                    case 'delete':
                        {
                            var id = data.payload;
                            handler('message', 'delete', id);
                            break;
                        }
                    default:
                        break;
                }
            };
            return socket;
        }
        /*
         * util
         */

    }, {
        key: '_getHTTPS',
        value: function _getHTTPS() {
            var protocol = this.useSecureProtocol ? 'https' : 'http';
            return protocol + '://' + this.url.api;
        }
    }, {
        key: '_getWWS',
        value: function _getWWS() {
            var protocol = this.useSecureProtocol ? 'wss' : 'ws';
            return protocol + '://' + this.url.stream;
        }
    }, {
        key: '_getApi',
        value: function _getApi(resource) {
            var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var action = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var params = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

            var r = resource !== null ? '/' + resource : '';
            var i = id !== null ? '/' + id : '';
            var a = action !== null ? '/' + action : '';
            var url = this._getHTTPS() + '/api/v1' + r + i + a;
            if (!params) return url;
            return url + '?' + this._queryParams(params);
        }
    }, {
        key: '_getStream',
        value: function _getStream() {
            var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            var url = this._getWWS() + '/api/v1/streaming';
            if (!params) return url;
            return url + '?' + this._queryParams(params);
        }
    }, {
        key: '_queryParams',
        value: function _queryParams(params) {
            if (!params) return '';
            var urlParams = new URLSearchParams();
            var _iteratorNormalCompletion3 = true;
            var _didIteratorError3 = false;
            var _iteratorError3 = undefined;

            try {
                for (var _iterator3 = Object.entries(params)[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                    var _step3$value = _slicedToArray(_step3.value, 2),
                        key = _step3$value[0],
                        value = _step3$value[1];

                    if (!value) continue;
                    if (Array.isArray(value)) {
                        var k = key + '[]';
                        var _iteratorNormalCompletion4 = true;
                        var _didIteratorError4 = false;
                        var _iteratorError4 = undefined;

                        try {
                            for (var _iterator4 = value[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                                var v = _step4.value;

                                urlParams.append(k, v);
                            }
                        } catch (err) {
                            _didIteratorError4 = true;
                            _iteratorError4 = err;
                        } finally {
                            try {
                                if (!_iteratorNormalCompletion4 && _iterator4.return) {
                                    _iterator4.return();
                                }
                            } finally {
                                if (_didIteratorError4) {
                                    throw _iteratorError4;
                                }
                            }
                        }
                    } else {
                        urlParams.set(key, value);
                    }
                }
            } catch (err) {
                _didIteratorError3 = true;
                _iteratorError3 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion3 && _iterator3.return) {
                        _iterator3.return();
                    }
                } finally {
                    if (_didIteratorError3) {
                        throw _iteratorError3;
                    }
                }
            }

            return urlParams.toString();
        }
    }, {
        key: '_fetch',
        value: function () {
            var _ref53 = _asyncToGenerator(regeneratorRuntime.mark(function _callee48(url) {
                var _ref54 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
                    _ref54$body = _ref54.body,
                    body = _ref54$body === undefined ? null : _ref54$body,
                    _ref54$method = _ref54.method,
                    method = _ref54$method === undefined ? 'GET' : _ref54$method,
                    _ref54$mode = _ref54.mode,
                    mode = _ref54$mode === undefined ? 'CORS' : _ref54$mode,
                    _ref54$headers = _ref54.headers,
                    headers = _ref54$headers === undefined ? {} : _ref54$headers;

                var response, info;
                return regeneratorRuntime.wrap(function _callee48$(_context48) {
                    while (1) {
                        switch (_context48.prev = _context48.next) {
                            case 0:
                                _context48.prev = 0;
                                _context48.next = 3;
                                return fetch(url, {
                                    body: body,
                                    method: method,
                                    mode: mode,
                                    headers: headers
                                });

                            case 3:
                                response = _context48.sent;

                                if (response.ok) {
                                    _context48.next = 7;
                                    break;
                                }

                                info = { status: response.status, statusText: response.statusText, response: response };
                                return _context48.abrupt('return', _response2.default.create(_response2.default.Type.invalidStatus, info));

                            case 7:
                                return _context48.abrupt('return', _response2.default.success({ response: response }));

                            case 10:
                                _context48.prev = 10;
                                _context48.t0 = _context48['catch'](0);
                                return _context48.abrupt('return', _response2.default.create(_response2.default.Type.networkError, _context48.t0));

                            case 13:
                            case 'end':
                                return _context48.stop();
                        }
                    }
                }, _callee48, this, [[0, 10]]);
            }));

            function _fetch(_x46) {
                return _ref53.apply(this, arguments);
            }

            return _fetch;
        }()
    }, {
        key: '_fetchJson',
        value: function () {
            var _ref55 = _asyncToGenerator(regeneratorRuntime.mark(function _callee49(url) {
                var _ref56 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
                    _ref56$body = _ref56.body,
                    body = _ref56$body === undefined ? null : _ref56$body,
                    _ref56$method = _ref56.method,
                    method = _ref56$method === undefined ? 'GET' : _ref56$method,
                    _ref56$mode = _ref56.mode,
                    mode = _ref56$mode === undefined ? 'CORS' : _ref56$mode,
                    _ref56$headers = _ref56.headers,
                    headers = _ref56$headers === undefined ? {} : _ref56$headers;

                var h, response, json, result;
                return regeneratorRuntime.wrap(function _callee49$(_context49) {
                    while (1) {
                        switch (_context49.prev = _context49.next) {
                            case 0:
                                h = {};

                                Object.assign(h, headers);
                                _context49.prev = 2;
                                _context49.next = 5;
                                return this._fetch(url, { body: body, method: method, mode: mode, headers: h });

                            case 5:
                                response = _context49.sent;

                                if (!response.isFailure()) {
                                    _context49.next = 8;
                                    break;
                                }

                                return _context49.abrupt('return', response);

                            case 8:
                                _context49.next = 10;
                                return response.value.response.json();

                            case 10:
                                json = _context49.sent;
                                result = _response2.default.success({ json: json });

                                result.headers = response.value.response.headers;
                                return _context49.abrupt('return', result);

                            case 16:
                                _context49.prev = 16;
                                _context49.t0 = _context49['catch'](2);
                                return _context49.abrupt('return', _response2.default.create(_response2.default.Type.networkError, _context49.t0));

                            case 19:
                            case 'end':
                                return _context49.stop();
                        }
                    }
                }, _callee49, this, [[2, 16]]);
            }));

            function _fetchJson(_x48) {
                return _ref55.apply(this, arguments);
            }

            return _fetchJson;
        }()
    }, {
        key: '_authFetch',
        value: function () {
            var _ref57 = _asyncToGenerator(regeneratorRuntime.mark(function _callee50(url) {
                var _ref58 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
                    _ref58$body = _ref58.body,
                    body = _ref58$body === undefined ? null : _ref58$body,
                    _ref58$method = _ref58.method,
                    method = _ref58$method === undefined ? 'GET' : _ref58$method,
                    _ref58$mode = _ref58.mode,
                    mode = _ref58$mode === undefined ? 'CORS' : _ref58$mode,
                    _ref58$headers = _ref58.headers,
                    headers = _ref58$headers === undefined ? {} : _ref58$headers;

                return regeneratorRuntime.wrap(function _callee50$(_context50) {
                    while (1) {
                        switch (_context50.prev = _context50.next) {
                            case 0:
                                _context50.next = 2;
                                return this._fetch(url, {
                                    body: body,
                                    method: method,
                                    mode: mode,
                                    headers: Object.assign({ Authorization: 'Bearer ' + this.credential.accessToken }, headers)
                                });

                            case 2:
                                return _context50.abrupt('return', _context50.sent);

                            case 3:
                            case 'end':
                                return _context50.stop();
                        }
                    }
                }, _callee50, this);
            }));

            function _authFetch(_x50) {
                return _ref57.apply(this, arguments);
            }

            return _authFetch;
        }()
    }, {
        key: '_authFetchJson',
        value: function () {
            var _ref59 = _asyncToGenerator(regeneratorRuntime.mark(function _callee51(url) {
                var _ref60 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
                    _ref60$body = _ref60.body,
                    body = _ref60$body === undefined ? null : _ref60$body,
                    _ref60$method = _ref60.method,
                    method = _ref60$method === undefined ? 'GET' : _ref60$method,
                    _ref60$mode = _ref60.mode,
                    mode = _ref60$mode === undefined ? 'CORS' : _ref60$mode,
                    _ref60$headers = _ref60.headers,
                    headers = _ref60$headers === undefined ? {} : _ref60$headers;

                return regeneratorRuntime.wrap(function _callee51$(_context51) {
                    while (1) {
                        switch (_context51.prev = _context51.next) {
                            case 0:
                                _context51.next = 2;
                                return this._fetchJson(url, {
                                    body: body,
                                    method: method,
                                    mode: mode,
                                    headers: Object.assign({ Authorization: 'Bearer ' + this.credential.accessToken }, headers)
                                });

                            case 2:
                                return _context51.abrupt('return', _context51.sent);

                            case 3:
                            case 'end':
                                return _context51.stop();
                        }
                    }
                }, _callee51, this);
            }));

            function _authFetchJson(_x52) {
                return _ref59.apply(this, arguments);
            }

            return _authFetchJson;
        }()
    }, {
        key: '_loadLinkUrl',
        value: function () {
            var _ref61 = _asyncToGenerator(regeneratorRuntime.mark(function _callee52(url, link) {
                var response, json, newLink, statuses, _statuses, notifications;

                return regeneratorRuntime.wrap(function _callee52$(_context52) {
                    while (1) {
                        switch (_context52.prev = _context52.next) {
                            case 0:
                                _context52.next = 2;
                                return this._authFetchJson(url);

                            case 2:
                                response = _context52.sent;

                                if (!response.isFailure()) {
                                    _context52.next = 5;
                                    break;
                                }

                                return _context52.abrupt('return', response);

                            case 5:
                                json = response.value.json;
                                newLink = response.link(link.type);
                                _context52.t0 = link.type;
                                _context52.next = _context52.t0 === _link2.default.Type.statuses ? 10 : _context52.t0 === _link2.default.Type.accounts ? 12 : _context52.t0 === _link2.default.Type.notifications ? 14 : 16;
                                break;

                            case 10:
                                statuses = json.map(function (j) {
                                    return new _status2.default(j);
                                });
                                return _context52.abrupt('return', response.copy({ link: newLink || link, statuses: statuses }));

                            case 12:
                                _statuses = json.map(function (j) {
                                    return new _account2.default(j);
                                });
                                return _context52.abrupt('return', response.copy({ link: newLink || link, statuses: _statuses }));

                            case 14:
                                notifications = json.map(function (j) {
                                    return new _notification2.default(j);
                                });
                                return _context52.abrupt('return', response.copy({ link: newLink || link, notifications: notifications }));

                            case 16:
                                throw new Error('not impl. link');

                            case 17:
                            case 'end':
                                return _context52.stop();
                        }
                    }
                }, _callee52, this);
            }));

            function _loadLinkUrl(_x54, _x55) {
                return _ref61.apply(this, arguments);
            }

            return _loadLinkUrl;
        }()
    }]);

    return Mastodon;
}();

exports.default = Mastodon;
//# sourceMappingURL=client.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNsaWVudC5qcyJdLCJuYW1lcyI6WyJNYXN0b2RvbiIsImNhbGxiYWNrIiwibmFtZSIsInNpdGUiLCJzY29wZXMiLCJ1cmwiLCJiYXNlIiwiYXBpIiwic3RyZWFtIiwiY3JlZGVudGlhbCIsImNsaWVudElEIiwiY2xpZW50U2VjcmV0IiwiYWNjZXNzVG9rZW4iLCJyZWZyZXNoVG9rZW4iLCJ1c2VTZWN1cmVQcm90b2NvbCIsImNyZWRlbnRpYWxzIiwiX2dldEFwaSIsIm1ldGhvZCIsImJvZHkiLCJVUkxTZWFyY2hQYXJhbXMiLCJzZXQiLCJqb2luIiwiX2ZldGNoSnNvbiIsInJlc3BvbnNlIiwiaXNTdWNjZXNzIiwidmFsdWUiLCJqc29uIiwiY2xpZW50X2lkIiwiY2xpZW50X3NlY3JldCIsInNldENyZWRlbnRpYWxzIiwiZXhwb3J0Q29uZmlnIiwib2F1dGgiLCJfZ2V0SFRUUFMiLCJnZXRBdXRob3JpemVVcmwiLCJyZWRpcmVjdF91cmkiLCJyZXNwb25zZV90eXBlIiwic2NvcGUiLCJjb2RlIiwiYXV0aCIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwiZ2V0T0F1dGhBY2Nlc3NUb2tlbiIsImdyYW50X3R5cGUiLCJlcnIiLCJzdWNjZXNzIiwiY3JlYXRlIiwiVHlwZSIsIm5ldHdvcmtFcnJvciIsImlkIiwiX2F1dGhGZXRjaEpzb24iLCJpc0ZhaWx1cmUiLCJhY2NvdW50IiwiY29weSIsImRpc3BsYXlOYW1lIiwibm90ZSIsImF2YXRhciIsImhlYWRlciIsImFjY291bnRzIiwibWFwIiwidiIsImxpbmsiLCJwYXJhbXMiLCJvbmx5TWVkaWEiLCJleGNsdWRlUmVwbGllcyIsIm9ubHlfbWVkaWEiLCJleGNsdWRlX3JlcGxpZXMiLCJzdGF0dXNlcyIsInJlbGF0aW9uc2hpcCIsImlkcyIsInRhcmdldHMiLCJBcnJheSIsImlzQXJyYXkiLCJyZWxhdGlvbnNoaXBzIiwicXVlcnkiLCJsaW1pdCIsInEiLCJGb3JtRGF0YSIsImFwcGVuZCIsIlN0cmluZyIsIl9hdXRoRmV0Y2giLCJ1cmkiLCJpbnN0YW5jZSIsImZpbGUiLCJhdHRhY2htZW50Iiwibm90aWZpY2F0aW9ucyIsIm5vdGlmaWNhdGlvbiIsInJlcG9ydHMiLCJqIiwiYWNjb3VudElkIiwic3RhdHVzSWRzIiwiY29tbWVudCIsInJlcG9ydCIsInJlc29sdmVSZW1vdGVBY2NvdW50cyIsInJlc3VsdCIsInN0YXR1cyIsImNvbnRleHQiLCJjYXJkIiwidGV4dCIsInJlcGx5VG8iLCJtZWRpYUlkcyIsInNlbnNpdGl2ZSIsInNwb2lsZXJUZXh0IiwidmlzaWJpbGl0eSIsImxvY2FsIiwiaGFzaHRhZyIsIm5leHQiLCJudWxsTGluayIsIl9sb2FkTGlua1VybCIsInByZXYiLCJoYW5kbGVyIiwiY3JlYXRlU3RyZWFtIiwidGFnIiwidGFyZ2V0IiwiYWNjZXNzX3Rva2VuIiwic29ja2V0IiwiV2ViU29ja2V0IiwiX2dldFN0cmVhbSIsIm9ub3BlbiIsIm9uY2xvc2UiLCJvbm1lc3NhZ2UiLCJkYXRhIiwiSlNPTiIsInBhcnNlIiwibXNnIiwicGF5bG9hZCIsImV2ZW50IiwicHJvdG9jb2wiLCJyZXNvdXJjZSIsImFjdGlvbiIsInIiLCJpIiwiYSIsIl9xdWVyeVBhcmFtcyIsIl9nZXRXV1MiLCJ1cmxQYXJhbXMiLCJPYmplY3QiLCJlbnRyaWVzIiwia2V5IiwiayIsInRvU3RyaW5nIiwibW9kZSIsImhlYWRlcnMiLCJmZXRjaCIsIm9rIiwiaW5mbyIsInN0YXR1c1RleHQiLCJpbnZhbGlkU3RhdHVzIiwiaCIsImFzc2lnbiIsIl9mZXRjaCIsIkF1dGhvcml6YXRpb24iLCJuZXdMaW5rIiwidHlwZSIsIkVycm9yIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztxakJBQUE7Ozs7O0FBR0E7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7Ozs7O0lBQ01BLFE7QUFDRjs7O0FBR0Esd0JBQWM7QUFBQTs7QUFDVixhQUFLQyxRQUFMLEdBQWdCLDJCQUFoQjtBQUNBO0FBQ0EsYUFBS0MsSUFBTCxHQUFZLGNBQVo7QUFDQSxhQUFLQyxJQUFMLEdBQVksSUFBWjtBQUNBLGFBQUtDLE1BQUwsR0FBYyxDQUFDLE1BQUQsRUFBUyxPQUFULEVBQWtCLFFBQWxCLENBQWQ7QUFDQTtBQUNBLGFBQUtDLEdBQUwsR0FBVztBQUNQQyxrQkFBTSxJQURDO0FBRVBDLGlCQUFLLElBRkU7QUFHUEMsb0JBQVE7QUFIRCxTQUFYO0FBS0E7QUFDQSxhQUFLQyxVQUFMLEdBQWtCO0FBQ2RDLHNCQUFVLElBREk7QUFFZEMsMEJBQWMsSUFGQTtBQUdkQyx5QkFBYSxJQUhDO0FBSWRDLDBCQUFjO0FBSkEsU0FBbEI7QUFNQTtBQUNBLGFBQUtDLGlCQUFMLEdBQXlCLElBQXpCO0FBQ0g7QUFDRDs7Ozs7Ozs7c0NBSWNaLEksRUFBTTtBQUNoQixpQkFBS0EsSUFBTCxHQUFZQSxJQUFaO0FBQ0g7QUFDRDs7Ozs7Ozt5Q0FJaUJDLEksRUFBTTtBQUNuQixpQkFBS0EsSUFBTCxHQUFZQSxJQUFaO0FBQ0g7QUFDRDs7Ozs7Ozt1Q0FJZVksVyxFQUFhO0FBQ3hCLGlCQUFLTixVQUFMLEdBQWtCO0FBQ2RDLDBCQUFVSyxZQUFZTCxRQURSO0FBRWRDLDhCQUFjSSxZQUFZSixZQUZaO0FBR2RDLDZCQUFhRyxZQUFZSCxXQUhYO0FBSWRDLDhCQUFjRSxZQUFZRjtBQUpaLGFBQWxCO0FBTUg7QUFDRDs7Ozs7OztrQ0FJVVQsTSxFQUFRO0FBQ2QsaUJBQUtBLE1BQUwsR0FBY0EsTUFBZDtBQUNIO0FBQ0Q7Ozs7Ozs7OztzQ0FNK0I7QUFBQSxnQkFBckJFLElBQXFCLFFBQXJCQSxJQUFxQjtBQUFBLGdCQUFmQyxHQUFlLFFBQWZBLEdBQWU7QUFBQSxnQkFBVkMsTUFBVSxRQUFWQSxNQUFVOztBQUMzQixpQkFBS0gsR0FBTCxHQUFXO0FBQ1BDLHNCQUFNQSxJQURDO0FBRVBDLHFCQUFLQSxPQUFPRCxJQUZMO0FBR1BFLHdCQUFRQSxVQUFVRjtBQUhYLGFBQVg7QUFLSDtBQUNEOzs7Ozs7O3VDQUllO0FBQ1gsbUJBQU87QUFDSEosc0JBQU0sS0FBS0EsSUFEUjtBQUVIQyxzQkFBTSxLQUFLQSxJQUZSO0FBR0hDLHdCQUFRLEtBQUtBLE1BSFY7QUFJSEMscUJBQUs7QUFDREMsMEJBQU0sS0FBS0QsR0FBTCxDQUFTQyxJQURkO0FBRURDLHlCQUFLLEtBQUtGLEdBQUwsQ0FBU0UsR0FGYjtBQUdEQyw0QkFBUSxLQUFLSCxHQUFMLENBQVNHO0FBSGhCLGlCQUpGO0FBU0hDLDRCQUFZO0FBQ1JDLDhCQUFVLEtBQUtELFVBQUwsQ0FBZ0JDLFFBRGxCO0FBRVJDLGtDQUFjLEtBQUtGLFVBQUwsQ0FBZ0JFLFlBRnRCO0FBR1JDLGlDQUFhLEtBQUtILFVBQUwsQ0FBZ0JHLFdBSHJCO0FBSVJDLGtDQUFjLEtBQUtKLFVBQUwsQ0FBZ0JJO0FBSnRCO0FBVFQsYUFBUDtBQWdCSDtBQUNEOzs7QUFHQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTJCVU4sbUMsR0FBTSxLQUFLUyxPQUFMLENBQWEsTUFBYixDO0FBQ05DLHNDLEdBQVMsTTtBQUNUQyxvQyxHQUFPLElBQUlDLGVBQUosRTs7QUFDYkQscUNBQUtFLEdBQUwsQ0FBUyxhQUFULEVBQXdCLEtBQUtsQixJQUE3QjtBQUNBZ0IscUNBQUtFLEdBQUwsQ0FBUyxlQUFULEVBQTBCLEtBQUtuQixRQUEvQjtBQUNBaUIscUNBQUtFLEdBQUwsQ0FBUyxRQUFULEVBQW1CLEtBQUtoQixNQUFMLENBQVlpQixJQUFaLENBQWlCLEdBQWpCLENBQW5COzt1Q0FDdUIsS0FBS0MsVUFBTCxDQUFnQmYsR0FBaEIsRUFBcUIsRUFBRVUsY0FBRixFQUFVQyxVQUFWLEVBQXJCLEM7OztBQUFqQkssd0M7O0FBQ04sb0NBQUlBLFNBQVNDLFNBQVQsRUFBSixFQUEwQjtBQUFBLDJEQUNlRCxTQUFTRSxLQUFULENBQWVDLElBRDlCLEVBQ2RDLFNBRGMsd0JBQ2RBLFNBRGMsRUFDSEMsYUFERyx3QkFDSEEsYUFERzs7QUFFdEIseUNBQUtDLGNBQUwsQ0FBb0I7QUFDaEJuQixrREFBVWlCLFNBRE07QUFFaEJoQixzREFBY2lCO0FBRkUscUNBQXBCO0FBSUg7aUVBQ01MLFE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDs7Ozs7OzswQ0FJa0I7QUFBQSx3Q0FDcUIsS0FBS08sWUFBTCxHQUFvQnJCLFVBRHpDO0FBQUEsZ0JBQ05DLFFBRE0seUJBQ05BLFFBRE07QUFBQSxnQkFDSUMsWUFESix5QkFDSUEsWUFESjs7QUFFZCxnQkFBTW9CLFFBQVEsa0JBQVdyQixRQUFYLEVBQXFCQyxZQUFyQixFQUFtQyxLQUFLcUIsU0FBTCxFQUFuQyxFQUFxRCxJQUFyRCxFQUEyRCxjQUEzRCxDQUFkO0FBQ0EsbUJBQU9ELE1BQU1FLGVBQU4sQ0FBc0I7QUFDekJDLDhCQUFjLEtBQUtqQyxRQURNO0FBRXpCa0MsK0JBQWUsTUFGVTtBQUd6QkMsdUJBQU8sS0FBS2hDLE1BQUwsQ0FBWWlCLElBQVosQ0FBaUIsR0FBakI7QUFIa0IsYUFBdEIsQ0FBUDtBQUtIO0FBQ0Q7Ozs7Ozs7OztvRkFLbUJnQixJOzs7Ozs7Ozs7eURBQ29CLEtBQUtQLFlBQUwsR0FBb0JyQixVLEVBQS9DQyxRLDBCQUFBQSxRLEVBQVVDLFksMEJBQUFBLFk7QUFDWm9CLHFDLEdBQVEsa0JBQVdyQixRQUFYLEVBQXFCQyxZQUFyQixFQUFtQyxLQUFLcUIsU0FBTCxFQUFuQyxFQUFxRCxJQUFyRCxFQUEyRCxjQUEzRCxDO0FBQ1JNLG9DLEdBQU8sSUFBSUMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUMxQ1YsMENBQU1XLG1CQUFOLENBQTBCTCxJQUExQixFQUFnQztBQUM1Qk0sb0RBQVksb0JBRGdCO0FBRTVCVCxzREFBYyxNQUFLakM7QUFGUyxxQ0FBaEMsRUFHRyxVQUFDMkMsR0FBRCxFQUFNaEMsV0FBTixFQUFtQkMsWUFBbkIsRUFBaUNVLFFBQWpDLEVBQThDO0FBQzdDLDRDQUFJcUIsR0FBSixFQUFTO0FBQ0xILG1EQUFPRyxHQUFQO0FBQ0gseUNBRkQsTUFHSztBQUNESixvREFBUSxFQUFFNUIsd0JBQUYsRUFBZUMsMEJBQWYsRUFBNkJVLGtCQUE3QixFQUFSO0FBQ0g7QUFDSixxQ0FWRDtBQVdILGlDQVpZLEM7Ozt1Q0FjNkNlLEk7Ozs7QUFBOUMxQiwyQyxTQUFBQSxXO0FBQWFDLDRDLFNBQUFBLFk7QUFBY1Usd0MsU0FBQUEsUTs7QUFDbkMscUNBQUtNLGNBQUwsQ0FBb0IsRUFBRW5CLGtCQUFGLEVBQVlDLDBCQUFaLEVBQTBCQyx3QkFBMUIsRUFBdUNDLDBCQUF2QyxFQUFwQjtrRUFDTyxtQkFBRWdDLE9BQUYsQ0FBVSxFQUFFakMsd0JBQUYsRUFBZUMsMEJBQWYsRUFBNkJVLGtCQUE3QixFQUFWLEM7Ozs7O2tFQUdBLG1CQUFFdUIsTUFBRixDQUFTLG1CQUFFQyxJQUFGLENBQU9DLFlBQWhCLGU7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHZjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7b0ZBQ2lCQyxFOzs7Ozs7QUFDUDVDLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUJpQyxFQUF6QixDOzt1Q0FDVyxLQUFLQyxjQUFMLENBQW9CN0MsR0FBcEIsQzs7O0FBQWpCa0Isd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OztrRUFDTzVCLFE7OztBQUNMNkIsdUMsR0FBVSxzQkFBWTdCLFNBQVNFLEtBQVQsQ0FBZUMsSUFBM0IsQztrRUFDVEgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFRCxnQkFBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQUVVL0MsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsVUFBYixFQUF5QixJQUF6QixFQUErQixvQkFBL0IsQzs7dUNBQ1csS0FBS2tDLGNBQUwsQ0FBb0I3QyxHQUFwQixDOzs7QUFBakJrQix3Qzs7cUNBQ0ZBLFNBQVM0QixTQUFULEU7Ozs7O2tFQUNPNUIsUTs7O0FBQ0w2Qix1QyxHQUFVLHNCQUFZN0IsU0FBU0UsS0FBVCxDQUFlQyxJQUEzQixDO2tFQUNUSCxTQUFTOEIsSUFBVCxDQUFjLEVBQUVELGdCQUFGLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Z0dBQytGLEU7OENBQWxFRSxXO29CQUFBQSxXLHFDQUFjLEk7dUNBQU1DLEk7b0JBQUFBLEksOEJBQU8sSTt5Q0FBTUMsTTtvQkFBQUEsTSxnQ0FBUyxJO3lDQUFNQyxNO29CQUFBQSxNLGdDQUFTLEk7Ozs7Ozs7QUFDNUVwRCxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxVQUFiLEVBQXlCLElBQXpCLEVBQStCLG9CQUEvQixDO0FBQ05DLHNDLEdBQVMsTztBQUNUQyxvQyxHQUFPLElBQUlDLGVBQUosRTs7QUFDYixvQ0FBSW1DLFdBQUosRUFDSXBDLEtBQUtFLEdBQUwsQ0FBUyxjQUFULEVBQXlCa0MsV0FBekI7QUFDSixvQ0FBSUMsSUFBSixFQUNJckMsS0FBS0UsR0FBTCxDQUFTLE1BQVQsRUFBaUJtQyxJQUFqQjtBQUNKLG9DQUFJQyxNQUFKLEVBQ0l0QyxLQUFLRSxHQUFMLENBQVMsUUFBVCxFQUFtQm9DLE1BQW5CO0FBQ0osb0NBQUlDLE1BQUosRUFDSXZDLEtBQUtFLEdBQUwsQ0FBUyxRQUFULEVBQW1CcUMsTUFBbkI7O3VDQUNtQixLQUFLUCxjQUFMLENBQW9CN0MsR0FBcEIsRUFBeUIsRUFBRVksY0FBRixFQUFVQyxVQUFWLEVBQXpCLEM7OztBQUFqQkssd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OztrRUFDTzVCLFE7OztBQUNMNkIsdUMsR0FBVSxzQkFBWTdCLFNBQVNFLEtBQVQsQ0FBZUMsSUFBM0IsQztrRUFDVEgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFRCxnQkFBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztvRkFDbUJILEU7Ozs7OztBQUNUNUMsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsVUFBYixFQUF5QmlDLEVBQXpCLEVBQTZCLFdBQTdCLEM7O3VDQUNXLEtBQUtDLGNBQUwsQ0FBb0I3QyxHQUFwQixDOzs7QUFBakJrQix3Qzs7cUNBQ0ZBLFNBQVM0QixTQUFULEU7Ozs7O2tFQUNPNUIsUTs7O0FBQ0xtQyx3QyxHQUFXbkMsU0FBU0UsS0FBVCxDQUFlQyxJQUFmLENBQW9CaUMsR0FBcEIsQ0FBd0I7QUFBQSwyQ0FBSyxzQkFBWUMsQ0FBWixDQUFMO0FBQUEsaUNBQXhCLEM7QUFDWEMsb0MsR0FBT3RDLFNBQVNzQyxJQUFULENBQWMsZUFBS2QsSUFBTCxDQUFVVyxRQUF4QixDO2tFQUNObkMsU0FBUzhCLElBQVQsQ0FBYyxFQUFFUSxVQUFGLEVBQVFILGtCQUFSLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O3FGQUNvQlQsRTs7Ozs7O0FBQ1Y1QyxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxVQUFiLEVBQXlCaUMsRUFBekIsRUFBNkIsV0FBN0IsQzs7dUNBQ1csS0FBS0MsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7a0VBQ081QixROzs7QUFDTG1DLHdDLEdBQVduQyxTQUFTRSxLQUFULENBQWVDLElBQWYsQ0FBb0JpQyxHQUFwQixDQUF3QjtBQUFBLDJDQUFLLHNCQUFZQyxDQUFaLENBQUw7QUFBQSxpQ0FBeEIsQztBQUNYQyxvQyxHQUFPdEMsU0FBU3NDLElBQVQsQ0FBYyxlQUFLZCxJQUFMLENBQVVXLFFBQXhCLEM7a0VBQ05uQyxTQUFTOEIsSUFBVCxDQUFjLEVBQUVRLFVBQUYsRUFBUUgsa0JBQVIsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O3FGQUMyQlQsRTtvQkFBSWEsTSx1RUFBUztBQUNoQ0MsK0JBQVcsS0FEcUI7QUFFaENDLG9DQUFnQjtBQUZnQixpQjs7Ozs7O0FBSTlCM0QsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsVUFBYixFQUF5QmlDLEVBQXpCLEVBQTZCLFVBQTdCLEVBQXlDO0FBQ2pEZ0IsZ0RBQVlILE9BQU9DLFNBRDhCO0FBRWpERyxxREFBaUJKLE9BQU9FO0FBRnlCLGlDQUF6QyxDOzt1Q0FJVyxLQUFLZCxjQUFMLENBQW9CN0MsR0FBcEIsQzs7O0FBQWpCa0Isd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OztrRUFDTzVCLFE7OztBQUNMNEMsd0MsR0FBVzVDLFNBQVNFLEtBQVQsQ0FBZUMsSUFBZixDQUFvQmlDLEdBQXBCLENBQXdCO0FBQUEsMkNBQUsscUJBQVdDLENBQVgsQ0FBTDtBQUFBLGlDQUF4QixDO0FBQ1hDLG9DLEdBQU90QyxTQUFTc0MsSUFBVCxDQUFjLGVBQUtkLElBQUwsQ0FBVW9CLFFBQXhCLEM7a0VBQ041QyxTQUFTOEIsSUFBVCxDQUFjLEVBQUVRLFVBQUYsRUFBUU0sa0JBQVIsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7cUZBQ29CbEIsRTs7Ozs7O0FBQ1Y1QyxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxVQUFiLEVBQXlCaUMsRUFBekIsRUFBNkIsUUFBN0IsQztBQUNOaEMsc0MsR0FBUyxNOzt1Q0FDUSxLQUFLaUMsY0FBTCxDQUFvQjdDLEdBQXBCLEVBQXlCLEVBQUVZLGNBQUYsRUFBekIsQzs7O0FBQWpCTSx3Qzs7cUNBQ0ZBLFNBQVM0QixTQUFULEU7Ozs7O2tFQUNPNUIsUTs7O0FBQ0w2Qyw0QyxHQUFlLDJCQUFpQjdDLFNBQVNFLEtBQVQsQ0FBZUMsSUFBaEMsQztrRUFDZEgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFZSwwQkFBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDc0JuQixFOzs7Ozs7QUFDWjVDLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUJpQyxFQUF6QixFQUE2QixVQUE3QixDO0FBQ05oQyxzQyxHQUFTLE07O3VDQUNRLEtBQUtpQyxjQUFMLENBQW9CN0MsR0FBcEIsRUFBeUIsRUFBRVksY0FBRixFQUF6QixDOzs7QUFBakJNLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTDZDLDRDLEdBQWUsMkJBQWlCN0MsU0FBU0UsS0FBVCxDQUFlQyxJQUFoQyxDO21FQUNkSCxTQUFTOEIsSUFBVCxDQUFjLEVBQUVlLDBCQUFGLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O3NGQUNtQm5CLEU7Ozs7OztBQUNUNUMsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsVUFBYixFQUF5QmlDLEVBQXpCLEVBQTZCLE9BQTdCLEM7QUFDTmhDLHNDLEdBQVMsTTs7dUNBQ1EsS0FBS2lDLGNBQUwsQ0FBb0I3QyxHQUFwQixFQUF5QixFQUFFWSxjQUFGLEVBQXpCLEM7OztBQUFqQk0sd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMNkMsNEMsR0FBZSwyQkFBaUI3QyxTQUFTRSxLQUFULENBQWVDLElBQWhDLEM7bUVBQ2RILFNBQVM4QixJQUFULENBQWMsRUFBRWUsMEJBQUYsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7c0ZBQ3FCbkIsRTs7Ozs7O0FBQ1g1QyxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxVQUFiLEVBQXlCaUMsRUFBekIsRUFBNkIsU0FBN0IsQztBQUNOaEMsc0MsR0FBUyxNOzt1Q0FDUSxLQUFLaUMsY0FBTCxDQUFvQjdDLEdBQXBCLEVBQXlCLEVBQUVZLGNBQUYsRUFBekIsQzs7O0FBQWpCTSx3Qzs7cUNBQ0ZBLFNBQVM0QixTQUFULEU7Ozs7O21FQUNPNUIsUTs7O0FBQ0w2Qyw0QyxHQUFlLDJCQUFpQjdDLFNBQVNFLEtBQVQsQ0FBZUMsSUFBaEMsQzttRUFDZEgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFZSwwQkFBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDa0JuQixFOzs7Ozs7QUFDUjVDLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUJpQyxFQUF6QixFQUE2QixNQUE3QixDO0FBQ05oQyxzQyxHQUFTLE07O3VDQUNRLEtBQUtpQyxjQUFMLENBQW9CN0MsR0FBcEIsRUFBeUIsRUFBRVksY0FBRixFQUF6QixDOzs7QUFBakJNLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTDZDLDRDLEdBQWUsMkJBQWlCN0MsU0FBU0UsS0FBVCxDQUFlQyxJQUFoQyxDO21FQUNkSCxTQUFTOEIsSUFBVCxDQUFjLEVBQUVlLDBCQUFGLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O3NGQUNvQm5CLEU7Ozs7OztBQUNWNUMsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsVUFBYixFQUF5QmlDLEVBQXpCLEVBQTZCLFFBQTdCLEM7QUFDTmhDLHNDLEdBQVMsTTs7dUNBQ1EsS0FBS2lDLGNBQUwsQ0FBb0I3QyxHQUFwQixFQUF5QixFQUFFWSxjQUFGLEVBQXpCLEM7OztBQUFqQk0sd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMNkMsNEMsR0FBZSwyQkFBaUI3QyxTQUFTRSxLQUFULENBQWVDLElBQWhDLEM7bUVBQ2RILFNBQVM4QixJQUFULENBQWMsRUFBRWUsMEJBQUYsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDdUJDLEc7Ozs7OztBQUNiQyx1QyxHQUFVQyxNQUFNQyxPQUFOLENBQWNILEdBQWQsSUFBcUJBLEdBQXJCLEdBQTJCLENBQUNBLEdBQUQsQztBQUNyQ2hFLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUIsSUFBekIsRUFBK0IsZUFBL0IsRUFBZ0QsRUFBRWlDLElBQUlxQixPQUFOLEVBQWhELEM7QUFDWjs7O3VDQUN1QixLQUFLcEIsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTGtELDZDLEdBQWdCbEQsU0FBU0UsS0FBVCxDQUFlQyxJQUFmLENBQW9CaUMsR0FBcEIsQ0FBd0I7QUFBQSwyQ0FBSywyQkFBaUJDLENBQWpCLENBQUw7QUFBQSxpQ0FBeEIsQzttRUFDZnJDLFNBQVM4QixJQUFULENBQWMsRUFBRW9CLDRCQUFGLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7c0ZBQ3FCQyxLO29CQUFPQyxLLHVFQUFRLEU7Ozs7OztBQUMxQnRFLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUIsSUFBekIsRUFBK0IsUUFBL0IsRUFBeUMsRUFBRTRELEdBQUdGLEtBQUwsRUFBWUMsWUFBWixFQUF6QyxDOzt1Q0FDVyxLQUFLekIsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTG1DLHdDLEdBQVduQyxTQUFTRSxLQUFULENBQWVDLElBQWYsQ0FBb0JpQyxHQUFwQixDQUF3QjtBQUFBLDJDQUFLLHNCQUFZQyxDQUFaLENBQUw7QUFBQSxpQ0FBeEIsQzttRUFDVnJDLFNBQVM4QixJQUFULENBQWMsRUFBRUssa0JBQUYsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FBRVVyRCxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxRQUFiLEM7O3VDQUNXLEtBQUtrQyxjQUFMLENBQW9CN0MsR0FBcEIsQzs7O0FBQWpCa0Isd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMbUMsd0MsR0FBV25DLFNBQVNFLEtBQVQsQ0FBZUMsSUFBZixDQUFvQmlDLEdBQXBCLENBQXdCO0FBQUEsMkNBQUssc0JBQVlDLENBQVosQ0FBTDtBQUFBLGlDQUF4QixDO0FBQ1hDLG9DLEdBQU90QyxTQUFTc0MsSUFBVCxDQUFjLGVBQUtkLElBQUwsQ0FBVVcsUUFBeEIsQzttRUFDTm5DLFNBQVM4QixJQUFULENBQWMsRUFBRVEsVUFBRixFQUFRSCxrQkFBUixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFFVXJELG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFlBQWIsQzs7dUNBQ1csS0FBS2tDLGNBQUwsQ0FBb0I3QyxHQUFwQixDOzs7QUFBakJrQix3Qzs7cUNBQ0ZBLFNBQVM0QixTQUFULEU7Ozs7O21FQUNPNUIsUTs7O0FBQ0w0Qyx3QyxHQUFXNUMsU0FBU0UsS0FBVCxDQUFlQyxJQUFmLENBQW9CaUMsR0FBcEIsQ0FBd0I7QUFBQSwyQ0FBSyxxQkFBV0MsQ0FBWCxDQUFMO0FBQUEsaUNBQXhCLEM7QUFDWEMsb0MsR0FBT3RDLFNBQVNzQyxJQUFULENBQWMsZUFBS2QsSUFBTCxDQUFVb0IsUUFBeEIsQzttRUFDTjVDLFNBQVM4QixJQUFULENBQWMsRUFBRVEsVUFBRixFQUFRTSxrQkFBUixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFFVTlELG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLGlCQUFiLEM7O3VDQUNXLEtBQUtrQyxjQUFMLENBQW9CN0MsR0FBcEIsQzs7O0FBQWpCa0Isd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMbUMsd0MsR0FBV25DLFNBQVNFLEtBQVQsQ0FBZUMsSUFBZixDQUFvQmlDLEdBQXBCLENBQXdCO0FBQUEsMkNBQUssc0JBQVlDLENBQVosQ0FBTDtBQUFBLGlDQUF4QixDO0FBQ1hDLG9DLEdBQU90QyxTQUFTc0MsSUFBVCxDQUFjLGVBQUtkLElBQUwsQ0FBVVcsUUFBeEIsQzttRUFDTm5DLFNBQVM4QixJQUFULENBQWMsRUFBRVEsVUFBRixFQUFRSCxrQkFBUixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O3NGQUM2QlQsRTs7Ozs7O0FBQ25CNUMsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsaUJBQWIsRUFBZ0NpQyxFQUFoQyxFQUFvQyxXQUFwQyxDO0FBQ05oQyxzQyxHQUFTLE07QUFDVEMsb0MsR0FBTyxJQUFJMkQsUUFBSixFOztBQUNiM0QscUNBQUs0RCxNQUFMLENBQVksSUFBWixFQUFrQkMsT0FBTzlCLEVBQVAsQ0FBbEI7O3VDQUNhLEtBQUsrQixVQUFMLENBQWdCM0UsR0FBaEIsRUFBcUIsRUFBRVksY0FBRixFQUFVQyxVQUFWLEVBQXJCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDMEIrQixFOzs7Ozs7QUFDaEI1QyxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxpQkFBYixFQUFnQ2lDLEVBQWhDLEVBQW9DLFFBQXBDLEM7QUFDTmhDLHNDLEdBQVMsTTtBQUNUQyxvQyxHQUFPLElBQUkyRCxRQUFKLEU7O0FBQ2IzRCxxQ0FBSzRELE1BQUwsQ0FBWSxJQUFaLEVBQWtCQyxPQUFPOUIsRUFBUCxDQUFsQjs7dUNBQ2EsS0FBSytCLFVBQUwsQ0FBZ0IzRSxHQUFoQixFQUFxQixFQUFFWSxjQUFGLEVBQVVDLFVBQVYsRUFBckIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVqQjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDdUIrRCxHOzs7Ozs7QUFDYjVFLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFNBQWIsQztBQUNOQyxzQyxHQUFTLE07QUFDVEMsb0MsR0FBTyxJQUFJMkQsUUFBSixFOztBQUNiM0QscUNBQUs0RCxNQUFMLENBQVksS0FBWixFQUFtQkcsR0FBbkI7O3VDQUN1QixLQUFLL0IsY0FBTCxDQUFvQjdDLEdBQXBCLEVBQXlCLEVBQUVZLGNBQUYsRUFBVUMsVUFBVixFQUF6QixDOzs7QUFBakJLLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTDZCLHVDLEdBQVUsc0JBQVk3QixTQUFTRSxLQUFULENBQWVDLElBQTNCLEM7bUVBQ1RILFNBQVM4QixJQUFULENBQWMsRUFBRUQsZ0JBQUYsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FBRVUvQyxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxVQUFiLEM7O3VDQUNXLEtBQUtNLFVBQUwsQ0FBZ0JqQixHQUFoQixDOzs7QUFBakJrQix3Qzs7cUNBQ0ZBLFNBQVM0QixTQUFULEU7Ozs7O21FQUNPNUIsUTs7O0FBQ0wyRCx3QyxHQUFXLHVCQUFhM0QsU0FBU0UsS0FBVCxDQUFlQyxJQUE1QixDO21FQUNWSCxTQUFTOEIsSUFBVCxDQUFjLEVBQUU2QixrQkFBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDNEJDLEk7Ozs7OztBQUNsQjlFLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLE9BQWIsQztBQUNOQyxzQyxHQUFTLE07QUFDVEMsb0MsR0FBTyxJQUFJMkQsUUFBSixFOztBQUNiM0QscUNBQUs0RCxNQUFMLENBQVksTUFBWixFQUFvQkssSUFBcEI7O3VDQUN1QixLQUFLakMsY0FBTCxDQUFvQjdDLEdBQXBCLEVBQXlCLEVBQUVZLGNBQUYsRUFBVUMsVUFBVixFQUF6QixDOzs7QUFBakJLLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTDZELDBDLEdBQWEseUJBQWU3RCxTQUFTRSxLQUFULENBQWVDLElBQTlCLEM7bUVBQ1pILFNBQVM4QixJQUFULENBQWMsRUFBRStCLHNCQUFGLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQUVVL0UsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsT0FBYixDOzt1Q0FDVyxLQUFLa0MsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTG1DLHdDLEdBQVduQyxTQUFTRSxLQUFULENBQWVDLElBQWYsQ0FBb0JpQyxHQUFwQixDQUF3QjtBQUFBLDJDQUFLLHNCQUFZQyxDQUFaLENBQUw7QUFBQSxpQ0FBeEIsQzttRUFDVnJDLFNBQVM4QixJQUFULENBQWMsRUFBRUssa0JBQUYsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FBRVVyRCxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxlQUFiLEM7O3VDQUNXLEtBQUtrQyxjQUFMLENBQW9CN0MsR0FBcEIsQzs7O0FBQWpCa0Isd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMOEQsNkMsR0FBZ0I5RCxTQUFTRSxLQUFULENBQWVDLElBQWYsQ0FBb0JpQyxHQUFwQixDQUF3QjtBQUFBLDJDQUFLLDJCQUFpQkMsQ0FBakIsQ0FBTDtBQUFBLGlDQUF4QixDO0FBQ2hCQyxvQyxHQUFPdEMsU0FBU3NDLElBQVQsQ0FBYyxlQUFLZCxJQUFMLENBQVVzQyxhQUF4QixDO21FQUNOOUQsU0FBUzhCLElBQVQsQ0FBYyxFQUFFUSxVQUFGLEVBQVF3Qiw0QkFBUixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDc0JwQyxFOzs7Ozs7QUFDWjVDLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLGVBQWIsRUFBOEJpQyxFQUE5QixDOzt1Q0FDVyxLQUFLQyxjQUFMLENBQW9CN0MsR0FBcEIsQzs7O0FBQWpCa0Isd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMK0QsNEMsR0FBZSwyQkFBaUIvRCxTQUFTRSxLQUFULENBQWVDLElBQWhDLEM7bUVBQ2RILFNBQVM4QixJQUFULENBQWMsRUFBRWlDLDBCQUFGLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FBRVVqRixtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxlQUFiLEVBQThCLE9BQTlCLEM7QUFDTkMsc0MsR0FBUyxNOzt1Q0FDRixLQUFLK0QsVUFBTCxDQUFnQjNFLEdBQWhCLEVBQXFCLEVBQUVZLGNBQUYsRUFBckIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVqQjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFFVVosbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsU0FBYixDOzt1Q0FDVyxLQUFLa0MsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTGdFLHVDLEdBQVVoRSxTQUFTRSxLQUFULENBQWVDLElBQWYsQ0FBb0JpQyxHQUFwQixDQUF3QjtBQUFBLDJDQUFLLHFCQUFXNkIsQ0FBWCxDQUFMO0FBQUEsaUNBQXhCLEM7bUVBQ1RqRSxTQUFTOEIsSUFBVCxDQUFjLEVBQUVrQyxnQkFBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7aUdBQ3dFLEU7OENBQXJERSxTO29CQUFBQSxTLG9DQUFZLEk7OENBQU1DLFM7b0JBQUFBLFMsb0NBQVksRTs0Q0FBSUMsTztvQkFBQUEsTyxrQ0FBVSxJOzs7Ozs7OztBQUNyRHRGLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFNBQWIsQztBQUNOQyxzQyxHQUFTLE07QUFDVEMsb0MsR0FBTyxJQUFJMkQsUUFBSixFOztBQUNiM0QscUNBQUs0RCxNQUFMLENBQVksWUFBWixFQUEwQlcsU0FBMUI7Ozs7O0FBQ0EsaURBQWlCQyxTQUFqQix1SEFBNEI7QUFBakJ6QyxzQ0FBaUI7O0FBQ3hCL0IseUNBQUs0RCxNQUFMLENBQVksY0FBWixFQUE0QjdCLEVBQTVCO0FBQ0g7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0QvQixxQ0FBSzRELE1BQUwsQ0FBWSxTQUFaLEVBQXVCYSxPQUF2Qjs7dUNBQ3VCLEtBQUt6QyxjQUFMLENBQW9CN0MsR0FBcEIsRUFBeUIsRUFBRVksY0FBRixFQUFVQyxVQUFWLEVBQXpCLEM7OztBQUFqQkssd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMcUUsc0MsR0FBUyxxQkFBV3JFLFNBQVNFLEtBQVQsQ0FBZUMsSUFBMUIsQzttRUFDUkgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFdUMsY0FBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7c0ZBQ2FsQixLO29CQUFPbUIscUIsdUVBQXdCLEs7Ozs7OztBQUNsQ3hGLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFFBQWIsRUFBdUIsSUFBdkIsRUFBNkIsSUFBN0IsRUFBbUMsRUFBRTRELEdBQUdGLEtBQUwsRUFBWWxDLFNBQVNxRCxxQkFBckIsRUFBbkMsQzs7dUNBQ1csS0FBSzNDLGNBQUwsQ0FBb0I3QyxHQUFwQixDOzs7QUFBakJrQix3Qzs7cUNBQ0ZBLFNBQVM0QixTQUFULEU7Ozs7O21FQUNPNUIsUTs7O0FBQ0x1RSxzQyxHQUFTLHFCQUFXdkUsU0FBU0UsS0FBVCxDQUFlQyxJQUExQixDO21FQUNSSCxTQUFTOEIsSUFBVCxDQUFjLEVBQUV5QyxjQUFGLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDa0I3QyxFOzs7Ozs7QUFDUjVDLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUJpQyxFQUF6QixDOzt1Q0FDVyxLQUFLQyxjQUFMLENBQW9CN0MsR0FBcEIsQzs7O0FBQWpCa0Isd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMd0Usc0MsR0FBUyxxQkFBV3hFLFNBQVNFLEtBQVQsQ0FBZUMsSUFBMUIsQzttRUFDUkgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFMEMsY0FBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDdUI5QyxFOzs7Ozs7QUFDYjVDLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUJpQyxFQUF6QixFQUE2QixTQUE3QixDOzt1Q0FDVyxLQUFLQyxjQUFMLENBQW9CN0MsR0FBcEIsQzs7O0FBQWpCa0Isd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMeUUsdUMsR0FBVSx1QkFBWXpFLFNBQVNFLEtBQVQsQ0FBZUMsSUFBM0IsQzttRUFDVEgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFMkMsZ0JBQUYsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7c0ZBQ29CL0MsRTs7Ozs7O0FBQ1Y1QyxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxVQUFiLEVBQXlCaUMsRUFBekIsRUFBNkIsTUFBN0IsQzs7dUNBQ1csS0FBS0MsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTDBFLG9DLEdBQU8sbUJBQVMxRSxTQUFTRSxLQUFULENBQWVDLElBQXhCLEM7bUVBQ05ILFNBQVM4QixJQUFULENBQWMsRUFBRTRDLFVBQUYsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7c0ZBQzRCaEQsRTs7Ozs7O0FBQ2xCNUMsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsVUFBYixFQUF5QmlDLEVBQXpCLEVBQTZCLGNBQTdCLEM7O3VDQUNXLEtBQUtDLGNBQUwsQ0FBb0I3QyxHQUFwQixDOzs7QUFBakJrQix3Qzs7cUNBQ0ZBLFNBQVM0QixTQUFULEU7Ozs7O21FQUNPNUIsUTs7O0FBQ0xtQyx3QyxHQUFXbkMsU0FBU0UsS0FBVCxDQUFlQyxJQUFmLENBQW9CaUMsR0FBcEIsQ0FBd0I7QUFBQSwyQ0FBSyxzQkFBWTZCLENBQVosQ0FBTDtBQUFBLGlDQUF4QixDO21FQUNWakUsU0FBUzhCLElBQVQsQ0FBYyxFQUFFSyxrQkFBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDNEJULEU7Ozs7OztBQUNsQjVDLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUJpQyxFQUF6QixFQUE2QixlQUE3QixDOzt1Q0FDVyxLQUFLQyxjQUFMLENBQW9CN0MsR0FBcEIsQzs7O0FBQWpCa0Isd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMbUMsd0MsR0FBV25DLFNBQVNFLEtBQVQsQ0FBZUMsSUFBZixDQUFvQmlDLEdBQXBCLENBQXdCO0FBQUEsMkNBQUssc0JBQVk2QixDQUFaLENBQUw7QUFBQSxpQ0FBeEIsQzttRUFDVmpFLFNBQVM4QixJQUFULENBQWMsRUFBRUssa0JBQUYsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7c0ZBQ2lCd0MsSTtpR0FBeUcsRTs0Q0FBakdDLE87b0JBQUFBLE8sa0NBQVUsSTs2Q0FBTUMsUTtvQkFBQUEsUSxtQ0FBVyxFOzhDQUFJQyxTO29CQUFBQSxTLG9DQUFZLEs7Z0RBQU9DLFc7b0JBQUFBLFcsc0NBQWMsSTsrQ0FBTUMsVTtvQkFBQUEsVSxxQ0FBYSxROzs7Ozs7OztBQUNsR2xHLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUIsSUFBekIsRUFBK0IsSUFBL0IsQztBQUNOQyxzQyxHQUFTLE07QUFDVEMsb0MsR0FBTyxJQUFJMkQsUUFBSixFOztBQUNiM0QscUNBQUs0RCxNQUFMLENBQVksUUFBWixFQUFzQm9CLElBQXRCO0FBQ0Esb0NBQUlDLE9BQUosRUFDSWpGLEtBQUs0RCxNQUFMLENBQVksZ0JBQVosRUFBOEJxQixPQUE5Qjs7Ozs7QUFDSixrREFBaUJDLFFBQWpCLDJIQUEyQjtBQUFoQm5ELHNDQUFnQjs7QUFDdkIvQix5Q0FBSzRELE1BQUwsQ0FBWSxhQUFaLEVBQTJCN0IsRUFBM0I7QUFDSDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDRCxvQ0FBSW9ELFNBQUosRUFDSW5GLEtBQUs0RCxNQUFMLENBQVksV0FBWixFQUF5QixNQUF6QjtBQUNKLG9DQUFJd0IsV0FBSixFQUNJcEYsS0FBSzRELE1BQUwsQ0FBWSxjQUFaLEVBQTRCd0IsV0FBNUI7QUFDSnBGLHFDQUFLNEQsTUFBTCxDQUFZLFlBQVosRUFBMEJ5QixVQUExQjs7dUNBQ3VCLEtBQUtyRCxjQUFMLENBQW9CN0MsR0FBcEIsRUFBeUIsRUFBRVksY0FBRixFQUFVQyxVQUFWLEVBQXpCLEM7OztBQUFqQkssd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMd0Usc0MsR0FBUyxxQkFBV3hFLFNBQVNFLEtBQVQsQ0FBZUMsSUFBMUIsQzttRUFDUkgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFMEMsY0FBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDbUI5QyxFOzs7Ozs7QUFDVDVDLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUJpQyxFQUF6QixDO0FBQ05oQyxzQyxHQUFTLFE7O3VDQUNGLEtBQUsrRCxVQUFMLENBQWdCM0UsR0FBaEIsRUFBcUIsRUFBRVksY0FBRixFQUFyQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWpCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O3NGQUNtQmdDLEU7Ozs7OztBQUNUNUMsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsVUFBYixFQUF5QmlDLEVBQXpCLEVBQTZCLFFBQTdCLEM7QUFDTmhDLHNDLEdBQVMsTTs7dUNBQ1EsS0FBS2lDLGNBQUwsQ0FBb0I3QyxHQUFwQixFQUF5QixFQUFFWSxjQUFGLEVBQXpCLEM7OztBQUFqQk0sd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMd0Usc0MsR0FBUyxxQkFBV3hFLFNBQVNFLEtBQVQsQ0FBZUMsSUFBMUIsQzttRUFDUkgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFMEMsY0FBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztzRkFDcUI5QyxFOzs7Ozs7QUFDWDVDLG1DLEdBQU0sS0FBS1csT0FBTCxDQUFhLFVBQWIsRUFBeUJpQyxFQUF6QixFQUE2QixVQUE3QixDO0FBQ05oQyxzQyxHQUFTLE07O3VDQUNRLEtBQUtpQyxjQUFMLENBQW9CN0MsR0FBcEIsRUFBeUIsRUFBRVksY0FBRixFQUF6QixDOzs7QUFBakJNLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTHdFLHNDLEdBQVMscUJBQVd4RSxTQUFTRSxLQUFULENBQWVDLElBQTFCLEM7bUVBQ1JILFNBQVM4QixJQUFULENBQWMsRUFBRTBDLGNBQUYsRUFBZCxDOzs7Ozs7Ozs7Ozs7Ozs7O0FBRVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7c0ZBQ3FCOUMsRTs7Ozs7O0FBQ1g1QyxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxVQUFiLEVBQXlCaUMsRUFBekIsRUFBNkIsV0FBN0IsQztBQUNOaEMsc0MsR0FBUyxNOzt1Q0FDUSxLQUFLaUMsY0FBTCxDQUFvQjdDLEdBQXBCLEVBQXlCLEVBQUVZLGNBQUYsRUFBekIsQzs7O0FBQWpCTSx3Qzs7cUNBQ0ZBLFNBQVM0QixTQUFULEU7Ozs7O21FQUNPNUIsUTs7O0FBQ0x3RSxzQyxHQUFTLHFCQUFXeEUsU0FBU0UsS0FBVCxDQUFlQyxJQUExQixDO21FQUNSSCxTQUFTOEIsSUFBVCxDQUFjLEVBQUUwQyxjQUFGLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O3NGQUN1QjlDLEU7Ozs7OztBQUNiNUMsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsVUFBYixFQUF5QmlDLEVBQXpCLEVBQTZCLGFBQTdCLEM7QUFDTmhDLHNDLEdBQVMsTTs7dUNBQ1EsS0FBS2lDLGNBQUwsQ0FBb0I3QyxHQUFwQixFQUF5QixFQUFFWSxjQUFGLEVBQXpCLEM7OztBQUFqQk0sd0M7O3FDQUNGQSxTQUFTNEIsU0FBVCxFOzs7OzttRUFDTzVCLFE7OztBQUNMd0Usc0MsR0FBUyxxQkFBV3hFLFNBQVNFLEtBQVQsQ0FBZUMsSUFBMUIsQzttRUFDUkgsU0FBUzhCLElBQVQsQ0FBYyxFQUFFMEMsY0FBRixFQUFkLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFWDs7O0FBR0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQUVVMUYsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsV0FBYixFQUEwQixJQUExQixFQUFnQyxNQUFoQyxDOzt1Q0FDVyxLQUFLa0MsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTDRDLHdDLEdBQVc1QyxTQUFTRSxLQUFULENBQWVDLElBQWYsQ0FBb0JpQyxHQUFwQixDQUF3QjtBQUFBLDJDQUFLLHFCQUFXQyxDQUFYLENBQUw7QUFBQSxpQ0FBeEIsQztBQUNYQyxvQyxHQUFPdEMsU0FBU3NDLElBQVQsQ0FBYyxlQUFLZCxJQUFMLENBQVVvQixRQUF4QixDO21FQUNONUMsU0FBUzhCLElBQVQsQ0FBYyxFQUFFUSxVQUFGLEVBQVFNLGtCQUFSLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O29CQUNrQnFDLEssdUVBQVEsSzs7Ozs7O0FBQ2hCbkcsbUMsR0FBTSxLQUFLVyxPQUFMLENBQWEsV0FBYixFQUEwQixJQUExQixFQUFnQyxRQUFoQyxFQUEwQyxFQUFFd0YsWUFBRixFQUExQyxDOzt1Q0FDVyxLQUFLdEQsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTDRDLHdDLEdBQVc1QyxTQUFTRSxLQUFULENBQWVDLElBQWYsQ0FBb0JpQyxHQUFwQixDQUF3QjtBQUFBLDJDQUFLLHFCQUFXQyxDQUFYLENBQUw7QUFBQSxpQ0FBeEIsQztBQUNYQyxvQyxHQUFPdEMsU0FBU3NDLElBQVQsQ0FBYyxlQUFLZCxJQUFMLENBQVVvQixRQUF4QixDO21FQUNONUMsU0FBUzhCLElBQVQsQ0FBYyxFQUFFUSxVQUFGLEVBQVFNLGtCQUFSLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7c0ZBQ3lCc0MsTztvQkFBU0QsSyx1RUFBUSxLOzs7Ozs7QUFDaENuRyxtQyxHQUFNLEtBQUtXLE9BQUwsQ0FBYSxXQUFiLEVBQTBCLEtBQTFCLEVBQWlDeUYsT0FBakMsRUFBMEMsRUFBRUQsWUFBRixFQUExQyxDOzt1Q0FDVyxLQUFLdEQsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTDRDLHdDLEdBQVc1QyxTQUFTRSxLQUFULENBQWVDLElBQWYsQ0FBb0JpQyxHQUFwQixDQUF3QjtBQUFBLDJDQUFLLHFCQUFXQyxDQUFYLENBQUw7QUFBQSxpQ0FBeEIsQztBQUNYQyxvQyxHQUFPdEMsU0FBU3NDLElBQVQsQ0FBYyxlQUFLZCxJQUFMLENBQVVvQixRQUF4QixDO21FQUNONUMsU0FBUzhCLElBQVQsQ0FBYyxFQUFFUSxVQUFGLEVBQVFNLGtCQUFSLEVBQWQsQzs7Ozs7Ozs7Ozs7Ozs7OztBQUVYOzs7Ozs7O3NGQUdjTixJOzs7OztzQ0FDTixDQUFDQSxJQUFELElBQVMsQ0FBQ0EsS0FBSzZDLElBQWYsSUFBdUIsQ0FBQzdDLEtBQUs2QyxJQUFMLENBQVVyRyxHOzs7OzttRUFDM0IsbUJBQUV5QyxNQUFGLENBQVMsbUJBQUVDLElBQUYsQ0FBTzRELFFBQWhCLEM7Ozs7dUNBQ0UsS0FBS0MsWUFBTCxDQUFrQi9DLEtBQUs2QyxJQUFMLENBQVVyRyxHQUE1QixFQUFpQ3dELElBQWpDLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7c0ZBRUhBLEk7Ozs7O3NDQUNOLENBQUNBLElBQUQsSUFBUyxDQUFDQSxLQUFLZ0QsSUFBZixJQUF1QixDQUFDaEQsS0FBS2dELElBQUwsQ0FBVXhHLEc7Ozs7O21FQUMzQixtQkFBRXlDLE1BQUYsQ0FBUyxtQkFBRUMsSUFBRixDQUFPNEQsUUFBaEIsQzs7Ozt1Q0FDRSxLQUFLQyxZQUFMLENBQWtCL0MsS0FBS2dELElBQUwsQ0FBVXhHLEdBQTVCLEVBQWlDd0QsSUFBakMsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVqQjs7Ozs7O3lDQUdpQmlELE8sRUFBUztBQUN0QixnQkFBTXRHLFNBQVMsTUFBZjtBQUNBLG1CQUFPLEtBQUt1RyxZQUFMLENBQWtCLEVBQUV2RyxjQUFGLEVBQWxCLEVBQThCc0csT0FBOUIsQ0FBUDtBQUNIOzs7MkNBQ2tCTixLLEVBQU9NLE8sRUFBUztBQUMvQixnQkFBTXRHLFNBQVNnRyxRQUFRLGNBQVIsR0FBeUIsUUFBeEM7QUFDQSxtQkFBTyxLQUFLTyxZQUFMLENBQWtCLEVBQUV2RyxjQUFGLEVBQWxCLEVBQThCc0csT0FBOUIsQ0FBUDtBQUNIOzs7NENBQ21CRSxHLEVBQUtSLEssRUFBT00sTyxFQUFTO0FBQ3JDLGdCQUFNdEcsU0FBU2dHLFFBQVEsZUFBUixHQUEwQixRQUF6QztBQUNBLG1CQUFPLEtBQUtPLFlBQUwsQ0FBa0IsRUFBRXZHLGNBQUYsRUFBVXdHLFFBQVYsRUFBbEIsRUFBbUNGLE9BQW5DLENBQVA7QUFDSDs7O3FDQUNZRyxNLEVBQVFILE8sRUFBUztBQUMxQkcsbUJBQU9DLFlBQVAsR0FBc0IsS0FBS3pHLFVBQUwsQ0FBZ0JHLFdBQXRDO0FBQ0EsZ0JBQU11RyxTQUFTLElBQUlDLFNBQUosQ0FBYyxLQUFLQyxVQUFMLENBQWdCSixNQUFoQixDQUFkLENBQWY7QUFDQUUsbUJBQU9HLE1BQVAsR0FBZ0I7QUFBQSx1QkFBTVIsUUFBUSxNQUFSLENBQU47QUFBQSxhQUFoQjtBQUNBSyxtQkFBT0ksT0FBUCxHQUFpQjtBQUFBLHVCQUFNVCxRQUFRLE9BQVIsQ0FBTjtBQUFBLGFBQWpCO0FBQ0FLLG1CQUFPSyxTQUFQLEdBQW1CLGVBQU87QUFDdEIsb0JBQU1DLE9BQU9DLEtBQUtDLEtBQUwsQ0FBV0MsSUFBSUgsSUFBZixDQUFiO0FBQ0Esb0JBQUksQ0FBQ0EsS0FBS0ksT0FBVixFQUNJO0FBQ0osd0JBQVFKLEtBQUtLLEtBQWI7QUFDSSx5QkFBSyxRQUFMO0FBQWU7QUFDWCxnQ0FBTS9CLFNBQVMscUJBQVcyQixLQUFLQyxLQUFMLENBQVdGLEtBQUtJLE9BQWhCLENBQVgsQ0FBZjtBQUNBZixvQ0FBUSxTQUFSLEVBQW1CLFFBQW5CLEVBQTZCZixNQUE3QjtBQUNBO0FBQ0g7QUFDRCx5QkFBSyxjQUFMO0FBQXFCO0FBQ2pCLGdDQUFNVCxlQUFlLDJCQUFpQm9DLEtBQUtDLEtBQUwsQ0FBV0YsS0FBS0ksT0FBaEIsQ0FBakIsQ0FBckI7QUFDQWYsb0NBQVEsU0FBUixFQUFtQixjQUFuQixFQUFtQ3hCLFlBQW5DO0FBQ0E7QUFDSDtBQUNELHlCQUFLLFFBQUw7QUFBZTtBQUNYLGdDQUFNckMsS0FBS3dFLEtBQUtJLE9BQWhCO0FBQ0FmLG9DQUFRLFNBQVIsRUFBbUIsUUFBbkIsRUFBNkI3RCxFQUE3QjtBQUNBO0FBQ0g7QUFDRDtBQUNJO0FBakJSO0FBbUJILGFBdkJEO0FBd0JBLG1CQUFPa0UsTUFBUDtBQUNIO0FBQ0Q7Ozs7OztvQ0FHWTtBQUNSLGdCQUFNWSxXQUFXLEtBQUtqSCxpQkFBTCxHQUF5QixPQUF6QixHQUFtQyxNQUFwRDtBQUNBLG1CQUFVaUgsUUFBVixXQUF3QixLQUFLMUgsR0FBTCxDQUFTRSxHQUFqQztBQUNIOzs7a0NBQ1M7QUFDTixnQkFBTXdILFdBQVcsS0FBS2pILGlCQUFMLEdBQXlCLEtBQXpCLEdBQWlDLElBQWxEO0FBQ0EsbUJBQVVpSCxRQUFWLFdBQXdCLEtBQUsxSCxHQUFMLENBQVNHLE1BQWpDO0FBQ0g7OztnQ0FDT3dILFEsRUFBbUQ7QUFBQSxnQkFBekMvRSxFQUF5Qyx1RUFBcEMsSUFBb0M7QUFBQSxnQkFBOUJnRixNQUE4Qix1RUFBckIsSUFBcUI7QUFBQSxnQkFBZm5FLE1BQWUsdUVBQU4sSUFBTTs7QUFDdkQsZ0JBQUlvRSxJQUFLRixhQUFhLElBQWQsR0FBdUIsTUFBTUEsUUFBN0IsR0FBeUMsRUFBakQ7QUFDQSxnQkFBSUcsSUFBS2xGLE9BQU8sSUFBUixHQUFpQixNQUFNQSxFQUF2QixHQUE2QixFQUFyQztBQUNBLGdCQUFJbUYsSUFBS0gsV0FBVyxJQUFaLEdBQXFCLE1BQU1BLE1BQTNCLEdBQXFDLEVBQTdDO0FBQ0EsZ0JBQUk1SCxNQUFTLEtBQUsyQixTQUFMLEVBQVQsZUFBbUNrRyxDQUFuQyxHQUF1Q0MsQ0FBdkMsR0FBMkNDLENBQS9DO0FBQ0EsZ0JBQUksQ0FBQ3RFLE1BQUwsRUFDSSxPQUFPekQsR0FBUDtBQUNKLG1CQUFVQSxHQUFWLFNBQWlCLEtBQUtnSSxZQUFMLENBQWtCdkUsTUFBbEIsQ0FBakI7QUFDSDs7O3FDQUN5QjtBQUFBLGdCQUFmQSxNQUFlLHVFQUFOLElBQU07O0FBQ3RCLGdCQUFNekQsTUFBUyxLQUFLaUksT0FBTCxFQUFULHNCQUFOO0FBQ0EsZ0JBQUksQ0FBQ3hFLE1BQUwsRUFDSSxPQUFPekQsR0FBUDtBQUNKLG1CQUFVQSxHQUFWLFNBQWlCLEtBQUtnSSxZQUFMLENBQWtCdkUsTUFBbEIsQ0FBakI7QUFDSDs7O3FDQUNZQSxNLEVBQVE7QUFDakIsZ0JBQUksQ0FBQ0EsTUFBTCxFQUNJLE9BQU8sRUFBUDtBQUNKLGdCQUFNeUUsWUFBWSxJQUFJcEgsZUFBSixFQUFsQjtBQUhpQjtBQUFBO0FBQUE7O0FBQUE7QUFJakIsc0NBQTJCcUgsT0FBT0MsT0FBUCxDQUFlM0UsTUFBZixDQUEzQixtSUFBbUQ7QUFBQTtBQUFBLHdCQUF2QzRFLEdBQXVDO0FBQUEsd0JBQWxDakgsS0FBa0M7O0FBQy9DLHdCQUFJLENBQUNBLEtBQUwsRUFDSTtBQUNKLHdCQUFJOEMsTUFBTUMsT0FBTixDQUFjL0MsS0FBZCxDQUFKLEVBQTBCO0FBQ3RCLDRCQUFNa0gsSUFBT0QsR0FBUCxPQUFOO0FBRHNCO0FBQUE7QUFBQTs7QUFBQTtBQUV0QixrREFBZ0JqSCxLQUFoQixtSUFBdUI7QUFBQSxvQ0FBWm1DLENBQVk7O0FBQ25CMkUsMENBQVV6RCxNQUFWLENBQWlCNkQsQ0FBakIsRUFBb0IvRSxDQUFwQjtBQUNIO0FBSnFCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLekIscUJBTEQsTUFNSztBQUNEMkUsa0NBQVVuSCxHQUFWLENBQWNzSCxHQUFkLEVBQW1CakgsS0FBbkI7QUFDSDtBQUNKO0FBaEJnQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztBQWlCakIsbUJBQU84RyxVQUFVSyxRQUFWLEVBQVA7QUFDSDs7OztzRkFDWXZJLEc7aUdBQW9FLEU7eUNBQTdEYSxJO29CQUFBQSxJLCtCQUFPLEk7MkNBQU1ELE07b0JBQUFBLE0saUNBQVMsSzt5Q0FBTzRILEk7b0JBQUFBLEksK0JBQU8sTTs0Q0FBUUMsTztvQkFBQUEsTyxrQ0FBVSxFOzs7Ozs7Ozs7dUNBRTNDQyxNQUFNMUksR0FBTixFQUFXO0FBQzlCYSw4Q0FEOEI7QUFFOUJELGtEQUY4QjtBQUc5QjRILDBDQUFNQSxJQUh3QjtBQUk5QkM7QUFKOEIsaUNBQVgsQzs7O0FBQWpCdkgsd0M7O29DQU1EQSxTQUFTeUgsRTs7Ozs7QUFDSkMsb0MsR0FBTyxFQUFFbEQsUUFBUXhFLFNBQVN3RSxNQUFuQixFQUEyQm1ELFlBQVkzSCxTQUFTMkgsVUFBaEQsRUFBNEQzSCxrQkFBNUQsRTttRUFDTixtQkFBRXVCLE1BQUYsQ0FBUyxtQkFBRUMsSUFBRixDQUFPb0csYUFBaEIsRUFBK0JGLElBQS9CLEM7OzttRUFFSixtQkFBRXBHLE9BQUYsQ0FBVSxFQUFFdEIsa0JBQUYsRUFBVixDOzs7OzttRUFHQSxtQkFBRXVCLE1BQUYsQ0FBUyxtQkFBRUMsSUFBRixDQUFPQyxZQUFoQixnQjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztzRkFHRTNDLEc7aUdBQW9FLEU7eUNBQTdEYSxJO29CQUFBQSxJLCtCQUFPLEk7MkNBQU1ELE07b0JBQUFBLE0saUNBQVMsSzt5Q0FBTzRILEk7b0JBQUFBLEksK0JBQU8sTTs0Q0FBUUMsTztvQkFBQUEsTyxrQ0FBVSxFOzs7Ozs7O0FBQ3BFTSxpQyxHQUFJLEU7O0FBQ1ZaLHVDQUFPYSxNQUFQLENBQWNELENBQWQsRUFBaUJOLE9BQWpCOzs7dUNBRTJCLEtBQUtRLE1BQUwsQ0FBWWpKLEdBQVosRUFBaUIsRUFBRWEsVUFBRixFQUFRRCxjQUFSLEVBQWdCNEgsVUFBaEIsRUFBc0JDLFNBQVNNLENBQS9CLEVBQWpCLEM7OztBQUFqQjdILHdDOztxQ0FDRkEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7O3VDQUNRQSxTQUFTRSxLQUFULENBQWVGLFFBQWYsQ0FBd0JHLElBQXhCLEU7OztBQUFiQSxvQztBQUNBb0Usc0MsR0FBUyxtQkFBRWpELE9BQUYsQ0FBVSxFQUFFbkIsVUFBRixFQUFWLEM7O0FBQ2ZvRSx1Q0FBT2dELE9BQVAsR0FBaUJ2SCxTQUFTRSxLQUFULENBQWVGLFFBQWYsQ0FBd0J1SCxPQUF6QzttRUFDT2hELE07Ozs7O21FQUdBLG1CQUFFaEQsTUFBRixDQUFTLG1CQUFFQyxJQUFGLENBQU9DLFlBQWhCLGdCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O3NGQUdFM0MsRztpR0FBb0UsRTt5Q0FBN0RhLEk7b0JBQUFBLEksK0JBQU8sSTsyQ0FBTUQsTTtvQkFBQUEsTSxpQ0FBUyxLO3lDQUFPNEgsSTtvQkFBQUEsSSwrQkFBTyxNOzRDQUFRQyxPO29CQUFBQSxPLGtDQUFVLEU7Ozs7Ozs7dUNBQzdELEtBQUtRLE1BQUwsQ0FBWWpKLEdBQVosRUFBaUI7QUFDMUJhLDhDQUQwQjtBQUUxQkQsa0RBRjBCO0FBRzFCNEgsOENBSDBCO0FBSTFCQyw2Q0FBU04sT0FBT2EsTUFBUCxDQUFjLEVBQUVFLDJCQUF5QixLQUFLOUksVUFBTCxDQUFnQkcsV0FBM0MsRUFBZCxFQUEwRWtJLE9BQTFFO0FBSmlCLGlDQUFqQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O3NGQU9JekksRztpR0FBb0UsRTt5Q0FBN0RhLEk7b0JBQUFBLEksK0JBQU8sSTsyQ0FBTUQsTTtvQkFBQUEsTSxpQ0FBUyxLO3lDQUFPNEgsSTtvQkFBQUEsSSwrQkFBTyxNOzRDQUFRQyxPO29CQUFBQSxPLGtDQUFVLEU7Ozs7Ozs7dUNBQ2pFLEtBQUt4SCxVQUFMLENBQWdCakIsR0FBaEIsRUFBcUI7QUFDOUJhLDhDQUQ4QjtBQUU5QkQsa0RBRjhCO0FBRzlCNEgsOENBSDhCO0FBSTlCQyw2Q0FBU04sT0FBT2EsTUFBUCxDQUFjLEVBQUVFLDJCQUF5QixLQUFLOUksVUFBTCxDQUFnQkcsV0FBM0MsRUFBZCxFQUEwRWtJLE9BQTFFO0FBSnFCLGlDQUFyQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O3NGQU9FekksRyxFQUFLd0QsSTs7Ozs7Ozs7dUNBQ0MsS0FBS1gsY0FBTCxDQUFvQjdDLEdBQXBCLEM7OztBQUFqQmtCLHdDOztxQ0FDQUEsU0FBUzRCLFNBQVQsRTs7Ozs7bUVBQ081QixROzs7QUFDTEcsb0MsR0FBT0gsU0FBU0UsS0FBVCxDQUFlQyxJO0FBQ3hCOEgsdUMsR0FBVWpJLFNBQVNzQyxJQUFULENBQWNBLEtBQUs0RixJQUFuQixDO2dEQUNONUYsS0FBSzRGLEk7b0VBQ0osZUFBSzFHLElBQUwsQ0FBVW9CLFEsMEJBSVYsZUFBS3BCLElBQUwsQ0FBVVcsUSwwQkFJVixlQUFLWCxJQUFMLENBQVVzQyxhOzs7O0FBUExsQix3QyxHQUFXekMsS0FBS2lDLEdBQUwsQ0FBUztBQUFBLDJDQUFLLHFCQUFXNkIsQ0FBWCxDQUFMO0FBQUEsaUNBQVQsQzttRUFDVmpFLFNBQVM4QixJQUFULENBQWMsRUFBRVEsTUFBTTJGLFdBQVczRixJQUFuQixFQUF5Qk0sa0JBQXpCLEVBQWQsQzs7O0FBR0RBLHlDLEdBQVd6QyxLQUFLaUMsR0FBTCxDQUFTO0FBQUEsMkNBQUssc0JBQVk2QixDQUFaLENBQUw7QUFBQSxpQ0FBVCxDO21FQUNWakUsU0FBUzhCLElBQVQsQ0FBYyxFQUFFUSxNQUFNMkYsV0FBVzNGLElBQW5CLEVBQXlCTSxtQkFBekIsRUFBZCxDOzs7QUFHRGtCLDZDLEdBQWdCM0QsS0FBS2lDLEdBQUwsQ0FBUztBQUFBLDJDQUFLLDJCQUFpQjZCLENBQWpCLENBQUw7QUFBQSxpQ0FBVCxDO21FQUNmakUsU0FBUzhCLElBQVQsQ0FBYyxFQUFFUSxNQUFNMkYsV0FBVzNGLElBQW5CLEVBQXlCd0IsNEJBQXpCLEVBQWQsQzs7O3NDQUdELElBQUlxRSxLQUFKLENBQVUsZ0JBQVYsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2tCQUlQMUosUTtBQUNmIiwiZmlsZSI6ImNsaWVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB0b3R0b2tvdGtkIG9uIDE3LzA0LzI0LlxuICovXG5pbXBvcnQgeyBPQXV0aDIgfSBmcm9tICdvYXV0aCc7XG5pbXBvcnQgTGluayBmcm9tICcuL2xpbmsnO1xuaW1wb3J0IFIgZnJvbSAnLi9yZXNwb25zZSc7XG5pbXBvcnQgQWNjb3VudCBmcm9tICcuL2VudGl0eS9hY2NvdW50JztcbmltcG9ydCBBdHRhY2htZW50IGZyb20gJy4vZW50aXR5L2F0dGFjaG1lbnQnO1xuaW1wb3J0IENhcmQgZnJvbSAnLi9lbnRpdHkvY2FyZCc7XG5pbXBvcnQgQ29udGV4dCBmcm9tICcuL2VudGl0eS9jb250ZXh0JztcbmltcG9ydCBJbnN0YW5jZSBmcm9tICcuL2VudGl0eS9pbnN0YW5jZSc7XG5pbXBvcnQgTm90aWZpY2F0aW9uIGZyb20gJy4vZW50aXR5L25vdGlmaWNhdGlvbic7XG5pbXBvcnQgUmVsYXRpb25zaGlwIGZyb20gJy4vZW50aXR5L3JlbGF0aW9uc2hpcCc7XG5pbXBvcnQgU3RhdHVzIGZyb20gJy4vZW50aXR5L3N0YXR1cyc7XG5pbXBvcnQgUmVzdWx0IGZyb20gJy4vZW50aXR5L3Jlc3VsdCc7XG5pbXBvcnQgUmVwb3J0IGZyb20gJy4vZW50aXR5L3JlcG9ydCc7XG5jbGFzcyBNYXN0b2RvbiB7XG4gICAgLyoqXG4gICAgICogY29uc3RydWN0b3JcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5jYWxsYmFjayA9ICd1cm46aWV0Zjp3ZzpvYXV0aDoyLjA6b29iJztcbiAgICAgICAgLy8gY2xpZW50IHBhcmFtc1xuICAgICAgICB0aGlzLm5hbWUgPSAndGhlbWlzdG8tbGliJztcbiAgICAgICAgdGhpcy5zaXRlID0gbnVsbDtcbiAgICAgICAgdGhpcy5zY29wZXMgPSBbJ3JlYWQnLCAnd3JpdGUnLCAnZm9sbG93J107XG4gICAgICAgIC8vIFVSTHNcbiAgICAgICAgdGhpcy51cmwgPSB7XG4gICAgICAgICAgICBiYXNlOiBudWxsLFxuICAgICAgICAgICAgYXBpOiBudWxsLFxuICAgICAgICAgICAgc3RyZWFtOiBudWxsLFxuICAgICAgICB9O1xuICAgICAgICAvLyBjcmVkZW50aWFsXG4gICAgICAgIHRoaXMuY3JlZGVudGlhbCA9IHtcbiAgICAgICAgICAgIGNsaWVudElEOiBudWxsLFxuICAgICAgICAgICAgY2xpZW50U2VjcmV0OiBudWxsLFxuICAgICAgICAgICAgYWNjZXNzVG9rZW46IG51bGwsXG4gICAgICAgICAgICByZWZyZXNoVG9rZW46IG51bGwsXG4gICAgICAgIH07XG4gICAgICAgIC8vIGh0dHBzIGZsYWdcbiAgICAgICAgdGhpcy51c2VTZWN1cmVQcm90b2NvbCA9IHRydWU7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIHNldCBjbGllbnQgbmFtZSBmb3IgYXBwIHJlZ2lzdHJhdGlvblxuICAgICAqIEBwYXJhbSBuYW1lIGNsaWVudCBuYW1lXG4gICAgICovXG4gICAgc2V0Q2xpZW50TmFtZShuYW1lKSB7XG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIHNldCBzaXRlIHVybCBmb3IgYXBwIHJlZ2lzdHJhdGlvblxuICAgICAqIEBwYXJhbSBzaXRlIGNsaWVudCBzaXRlIFVSTFxuICAgICAqL1xuICAgIHNldENsaWVudFNpdGVVcmwoc2l0ZSkge1xuICAgICAgICB0aGlzLnNpdGUgPSBzaXRlO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBzZXQgY3JlZGVudGlhbHMgZm9yIE9BdXRoXG4gICAgICogQHBhcmFtIGNyZWRlbnRpYWxzXG4gICAgICovXG4gICAgc2V0Q3JlZGVudGlhbHMoY3JlZGVudGlhbHMpIHtcbiAgICAgICAgdGhpcy5jcmVkZW50aWFsID0ge1xuICAgICAgICAgICAgY2xpZW50SUQ6IGNyZWRlbnRpYWxzLmNsaWVudElELFxuICAgICAgICAgICAgY2xpZW50U2VjcmV0OiBjcmVkZW50aWFscy5jbGllbnRTZWNyZXQsXG4gICAgICAgICAgICBhY2Nlc3NUb2tlbjogY3JlZGVudGlhbHMuYWNjZXNzVG9rZW4sXG4gICAgICAgICAgICByZWZyZXNoVG9rZW46IGNyZWRlbnRpYWxzLnJlZnJlc2hUb2tlbixcbiAgICAgICAgfTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogc2V0IHNjb3BlcyBmb3IgYXBwIHJlZ2lzdHJhdGlvblxuICAgICAqIEBwYXJhbSBzY29wZXNcbiAgICAgKi9cbiAgICBzZXRTY29wZXMoc2NvcGVzKSB7XG4gICAgICAgIHRoaXMuc2NvcGVzID0gc2NvcGVzO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBzZXQgVVJMcyBmb3IgZmV0Y2hcbiAgICAgKiBAcGFyYW0gYmFzZVxuICAgICAqIEBwYXJhbSBhcGlcbiAgICAgKiBAcGFyYW0gc3RyZWFtXG4gICAgICovXG4gICAgc2V0VVJMcyh7IGJhc2UsIGFwaSwgc3RyZWFtIH0pIHtcbiAgICAgICAgdGhpcy51cmwgPSB7XG4gICAgICAgICAgICBiYXNlOiBiYXNlLFxuICAgICAgICAgICAgYXBpOiBhcGkgfHwgYmFzZSxcbiAgICAgICAgICAgIHN0cmVhbTogc3RyZWFtIHx8IGJhc2UsXG4gICAgICAgIH07XG4gICAgfVxuICAgIC8qKlxuICAgICAqIGV4cG9ydCBjbGllbnQgY29uZmlnXG4gICAgICogQHJldHVybnMge3tuYW1lOiBzdHJpbmcsIHNpdGU6IHN0cmluZywgc2NvcGVzOiBbc3RyaW5nXSwgdXJsOiBNYXN0b2Rvbi5VUkxzLCBjcmVkZW50aWFsOiBNYXN0b2Rvbi5DcmVkZW50aWFsc319XG4gICAgICovXG4gICAgZXhwb3J0Q29uZmlnKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmFtZTogdGhpcy5uYW1lLFxuICAgICAgICAgICAgc2l0ZTogdGhpcy5zaXRlLFxuICAgICAgICAgICAgc2NvcGVzOiB0aGlzLnNjb3BlcyxcbiAgICAgICAgICAgIHVybDoge1xuICAgICAgICAgICAgICAgIGJhc2U6IHRoaXMudXJsLmJhc2UsXG4gICAgICAgICAgICAgICAgYXBpOiB0aGlzLnVybC5hcGksXG4gICAgICAgICAgICAgICAgc3RyZWFtOiB0aGlzLnVybC5zdHJlYW0sXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY3JlZGVudGlhbDoge1xuICAgICAgICAgICAgICAgIGNsaWVudElEOiB0aGlzLmNyZWRlbnRpYWwuY2xpZW50SUQsXG4gICAgICAgICAgICAgICAgY2xpZW50U2VjcmV0OiB0aGlzLmNyZWRlbnRpYWwuY2xpZW50U2VjcmV0LFxuICAgICAgICAgICAgICAgIGFjY2Vzc1Rva2VuOiB0aGlzLmNyZWRlbnRpYWwuYWNjZXNzVG9rZW4sXG4gICAgICAgICAgICAgICAgcmVmcmVzaFRva2VuOiB0aGlzLmNyZWRlbnRpYWwucmVmcmVzaFRva2VuLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgLypcbiAgICAgKiBBdXRoXG4gICAgICovXG4gICAgLyoqXG4gICAgICogUmVnaXN0ZXJpbmcgYW4gYXBwbGljYXRpb246XG4gICAgICpcbiAgICAgKiBQT1NUIC9hcGkvdjEvYXBwc1xuICAgICAqXG4gICAgICogRm9ybSBkYXRhOlxuICAgICAqXG4gICAgICogY2xpZW50X25hbWU6XG4gICAgICogICBOYW1lIG9mIHlvdXIgYXBwbGljYXRpb25cbiAgICAgKiByZWRpcmVjdF91cmlzOlxuICAgICAqICAgV2hlcmUgdGhlIHVzZXIgc2hvdWxkIGJlIHJlZGlyZWN0ZWQgYWZ0ZXIgYXV0aG9yaXphdGlvblxuICAgICAqICAgKGZvciBubyByZWRpcmVjdCwgdXNlIHVybjppZXRmOndnOm9hdXRoOjIuMDpvb2IpXG4gICAgICogc2NvcGVzOlxuICAgICAqICAgVGhpcyBjYW4gYmUgYSBzcGFjZS1zZXBhcmF0ZWQgbGlzdCBvZiB0aGUgZm9sbG93aW5nIGl0ZW1zOiBcInJlYWRcIiwgXCJ3cml0ZVwiIGFuZCBcImZvbGxvd1wiXG4gICAgICogICAoc2VlIHRoaXMgcGFnZSBmb3IgZGV0YWlscyBvbiB3aGF0IHRoZSBzY29wZXMgZG8pXG4gICAgICogICB3ZWJzaXRlOiAob3B0aW9uYWwpXG4gICAgICogICBVUkwgdG8gdGhlIGhvbWVwYWdlIG9mIHlvdXIgYXBwXG4gICAgICpcbiAgICAgKiBDcmVhdGVzIGEgbmV3IE9BdXRoIGFwcC4gUmV0dXJucyBpZCwgY2xpZW50X2lkIGFuZCBjbGllbnRfc2VjcmV0IHdoaWNoIGNhbiBiZSB1c2VkXG4gICAgICogd2l0aCBPQXV0aCBhdXRoZW50aWNhdGlvbiBpbiB5b3VyIDNyZCBwYXJ0eSBhcHAuXG4gICAgICpcbiAgICAgKiBUaGVzZSB2YWx1ZXMgc2hvdWxkIGJlIHJlcXVlc3RlZCBpbiB0aGUgYXBwIGl0c2VsZiBmcm9tIHRoZSBBUEkgZm9yIGVhY2hcbiAgICAgKiBuZXcgYXBwIGluc3RhbGwgKyBtYXN0b2RvbiBkb21haW4gY29tYm8sIGFuZCBzdG9yZWQgaW4gdGhlIGFwcCBmb3IgZnV0dXJlIHJlcXVlc3RzLlxuICAgICAqXG4gICAgICogQHJldHVybnMge1Byb21pc2U8Unt9Pn1cbiAgICAgKi9cbiAgICBhc3luYyByZWdpc3RlckFwcCgpIHtcbiAgICAgICAgY29uc3QgYXBpID0gdGhpcy5fZ2V0QXBpKCdhcHBzJyk7XG4gICAgICAgIGNvbnN0IG1ldGhvZCA9ICdQT1NUJztcbiAgICAgICAgY29uc3QgYm9keSA9IG5ldyBVUkxTZWFyY2hQYXJhbXMoKTtcbiAgICAgICAgYm9keS5zZXQoJ2NsaWVudF9uYW1lJywgdGhpcy5uYW1lKTtcbiAgICAgICAgYm9keS5zZXQoJ3JlZGlyZWN0X3VyaXMnLCB0aGlzLmNhbGxiYWNrKTtcbiAgICAgICAgYm9keS5zZXQoJ3Njb3BlcycsIHRoaXMuc2NvcGVzLmpvaW4oJyAnKSk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fZmV0Y2hKc29uKGFwaSwgeyBtZXRob2QsIGJvZHkgfSk7XG4gICAgICAgIGlmIChyZXNwb25zZS5pc1N1Y2Nlc3MoKSkge1xuICAgICAgICAgICAgY29uc3QgeyBjbGllbnRfaWQsIGNsaWVudF9zZWNyZXQgfSA9IHJlc3BvbnNlLnZhbHVlLmpzb247XG4gICAgICAgICAgICB0aGlzLnNldENyZWRlbnRpYWxzKHtcbiAgICAgICAgICAgICAgICBjbGllbnRJRDogY2xpZW50X2lkLFxuICAgICAgICAgICAgICAgIGNsaWVudFNlY3JldDogY2xpZW50X3NlY3JldCxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogZ2V0IE9BdXRoIGF1dGhvcml6aW5nIFVSTFxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAgICovXG4gICAgZ2V0QXV0aG9yaXplVVJMKCkge1xuICAgICAgICBjb25zdCB7IGNsaWVudElELCBjbGllbnRTZWNyZXQgfSA9IHRoaXMuZXhwb3J0Q29uZmlnKCkuY3JlZGVudGlhbDtcbiAgICAgICAgY29uc3Qgb2F1dGggPSBuZXcgT0F1dGgyKGNsaWVudElELCBjbGllbnRTZWNyZXQsIHRoaXMuX2dldEhUVFBTKCksIG51bGwsICcvb2F1dGgvdG9rZW4nKTtcbiAgICAgICAgcmV0dXJuIG9hdXRoLmdldEF1dGhvcml6ZVVybCh7XG4gICAgICAgICAgICByZWRpcmVjdF91cmk6IHRoaXMuY2FsbGJhY2ssXG4gICAgICAgICAgICByZXNwb25zZV90eXBlOiAnY29kZScsXG4gICAgICAgICAgICBzY29wZTogdGhpcy5zY29wZXMuam9pbignICcpLFxuICAgICAgICB9KTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogc2VuZCBPQXV0aCBjb2RlXG4gICAgICogQHBhcmFtIGNvZGVcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxSe30+fVxuICAgICAqL1xuICAgIGFzeW5jIGF1dGhlbnRpY2F0ZShjb2RlKSB7XG4gICAgICAgIGNvbnN0IHsgY2xpZW50SUQsIGNsaWVudFNlY3JldCB9ID0gdGhpcy5leHBvcnRDb25maWcoKS5jcmVkZW50aWFsO1xuICAgICAgICBjb25zdCBvYXV0aCA9IG5ldyBPQXV0aDIoY2xpZW50SUQsIGNsaWVudFNlY3JldCwgdGhpcy5fZ2V0SFRUUFMoKSwgbnVsbCwgJy9vYXV0aC90b2tlbicpO1xuICAgICAgICBjb25zdCBhdXRoID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgb2F1dGguZ2V0T0F1dGhBY2Nlc3NUb2tlbihjb2RlLCB7XG4gICAgICAgICAgICAgICAgZ3JhbnRfdHlwZTogJ2F1dGhvcml6YXRpb25fY29kZScsXG4gICAgICAgICAgICAgICAgcmVkaXJlY3RfdXJpOiB0aGlzLmNhbGxiYWNrLFxuICAgICAgICAgICAgfSwgKGVyciwgYWNjZXNzVG9rZW4sIHJlZnJlc2hUb2tlbiwgcmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh7IGFjY2Vzc1Rva2VuLCByZWZyZXNoVG9rZW4sIHJlc3BvbnNlIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHsgYWNjZXNzVG9rZW4sIHJlZnJlc2hUb2tlbiwgcmVzcG9uc2UgfSA9IGF3YWl0IGF1dGg7XG4gICAgICAgICAgICB0aGlzLnNldENyZWRlbnRpYWxzKHsgY2xpZW50SUQsIGNsaWVudFNlY3JldCwgYWNjZXNzVG9rZW4sIHJlZnJlc2hUb2tlbiB9KTtcbiAgICAgICAgICAgIHJldHVybiBSLnN1Y2Nlc3MoeyBhY2Nlc3NUb2tlbiwgcmVmcmVzaFRva2VuLCByZXNwb25zZSB9KTtcbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICAgIHJldHVybiBSLmNyZWF0ZShSLlR5cGUubmV0d29ya0Vycm9yLCBlcnJvcik7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLypcbiAgICAgKiBBY2NvdW50c1xuICAgICAqL1xuICAgIC8vIEZldGNoaW5nIGFuIGFjY291bnQ6XG4gICAgLy9cbiAgICAvLyBHRVQgL2FwaS92MS9hY2NvdW50cy86aWRcbiAgICAvL1xuICAgIC8vIFJldHVybnMgYW4gQWNjb3VudC5cbiAgICBhc3luYyBnZXRBY2NvdW50KGlkKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnYWNjb3VudHMnLCBpZCk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwpO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IGFjY291bnQgPSBuZXcgQWNjb3VudChyZXNwb25zZS52YWx1ZS5qc29uKTtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmNvcHkoeyBhY2NvdW50IH0pO1xuICAgIH1cbiAgICAvLyBHZXR0aW5nIHRoZSBjdXJyZW50IHVzZXI6XG4gICAgLy9cbiAgICAvLyBHRVQgL2FwaS92MS9hY2NvdW50cy92ZXJpZnlfY3JlZGVudGlhbHNcbiAgICAvL1xuICAgIC8vIFJldHVybnMgdGhlIGF1dGhlbnRpY2F0ZWQgdXNlcidzIEFjY291bnQuXG4gICAgYXN5bmMgZ2V0Q3VycmVudEFjY291bnQoKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnYWNjb3VudHMnLCBudWxsLCAndmVyaWZ5X2NyZWRlbnRpYWxzJyk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwpO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IGFjY291bnQgPSBuZXcgQWNjb3VudChyZXNwb25zZS52YWx1ZS5qc29uKTtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmNvcHkoeyBhY2NvdW50IH0pO1xuICAgIH1cbiAgICAvLyAgIFVwZGF0aW5nIHRoZSBjdXJyZW50IHVzZXI6XG4gICAgLy9cbiAgICAvLyAgIFBBVENIIC9hcGkvdjEvYWNjb3VudHMvdXBkYXRlX2NyZWRlbnRpYWxzXG4gICAgLy9cbiAgICAvLyAgIEZvcm0gZGF0YTpcbiAgICAvL1xuICAgIC8vICAgZGlzcGxheV9uYW1lOiBUaGUgbmFtZSB0byBkaXNwbGF5IGluIHRoZSB1c2VyJ3MgcHJvZmlsZVxuICAgIC8vICAgbm90ZTogQSBuZXcgYmlvZ3JhcGh5IGZvciB0aGUgdXNlclxuICAgIC8vICAgYXZhdGFyOiBBIGJhc2U2NCBlbmNvZGVkIGltYWdlIHRvIGRpc3BsYXkgYXMgdGhlIHVzZXIncyBhdmF0YXIgKGUuZy4gZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFVb0FBQURyQ0FZQUFBQS4uLilcbiAgICAvLyAgIGhlYWRlcjogQSBiYXNlNjQgZW5jb2RlZCBpbWFnZSB0byBkaXNwbGF5IGFzIHRoZSB1c2VyJ3MgaGVhZGVyIGltYWdlIChlLmcuIGRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBVW9BQUFEckNBWUFBQUEuLi4pXG4gICAgYXN5bmMgdXBkYXRlQ3VycmVudEFjY291bnQoeyBkaXNwbGF5TmFtZSA9IG51bGwsIG5vdGUgPSBudWxsLCBhdmF0YXIgPSBudWxsLCBoZWFkZXIgPSBudWxsIH0gPSB7fSkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ2FjY291bnRzJywgbnVsbCwgJ3VwZGF0ZV9jcmVkZW50aWFscycpO1xuICAgICAgICBjb25zdCBtZXRob2QgPSAnUEFUQ0gnO1xuICAgICAgICBjb25zdCBib2R5ID0gbmV3IFVSTFNlYXJjaFBhcmFtcygpO1xuICAgICAgICBpZiAoZGlzcGxheU5hbWUpXG4gICAgICAgICAgICBib2R5LnNldCgnZGlzcGxheV9uYW1lJywgZGlzcGxheU5hbWUpO1xuICAgICAgICBpZiAobm90ZSlcbiAgICAgICAgICAgIGJvZHkuc2V0KCdub3RlJywgbm90ZSk7XG4gICAgICAgIGlmIChhdmF0YXIpXG4gICAgICAgICAgICBib2R5LnNldCgnYXZhdGFyJywgYXZhdGFyKTtcbiAgICAgICAgaWYgKGhlYWRlcilcbiAgICAgICAgICAgIGJvZHkuc2V0KCdoZWFkZXInLCBoZWFkZXIpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsLCB7IG1ldGhvZCwgYm9keSB9KTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBhY2NvdW50ID0gbmV3IEFjY291bnQocmVzcG9uc2UudmFsdWUuanNvbik7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgYWNjb3VudCB9KTtcbiAgICB9XG4gICAgLy8gICBHZXR0aW5nIGFuIGFjY291bnQncyBmb2xsb3dlcnM6XG4gICAgLy9cbiAgICAvLyAgIEdFVCAvYXBpL3YxL2FjY291bnRzLzppZC9mb2xsb3dlcnNcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhbiBhcnJheSBvZiBBY2NvdW50cy5cbiAgICBhc3luYyBnZXRGb2xsb3dlcnMoaWQpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdhY2NvdW50cycsIGlkLCAnZm9sbG93ZXJzJyk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwpO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IGFjY291bnRzID0gcmVzcG9uc2UudmFsdWUuanNvbi5tYXAodiA9PiBuZXcgQWNjb3VudCh2KSk7XG4gICAgICAgIGNvbnN0IGxpbmsgPSByZXNwb25zZS5saW5rKExpbmsuVHlwZS5hY2NvdW50cyk7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgbGluaywgYWNjb3VudHMgfSk7XG4gICAgfVxuICAgIC8vICAgR2V0dGluZyB3aG8gYWNjb3VudCBpcyBmb2xsb3dpbmc6XG4gICAgLy9cbiAgICAvLyAgIEdFVCAvYXBpL3YxL2FjY291bnRzLzppZC9mb2xsb3dpbmdcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhbiBhcnJheSBvZiBBY2NvdW50cy5cbiAgICBhc3luYyBnZXRGb2xsb3dpbmdzKGlkKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnYWNjb3VudHMnLCBpZCwgJ2ZvbGxvd2luZycpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBhY2NvdW50cyA9IHJlc3BvbnNlLnZhbHVlLmpzb24ubWFwKHYgPT4gbmV3IEFjY291bnQodikpO1xuICAgICAgICBjb25zdCBsaW5rID0gcmVzcG9uc2UubGluayhMaW5rLlR5cGUuYWNjb3VudHMpO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGxpbmssIGFjY291bnRzIH0pO1xuICAgIH1cbiAgICAvLyAgIEdldHRpbmcgYW4gYWNjb3VudCdzIHN0YXR1c2VzOlxuICAgIC8vXG4gICAgLy8gICBHRVQgL2FwaS92MS9hY2NvdW50cy86aWQvc3RhdHVzZXNcbiAgICAvL1xuICAgIC8vICAgUXVlcnkgcGFyYW1ldGVyczpcbiAgICAvL1xuICAgIC8vICAgb25seV9tZWRpYSAob3B0aW9uYWwpOiBPbmx5IHJldHVybiBzdGF0dXNlcyB0aGF0IGhhdmUgbWVkaWEgYXR0YWNobWVudHNcbiAgICAvLyAgIGV4Y2x1ZGVfcmVwbGllcyAob3B0aW9uYWwpOiBTa2lwIHN0YXR1c2VzIHRoYXQgcmVwbHkgdG8gb3RoZXIgc3RhdHVzZXNcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhbiBhcnJheSBvZiBTdGF0dXNlcy5cbiAgICBhc3luYyBnZXRTdGF0dXNlc09mQWNjb3VudChpZCwgcGFyYW1zID0ge1xuICAgICAgICAgICAgb25seU1lZGlhOiBmYWxzZSxcbiAgICAgICAgICAgIGV4Y2x1ZGVSZXBsaWVzOiB0cnVlXG4gICAgICAgIH0pIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdhY2NvdW50cycsIGlkLCAnc3RhdHVzZXMnLCB7XG4gICAgICAgICAgICBvbmx5X21lZGlhOiBwYXJhbXMub25seU1lZGlhLFxuICAgICAgICAgICAgZXhjbHVkZV9yZXBsaWVzOiBwYXJhbXMuZXhjbHVkZVJlcGxpZXMsXG4gICAgICAgIH0pO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBzdGF0dXNlcyA9IHJlc3BvbnNlLnZhbHVlLmpzb24ubWFwKHYgPT4gbmV3IFN0YXR1cyh2KSk7XG4gICAgICAgIGNvbnN0IGxpbmsgPSByZXNwb25zZS5saW5rKExpbmsuVHlwZS5zdGF0dXNlcyk7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgbGluaywgc3RhdHVzZXMgfSk7XG4gICAgfVxuICAgIC8vICAgRm9sbG93aW5nIGFuIGFjY291bnQ6XG4gICAgLy9cbiAgICAvLyAgIFBPU1QgL2FwaS92MS9hY2NvdW50cy86aWQvZm9sbG93XG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgdGhlIHRhcmdldCBhY2NvdW50J3MgUmVsYXRpb25zaGlwLlxuICAgIGFzeW5jIGZvbGxvd0FjY291bnQoaWQpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdhY2NvdW50cycsIGlkLCAnZm9sbG93Jyk7XG4gICAgICAgIGNvbnN0IG1ldGhvZCA9ICdQT1NUJztcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCB0aGlzLl9hdXRoRmV0Y2hKc29uKHVybCwgeyBtZXRob2QgfSk7XG4gICAgICAgIGlmIChyZXNwb25zZS5pc0ZhaWx1cmUoKSlcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgY29uc3QgcmVsYXRpb25zaGlwID0gbmV3IFJlbGF0aW9uc2hpcChyZXNwb25zZS52YWx1ZS5qc29uKTtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmNvcHkoeyByZWxhdGlvbnNoaXAgfSk7XG4gICAgfVxuICAgIC8vICAgVW5mb2xsb3dpbmcgYW4gYWNjb3VudDpcbiAgICAvL1xuICAgIC8vICAgUE9TVCAvYXBpL3YxL2FjY291bnRzLzppZC91bmZvbGxvd1xuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIHRoZSB0YXJnZXQgYWNjb3VudCdzIFJlbGF0aW9uc2hpcC5cbiAgICBhc3luYyB1bmZvbGxvd0FjY291bnQoaWQpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdhY2NvdW50cycsIGlkLCAndW5mb2xsb3cnKTtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ1BPU1QnO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsLCB7IG1ldGhvZCB9KTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCByZWxhdGlvbnNoaXAgPSBuZXcgUmVsYXRpb25zaGlwKHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IHJlbGF0aW9uc2hpcCB9KTtcbiAgICB9XG4gICAgLy8gICBCbG9ja2luZyBhbiBhY2NvdW50OlxuICAgIC8vXG4gICAgLy8gICBQT1NUIC9hcGkvdjEvYWNjb3VudHMvOmlkL2Jsb2NrXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgdGhlIHRhcmdldCBhY2NvdW50J3MgUmVsYXRpb25zaGlwLlxuICAgIGFzeW5jIGJsb2NrQWNjb3VudChpZCkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ2FjY291bnRzJywgaWQsICdibG9jaycpO1xuICAgICAgICBjb25zdCBtZXRob2QgPSAnUE9TVCc7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwsIHsgbWV0aG9kIH0pO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IHJlbGF0aW9uc2hpcCA9IG5ldyBSZWxhdGlvbnNoaXAocmVzcG9uc2UudmFsdWUuanNvbik7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgcmVsYXRpb25zaGlwIH0pO1xuICAgIH1cbiAgICAvLyAgIFVuYmxvY2tpbmcgYW4gYWNjb3VudDpcbiAgICAvL1xuICAgIC8vICAgUE9TVCAvYXBpL3YxL2FjY291bnRzLzppZC91bmJsb2NrXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgdGhlIHRhcmdldCBhY2NvdW50J3MgUmVsYXRpb25zaGlwLlxuICAgIGFzeW5jIHVuYmxvY2tBY2NvdW50KGlkKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnYWNjb3VudHMnLCBpZCwgJ3VuYmxvY2snKTtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ1BPU1QnO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsLCB7IG1ldGhvZCB9KTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCByZWxhdGlvbnNoaXAgPSBuZXcgUmVsYXRpb25zaGlwKHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IHJlbGF0aW9uc2hpcCB9KTtcbiAgICB9XG4gICAgLy8gICBNdXRpbmcgYW4gYWNjb3VudDpcbiAgICAvL1xuICAgIC8vICAgUE9TVCAvYXBpL3YxL2FjY291bnRzLzppZC9tdXRlXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgdGhlIHRhcmdldCBhY2NvdW50J3MgUmVsYXRpb25zaGlwLlxuICAgIGFzeW5jIG11dGVBY2NvdW50KGlkKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnYWNjb3VudHMnLCBpZCwgJ211dGUnKTtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ1BPU1QnO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsLCB7IG1ldGhvZCB9KTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCByZWxhdGlvbnNoaXAgPSBuZXcgUmVsYXRpb25zaGlwKHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IHJlbGF0aW9uc2hpcCB9KTtcbiAgICB9XG4gICAgLy8gICBVbm11dGluZyBhbiBhY2NvdW50OlxuICAgIC8vXG4gICAgLy8gICBQT1NUIC9hcGkvdjEvYWNjb3VudHMvOmlkL3VubXV0ZVxuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIHRoZSB0YXJnZXQgYWNjb3VudCdzIFJlbGF0aW9uc2hpcC5cbiAgICBhc3luYyB1bm11dGVBY2NvdW50KGlkKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnYWNjb3VudHMnLCBpZCwgJ3VubXV0ZScpO1xuICAgICAgICBjb25zdCBtZXRob2QgPSAnUE9TVCc7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwsIHsgbWV0aG9kIH0pO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IHJlbGF0aW9uc2hpcCA9IG5ldyBSZWxhdGlvbnNoaXAocmVzcG9uc2UudmFsdWUuanNvbik7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgcmVsYXRpb25zaGlwIH0pO1xuICAgIH1cbiAgICAvLyAgIEdldHRpbmcgYW4gYWNjb3VudCdzIHJlbGF0aW9uc2hpcHM6XG4gICAgLy9cbiAgICAvLyAgIEdFVCAvYXBpL3YxL2FjY291bnRzL3JlbGF0aW9uc2hpcHNcbiAgICAvL1xuICAgIC8vICAgUXVlcnkgcGFyYW1ldGVyczpcbiAgICAvL1xuICAgIC8vICAgaWQgKGNhbiBiZSBhcnJheSk6IEFjY291bnQgSURzXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgYW4gYXJyYXkgb2YgUmVsYXRpb25zaGlwcyBvZiB0aGUgY3VycmVudCB1c2VyIHRvIGEgbGlzdCBvZiBnaXZlbiBhY2NvdW50cy5cbiAgICBhc3luYyBnZXRSZWxhdGlvbnNoaXBzKGlkcykge1xuICAgICAgICBjb25zdCB0YXJnZXRzID0gQXJyYXkuaXNBcnJheShpZHMpID8gaWRzIDogW2lkc107XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnYWNjb3VudHMnLCBudWxsLCAncmVsYXRpb25zaGlwcycsIHsgaWQ6IHRhcmdldHMgfSk7XG4gICAgICAgIC8vIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnYWNjb3VudHMnLCBudWxsLCBgcmVsYXRpb25zaGlwcz9pZFtdPSR7aWRzWzBdfSZpZFtdPSR7aWRzWzFdfWAsIHtpZDogdGFyZ2V0c30pXG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwpO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IHJlbGF0aW9uc2hpcHMgPSByZXNwb25zZS52YWx1ZS5qc29uLm1hcCh2ID0+IG5ldyBSZWxhdGlvbnNoaXAodikpO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IHJlbGF0aW9uc2hpcHMgfSk7XG4gICAgfVxuICAgIC8vICAgU2VhcmNoaW5nIGZvciBhY2NvdW50czpcbiAgICAvL1xuICAgIC8vICAgR0VUIC9hcGkvdjEvYWNjb3VudHMvc2VhcmNoXG4gICAgLy9cbiAgICAvLyAgIFF1ZXJ5IHBhcmFtZXRlcnM6XG4gICAgLy9cbiAgICAvLyAgIHE6IFdoYXQgdG8gc2VhcmNoIGZvclxuICAgIC8vICAgbGltaXQ6IE1heGltdW0gbnVtYmVyIG9mIG1hdGNoaW5nIGFjY291bnRzIHRvIHJldHVybiAoZGVmYXVsdDogNDApXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgYW4gYXJyYXkgb2YgbWF0Y2hpbmcgQWNjb3VudHMuXG4gICAgLy8gICBXaWxsIGxvb2t1cCBhbiBhY2NvdW50IHJlbW90ZWx5IGlmIHRoZSBzZWFyY2ggdGVybSBpc1xuICAgIC8vICAgaW4gdGhlIHVzZXJuYW1lQGRvbWFpbiBmb3JtYXQgYW5kIG5vdCB5ZXQgaW4gdGhlIGRhdGFiYXNlLlxuICAgIGFzeW5jIHNlYXJjaEFjY291bnRzKHF1ZXJ5LCBsaW1pdCA9IDQwKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnYWNjb3VudHMnLCBudWxsLCAnc2VhcmNoJywgeyBxOiBxdWVyeSwgbGltaXQgfSk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwpO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IGFjY291bnRzID0gcmVzcG9uc2UudmFsdWUuanNvbi5tYXAodiA9PiBuZXcgQWNjb3VudCh2KSk7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgYWNjb3VudHMgfSk7XG4gICAgfVxuICAgIC8qXG4gICAgKiBCbG9ja3NcbiAgICAqL1xuICAgIC8vICAgRmV0Y2hpbmcgYSB1c2VyJ3MgYmxvY2tzOlxuICAgIC8vXG4gICAgLy8gICBHRVQgL2FwaS92MS9ibG9ja3NcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhbiBhcnJheSBvZiBBY2NvdW50cyBibG9ja2VkIGJ5IHRoZSBhdXRoZW50aWNhdGVkIHVzZXIuXG4gICAgYXN5bmMgZ2V0QmxvY2tzKCkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ2Jsb2NrcycpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBhY2NvdW50cyA9IHJlc3BvbnNlLnZhbHVlLmpzb24ubWFwKHYgPT4gbmV3IEFjY291bnQodikpO1xuICAgICAgICBjb25zdCBsaW5rID0gcmVzcG9uc2UubGluayhMaW5rLlR5cGUuYWNjb3VudHMpO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGxpbmssIGFjY291bnRzIH0pO1xuICAgIH1cbiAgICAvKlxuICAgICAqIEZhdm91cml0ZXNcbiAgICAgKi9cbiAgICAvLyAgIEZldGNoaW5nIGEgdXNlcidzIGZhdm91cml0ZXM6XG4gICAgLy9cbiAgICAvLyAgIEdFVCAvYXBpL3YxL2Zhdm91cml0ZXNcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhbiBhcnJheSBvZiBTdGF0dXNlcyBmYXZvdXJpdGVkIGJ5IHRoZSBhdXRoZW50aWNhdGVkIHVzZXIuXG4gICAgYXN5bmMgZ2V0RmF2b3JpdGVzKCkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ2Zhdm91cml0ZXMnKTtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCB0aGlzLl9hdXRoRmV0Y2hKc29uKHVybCk7XG4gICAgICAgIGlmIChyZXNwb25zZS5pc0ZhaWx1cmUoKSlcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgY29uc3Qgc3RhdHVzZXMgPSByZXNwb25zZS52YWx1ZS5qc29uLm1hcCh2ID0+IG5ldyBTdGF0dXModikpO1xuICAgICAgICBjb25zdCBsaW5rID0gcmVzcG9uc2UubGluayhMaW5rLlR5cGUuc3RhdHVzZXMpO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGxpbmssIHN0YXR1c2VzIH0pO1xuICAgIH1cbiAgICAvKlxuICAgICAqIEZvbGxvdyBSZXF1ZXN0c1xuICAgICAqL1xuICAgIC8vICAgRmV0Y2hpbmcgYSBsaXN0IG9mIGZvbGxvdyByZXF1ZXN0czpcbiAgICAvL1xuICAgIC8vICAgR0VUIC9hcGkvdjEvZm9sbG93X3JlcXVlc3RzXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgYW4gYXJyYXkgb2YgQWNjb3VudHMgd2hpY2ggaGF2ZSByZXF1ZXN0ZWQgdG8gZm9sbG93IHRoZSBhdXRoZW50aWNhdGVkIHVzZXIuXG4gICAgYXN5bmMgZ2V0Rm9sbG93UmVxdWVzdHMoKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnZm9sbG93X3JlcXVlc3RzJyk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwpO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IGFjY291bnRzID0gcmVzcG9uc2UudmFsdWUuanNvbi5tYXAodiA9PiBuZXcgQWNjb3VudCh2KSk7XG4gICAgICAgIGNvbnN0IGxpbmsgPSByZXNwb25zZS5saW5rKExpbmsuVHlwZS5hY2NvdW50cyk7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgbGluaywgYWNjb3VudHMgfSk7XG4gICAgfVxuICAgIC8vICAgQXV0aG9yaXppbmcgZm9sbG93IHJlcXVlc3RzOlxuICAgIC8vXG4gICAgLy8gICBQT1NUIC9hcGkvdjEvZm9sbG93X3JlcXVlc3RzLzppZC9hdXRob3JpemVcbiAgICAvL1xuICAgIC8vICAgRm9ybSBkYXRhOlxuICAgIC8vXG4gICAgLy8gICBpZDogVGhlIGlkIG9mIHRoZSBhY2NvdW50IHRvIGF1dGhvcml6ZSBvciByZWplY3RcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhbiBlbXB0eSBvYmplY3QuXG4gICAgYXN5bmMgYXV0aG9yaXplRm9sbG93UmVxdWVzdChpZCkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ2ZvbGxvd19yZXF1ZXN0cycsIGlkLCAnYXV0aG9yaXplJyk7XG4gICAgICAgIGNvbnN0IG1ldGhvZCA9ICdQT1NUJztcbiAgICAgICAgY29uc3QgYm9keSA9IG5ldyBGb3JtRGF0YSgpO1xuICAgICAgICBib2R5LmFwcGVuZCgnaWQnLCBTdHJpbmcoaWQpKTtcbiAgICAgICAgcmV0dXJuIGF3YWl0IHRoaXMuX2F1dGhGZXRjaCh1cmwsIHsgbWV0aG9kLCBib2R5IH0pO1xuICAgIH1cbiAgICAvLyAgIFJlamVjdGluZyBmb2xsb3cgcmVxdWVzdHM6XG4gICAgLy9cbiAgICAvLyAgIFBPU1QgL2FwaS92MS9mb2xsb3dfcmVxdWVzdHMvOmlkL3JlamVjdFxuICAgIC8vXG4gICAgLy8gICBGb3JtIGRhdGE6XG4gICAgLy9cbiAgICAvLyAgIGlkOiBUaGUgaWQgb2YgdGhlIGFjY291bnQgdG8gYXV0aG9yaXplIG9yIHJlamVjdFxuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIGFuIGVtcHR5IG9iamVjdC5cbiAgICBhc3luYyByZWplY3RGb2xsb3dSZXF1ZXN0KGlkKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnZm9sbG93X3JlcXVlc3RzJywgaWQsICdyZWplY3QnKTtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ1BPU1QnO1xuICAgICAgICBjb25zdCBib2R5ID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgICAgIGJvZHkuYXBwZW5kKCdpZCcsIFN0cmluZyhpZCkpO1xuICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy5fYXV0aEZldGNoKHVybCwgeyBtZXRob2QsIGJvZHkgfSk7XG4gICAgfVxuICAgIC8qXG4gICAgICogRm9sbG93c1xuICAgICAqL1xuICAgIC8vICAgRm9sbG93aW5nIGEgcmVtb3RlIHVzZXI6XG4gICAgLy9cbiAgICAvLyAgIFBPU1QgL2FwaS92MS9mb2xsb3dzXG4gICAgLy9cbiAgICAvLyAgIEZvcm0gZGF0YTpcbiAgICAvL1xuICAgIC8vICAgdXJpOiB1c2VybmFtZUBkb21haW4gb2YgdGhlIHBlcnNvbiB5b3Ugd2FudCB0byBmb2xsb3dcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyB0aGUgbG9jYWwgcmVwcmVzZW50YXRpb24gb2YgdGhlIGZvbGxvd2VkIGFjY291bnQsIGFzIGFuIEFjY291bnQuXG4gICAgYXN5bmMgZm9sbG93UmVtb3RlVXNlcih1cmkpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdmb2xsb3dzJyk7XG4gICAgICAgIGNvbnN0IG1ldGhvZCA9ICdQT1NUJztcbiAgICAgICAgY29uc3QgYm9keSA9IG5ldyBGb3JtRGF0YSgpO1xuICAgICAgICBib2R5LmFwcGVuZCgndXJpJywgdXJpKTtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCB0aGlzLl9hdXRoRmV0Y2hKc29uKHVybCwgeyBtZXRob2QsIGJvZHkgfSk7XG4gICAgICAgIGlmIChyZXNwb25zZS5pc0ZhaWx1cmUoKSlcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgY29uc3QgYWNjb3VudCA9IG5ldyBBY2NvdW50KHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGFjY291bnQgfSk7XG4gICAgfVxuICAgIC8qXG4gICAgICogSW5zdGFuY2VzXG4gICAgICovXG4gICAgLy8gICBHZXR0aW5nIGluc3RhbmNlIGluZm9ybWF0aW9uOlxuICAgIC8vXG4gICAgLy8gICBHRVQgL2FwaS92MS9pbnN0YW5jZVxuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIHRoZSBjdXJyZW50IEluc3RhbmNlLiBEb2VzIG5vdCByZXF1aXJlIGF1dGhlbnRpY2F0aW9uLlxuICAgIGFzeW5jIGdldEluc3RhbmNlSW5mbygpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdpbnN0YW5jZScpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2ZldGNoSnNvbih1cmwpO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IGluc3RhbmNlID0gbmV3IEluc3RhbmNlKHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGluc3RhbmNlIH0pO1xuICAgIH1cbiAgICAvKlxuICAgICogTWVkaWFcbiAgICAqL1xuICAgIC8vICAgVXBsb2FkaW5nIGEgbWVkaWEgYXR0YWNobWVudDpcbiAgICAvL1xuICAgIC8vICAgUE9TVCAvYXBpL3YxL21lZGlhXG4gICAgLy9cbiAgICAvLyAgIEZvcm0gZGF0YTpcbiAgICAvL1xuICAgIC8vICAgZmlsZTogTWVkaWEgdG8gYmUgdXBsb2FkZWRcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhbiBBdHRhY2htZW50IHRoYXQgY2FuIGJlIHVzZWQgd2hlbiBjcmVhdGluZyBhIHN0YXR1c2VzLlxuICAgIGFzeW5jIHVwbG9hZE1lZGlhQXR0YWNobWVudChmaWxlKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnbWVkaWEnKTtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ1BPU1QnO1xuICAgICAgICBjb25zdCBib2R5ID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgICAgIGJvZHkuYXBwZW5kKCdmaWxlJywgZmlsZSk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwsIHsgbWV0aG9kLCBib2R5IH0pO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IGF0dGFjaG1lbnQgPSBuZXcgQXR0YWNobWVudChyZXNwb25zZS52YWx1ZS5qc29uKTtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmNvcHkoeyBhdHRhY2htZW50IH0pO1xuICAgIH1cbiAgICAvKlxuICAgICAqIE11dGVzXG4gICAgICovXG4gICAgLy8gICBGZXRjaGluZyBhIHVzZXIncyBtdXRlczpcbiAgICAvL1xuICAgIC8vICAgR0VUIC9hcGkvdjEvbXV0ZXNcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhbiBhcnJheSBvZiBBY2NvdW50cyBtdXRlZCBieSB0aGUgYXV0aGVudGljYXRlZCB1c2VyLlxuICAgIGFzeW5jIGdldE11dGVzKCkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ211dGVzJyk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwpO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IGFjY291bnRzID0gcmVzcG9uc2UudmFsdWUuanNvbi5tYXAodiA9PiBuZXcgQWNjb3VudCh2KSk7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgYWNjb3VudHMgfSk7XG4gICAgfVxuICAgIC8qXG4gICAgICogTm90aWZpY2F0aW9uc1xuICAgICAqL1xuICAgIC8vICAgRmV0Y2hpbmcgYSB1c2VyJ3Mgbm90aWZpY2F0aW9uczpcbiAgICAvL1xuICAgIC8vICAgR0VUIC9hcGkvdjEvbm90aWZpY2F0aW9uc1xuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIGEgbGlzdCBvZiBOb3RpZmljYXRpb25zIGZvciB0aGUgYXV0aGVudGljYXRlZCB1c2VyLlxuICAgIGFzeW5jIGdldE5vdGlmaWNhdGlvbnMoKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnbm90aWZpY2F0aW9ucycpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBub3RpZmljYXRpb25zID0gcmVzcG9uc2UudmFsdWUuanNvbi5tYXAodiA9PiBuZXcgTm90aWZpY2F0aW9uKHYpKTtcbiAgICAgICAgY29uc3QgbGluayA9IHJlc3BvbnNlLmxpbmsoTGluay5UeXBlLm5vdGlmaWNhdGlvbnMpO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGxpbmssIG5vdGlmaWNhdGlvbnMgfSk7XG4gICAgfVxuICAgIC8vICAgR2V0dGluZyBhIHNpbmdsZSBub3RpZmljYXRpb246XG4gICAgLy9cbiAgICAvLyAgIEdFVCAvYXBpL3YxL25vdGlmaWNhdGlvbnMvOmlkXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgdGhlIE5vdGlmaWNhdGlvbi5cbiAgICBhc3luYyBnZXROb3RpZmljYXRpb24oaWQpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdub3RpZmljYXRpb25zJywgaWQpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBub3RpZmljYXRpb24gPSBuZXcgTm90aWZpY2F0aW9uKHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IG5vdGlmaWNhdGlvbiB9KTtcbiAgICB9XG4gICAgLy8gICBDbGVhcmluZyBub3RpZmljYXRpb25zOlxuICAgIC8vXG4gICAgLy8gICBQT1NUIC9hcGkvdjEvbm90aWZpY2F0aW9ucy9jbGVhclxuICAgIC8vXG4gICAgLy8gICBEZWxldGVzIGFsbCBub3RpZmljYXRpb25zIGZyb20gdGhlIE1hc3RvZG9uIHNlcnZlciBmb3IgdGhlIGF1dGhlbnRpY2F0ZWQgdXNlci4gUmV0dXJucyBhbiBlbXB0eSBvYmplY3QuXG4gICAgYXN5bmMgY2xlYXJOb3RpZmljYXRpb25zKCkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ25vdGlmaWNhdGlvbnMnLCAnY2xlYXInKTtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ1BPU1QnO1xuICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy5fYXV0aEZldGNoKHVybCwgeyBtZXRob2QgfSk7XG4gICAgfVxuICAgIC8qXG4gICAgICogUmVwb3J0c1xuICAgICAqL1xuICAgIC8vICAgRmV0Y2hpbmcgYSB1c2VyJ3MgcmVwb3J0czpcbiAgICAvL1xuICAgIC8vICAgR0VUIC9hcGkvdjEvcmVwb3J0c1xuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIGEgbGlzdCBvZiBSZXBvcnRzIG1hZGUgYnkgdGhlIGF1dGhlbnRpY2F0ZWQgdXNlci5cbiAgICBhc3luYyBmZXRjaFJlcG9ydHMoKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgncmVwb3J0cycpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCByZXBvcnRzID0gcmVzcG9uc2UudmFsdWUuanNvbi5tYXAoaiA9PiBuZXcgUmVwb3J0KGopKTtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmNvcHkoeyByZXBvcnRzIH0pO1xuICAgIH1cbiAgICAvLyAgIFJlcG9ydGluZyBhIHVzZXI6XG4gICAgLy9cbiAgICAvLyAgIFBPU1QgL2FwaS92MS9yZXBvcnRzXG4gICAgLy9cbiAgICAvLyAgIEZvcm0gZGF0YTpcbiAgICAvL1xuICAgIC8vICAgYWNjb3VudF9pZDogVGhlIElEIG9mIHRoZSBhY2NvdW50IHRvIHJlcG9ydFxuICAgIC8vICAgc3RhdHVzX2lkczogVGhlIElEcyBvZiBzdGF0dXNlcyB0byByZXBvcnQgKGNhbiBiZSBhbiBhcnJheSlcbiAgICAvLyAgIGNvbW1lbnQ6IEEgY29tbWVudCB0byBhc3NvY2lhdGUgd2l0aCB0aGUgcmVwb3J0LlxuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIHRoZSBmaW5pc2hlZCBSZXBvcnQuXG4gICAgYXN5bmMgcmVwb3J0VXNlcih7IGFjY291bnRJZCA9IG51bGwsIHN0YXR1c0lkcyA9IFtdLCBjb21tZW50ID0gbnVsbCB9ID0ge30pIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdyZXBvcnRzJyk7XG4gICAgICAgIGNvbnN0IG1ldGhvZCA9ICdQT1NUJztcbiAgICAgICAgY29uc3QgYm9keSA9IG5ldyBGb3JtRGF0YSgpO1xuICAgICAgICBib2R5LmFwcGVuZCgnYWNjb3VudF9pZCcsIGFjY291bnRJZCk7XG4gICAgICAgIGZvciAoY29uc3QgaWQgb2Ygc3RhdHVzSWRzKSB7XG4gICAgICAgICAgICBib2R5LmFwcGVuZCgnc3RhdHVzX2lkc1tdJywgaWQpO1xuICAgICAgICB9XG4gICAgICAgIGJvZHkuYXBwZW5kKCdjb21tZW50JywgY29tbWVudCk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwsIHsgbWV0aG9kLCBib2R5IH0pO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IHJlcG9ydCA9IG5ldyBSZXBvcnQocmVzcG9uc2UudmFsdWUuanNvbik7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgcmVwb3J0IH0pO1xuICAgIH1cbiAgICAvKlxuICAgICogU2VhcmNoXG4gICAgKi9cbiAgICAvLyAgIFNlYXJjaGluZyBmb3IgY29udGVudDpcbiAgICAvL1xuICAgIC8vICAgR0VUIC9hcGkvdjEvc2VhcmNoXG4gICAgLy9cbiAgICAvLyAgIEZvcm0gZGF0YTpcbiAgICAvL1xuICAgIC8vICAgcTogVGhlIHNlYXJjaCBxdWVyeVxuICAgIC8vICAgcmVzb2x2ZTogV2hldGhlciB0byByZXNvbHZlIG5vbi1sb2NhbCBhY2NvdW50c1xuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIFJlc3VsdHMuIElmIHEgaXMgYSBVUkwsIE1hc3RvZG9uIHdpbGwgYXR0ZW1wdCB0byBmZXRjaCB0aGUgcHJvdmlkZWQgYWNjb3VudFxuICAgIC8vIG9yIHN0YXR1c2VzLiBPdGhlcndpc2UsIGl0IHdpbGwgZG8gYSBsb2NhbCBhY2NvdW50IGFuZCBoYXNodGFnIHNlYXJjaC5cbiAgICBhc3luYyBzZWFyY2gocXVlcnksIHJlc29sdmVSZW1vdGVBY2NvdW50cyA9IGZhbHNlKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnc2VhcmNoJywgbnVsbCwgbnVsbCwgeyBxOiBxdWVyeSwgcmVzb2x2ZTogcmVzb2x2ZVJlbW90ZUFjY291bnRzIH0pO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCByZXN1bHQgPSBuZXcgUmVzdWx0KHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IHJlc3VsdCB9KTtcbiAgICB9XG4gICAgLypcbiAgICAqIFN0YXR1c2VzXG4gICAgKi9cbiAgICAvLyAgIEZldGNoaW5nIGEgc3RhdHVzOlxuICAgIC8vXG4gICAgLy8gICBHRVQgL2FwaS92MS9zdGF0dXNlcy86aWRcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhIFN0YXR1cy5cbiAgICBhc3luYyBmZXRjaFN0YXR1cyhpZCkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ3N0YXR1c2VzJywgaWQpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBzdGF0dXMgPSBuZXcgU3RhdHVzKHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IHN0YXR1cyB9KTtcbiAgICB9XG4gICAgLy8gICBHZXR0aW5nIHN0YXR1c2VzIGNvbnRleHQ6XG4gICAgLy9cbiAgICAvLyAgIEdFVCAvYXBpL3YxL3N0YXR1c2VzLzppZC9jb250ZXh0XG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgYSBDb250ZXh0LlxuICAgIGFzeW5jIGdldFN0YXR1c0NvbnRleHQoaWQpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdzdGF0dXNlcycsIGlkLCAnY29udGV4dCcpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBjb250ZXh0ID0gbmV3IENvbnRleHQocmVzcG9uc2UudmFsdWUuanNvbik7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgY29udGV4dCB9KTtcbiAgICB9XG4gICAgLy8gICBHZXR0aW5nIGEgY2FyZCBhc3NvY2lhdGVkIHdpdGggYSBzdGF0dXNlczpcbiAgICAvL1xuICAgIC8vICAgR0VUIC9hcGkvdjEvc3RhdHVzZXMvOmlkL2NhcmRcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhIENhcmQuXG4gICAgYXN5bmMgZ2V0U3RhdHVzQ2FyZChpZCkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ3N0YXR1c2VzJywgaWQsICdjYXJkJyk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwpO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IGNhcmQgPSBuZXcgQ2FyZChyZXNwb25zZS52YWx1ZS5qc29uKTtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmNvcHkoeyBjYXJkIH0pO1xuICAgIH1cbiAgICAvLyAgIEdldHRpbmcgd2hvIHJlYmxvZ2dlZCBhIHN0YXR1c2VzOlxuICAgIC8vXG4gICAgLy8gICBHRVQgL2FwaS92MS9zdGF0dXNlcy86aWQvcmVibG9nZ2VkX2J5XG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgYW4gYXJyYXkgb2YgQWNjb3VudHMuXG4gICAgYXN5bmMgZ2V0UmVibG9nZ2luZ0FjY291bnRzKGlkKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnc3RhdHVzZXMnLCBpZCwgJ3JlYmxvZ2dlZF9ieScpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBhY2NvdW50cyA9IHJlc3BvbnNlLnZhbHVlLmpzb24ubWFwKGogPT4gbmV3IEFjY291bnQoaikpO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGFjY291bnRzIH0pO1xuICAgIH1cbiAgICAvLyAgIEdldHRpbmcgd2hvIGZhdm91cml0ZWQgYSBzdGF0dXNlczpcbiAgICAvL1xuICAgIC8vICAgR0VUIC9hcGkvdjEvc3RhdHVzZXMvOmlkL2Zhdm91cml0ZWRfYnlcbiAgICAvL1xuICAgIC8vICAgUmV0dXJucyBhbiBhcnJheSBvZiBBY2NvdW50cy5cbiAgICBhc3luYyBnZXRGYXZvcml0aW5nQWNjb3VudHMoaWQpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdzdGF0dXNlcycsIGlkLCAnZmF2b3VyaXRlZF9ieScpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBhY2NvdW50cyA9IHJlc3BvbnNlLnZhbHVlLmpzb24ubWFwKGogPT4gbmV3IEFjY291bnQoaikpO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGFjY291bnRzIH0pO1xuICAgIH1cbiAgICAvLyAgIFBvc3RpbmcgYSBuZXcgc3RhdHVzZXM6XG4gICAgLy9cbiAgICAvLyAgIFBPU1QgL2FwaS92MS9zdGF0dXNlc1xuICAgIC8vXG4gICAgLy8gICBGb3JtIGRhdGE6XG4gICAgLy9cbiAgICAvLyAgIHN0YXR1czogVGhlIHRleHQgb2YgdGhlIHN0YXR1c2VzXG4gICAgLy8gICBpbl9yZXBseV90b19pZCAob3B0aW9uYWwpOiBsb2NhbCBJRCBvZiB0aGUgc3RhdHVzZXMgeW91IHdhbnQgdG8gcmVwbHkgdG9cbiAgICAvLyAgIG1lZGlhX2lkcyAob3B0aW9uYWwpOiBhcnJheSBvZiBtZWRpYSBJRHMgdG8gYXR0YWNoIHRvIHRoZSBzdGF0dXNlcyAobWF4aW11bSA0KVxuICAgIC8vICAgc2Vuc2l0aXZlIChvcHRpb25hbCk6IHNldCB0aGlzIHRvIG1hcmsgdGhlIG1lZGlhIG9mIHRoZSBzdGF0dXNlcyBhcyBOU0ZXXG4gICAgLy8gICBzcG9pbGVyX3RleHQgKG9wdGlvbmFsKTogdGV4dCB0byBiZSBzaG93biBhcyBhIHdhcm5pbmcgYmVmb3JlIHRoZSBhY3R1YWwgY29udGVudFxuICAgIC8vICAgdmlzaWJpbGl0eSAob3B0aW9uYWwpOiBlaXRoZXIgXCJkaXJlY3RcIiwgXCJwcml2YXRlXCIsIFwidW5saXN0ZWRcIiBvciBcInB1YmxpY1wiXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgdGhlIG5ldyBTdGF0dXMuXG4gICAgYXN5bmMgcG9zdFN0YXR1cyh0ZXh0LCB7IHJlcGx5VG8gPSBudWxsLCBtZWRpYUlkcyA9IFtdLCBzZW5zaXRpdmUgPSBmYWxzZSwgc3BvaWxlclRleHQgPSBudWxsLCB2aXNpYmlsaXR5ID0gJ3B1YmxpYycsIH0gPSB7fSkge1xuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLl9nZXRBcGkoJ3N0YXR1c2VzJywgbnVsbCwgbnVsbCk7XG4gICAgICAgIGNvbnN0IG1ldGhvZCA9ICdQT1NUJztcbiAgICAgICAgY29uc3QgYm9keSA9IG5ldyBGb3JtRGF0YSgpO1xuICAgICAgICBib2R5LmFwcGVuZCgnc3RhdHVzJywgdGV4dCk7XG4gICAgICAgIGlmIChyZXBseVRvKVxuICAgICAgICAgICAgYm9keS5hcHBlbmQoJ2luX3JlcGx5X3RvX2lkJywgcmVwbHlUbyk7XG4gICAgICAgIGZvciAoY29uc3QgaWQgb2YgbWVkaWFJZHMpIHtcbiAgICAgICAgICAgIGJvZHkuYXBwZW5kKCdtZWRpYV9pZHNbXScsIGlkKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoc2Vuc2l0aXZlKVxuICAgICAgICAgICAgYm9keS5hcHBlbmQoJ3NlbnNpdGl2ZScsICd0cnVlJyk7XG4gICAgICAgIGlmIChzcG9pbGVyVGV4dClcbiAgICAgICAgICAgIGJvZHkuYXBwZW5kKCdzcG9pbGVyX3RleHQnLCBzcG9pbGVyVGV4dCk7XG4gICAgICAgIGJvZHkuYXBwZW5kKCd2aXNpYmlsaXR5JywgdmlzaWJpbGl0eSk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwsIHsgbWV0aG9kLCBib2R5IH0pO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IHN0YXR1cyA9IG5ldyBTdGF0dXMocmVzcG9uc2UudmFsdWUuanNvbik7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgc3RhdHVzIH0pO1xuICAgIH1cbiAgICAvLyAgIERlbGV0aW5nIGEgc3RhdHVzZXM6XG4gICAgLy9cbiAgICAvLyAgIERFTEVURSAvYXBpL3YxL3N0YXR1c2VzLzppZFxuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIGFuIGVtcHR5IG9iamVjdC5cbiAgICBhc3luYyBkZWxldGVTdGF0dXMoaWQpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdzdGF0dXNlcycsIGlkKTtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ0RFTEVURSc7XG4gICAgICAgIHJldHVybiBhd2FpdCB0aGlzLl9hdXRoRmV0Y2godXJsLCB7IG1ldGhvZCB9KTtcbiAgICB9XG4gICAgLy8gICBSZWJsb2dnaW5nIGEgc3RhdHVzZXM6XG4gICAgLy9cbiAgICAvLyAgIFBPU1QgL2FwaS92MS9zdGF0dXNlcy86aWQvcmVibG9nXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgdGhlIHRhcmdldCBTdGF0dXMuXG4gICAgYXN5bmMgcmVibG9nU3RhdHVzKGlkKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnc3RhdHVzZXMnLCBpZCwgJ3JlYmxvZycpO1xuICAgICAgICBjb25zdCBtZXRob2QgPSAnUE9TVCc7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwsIHsgbWV0aG9kIH0pO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IHN0YXR1cyA9IG5ldyBTdGF0dXMocmVzcG9uc2UudmFsdWUuanNvbik7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgc3RhdHVzIH0pO1xuICAgIH1cbiAgICAvLyAgIFVucmVibG9nZ2luZyBhIHN0YXR1c2VzOlxuICAgIC8vXG4gICAgLy8gICBQT1NUIC9hcGkvdjEvc3RhdHVzZXMvOmlkL3VucmVibG9nXG4gICAgLy9cbiAgICAvLyAgIFJldHVybnMgdGhlIHRhcmdldCBTdGF0dXMuXG4gICAgYXN5bmMgdW5yZWJsb2dTdGF0dXMoaWQpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdzdGF0dXNlcycsIGlkLCAndW5yZWJsb2cnKTtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ1BPU1QnO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsLCB7IG1ldGhvZCB9KTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBzdGF0dXMgPSBuZXcgU3RhdHVzKHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IHN0YXR1cyB9KTtcbiAgICB9XG4gICAgLy8gICBGYXZvdXJpdGluZyBhIHN0YXR1c2VzOlxuICAgIC8vXG4gICAgLy8gICBQT1NUIC9hcGkvdjEvc3RhdHVzZXMvOmlkL2Zhdm91cml0ZVxuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIHRoZSB0YXJnZXQgU3RhdHVzLlxuICAgIGFzeW5jIGZhdm9yaXRlU3RhdHVzKGlkKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuX2dldEFwaSgnc3RhdHVzZXMnLCBpZCwgJ2Zhdm91cml0ZScpO1xuICAgICAgICBjb25zdCBtZXRob2QgPSAnUE9TVCc7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5fYXV0aEZldGNoSnNvbih1cmwsIHsgbWV0aG9kIH0pO1xuICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgIGNvbnN0IHN0YXR1cyA9IG5ldyBTdGF0dXMocmVzcG9uc2UudmFsdWUuanNvbik7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgc3RhdHVzIH0pO1xuICAgIH1cbiAgICAvLyAgIFVuZmF2b3VyaXRpbmcgYSBzdGF0dXNlczpcbiAgICAvL1xuICAgIC8vICAgUE9TVCAvYXBpL3YxL3N0YXR1c2VzLzppZC91bmZhdm91cml0ZVxuICAgIC8vXG4gICAgLy8gICBSZXR1cm5zIHRoZSB0YXJnZXQgU3RhdHVzLlxuICAgIGFzeW5jIHVuZmF2b3JpdGVTdGF0dXMoaWQpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCdzdGF0dXNlcycsIGlkLCAndW5mYXZvdXJpdGUnKTtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ1BPU1QnO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsLCB7IG1ldGhvZCB9KTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBzdGF0dXMgPSBuZXcgU3RhdHVzKHJlc3BvbnNlLnZhbHVlLmpzb24pO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IHN0YXR1cyB9KTtcbiAgICB9XG4gICAgLypcbiAgICAgKiBUaW1lbGluZXNcbiAgICAgKi9cbiAgICAvLyAgIFJldHJpZXZpbmcgYSBob21lIHRpbWVsaW5lOlxuICAgIC8vXG4gICAgLy8gICBHRVQgL2FwaS92MS90aW1lbGluZXMvaG9tZVxuICAgIGFzeW5jIGdldEhvbWVUaW1lTGluZSgpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCd0aW1lbGluZXMnLCBudWxsLCAnaG9tZScpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2F1dGhGZXRjaEpzb24odXJsKTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmlzRmFpbHVyZSgpKVxuICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICBjb25zdCBzdGF0dXNlcyA9IHJlc3BvbnNlLnZhbHVlLmpzb24ubWFwKHYgPT4gbmV3IFN0YXR1cyh2KSk7XG4gICAgICAgIGNvbnN0IGxpbmsgPSByZXNwb25zZS5saW5rKExpbmsuVHlwZS5zdGF0dXNlcyk7XG4gICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgbGluaywgc3RhdHVzZXMgfSk7XG4gICAgfVxuICAgIC8vICAgUmV0cmlldmluZyBhIHB1YmxpYyB0aW1lbGluZTpcbiAgICAvL1xuICAgIC8vICAgR0VUIC9hcGkvdjEvdGltZWxpbmVzL3B1YmxpY1xuICAgIC8vXG4gICAgLy8gICBRdWVyeSBwYXJhbWV0ZXJzOlxuICAgIC8vICAgbG9jYWw6IE9ubHkgcmV0dXJuIHN0YXR1c2VzIG9yaWdpbmF0aW5nIGZyb20gdGhpcyBpbnN0YW5jZVxuICAgIGFzeW5jIGdldFRpbWVMaW5lKGxvY2FsID0gZmFsc2UpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCd0aW1lbGluZXMnLCBudWxsLCAncHVibGljJywgeyBsb2NhbCB9KTtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCB0aGlzLl9hdXRoRmV0Y2hKc29uKHVybCk7XG4gICAgICAgIGlmIChyZXNwb25zZS5pc0ZhaWx1cmUoKSlcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgY29uc3Qgc3RhdHVzZXMgPSByZXNwb25zZS52YWx1ZS5qc29uLm1hcCh2ID0+IG5ldyBTdGF0dXModikpO1xuICAgICAgICBjb25zdCBsaW5rID0gcmVzcG9uc2UubGluayhMaW5rLlR5cGUuc3RhdHVzZXMpO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGxpbmssIHN0YXR1c2VzIH0pO1xuICAgIH1cbiAgICAvLyAgIFJldHJpZXZpbmcgYSBoYXNodGFnIHRpbWVsaW5lOlxuICAgIC8vXG4gICAgLy8gICBHRVQgL2FwaS92MS90aW1lbGluZXMvdGFnLzpoYXNodGFnXG4gICAgLy9cbiAgICAvLyAgIFF1ZXJ5IHBhcmFtZXRlcnM6XG4gICAgLy8gICBsb2NhbDogT25seSByZXR1cm4gc3RhdHVzZXMgb3JpZ2luYXRpbmcgZnJvbSB0aGlzIGluc3RhbmNlXG4gICAgYXN5bmMgZ2V0SGFzaFRhZ1RpbWVMaW5lKGhhc2h0YWcsIGxvY2FsID0gZmFsc2UpIHtcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5fZ2V0QXBpKCd0aW1lbGluZXMnLCAndGFnJywgaGFzaHRhZywgeyBsb2NhbCB9KTtcbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCB0aGlzLl9hdXRoRmV0Y2hKc29uKHVybCk7XG4gICAgICAgIGlmIChyZXNwb25zZS5pc0ZhaWx1cmUoKSlcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgY29uc3Qgc3RhdHVzZXMgPSByZXNwb25zZS52YWx1ZS5qc29uLm1hcCh2ID0+IG5ldyBTdGF0dXModikpO1xuICAgICAgICBjb25zdCBsaW5rID0gcmVzcG9uc2UubGluayhMaW5rLlR5cGUuc3RhdHVzZXMpO1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGxpbmssIHN0YXR1c2VzIH0pO1xuICAgIH1cbiAgICAvKlxuICAgICogTGlua1xuICAgICovXG4gICAgYXN5bmMgZ2V0TmV4dChsaW5rKSB7XG4gICAgICAgIGlmICghbGluayB8fCAhbGluay5uZXh0IHx8ICFsaW5rLm5leHQudXJsKVxuICAgICAgICAgICAgcmV0dXJuIFIuY3JlYXRlKFIuVHlwZS5udWxsTGluayk7XG4gICAgICAgIHJldHVybiBhd2FpdCB0aGlzLl9sb2FkTGlua1VybChsaW5rLm5leHQudXJsLCBsaW5rKTtcbiAgICB9XG4gICAgYXN5bmMgZ2V0UHJldihsaW5rKSB7XG4gICAgICAgIGlmICghbGluayB8fCAhbGluay5wcmV2IHx8ICFsaW5rLnByZXYudXJsKVxuICAgICAgICAgICAgcmV0dXJuIFIuY3JlYXRlKFIuVHlwZS5udWxsTGluayk7XG4gICAgICAgIHJldHVybiBhd2FpdCB0aGlzLl9sb2FkTGlua1VybChsaW5rLnByZXYudXJsLCBsaW5rKTtcbiAgICB9XG4gICAgLypcbiAgICAgKiBTdHJlYW1cbiAgICAgKi9cbiAgICBjcmVhdGVVc2VyU3RyZWFtKGhhbmRsZXIpIHtcbiAgICAgICAgY29uc3Qgc3RyZWFtID0gJ3VzZXInO1xuICAgICAgICByZXR1cm4gdGhpcy5jcmVhdGVTdHJlYW0oeyBzdHJlYW0gfSwgaGFuZGxlcik7XG4gICAgfVxuICAgIGNyZWF0ZVB1YmxpY1N0cmVhbShsb2NhbCwgaGFuZGxlcikge1xuICAgICAgICBjb25zdCBzdHJlYW0gPSBsb2NhbCA/ICdwdWJsaWM6bG9jYWwnIDogJ3B1YmxpYyc7XG4gICAgICAgIHJldHVybiB0aGlzLmNyZWF0ZVN0cmVhbSh7IHN0cmVhbSB9LCBoYW5kbGVyKTtcbiAgICB9XG4gICAgY3JlYXRlSGFzaFRhZ1N0cmVhbSh0YWcsIGxvY2FsLCBoYW5kbGVyKSB7XG4gICAgICAgIGNvbnN0IHN0cmVhbSA9IGxvY2FsID8gJ2hhc2h0YWc6bG9jYWwnIDogJ3B1YmxpYyc7XG4gICAgICAgIHJldHVybiB0aGlzLmNyZWF0ZVN0cmVhbSh7IHN0cmVhbSwgdGFnIH0sIGhhbmRsZXIpO1xuICAgIH1cbiAgICBjcmVhdGVTdHJlYW0odGFyZ2V0LCBoYW5kbGVyKSB7XG4gICAgICAgIHRhcmdldC5hY2Nlc3NfdG9rZW4gPSB0aGlzLmNyZWRlbnRpYWwuYWNjZXNzVG9rZW47XG4gICAgICAgIGNvbnN0IHNvY2tldCA9IG5ldyBXZWJTb2NrZXQodGhpcy5fZ2V0U3RyZWFtKHRhcmdldCkpO1xuICAgICAgICBzb2NrZXQub25vcGVuID0gKCkgPT4gaGFuZGxlcignb3BlbicpO1xuICAgICAgICBzb2NrZXQub25jbG9zZSA9ICgpID0+IGhhbmRsZXIoJ2Nsb3NlJyk7XG4gICAgICAgIHNvY2tldC5vbm1lc3NhZ2UgPSBtc2cgPT4ge1xuICAgICAgICAgICAgY29uc3QgZGF0YSA9IEpTT04ucGFyc2UobXNnLmRhdGEpO1xuICAgICAgICAgICAgaWYgKCFkYXRhLnBheWxvYWQpXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgc3dpdGNoIChkYXRhLmV2ZW50KSB7XG4gICAgICAgICAgICAgICAgY2FzZSAndXBkYXRlJzoge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBzdGF0dXMgPSBuZXcgU3RhdHVzKEpTT04ucGFyc2UoZGF0YS5wYXlsb2FkKSk7XG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZXIoJ21lc3NhZ2UnLCAndXBkYXRlJywgc3RhdHVzKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNhc2UgJ25vdGlmaWNhdGlvbic6IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgbm90aWZpY2F0aW9uID0gbmV3IE5vdGlmaWNhdGlvbihKU09OLnBhcnNlKGRhdGEucGF5bG9hZCkpO1xuICAgICAgICAgICAgICAgICAgICBoYW5kbGVyKCdtZXNzYWdlJywgJ25vdGlmaWNhdGlvbicsIG5vdGlmaWNhdGlvbik7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjYXNlICdkZWxldGUnOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGlkID0gZGF0YS5wYXlsb2FkO1xuICAgICAgICAgICAgICAgICAgICBoYW5kbGVyKCdtZXNzYWdlJywgJ2RlbGV0ZScsIGlkKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICByZXR1cm4gc29ja2V0O1xuICAgIH1cbiAgICAvKlxuICAgICAqIHV0aWxcbiAgICAgKi9cbiAgICBfZ2V0SFRUUFMoKSB7XG4gICAgICAgIGNvbnN0IHByb3RvY29sID0gdGhpcy51c2VTZWN1cmVQcm90b2NvbCA/ICdodHRwcycgOiAnaHR0cCc7XG4gICAgICAgIHJldHVybiBgJHtwcm90b2NvbH06Ly8ke3RoaXMudXJsLmFwaX1gO1xuICAgIH1cbiAgICBfZ2V0V1dTKCkge1xuICAgICAgICBjb25zdCBwcm90b2NvbCA9IHRoaXMudXNlU2VjdXJlUHJvdG9jb2wgPyAnd3NzJyA6ICd3cyc7XG4gICAgICAgIHJldHVybiBgJHtwcm90b2NvbH06Ly8ke3RoaXMudXJsLnN0cmVhbX1gO1xuICAgIH1cbiAgICBfZ2V0QXBpKHJlc291cmNlLCBpZCA9IG51bGwsIGFjdGlvbiA9IG51bGwsIHBhcmFtcyA9IG51bGwpIHtcbiAgICAgICAgbGV0IHIgPSAocmVzb3VyY2UgIT09IG51bGwpID8gKCcvJyArIHJlc291cmNlKSA6ICcnO1xuICAgICAgICBsZXQgaSA9IChpZCAhPT0gbnVsbCkgPyAoJy8nICsgaWQpIDogJyc7XG4gICAgICAgIGxldCBhID0gKGFjdGlvbiAhPT0gbnVsbCkgPyAoJy8nICsgYWN0aW9uKSA6ICcnO1xuICAgICAgICBsZXQgdXJsID0gYCR7dGhpcy5fZ2V0SFRUUFMoKX0vYXBpL3YxJHtyfSR7aX0ke2F9YDtcbiAgICAgICAgaWYgKCFwYXJhbXMpXG4gICAgICAgICAgICByZXR1cm4gdXJsO1xuICAgICAgICByZXR1cm4gYCR7dXJsfT8ke3RoaXMuX3F1ZXJ5UGFyYW1zKHBhcmFtcyl9YDtcbiAgICB9XG4gICAgX2dldFN0cmVhbShwYXJhbXMgPSBudWxsKSB7XG4gICAgICAgIGNvbnN0IHVybCA9IGAke3RoaXMuX2dldFdXUygpfS9hcGkvdjEvc3RyZWFtaW5nYDtcbiAgICAgICAgaWYgKCFwYXJhbXMpXG4gICAgICAgICAgICByZXR1cm4gdXJsO1xuICAgICAgICByZXR1cm4gYCR7dXJsfT8ke3RoaXMuX3F1ZXJ5UGFyYW1zKHBhcmFtcyl9YDtcbiAgICB9XG4gICAgX3F1ZXJ5UGFyYW1zKHBhcmFtcykge1xuICAgICAgICBpZiAoIXBhcmFtcylcbiAgICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgY29uc3QgdXJsUGFyYW1zID0gbmV3IFVSTFNlYXJjaFBhcmFtcygpO1xuICAgICAgICBmb3IgKGNvbnN0IFtrZXksIHZhbHVlXSBvZiBPYmplY3QuZW50cmllcyhwYXJhbXMpKSB7XG4gICAgICAgICAgICBpZiAoIXZhbHVlKVxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgayA9IGAke2tleX1bXWA7XG4gICAgICAgICAgICAgICAgZm9yIChjb25zdCB2IG9mIHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgIHVybFBhcmFtcy5hcHBlbmQoaywgdik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdXJsUGFyYW1zLnNldChrZXksIHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdXJsUGFyYW1zLnRvU3RyaW5nKCk7XG4gICAgfVxuICAgIGFzeW5jIF9mZXRjaCh1cmwsIHsgYm9keSA9IG51bGwsIG1ldGhvZCA9ICdHRVQnLCBtb2RlID0gJ0NPUlMnLCBoZWFkZXJzID0ge30gfSA9IHt9KSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgICAgICAgICAgIGJvZHksXG4gICAgICAgICAgICAgICAgbWV0aG9kLFxuICAgICAgICAgICAgICAgIG1vZGU6IG1vZGUsXG4gICAgICAgICAgICAgICAgaGVhZGVycyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYgKCFyZXNwb25zZS5vaykge1xuICAgICAgICAgICAgICAgIGNvbnN0IGluZm8gPSB7IHN0YXR1czogcmVzcG9uc2Uuc3RhdHVzLCBzdGF0dXNUZXh0OiByZXNwb25zZS5zdGF0dXNUZXh0LCByZXNwb25zZSB9O1xuICAgICAgICAgICAgICAgIHJldHVybiBSLmNyZWF0ZShSLlR5cGUuaW52YWxpZFN0YXR1cywgaW5mbyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gUi5zdWNjZXNzKHsgcmVzcG9uc2UgfSk7XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICByZXR1cm4gUi5jcmVhdGUoUi5UeXBlLm5ldHdvcmtFcnJvciwgZXJyb3IpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGFzeW5jIF9mZXRjaEpzb24odXJsLCB7IGJvZHkgPSBudWxsLCBtZXRob2QgPSAnR0VUJywgbW9kZSA9ICdDT1JTJywgaGVhZGVycyA9IHt9IH0gPSB7fSkge1xuICAgICAgICBjb25zdCBoID0ge307XG4gICAgICAgIE9iamVjdC5hc3NpZ24oaCwgaGVhZGVycyk7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IHRoaXMuX2ZldGNoKHVybCwgeyBib2R5LCBtZXRob2QsIG1vZGUsIGhlYWRlcnM6IGggfSk7XG4gICAgICAgICAgICBpZiAocmVzcG9uc2UuaXNGYWlsdXJlKCkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICAgICAgY29uc3QganNvbiA9IGF3YWl0IHJlc3BvbnNlLnZhbHVlLnJlc3BvbnNlLmpzb24oKTtcbiAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IFIuc3VjY2Vzcyh7IGpzb24gfSk7XG4gICAgICAgICAgICByZXN1bHQuaGVhZGVycyA9IHJlc3BvbnNlLnZhbHVlLnJlc3BvbnNlLmhlYWRlcnM7XG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgcmV0dXJuIFIuY3JlYXRlKFIuVHlwZS5uZXR3b3JrRXJyb3IsIGVycm9yKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBhc3luYyBfYXV0aEZldGNoKHVybCwgeyBib2R5ID0gbnVsbCwgbWV0aG9kID0gJ0dFVCcsIG1vZGUgPSAnQ09SUycsIGhlYWRlcnMgPSB7fSB9ID0ge30pIHtcbiAgICAgICAgcmV0dXJuIGF3YWl0IHRoaXMuX2ZldGNoKHVybCwge1xuICAgICAgICAgICAgYm9keSxcbiAgICAgICAgICAgIG1ldGhvZCxcbiAgICAgICAgICAgIG1vZGUsXG4gICAgICAgICAgICBoZWFkZXJzOiBPYmplY3QuYXNzaWduKHsgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke3RoaXMuY3JlZGVudGlhbC5hY2Nlc3NUb2tlbn1gIH0sIGhlYWRlcnMpLFxuICAgICAgICB9KTtcbiAgICB9XG4gICAgYXN5bmMgX2F1dGhGZXRjaEpzb24odXJsLCB7IGJvZHkgPSBudWxsLCBtZXRob2QgPSAnR0VUJywgbW9kZSA9ICdDT1JTJywgaGVhZGVycyA9IHt9IH0gPSB7fSkge1xuICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy5fZmV0Y2hKc29uKHVybCwge1xuICAgICAgICAgICAgYm9keSxcbiAgICAgICAgICAgIG1ldGhvZCxcbiAgICAgICAgICAgIG1vZGUsXG4gICAgICAgICAgICBoZWFkZXJzOiBPYmplY3QuYXNzaWduKHsgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke3RoaXMuY3JlZGVudGlhbC5hY2Nlc3NUb2tlbn1gIH0sIGhlYWRlcnMpLFxuICAgICAgICB9KTtcbiAgICB9XG4gICAgYXN5bmMgX2xvYWRMaW5rVXJsKHVybCwgbGluaykge1xuICAgICAgICBsZXQgcmVzcG9uc2UgPSBhd2FpdCB0aGlzLl9hdXRoRmV0Y2hKc29uKHVybCk7XG4gICAgICAgIGlmIChyZXNwb25zZS5pc0ZhaWx1cmUoKSlcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgY29uc3QganNvbiA9IHJlc3BvbnNlLnZhbHVlLmpzb247XG4gICAgICAgIGxldCBuZXdMaW5rID0gcmVzcG9uc2UubGluayhsaW5rLnR5cGUpO1xuICAgICAgICBzd2l0Y2ggKGxpbmsudHlwZSkge1xuICAgICAgICAgICAgY2FzZSBMaW5rLlR5cGUuc3RhdHVzZXM6IHtcbiAgICAgICAgICAgICAgICBjb25zdCBzdGF0dXNlcyA9IGpzb24ubWFwKGogPT4gbmV3IFN0YXR1cyhqKSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmNvcHkoeyBsaW5rOiBuZXdMaW5rIHx8IGxpbmssIHN0YXR1c2VzIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2FzZSBMaW5rLlR5cGUuYWNjb3VudHM6IHtcbiAgICAgICAgICAgICAgICBjb25zdCBzdGF0dXNlcyA9IGpzb24ubWFwKGogPT4gbmV3IEFjY291bnQoaikpO1xuICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5jb3B5KHsgbGluazogbmV3TGluayB8fCBsaW5rLCBzdGF0dXNlcyB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhc2UgTGluay5UeXBlLm5vdGlmaWNhdGlvbnM6IHtcbiAgICAgICAgICAgICAgICBjb25zdCBub3RpZmljYXRpb25zID0ganNvbi5tYXAoaiA9PiBuZXcgTm90aWZpY2F0aW9uKGopKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuY29weSh7IGxpbms6IG5ld0xpbmsgfHwgbGluaywgbm90aWZpY2F0aW9ucyB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdub3QgaW1wbC4gbGluaycpO1xuICAgICAgICB9XG4gICAgfVxufVxuZXhwb3J0IGRlZmF1bHQgTWFzdG9kb247XG4vLyMgc291cmNlTWFwcGluZ1VSTD1jbGllbnQuanMubWFwIl19