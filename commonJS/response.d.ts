import Link from './link';
declare class Response<T> {
    type: Response.Type;
    value: T;
    headers?: Headers;
    error: any;
    constructor(type: Response.Type, value: T, error: any);
    link(type: any): Link;
    isSuccess(): boolean;
    isFailure(): boolean;
    copy<S>(newValue: S): Response<S>;
}
declare module Response {
    enum Type {
        success = 0,
        networkError = 1,
        invalidStatus = 2,
        nullLink = 3,
    }
    function create<T>(type: any, error?: any): Response<T>;
    function success<T>(value: T): Response<T>;
}
export default Response;
