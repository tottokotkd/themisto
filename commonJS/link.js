"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by tottokotkd on 17/04/26.
 */
var Link = function Link(type, obj) {
    _classCallCheck(this, Link);

    this.type = type;
    this.next = obj.next || null;
    this.prev = obj.prev || null;
};

(function (Link) {
    var Type;
    (function (Type) {
        Type[Type["statuses"] = 0] = "statuses";
        Type[Type["accounts"] = 1] = "accounts";
        Type[Type["notifications"] = 2] = "notifications";
    })(Type = Link.Type || (Link.Type = {}));
})(Link || (Link = {}));
exports.default = Link;
//# sourceMappingURL=link.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxpbmsuanMiXSwibmFtZXMiOlsiTGluayIsInR5cGUiLCJvYmoiLCJuZXh0IiwicHJldiIsIlR5cGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7OztJQUdNQSxJLEdBQ0YsY0FBWUMsSUFBWixFQUFrQkMsR0FBbEIsRUFBdUI7QUFBQTs7QUFDbkIsU0FBS0QsSUFBTCxHQUFZQSxJQUFaO0FBQ0EsU0FBS0UsSUFBTCxHQUFZRCxJQUFJQyxJQUFKLElBQVksSUFBeEI7QUFDQSxTQUFLQyxJQUFMLEdBQVlGLElBQUlFLElBQUosSUFBWSxJQUF4QjtBQUNILEM7O0FBRUwsQ0FBQyxVQUFVSixJQUFWLEVBQWdCO0FBQ2IsUUFBSUssSUFBSjtBQUNBLEtBQUMsVUFBVUEsSUFBVixFQUFnQjtBQUNiQSxhQUFLQSxLQUFLLFVBQUwsSUFBbUIsQ0FBeEIsSUFBNkIsVUFBN0I7QUFDQUEsYUFBS0EsS0FBSyxVQUFMLElBQW1CLENBQXhCLElBQTZCLFVBQTdCO0FBQ0FBLGFBQUtBLEtBQUssZUFBTCxJQUF3QixDQUE3QixJQUFrQyxlQUFsQztBQUNILEtBSkQsRUFJR0EsT0FBT0wsS0FBS0ssSUFBTCxLQUFjTCxLQUFLSyxJQUFMLEdBQVksRUFBMUIsQ0FKVjtBQUtILENBUEQsRUFPR0wsU0FBU0EsT0FBTyxFQUFoQixDQVBIO2tCQVFlQSxJO0FBQ2YiLCJmaWxlIjoibGluay5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSB0b3R0b2tvdGtkIG9uIDE3LzA0LzI2LlxuICovXG5jbGFzcyBMaW5rIHtcbiAgICBjb25zdHJ1Y3Rvcih0eXBlLCBvYmopIHtcbiAgICAgICAgdGhpcy50eXBlID0gdHlwZTtcbiAgICAgICAgdGhpcy5uZXh0ID0gb2JqLm5leHQgfHwgbnVsbDtcbiAgICAgICAgdGhpcy5wcmV2ID0gb2JqLnByZXYgfHwgbnVsbDtcbiAgICB9XG59XG4oZnVuY3Rpb24gKExpbmspIHtcbiAgICB2YXIgVHlwZTtcbiAgICAoZnVuY3Rpb24gKFR5cGUpIHtcbiAgICAgICAgVHlwZVtUeXBlW1wic3RhdHVzZXNcIl0gPSAwXSA9IFwic3RhdHVzZXNcIjtcbiAgICAgICAgVHlwZVtUeXBlW1wiYWNjb3VudHNcIl0gPSAxXSA9IFwiYWNjb3VudHNcIjtcbiAgICAgICAgVHlwZVtUeXBlW1wibm90aWZpY2F0aW9uc1wiXSA9IDJdID0gXCJub3RpZmljYXRpb25zXCI7XG4gICAgfSkoVHlwZSA9IExpbmsuVHlwZSB8fCAoTGluay5UeXBlID0ge30pKTtcbn0pKExpbmsgfHwgKExpbmsgPSB7fSkpO1xuZXhwb3J0IGRlZmF1bHQgTGluaztcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWxpbmsuanMubWFwIl19