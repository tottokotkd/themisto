declare class Entity {
    protected json: any;
    protected cache: any;
    constructor(json: any);
    date(str: any): Date;
    array(obj: any): any[];
    getTimeRange(start: any, end: any): {
        days: number;
        hours: number;
        minutes: number;
        seconds: number;
    };
}
export default Entity;
