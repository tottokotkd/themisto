import Link from './link';
import R from './response';
import Account from './entity/account';
import Attachment from './entity/attachment';
import Card from './entity/card';
import Context from './entity/context';
import Instance from './entity/instance';
import Notification from './entity/notification';
import Relationship from './entity/relationship';
import Status from './entity/status';
import Result from './entity/result';
import Report from './entity/report';
declare class Mastodon {
    /**
     * callback URL
     */
    private callback;
    /**
     * client name
     */
    private name;
    /**
     * site URL
     */
    private site;
    /**
     * OAuth scope
     */
    private scopes;
    /**
     * mastodon server URLs
     */
    private url;
    /**
     * OAuth credentials
     */
    private credential;
    /**
     * https / wss flag
     */
    useSecureProtocol: boolean;
    /**
     * constructor
     */
    constructor();
    /**
     * set client name for app registration
     * @param name client name
     */
    setClientName(name: string): void;
    /**
     * set site url for app registration
     * @param site client site URL
     */
    setClientSiteUrl(site: string): void;
    /**
     * set credentials for OAuth
     * @param credentials
     */
    setCredentials(credentials: Mastodon.Credentials): void;
    /**
     * set scopes for app registration
     * @param scopes
     */
    setScopes(scopes: [string]): void;
    /**
     * set URLs for fetch
     * @param base
     * @param api
     * @param stream
     */
    setURLs({base, api, stream}: {
        base: any;
        api: any;
        stream: any;
    }): void;
    /**
     * export client config
     * @returns {{name: string, site: string, scopes: [string], url: Mastodon.URLs, credential: Mastodon.Credentials}}
     */
    exportConfig(): {
        name: string;
        site: string;
        scopes: [string];
        url: Mastodon.URLs;
        credential: Mastodon.Credentials;
    };
    /**
     * Registering an application:
     *
     * POST /api/v1/apps
     *
     * Form data:
     *
     * client_name:
     *   Name of your application
     * redirect_uris:
     *   Where the user should be redirected after authorization
     *   (for no redirect, use urn:ietf:wg:oauth:2.0:oob)
     * scopes:
     *   This can be a space-separated list of the following items: "read", "write" and "follow"
     *   (see this page for details on what the scopes do)
     *   website: (optional)
     *   URL to the homepage of your app
     *
     * Creates a new OAuth app. Returns id, client_id and client_secret which can be used
     * with OAuth authentication in your 3rd party app.
     *
     * These values should be requested in the app itself from the API for each
     * new app install + mastodon domain combo, and stored in the app for future requests.
     *
     * @returns {Promise<R{}>}
     */
    registerApp(): Promise<R<{}>>;
    /**
     * get OAuth authorizing URL
     * @returns {string}
     */
    getAuthorizeURL(): string;
    /**
     * send OAuth code
     * @param code
     * @returns {Promise<R{}>}
     */
    authenticate(code: string): Promise<R<{}>>;
    getAccount(id: number): Promise<R<{
        account: Account;
    }> | R<{}>>;
    getCurrentAccount(): Promise<R<{
        account: Account;
    }> | R<{}>>;
    updateCurrentAccount({displayName, note, avatar, header}?: {
        displayName?: any;
        note?: any;
        avatar?: any;
        header?: any;
    }): Promise<R<{
        account: Account;
    }> | R<{}>>;
    getFollowers(id: number): Promise<R<{
        link: Link;
        accounts: [Account];
    }> | R<{}>>;
    getFollowings(id: number): Promise<R<{
        link: Link;
        accounts: [Account];
    }> | R<{}>>;
    getStatusesOfAccount(id: number, params?: {
        onlyMedia: boolean;
        excludeReplies: boolean;
    }): Promise<R<{
        link: Link;
        statuses: [Status];
    }> | R<{}>>;
    followAccount(id: number): Promise<R<{
        relationship: Relationship;
    }> | R<{}>>;
    unfollowAccount(id: number): Promise<R<{
        relationship: Relationship;
    }> | R<{}>>;
    blockAccount(id: number): Promise<R<{
        relationship: Relationship;
    }> | R<{}>>;
    unblockAccount(id: number): Promise<R<{
        relationship: Relationship;
    }> | R<{}>>;
    muteAccount(id: number): Promise<R<{
        relationship: Relationship;
    }> | R<{}>>;
    unmuteAccount(id: number): Promise<R<{
        relationship: Relationship;
    }> | R<{}>>;
    getRelationships(ids: any): Promise<R<{
        relationships: [Relationship];
    }> | R<{}>>;
    searchAccounts(query: string, limit?: number): Promise<R<{
        accounts: [Account];
    }> | R<{}>>;
    getBlocks(): Promise<R<{
        link: Link;
        accounts: [Account];
    }> | R<{}>>;
    getFavorites(): Promise<R<{
        link: Link;
        statuses: [Status];
    }> | R<{}>>;
    getFollowRequests(): Promise<R<{
        link: Link;
        accounts: [Account];
    }> | R<{}>>;
    authorizeFollowRequest(id: number): Promise<R<{}>>;
    rejectFollowRequest(id: number): Promise<R<{}>>;
    followRemoteUser(uri: string): Promise<R<{
        account: Account;
    }> | R<{}>>;
    getInstanceInfo(): Promise<R<{
        instance: Instance;
    }> | R<{}>>;
    uploadMediaAttachment(file: any): Promise<R<{
        attachment: Attachment;
    }> | R<{}>>;
    getMutes(): Promise<R<{
        accounts: [Account];
    }> | R<{}>>;
    getNotifications(): Promise<R<{
        link: Link;
        notifications: [Notification];
    }> | R<{}>>;
    getNotification(id: number): Promise<R<{
        notification: Notification;
    }> | R<{}>>;
    clearNotifications(): Promise<R<{}>>;
    fetchReports(): Promise<R<{
        reports: [Report];
    }> | R<{}>>;
    reportUser({accountId, statusIds, comment}?: {
        accountId?: any;
        statusIds?: any[];
        comment?: any;
    }): Promise<R<{
        report: Report;
    }> | R<{}>>;
    search(query: string, resolveRemoteAccounts?: boolean): Promise<R<{
        result: Result;
    }> | R<{}>>;
    fetchStatus(id: number): Promise<R<{
        status: Status;
    }> | R<{}>>;
    getStatusContext(id: number): Promise<R<{
        context: Context;
    }> | R<{}>>;
    getStatusCard(id: number): Promise<R<{
        card: Card;
    }> | R<{}>>;
    getRebloggingAccounts(id: number): Promise<R<{
        accounts: [Account];
    }> | R<{}>>;
    getFavoritingAccounts(id: number): Promise<R<{
        accounts: [Account];
    }> | R<{}>>;
    postStatus(text: string, {replyTo, mediaIds, sensitive, spoilerText, visibility}?: {
        replyTo?: any;
        mediaIds?: any[];
        sensitive?: boolean;
        spoilerText?: any;
        visibility?: string;
    }): Promise<R<{
        status: Status;
    }> | R<{}>>;
    deleteStatus(id: number): Promise<R<{}>>;
    reblogStatus(id: number): Promise<R<{
        status: Status;
    }> | R<{}>>;
    unreblogStatus(id: number): Promise<R<{
        status: Status;
    }> | R<{}>>;
    favoriteStatus(id: number): Promise<R<{
        status: Status;
    }> | R<{}>>;
    unfavoriteStatus(id: number): Promise<R<{
        status: Status;
    }> | R<{}>>;
    getHomeTimeLine(): Promise<R<{
        link: Link;
        statuses: [Status];
    }> | R<{}>>;
    getTimeLine(local?: boolean): Promise<R<{
        link: Link;
        statuses: [Status];
    }> | R<{}>>;
    getHashTagTimeLine(hashtag: string, local?: boolean): Promise<R<{
        link: Link;
        statuses: [Status];
    }> | R<{}>>;
    getNext(link: Link): Promise<any>;
    getPrev(link: Link): Promise<any>;
    createUserStream(handler: Mastodon.StreamHandler): WebSocket;
    createPublicStream(local: boolean, handler: Mastodon.StreamHandler): WebSocket;
    createHashTagStream(tag: any, local: boolean, handler: Mastodon.StreamHandler): WebSocket;
    createStream(target: any, handler: Mastodon.StreamHandler): WebSocket;
    _getHTTPS(): string;
    _getWWS(): string;
    _getApi(resource: string, id?: string | number, action?: string, params?: object): string;
    _getStream(params?: object): string;
    _queryParams(params: object): string;
    _fetch(url: string, {body, method, mode, headers}?: RequestInit): Promise<R<{
        response?: Response;
        status?: number;
        statusText?: string;
    }>>;
    _fetchJson(url: any, {body, method, mode, headers}?: RequestInit): Promise<R<{
        json?: any;
    }>>;
    _authFetch(url: any, {body, method, mode, headers}?: RequestInit): Promise<R<{
        response?: Response;
        status?: number;
        statusText?: string;
    }>>;
    _authFetchJson(url: any, {body, method, mode, headers}?: RequestInit): Promise<R<{
        json?: any;
    }>>;
    _loadLinkUrl(url: string, link: Link): Promise<any>;
}
declare module Mastodon {
    interface Credentials {
        clientID?: string;
        clientSecret?: string;
        accessToken?: string;
        refreshToken?: string;
    }
    interface URLs {
        base?: string;
        api?: string;
        stream?: string;
    }
    type StreamHandler = (mode: string, event?: string, payload?: any) => void;
}
export default Mastodon;
