'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Created by tottokotkd on 17/04/24.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */


var _isInteger = require('lodash/isInteger');

var _isInteger2 = _interopRequireDefault(_isInteger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Entity = function () {
    function Entity(json) {
        _classCallCheck(this, Entity);

        this.json = json;
        this.cache = {};
    }

    _createClass(Entity, [{
        key: 'date',
        value: function date(str) {
            return new Date(str);
        }
    }, {
        key: 'array',
        value: function array(obj) {
            if (Array.isArray(obj)) {
                return obj;
            }
            return [];
        }
    }, {
        key: 'getTimeRange',
        value: function getTimeRange(start, end) {
            if (!(0, _isInteger2.default)(start) || !(0, _isInteger2.default)(end)) return null;
            var length = Math.floor((end - start) / 1000);
            var days = Math.floor(length / (3600 * 24));
            length -= days * 3600 * 24;
            var hours = Math.floor(length / 3600);
            length -= hours * 3600;
            var minutes = Math.floor(length / 60);
            length -= minutes * 60;
            var seconds = length;
            return { days: days, hours: hours, minutes: minutes, seconds: seconds };
        }
    }]);

    return Entity;
}();

exports.default = Entity;
//# sourceMappingURL=entity.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVudGl0eS5qcyJdLCJuYW1lcyI6WyJFbnRpdHkiLCJqc29uIiwiY2FjaGUiLCJzdHIiLCJEYXRlIiwib2JqIiwiQXJyYXkiLCJpc0FycmF5Iiwic3RhcnQiLCJlbmQiLCJsZW5ndGgiLCJNYXRoIiwiZmxvb3IiLCJkYXlzIiwiaG91cnMiLCJtaW51dGVzIiwic2Vjb25kcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7O3FqQkFBQTs7Ozs7QUFHQTs7Ozs7Ozs7SUFDTUEsTTtBQUNGLG9CQUFZQyxJQUFaLEVBQWtCO0FBQUE7O0FBQ2QsYUFBS0EsSUFBTCxHQUFZQSxJQUFaO0FBQ0EsYUFBS0MsS0FBTCxHQUFhLEVBQWI7QUFDSDs7Ozs2QkFDSUMsRyxFQUFLO0FBQ04sbUJBQU8sSUFBSUMsSUFBSixDQUFTRCxHQUFULENBQVA7QUFDSDs7OzhCQUNLRSxHLEVBQUs7QUFDUCxnQkFBSUMsTUFBTUMsT0FBTixDQUFjRixHQUFkLENBQUosRUFBd0I7QUFDcEIsdUJBQU9BLEdBQVA7QUFDSDtBQUNELG1CQUFPLEVBQVA7QUFDSDs7O3FDQUNZRyxLLEVBQU9DLEcsRUFBSztBQUNyQixnQkFBSSxDQUFDLHlCQUFVRCxLQUFWLENBQUQsSUFBcUIsQ0FBQyx5QkFBVUMsR0FBVixDQUExQixFQUNJLE9BQU8sSUFBUDtBQUNKLGdCQUFJQyxTQUFTQyxLQUFLQyxLQUFMLENBQVcsQ0FBQ0gsTUFBTUQsS0FBUCxJQUFnQixJQUEzQixDQUFiO0FBQ0EsZ0JBQU1LLE9BQU9GLEtBQUtDLEtBQUwsQ0FBV0YsVUFBVSxPQUFPLEVBQWpCLENBQVgsQ0FBYjtBQUNBQSxzQkFBVUcsT0FBTyxJQUFQLEdBQWMsRUFBeEI7QUFDQSxnQkFBTUMsUUFBUUgsS0FBS0MsS0FBTCxDQUFXRixTQUFTLElBQXBCLENBQWQ7QUFDQUEsc0JBQVVJLFFBQVEsSUFBbEI7QUFDQSxnQkFBTUMsVUFBVUosS0FBS0MsS0FBTCxDQUFXRixTQUFTLEVBQXBCLENBQWhCO0FBQ0FBLHNCQUFVSyxVQUFVLEVBQXBCO0FBQ0EsZ0JBQU1DLFVBQVVOLE1BQWhCO0FBQ0EsbUJBQU8sRUFBRUcsVUFBRixFQUFRQyxZQUFSLEVBQWVDLGdCQUFmLEVBQXdCQyxnQkFBeEIsRUFBUDtBQUNIOzs7Ozs7a0JBRVVoQixNO0FBQ2YiLCJmaWxlIjoiZW50aXR5LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjQuXG4gKi9cbmltcG9ydCBpc0ludGVnZXIgZnJvbSAnbG9kYXNoL2lzSW50ZWdlcic7XG5jbGFzcyBFbnRpdHkge1xuICAgIGNvbnN0cnVjdG9yKGpzb24pIHtcbiAgICAgICAgdGhpcy5qc29uID0ganNvbjtcbiAgICAgICAgdGhpcy5jYWNoZSA9IHt9O1xuICAgIH1cbiAgICBkYXRlKHN0cikge1xuICAgICAgICByZXR1cm4gbmV3IERhdGUoc3RyKTtcbiAgICB9XG4gICAgYXJyYXkob2JqKSB7XG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KG9iaikpIHtcbiAgICAgICAgICAgIHJldHVybiBvYmo7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIFtdO1xuICAgIH1cbiAgICBnZXRUaW1lUmFuZ2Uoc3RhcnQsIGVuZCkge1xuICAgICAgICBpZiAoIWlzSW50ZWdlcihzdGFydCkgfHwgIWlzSW50ZWdlcihlbmQpKVxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIGxldCBsZW5ndGggPSBNYXRoLmZsb29yKChlbmQgLSBzdGFydCkgLyAxMDAwKTtcbiAgICAgICAgY29uc3QgZGF5cyA9IE1hdGguZmxvb3IobGVuZ3RoIC8gKDM2MDAgKiAyNCkpO1xuICAgICAgICBsZW5ndGggLT0gZGF5cyAqIDM2MDAgKiAyNDtcbiAgICAgICAgY29uc3QgaG91cnMgPSBNYXRoLmZsb29yKGxlbmd0aCAvIDM2MDApO1xuICAgICAgICBsZW5ndGggLT0gaG91cnMgKiAzNjAwO1xuICAgICAgICBjb25zdCBtaW51dGVzID0gTWF0aC5mbG9vcihsZW5ndGggLyA2MCk7XG4gICAgICAgIGxlbmd0aCAtPSBtaW51dGVzICogNjA7XG4gICAgICAgIGNvbnN0IHNlY29uZHMgPSBsZW5ndGg7XG4gICAgICAgIHJldHVybiB7IGRheXMsIGhvdXJzLCBtaW51dGVzLCBzZWNvbmRzIH07XG4gICAgfVxufVxuZXhwb3J0IGRlZmF1bHQgRW50aXR5O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9ZW50aXR5LmpzLm1hcCJdfQ==