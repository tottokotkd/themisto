/**
 * Created by tottokotkd on 17/04/26.
 */
declare class Link {
    next: any;
    prev: any;
    type: any;
    constructor(type: any, obj: any);
}
declare module Link {
    enum Type {
        statuses = 0,
        accounts = 1,
        notifications = 2,
    }
}
export default Link;
