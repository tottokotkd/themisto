/**
 * Created by tottokotkd on 17/04/24.
 */
import 'babel-polyfill';
import Mastodon from './client';
import _Link from './link';
import _Response from './response';
import _Account from './entity/account';
import _Attachment from './entity/attachment';
import _Application from './entity/application';
import _Card from './entity/card';
import _Context from './entity/context';
import _Error from './entity/error';
import _Instance from './entity/instance';
import _Mention from './entity/mention';
import _Notification from './entity/notification';
import _Relationship from './entity/relationship';
import _Report from './entity/report';
import _Result from './entity/result';
import _Status from './entity/status';
import _Tag from './entity/tag';
export default Mastodon;
export declare const Link: typeof _Link;
export declare const Response: typeof _Response;
export declare const Account: typeof _Account;
export declare const Attachment: typeof _Attachment;
export declare const Application: typeof _Application;
export declare const Card: typeof _Card;
export declare const Context: typeof _Context;
export declare const Error: typeof _Error;
export declare const Instance: typeof _Instance;
export declare const Mention: typeof _Mention;
export declare const Notification: typeof _Notification;
export declare const Relationship: typeof _Relationship;
export declare const Report: typeof _Report;
export declare const Result: typeof _Result;
export declare const Status: typeof _Status;
export declare const Tag: typeof _Tag;
