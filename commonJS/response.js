'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Created by tottokotkd on 17/04/25.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */


var _parseLinkHeader = require('parse-link-header');

var _parseLinkHeader2 = _interopRequireDefault(_parseLinkHeader);

var _link = require('./link');

var _link2 = _interopRequireDefault(_link);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Response = function () {
    function Response(type, value, error) {
        _classCallCheck(this, Response);

        this.type = type;
        this.value = value;
        this.error = error;
    }

    _createClass(Response, [{
        key: 'link',
        value: function link(type) {
            if (!this.headers || !this.headers.has('Link')) return null;
            var obj = (0, _parseLinkHeader2.default)(this.headers.get('Link'));
            return new _link2.default(type, obj);
        }
    }, {
        key: 'isSuccess',
        value: function isSuccess() {
            return this.type === Response.Type.success;
        }
    }, {
        key: 'isFailure',
        value: function isFailure() {
            return !this.isSuccess();
        }
    }, {
        key: 'copy',
        value: function copy(newValue) {
            return new Response(this.type, newValue, this.error);
        }
    }]);

    return Response;
}();

(function (Response) {
    var Type;
    (function (Type) {
        Type[Type["success"] = 0] = "success";
        Type[Type["networkError"] = 1] = "networkError";
        Type[Type["invalidStatus"] = 2] = "invalidStatus";
        Type[Type["nullLink"] = 3] = "nullLink";
    })(Type = Response.Type || (Response.Type = {}));
    function create(type) {
        var error = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

        return new Response(type, null, error);
    }
    Response.create = create;
    function success(value) {
        return new Response(Response.Type.success, value, null);
    }
    Response.success = success;
})(Response || (Response = {}));
exports.default = Response;
//# sourceMappingURL=response.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlc3BvbnNlLmpzIl0sIm5hbWVzIjpbIlJlc3BvbnNlIiwidHlwZSIsInZhbHVlIiwiZXJyb3IiLCJoZWFkZXJzIiwiaGFzIiwib2JqIiwiZ2V0IiwiVHlwZSIsInN1Y2Nlc3MiLCJpc1N1Y2Nlc3MiLCJuZXdWYWx1ZSIsImNyZWF0ZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O3FqQkFBQTs7Ozs7QUFHQTs7OztBQUNBOzs7Ozs7OztJQUNNQSxRO0FBQ0Ysc0JBQVlDLElBQVosRUFBa0JDLEtBQWxCLEVBQXlCQyxLQUF6QixFQUFnQztBQUFBOztBQUM1QixhQUFLRixJQUFMLEdBQVlBLElBQVo7QUFDQSxhQUFLQyxLQUFMLEdBQWFBLEtBQWI7QUFDQSxhQUFLQyxLQUFMLEdBQWFBLEtBQWI7QUFDSDs7Ozs2QkFDSUYsSSxFQUFNO0FBQ1AsZ0JBQUksQ0FBQyxLQUFLRyxPQUFOLElBQWlCLENBQUMsS0FBS0EsT0FBTCxDQUFhQyxHQUFiLENBQWlCLE1BQWpCLENBQXRCLEVBQ0ksT0FBTyxJQUFQO0FBQ0osZ0JBQU1DLE1BQU0sK0JBQWlCLEtBQUtGLE9BQUwsQ0FBYUcsR0FBYixDQUFpQixNQUFqQixDQUFqQixDQUFaO0FBQ0EsbUJBQU8sbUJBQVNOLElBQVQsRUFBZUssR0FBZixDQUFQO0FBQ0g7OztvQ0FDVztBQUNSLG1CQUFPLEtBQUtMLElBQUwsS0FBY0QsU0FBU1EsSUFBVCxDQUFjQyxPQUFuQztBQUNIOzs7b0NBQ1c7QUFDUixtQkFBTyxDQUFDLEtBQUtDLFNBQUwsRUFBUjtBQUNIOzs7NkJBQ0lDLFEsRUFBVTtBQUNYLG1CQUFPLElBQUlYLFFBQUosQ0FBYSxLQUFLQyxJQUFsQixFQUF3QlUsUUFBeEIsRUFBa0MsS0FBS1IsS0FBdkMsQ0FBUDtBQUNIOzs7Ozs7QUFFTCxDQUFDLFVBQVVILFFBQVYsRUFBb0I7QUFDakIsUUFBSVEsSUFBSjtBQUNBLEtBQUMsVUFBVUEsSUFBVixFQUFnQjtBQUNiQSxhQUFLQSxLQUFLLFNBQUwsSUFBa0IsQ0FBdkIsSUFBNEIsU0FBNUI7QUFDQUEsYUFBS0EsS0FBSyxjQUFMLElBQXVCLENBQTVCLElBQWlDLGNBQWpDO0FBQ0FBLGFBQUtBLEtBQUssZUFBTCxJQUF3QixDQUE3QixJQUFrQyxlQUFsQztBQUNBQSxhQUFLQSxLQUFLLFVBQUwsSUFBbUIsQ0FBeEIsSUFBNkIsVUFBN0I7QUFDSCxLQUxELEVBS0dBLE9BQU9SLFNBQVNRLElBQVQsS0FBa0JSLFNBQVNRLElBQVQsR0FBZ0IsRUFBbEMsQ0FMVjtBQU1BLGFBQVNJLE1BQVQsQ0FBZ0JYLElBQWhCLEVBQW9DO0FBQUEsWUFBZEUsS0FBYyx1RUFBTixJQUFNOztBQUNoQyxlQUFPLElBQUlILFFBQUosQ0FBYUMsSUFBYixFQUFtQixJQUFuQixFQUF5QkUsS0FBekIsQ0FBUDtBQUNIO0FBQ0RILGFBQVNZLE1BQVQsR0FBa0JBLE1BQWxCO0FBQ0EsYUFBU0gsT0FBVCxDQUFpQlAsS0FBakIsRUFBd0I7QUFDcEIsZUFBTyxJQUFJRixRQUFKLENBQWFBLFNBQVNRLElBQVQsQ0FBY0MsT0FBM0IsRUFBb0NQLEtBQXBDLEVBQTJDLElBQTNDLENBQVA7QUFDSDtBQUNERixhQUFTUyxPQUFULEdBQW1CQSxPQUFuQjtBQUNILENBaEJELEVBZ0JHVCxhQUFhQSxXQUFXLEVBQXhCLENBaEJIO2tCQWlCZUEsUTtBQUNmIiwiZmlsZSI6InJlc3BvbnNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHRvdHRva290a2Qgb24gMTcvMDQvMjUuXG4gKi9cbmltcG9ydCBMaW5rSGVhZGVyUGFyc2VyIGZyb20gJ3BhcnNlLWxpbmstaGVhZGVyJztcbmltcG9ydCBMaW5rIGZyb20gJy4vbGluayc7XG5jbGFzcyBSZXNwb25zZSB7XG4gICAgY29uc3RydWN0b3IodHlwZSwgdmFsdWUsIGVycm9yKSB7XG4gICAgICAgIHRoaXMudHlwZSA9IHR5cGU7XG4gICAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgICAgICAgdGhpcy5lcnJvciA9IGVycm9yO1xuICAgIH1cbiAgICBsaW5rKHR5cGUpIHtcbiAgICAgICAgaWYgKCF0aGlzLmhlYWRlcnMgfHwgIXRoaXMuaGVhZGVycy5oYXMoJ0xpbmsnKSlcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICBjb25zdCBvYmogPSBMaW5rSGVhZGVyUGFyc2VyKHRoaXMuaGVhZGVycy5nZXQoJ0xpbmsnKSk7XG4gICAgICAgIHJldHVybiBuZXcgTGluayh0eXBlLCBvYmopO1xuICAgIH1cbiAgICBpc1N1Y2Nlc3MoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnR5cGUgPT09IFJlc3BvbnNlLlR5cGUuc3VjY2VzcztcbiAgICB9XG4gICAgaXNGYWlsdXJlKCkge1xuICAgICAgICByZXR1cm4gIXRoaXMuaXNTdWNjZXNzKCk7XG4gICAgfVxuICAgIGNvcHkobmV3VmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBSZXNwb25zZSh0aGlzLnR5cGUsIG5ld1ZhbHVlLCB0aGlzLmVycm9yKTtcbiAgICB9XG59XG4oZnVuY3Rpb24gKFJlc3BvbnNlKSB7XG4gICAgdmFyIFR5cGU7XG4gICAgKGZ1bmN0aW9uIChUeXBlKSB7XG4gICAgICAgIFR5cGVbVHlwZVtcInN1Y2Nlc3NcIl0gPSAwXSA9IFwic3VjY2Vzc1wiO1xuICAgICAgICBUeXBlW1R5cGVbXCJuZXR3b3JrRXJyb3JcIl0gPSAxXSA9IFwibmV0d29ya0Vycm9yXCI7XG4gICAgICAgIFR5cGVbVHlwZVtcImludmFsaWRTdGF0dXNcIl0gPSAyXSA9IFwiaW52YWxpZFN0YXR1c1wiO1xuICAgICAgICBUeXBlW1R5cGVbXCJudWxsTGlua1wiXSA9IDNdID0gXCJudWxsTGlua1wiO1xuICAgIH0pKFR5cGUgPSBSZXNwb25zZS5UeXBlIHx8IChSZXNwb25zZS5UeXBlID0ge30pKTtcbiAgICBmdW5jdGlvbiBjcmVhdGUodHlwZSwgZXJyb3IgPSBudWxsKSB7XG4gICAgICAgIHJldHVybiBuZXcgUmVzcG9uc2UodHlwZSwgbnVsbCwgZXJyb3IpO1xuICAgIH1cbiAgICBSZXNwb25zZS5jcmVhdGUgPSBjcmVhdGU7XG4gICAgZnVuY3Rpb24gc3VjY2Vzcyh2YWx1ZSkge1xuICAgICAgICByZXR1cm4gbmV3IFJlc3BvbnNlKFJlc3BvbnNlLlR5cGUuc3VjY2VzcywgdmFsdWUsIG51bGwpO1xuICAgIH1cbiAgICBSZXNwb25zZS5zdWNjZXNzID0gc3VjY2Vzcztcbn0pKFJlc3BvbnNlIHx8IChSZXNwb25zZSA9IHt9KSk7XG5leHBvcnQgZGVmYXVsdCBSZXNwb25zZTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPXJlc3BvbnNlLmpzLm1hcCJdfQ==