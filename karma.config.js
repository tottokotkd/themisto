var path = require('path')
var argv = require('yargs').argv
process.env.BABEL_ENV = 'karma'

const coverage_reporters = [
  { type: 'text-summary' },
]
const reporters = [
  'spec',
  'coverage',
]
var browsers = ['PhantomJS'] // for local builds

coverage_reporters.push({ type : 'html', dir : 'coverage', 'subdir' : '.' })

module.exports = function (config) {
  config.set({
    browsers: browsers,
    coverageReporter: {
      reporters: coverage_reporters,
    },
    files: [
      './node_modules/phantomjs-polyfill/bind-polyfill.js',
      './node_modules/url-search-params/build/url-search-params.js',
      './node_modules/whatwg-fetch/fetch.js',
      'tests.webpack.js',
    ],
    singleRun: !argv.watch,
    frameworks: [
      'mocha',
      'chai-sinon',
      'chai-as-promised',
      'chai',
    ],
    preprocessors: {
      'tests.webpack.js': ['webpack', 'sourcemap'],
    },
    reporters: reporters,
    // logLevel: config.LOG_DEBUG,
    // client: {
    //   captureConsole: true,
    //   mocha: {
    //     bail: true
    //   },
    // },
    plugins: [
      'karma-chai',
      'karma-chai-as-promised',
      'karma-chai-sinon',
      'karma-chrome-launcher',
      'karma-coverage',
      'karma-coveralls',
      'karma-mocha',
      'karma-phantomjs-launcher',
      'karma-sauce-launcher',
      'karma-sourcemap-loader',
      'karma-spec-reporter',
      'karma-webpack',
    ],
    webpack: {
      cache: true,
      devtool: 'inline-source-map',
      debug: true,
      module: {
        preLoaders: [
          {
            test: /\.json$/,
            exclude: /node_modules/,
            loader: 'json',
          },
          {
            test: /.js$/,
            include: /spec/,
            exclude: /(bower_components|node_modules)/,
            loader: 'babel',
            query: {
              cacheDirectory: true,
            },
          },
          {
            test: /\.js?$/,
            include: /lib/,
            exclude: /(node_modules|bower_components|spec)/,
            loader: 'babel-istanbul',
            query: {
              cacheDirectory: true,
            },
          },
        ],
        loaders: [
          {
            test: /\.js$/,
            include: path.resolve(__dirname, '../lib'),
            exclude: /(bower_components|node_modules|spec)/,
            loader: 'babel',
            query: {
              cacheDirectory: true,
            },
          }, {
            test: /\.jpg$/,
            loader: 'url-loader?mimetype=image/jpg',
          },
        ],
      },
    },
  })
}
