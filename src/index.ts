/**
 * Created by tottokotkd on 17/04/24.
 */

import 'babel-polyfill'

import Mastodon from './client'
import _Link from './link'
import _Response from './response'

import _Account from './entity/account'
import _Attachment from './entity/attachment'
import _Application from './entity/application'
import _Card from './entity/card'
import _Context from './entity/context'
import _Error from './entity/error'
import _Instance from './entity/instance'
import _Mention from './entity/mention'
import _Notification from './entity/notification'
import _Relationship from './entity/relationship'
import _Report from './entity/report'
import _Result from './entity/result'
import _Status from './entity/status'
import _Tag from './entity/tag'

export default Mastodon
export const Link = _Link
export const Response = _Response

export const Account = _Account
export const Attachment = _Attachment
export const Application = _Application
export const Card = _Card
export const Context = _Context
export const Error = _Error
export const Instance = _Instance
export const Mention = _Mention
export const Notification = _Notification
export const Relationship = _Relationship
export const Report = _Report
export const Result = _Result
export const Status = _Status
export const Tag = _Tag
