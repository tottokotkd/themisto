/**
 * Created by tottokotkd on 17/04/24.
 */

import some from 'lodash/some'

import Entity from '../entity'

class Attachment extends Entity {

  // ID of the attachment
  id () {
    return this.json.id
  }

  // One of: "image", "video", "gifv"
  type () {
    return this.json.type
  }

  // URL of the locally hosted version of the image
  url () {
    return this.json.url
  }

  // For remote images, the remote URL of the original image
  remoteUrl () {
    return this.json.remote_url
  }

  // URL of the preview image
  previewUrl () {
    return this.json.preview_url
  }

  // Shorter URL for the image, for insertion into text (only present on local images)
  textUrl () {
    return this.json.text_url
  }

  hasUrl (url) {
    const urls = [this.url(), this.remoteUrl(), this.previewUrl(), this.textUrl()]
    return some(urls, u => u === url)
  }
}

export default Attachment
