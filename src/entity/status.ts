/**
 * Created by tottokotkd on 17/04/24.
 */

import jQuery from 'jquery'
import find from 'lodash/find'

import Entity from '../entity'
import Account from './account'
import Application from './application'
import Attachment from './attachment'
import Mention from './mention'
import Tag from './tag'

class Status extends Entity {

  constructor (json) {
    super(json)

    this.cache.created_at = json.created_at ? Date.parse(json.created_at) : null
  }

  // The ID of the statuses
  id () {
    return this.json.id
  }

  // A Fediverse-unique resource ID
  uri () {
    return this.json.uri
  }

  // URL to the statuses page (can be remote)
  url () {
    return this.json.url
  }

  // The Account which posted the statuses
  account () {
    if (this.json.account) {
      return new Account(this.json.account)
    }

    return null
  }

  // null or the ID of the statuses it replies to
  inReplyToId () {
    return this.json.in_reply_to_id
  }

  // null or the ID of the account it replies to
  inReplyToAccountId () {
    return this.json.in_reply_to_account_id
  }

  // null or the reblogged Status
  reblog () {
    if (this.json.reblog) {
      return new Status(this.json.reblog)
    }

    return null
  }

  // Body of the statuses; this will contain HTML (remote HTML already sanitized)
  content () {
    if (this.cache.content) return this.cache.content

    const arr = jQuery.parseHTML(this.json.content)
    const content = jQuery(arr)

    const attachments = this.mediaAttachments()
    const mentions = this.mentions()
    const tags = this.tags()

    jQuery('a', content).each((index, element: HTMLAnchorElement) => {
      const href = element.href
      if (!href) return

      const attachment = find(attachments, a => a.hasUrl(href))
      if (attachment) {
        element.innerHTML = ':)'
        element.className = `attachment_link ${attachment.type()}`
        return
      }

      const tag = find(tags, t => t.hasUrl(href))
      const frontLetter = element.innerHTML[0]
      const parent = jQuery(element).parent('.tag')
      if (tag || (frontLetter === '#') || parent.length !== 0) {
        element.className = 'tag_link'
        return
      }

      const mention = find(mentions, m => m.hasUrl(href))
      if (mention) {
        element.className = 'mention_link'
        return
      }

      element.className = 'external_link'
    })

    const container = jQuery('<div/>')
    content.each((index, element) => {
      container.append(element)
    })
    this.cache.content = container.html()

    return this.cache.content
  }

  // The time the statuses was created
  createdAt () {
    if (this.cache.created_at) {
      return new Date(this.cache.created_at)
    }

    return null
  }

  createdBefore (current) {
    return this.getTimeRange(this.cache.created_at, current)
  }

  // The number of reblogs for the statuses
  reblogsCount () {
    return this.json.reblogs_count
  }

  // The number of favourites for the statuses
  favouritesCount () {
    return this.json.favourites_count
  }

  // Whether the authenticated user has reblogged the statuses
  reblogged () {
    return !!this.json.reblogged
  }

  // Whether the authenticated user has favorited the statuses
  favorited () {
    return !!this.json.favourited
  }

  // Whether media attachments should be hidden by default
  sensitive () {
    return !!this.json.sensitive
  }

  // If not empty, warning text that should be displayed before the actual content
  spoilerText () {
    return this.json.spoiler_text
  }

  // One of: public, unlisted, private, direct
  visibility (): Status.Visibility {
    switch (this.json.visibility) {
      case 'public':
        return Status.Visibility.public
      case 'unlisted':
        return Status.Visibility.unlisted
      case 'private':
        return Status.Visibility.private
      case 'direct':
        return Status.Visibility.direct
      default:
        return null
    }
  }

  // An array of Attachments
  mediaAttachments () {
    return this.array(this.json.media_attachments).map(json => new Attachment(json))
  }

  // Whether Attachments exist or not
  hasMediaAttachments () {
    return !!this.json.media_attachments
  }

  // An array of Mentions
  mentions () {
    return this.array(this.json.mentions).map(json => new Mention(json))
  }

  // An array of Tags
  tags () {
    return this.array(this.json.tags).map(json => new Tag(json))
  }

  // Application from which the statuses was posted
  application () {
    if (this.json.application) {
      return new Application(this.json.application)
    }

    return null
  }

}

module Status {
  export enum Visibility {
    public,
    unlisted,
    private,
    direct,
  }
}

export default Status
