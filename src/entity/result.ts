/**
 * Created by tottokotkd on 17/04/24.
 */

import Account from './account'
import Entity from '../entity'
import Status from './status'

class Result extends Entity {

  // An array of matched Accounts
  accounts () {
    return (this.json.accounts || []).map(j => new Account(j))
  }

  // An array of matchhed Statuses
  statuses () {
    return (this.json.statuses || []).map(j => new Status(j))
  }

  // An array of matched hashtags, as strings
  hashtags () {
    return (this.json.hashtags || [])
  }

}

export default Result
