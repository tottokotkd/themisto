/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'

class Relationship extends Entity {

  // Target account id
  id () {
    return this.json.id
  }

  // Whether the user is currently following the account
  following () {
    return !!this.json.following
  }

  // Whether the user is currently being followed by the account
  followedBy () {
    return !!this.json.followed_by
  }

  // Whether the user is currently blocking the account
  blocking () {
    return !!this.json.blocking
  }

  // Whether the user is currently muting the account
  muting () {
    return !!this.json.muting
  }

  // Whether the user has requested to follow the account
  requested () {
    return !!this.json.requested
  }

}

export default Relationship
