/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'

class Instance extends Entity {

  // URI of the current instance
  uri () {
    return this.json.uri
  }

  // The instance's title
  title () {
    return this.json.title
  }

  // A description for the instance
  description () {
    return this.json.description
  }

  // An email address which can be used to contact the instance administrator
  email () {
    return this.json.email
  }

}

export default Instance
