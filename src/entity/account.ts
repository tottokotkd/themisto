/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'

class Account extends Entity {

  // ID of the account
  id () {
    return this.json.id
  }

  // The username of the account
  username () {
    return this.json.username
  }

  // Equals username for local users, includes @domain for remote ones
  acct () {
    return this.json.acct
  }

  // The account's display name
  displayName () {
    return this.json.display_name
  }

  // Boolean for when the account cannot be followed without waiting for approval first
  locked () {
    return !!this.json.locked
  }

  // The time the account was created
  createdAt () {
    return Date.parse(this.json.created_at)
  }

  // The number of followers for the account
  followersCount () {
    return this.json.followers_count
  }

  // The number of accounts the given account is following
  followingCount () {
    return this.json.following_count
  }

  // The number of statuses the account has made
  statusesCount () {
    return this.json.statuses_count
  }

  // Biography of user
  note () {
    return this.json.note
  }

  // URL of the user's profile page (can be remote)
  url () {
    return this.json.url
  }

  // URL to the avatar image
  avatar () {
    if (!this.json.avatar) return null
    const url = this.json.avatar || ''
    return url.match(new RegExp('^/')) ? '' : url
  }

  // URL to the avatar static image (gif)
  staticAvatar () {
    return this.json.avatar_static
  }

  // URL to the header image
  header () {
    return this.json.header
  }

  // URL to the header static image (gif)
  staticHeader () {
    return this.json.header_static
  }
}

export default Account
