/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'

class Card extends Entity {

  // The url associated with the card
  url () {
    return this.json.url
  }

  // The title of the card
  title () {
    return this.json.title
  }

  // The card description
  description () {
    return this.json.description
  }

  // The image associated with the card, if any
  image () {
    return this.json.image
  }

}

export default Card
