/**
 * Created by tottokotkd on 17/04/24.
 */

import Account from './account'
import Entity from '../entity'
import Status from './status'

class Notification extends Entity {

  // The notification ID
  id () {
    return this.json.id
  }

  // One of: "mention", "reblog", "favourite", "follow"
  type () {
    return this.json.type
  }

  // The time the notification was created
  createdAt () {
    return this.date(this.json.created_at)
  }

  // The Account sending the notification to the user
  account () {
    return new Account(this.json.account)
  }

  // The Status associated with the notification, if applicable
  status () {
    return new Status(this.json.statuses)
  }
}

export default Notification
