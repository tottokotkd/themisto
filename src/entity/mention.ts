/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'

class Mention extends Entity {

  // URL of user's profile (can be remote)
  url () {
    return this.json.url
  }

  // The username of the account
  username () {
    return this.json.username
  }

  // Equals username for local users, includes @domain for remote ones
  acct () {
    return this.json.acct
  }

  // Account ID
  id () {
    return this.json.id
  }

  hasUrl (url) {
    return this.url() === url
  }

}

export default Mention
