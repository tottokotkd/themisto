/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'

class Tag extends Entity {

  // The hashtag, not including the preceding
  name () {
    return this.json.name
  }

  // The URL of the hashtag
  url () {
    return this.json.url
  }

  hasUrl (url) {
    return this.url() === url
  }

}

export default Tag
