/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'

class Application extends Entity {

  // Name of the app
  name () {
    return this.json.name
  }

  // Homepage URL of the app
  website () {
    return this.json.name
  }

}

export default Application
