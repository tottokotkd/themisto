/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'

class Report extends Entity {

  // The ID of the report
  id () {
    return this.json.id
  }

  // The action taken in response to the report
  actionTaken () {
    return this.json.action_taken
  }

}

export default Report
