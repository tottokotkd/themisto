/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'

class Error extends Entity {

  // A textual description of the error
  error () {
    return this.json.error
  }

}

export default Error
