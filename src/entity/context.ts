/**
 * Created by tottokotkd on 17/04/24.
 */

import Entity from '../entity'
import Status from './status'

class Context extends Entity {

  // The ancestors of the statuses in the conversation, as a list of Statuses
  ancestors () {
    return this.json.ancestors.map(Status.constructor)
  }

  // The descendants of the statuses in the conversation, as a list of Statuses
  descendants () {
    return this.json.descendants(Status.constructor)
  }

}

export default Context
