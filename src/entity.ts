/**
 * Created by tottokotkd on 17/04/24.
 */

import isInteger from 'lodash/isInteger'

class Entity {
  protected json: any
  protected cache: any

  constructor (json) {
    this.json = json
    this.cache = {}
  }

  date (str) {
    return new Date(str)
  }

  array (obj) {
    if (Array.isArray(obj)) {
      return obj
    }

    return []
  }

  getTimeRange (start, end) {
    if (!isInteger(start) || !isInteger(end)) return null

    let length = Math.floor((end - start) / 1000)
    const days = Math.floor(length / (3600 * 24))

    length -= days * 3600 * 24
    const hours = Math.floor(length / 3600)

    length -= hours * 3600
    const minutes = Math.floor(length / 60)

    length -= minutes * 60
    const seconds = length

    return {days, hours, minutes, seconds}
  }

}

export default Entity
