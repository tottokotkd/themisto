/**
 * Created by tottokotkd on 17/04/25.
 */

import LinkHeaderParser from 'parse-link-header'
import Link from './link'

class Response<T> {
  public type: Response.Type
  public value: T
  public headers?: Headers
  public error: any

  constructor (type: Response.Type, value: T, error: any) {
    this.type = type
    this.value = value
    this.error = error
  }

  link (type) {
    if (!this.headers || !this.headers.has('Link')) return null
    const obj = LinkHeaderParser(this.headers.get('Link'))
    return new Link(type, obj)
  }

  isSuccess () {
    return this.type === Response.Type.success
  }

  isFailure () {
    return !this.isSuccess()
  }

  copy<S> (newValue: S): Response<S> {
    return new Response(this.type, newValue, this.error)
  }
}

module Response {

  export enum Type {
    success,
    networkError,
    invalidStatus,
    nullLink,
  }

  export function create<T> (type, error = null): Response<T> {
    return new Response<T>(type, null, error)
  }

  export function success<T> (value: T): Response<T> {
    return new Response<T>(Response.Type.success, value, null)
  }
}

export default Response
