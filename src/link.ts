/**
 * Created by tottokotkd on 17/04/26.
 */

class Link {
  public next: any
  public prev: any
  public type: any

  constructor (type, obj) {
    this.type = type
    this.next = obj.next || null
    this.prev = obj.prev || null
  }
}

module Link {
  export enum Type {
    statuses,
    accounts,
    notifications,
  }
}

export default Link
