/**
 * Created by tottokotkd on 17/04/24.
 */

import {OAuth2} from 'oauth'

import Link from './link'
import R from './response'

import Account from './entity/account'
import Attachment from './entity/attachment'
import Card from './entity/card'
import Context from './entity/context'
import Instance from './entity/instance'
import Notification from './entity/notification'
import Relationship from './entity/relationship'
import Status from './entity/status'
import Result from './entity/result'
import Report from './entity/report'

class Mastodon {
  /**
   * callback URL
   */
  private callback: string

  /**
   * client name
   */
  private name: string

  /**
   * site URL
   */
  private site: string

  /**
   * OAuth scope
   */
  private scopes: [string]

  /**
   * mastodon server URLs
   */
  private url: Mastodon.URLs

  /**
   * OAuth credentials
   */
  private credential: Mastodon.Credentials

  /**
   * https / wss flag
   */
  public useSecureProtocol: boolean

  /**
   * constructor
   */
  constructor () {
    this.callback = 'urn:ietf:wg:oauth:2.0:oob'

    // client params
    this.name = 'themisto-lib'
    this.site = null
    this.scopes = ['read', 'write', 'follow']

    // URLs
    this.url = {
      base: null,
      api: null,
      stream: null,
    }

    // credential
    this.credential = {
      clientID: null,
      clientSecret: null,
      accessToken: null,
      refreshToken: null,
    }

    // https flag
    this.useSecureProtocol = true
  }

  /**
   * set client name for app registration
   * @param name client name
   */
  setClientName (name: string) {
    this.name = name
  }

  /**
   * set site url for app registration
   * @param site client site URL
   */
  setClientSiteUrl (site: string) {
    this.site = site
  }

  /**
   * set credentials for OAuth
   * @param credentials
   */
  setCredentials (credentials: Mastodon.Credentials) {
    this.credential = {
      clientID: credentials.clientID,
      clientSecret: credentials.clientSecret,
      accessToken: credentials.accessToken,
      refreshToken: credentials.refreshToken,
    }
  }

  /**
   * set scopes for app registration
   * @param scopes
   */
  setScopes (scopes: [string]) {
    this.scopes = scopes
  }

  /**
   * set URLs for fetch
   * @param base
   * @param api
   * @param stream
   */
  setURLs ({base, api, stream}) {
    this.url = {
      base: base,
      api: api || base,
      stream: stream || base,
    }
  }

  /**
   * export client config
   * @returns {{name: string, site: string, scopes: [string], url: Mastodon.URLs, credential: Mastodon.Credentials}}
   */
  exportConfig (): {name: string, site: string, scopes: [string], url: Mastodon.URLs, credential: Mastodon.Credentials} {
    return {
      name: this.name,
      site: this.site,
      scopes: this.scopes,
      url: {
        base: this.url.base,
        api: this.url.api,
        stream: this.url.stream,
      },
      credential: {
        clientID: this.credential.clientID,
        clientSecret: this.credential.clientSecret,
        accessToken: this.credential.accessToken,
        refreshToken: this.credential.refreshToken,
      },
    }
  }

  /*
   * Auth
   */

  /**
   * Registering an application:
   *
   * POST /api/v1/apps
   *
   * Form data:
   *
   * client_name:
   *   Name of your application
   * redirect_uris:
   *   Where the user should be redirected after authorization
   *   (for no redirect, use urn:ietf:wg:oauth:2.0:oob)
   * scopes:
   *   This can be a space-separated list of the following items: "read", "write" and "follow"
   *   (see this page for details on what the scopes do)
   *   website: (optional)
   *   URL to the homepage of your app
   *
   * Creates a new OAuth app. Returns id, client_id and client_secret which can be used
   * with OAuth authentication in your 3rd party app.
   *
   * These values should be requested in the app itself from the API for each
   * new app install + mastodon domain combo, and stored in the app for future requests.
   *
   * @returns {Promise<R{}>}
   */
  async registerApp(): Promise<R<{}>> {
    const api = this._getApi('apps')
    const method = 'POST'
    const body = new URLSearchParams()
    body.set('client_name', this.name)
    body.set('redirect_uris', this.callback)
    body.set('scopes', this.scopes.join(' '))

    const response = await this._fetchJson(api, {method, body})
    if (response.isSuccess()) {
      const {client_id, client_secret} = response.value.json
      this.setCredentials({
        clientID: client_id,
        clientSecret: client_secret,
      })
    }

    return response
  }

  /**
   * get OAuth authorizing URL
   * @returns {string}
   */
  getAuthorizeURL (): string {
    const {clientID, clientSecret} = this.exportConfig().credential
    const oauth = new OAuth2(clientID, clientSecret, this._getHTTPS(), null, '/oauth/token')
    return oauth.getAuthorizeUrl({
      redirect_uri: this.callback,
      response_type: 'code',
      scope: this.scopes.join(' '),
    })
  }

  /**
   * send OAuth code
   * @param code
   * @returns {Promise<R{}>}
   */
  async authenticate (code: string) {
    const {clientID, clientSecret} = this.exportConfig().credential
    const oauth = new OAuth2(clientID, clientSecret, this._getHTTPS(), null, '/oauth/token')
    const auth = new Promise<{accessToken, refreshToken, response}>((resolve, reject) => {
      oauth.getOAuthAccessToken(code, {
        grant_type: 'authorization_code',
        redirect_uri: this.callback,
      }, (err, accessToken, refreshToken, response) => {
        if (err) {
          reject(err)
        } else {
          resolve({accessToken, refreshToken, response})
        }
      })
    })

    try {
      const {accessToken, refreshToken, response} = await auth
      this.setCredentials({clientID, clientSecret, accessToken, refreshToken})
      return R.success({accessToken, refreshToken, response})
    } catch (error) {
      return R.create(R.Type.networkError, error)
    }
  }

  /*
   * Accounts
   */

  // Fetching an account:
  //
  // GET /api/v1/accounts/:id
  //
  // Returns an Account.

  async getAccount (id: number): Promise<R<{account: Account}> | R<{}>> {
    const url = this._getApi('accounts', id)
    const response = await this._authFetchJson(url)

    if (response.isFailure()) return response

    const account = new Account(response.value.json)
    return response.copy({account})
  }

  // Getting the current user:
  //
  // GET /api/v1/accounts/verify_credentials
  //
  // Returns the authenticated user's Account.

  async getCurrentAccount (): Promise<R<{account: Account}> | R<{}>> {
    const url = this._getApi('accounts', null, 'verify_credentials')
    const response = await this._authFetchJson(url)

    if (response.isFailure()) return response

    const account = new Account(response.value.json)
    return response.copy({account})
  }

  //   Updating the current user:
  //
  //   PATCH /api/v1/accounts/update_credentials
  //
  //   Form data:
  //
  //   display_name: The name to display in the user's profile
  //   note: A new biography for the user
  //   avatar: A base64 encoded image to display as the user's avatar (e.g. data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUoAAADrCAYAAAA...)
  //   header: A base64 encoded image to display as the user's header image (e.g. data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUoAAADrCAYAAAA...)

  async updateCurrentAccount (
    {
      displayName = null,
      note = null,
      avatar = null,
      header = null
    } = {}): Promise<R<{account: Account}> | R<{}>> {
    const url = this._getApi('accounts', null, 'update_credentials')
    const method = 'PATCH'

    const body = new URLSearchParams()
    if (displayName) body.set('display_name', displayName)
    if (note) body.set('note', note)
    if (avatar) body.set('avatar', avatar)
    if (header) body.set('header', header)

    const response = await this._authFetchJson(url, {method, body})
    if (response.isFailure()) return response

    const account = new Account(response.value.json)
    return response.copy({account})
  }

  //   Getting an account's followers:
  //
  //   GET /api/v1/accounts/:id/followers
  //
  //   Returns an array of Accounts.

  async getFollowers (id: number): Promise<R<{link: Link, accounts: [Account]}> | R<{}>> {
    const url = this._getApi('accounts', id, 'followers')

    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const accounts = response.value.json.map(v => new Account(v))
    const link = response.link(Link.Type.accounts)
    return response.copy({link, accounts})
  }

  //   Getting who account is following:
  //
  //   GET /api/v1/accounts/:id/following
  //
  //   Returns an array of Accounts.

  async getFollowings (id: number): Promise<R<{link: Link, accounts: [Account]}> | R<{}>> {
    const url = this._getApi('accounts', id, 'following')

    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const accounts = response.value.json.map(v => new Account(v))
    const link = response.link(Link.Type.accounts)
    return response.copy({link, accounts})
  }

  //   Getting an account's statuses:
  //
  //   GET /api/v1/accounts/:id/statuses
  //
  //   Query parameters:
  //
  //   only_media (optional): Only return statuses that have media attachments
  //   exclude_replies (optional): Skip statuses that reply to other statuses
  //
  //   Returns an array of Statuses.

  async getStatusesOfAccount (
    id: number,
    params = {
      onlyMedia: false,
      excludeReplies: true
    }): Promise<R<{link: Link, statuses: [Status]}> | R<{}>> {
    const url = this._getApi('accounts', id, 'statuses', {
      only_media: params.onlyMedia,
      exclude_replies: params.excludeReplies,
    })

    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const statuses = response.value.json.map(v => new Status(v))
    const link = response.link(Link.Type.statuses)
    return response.copy({link, statuses})
  }

  //   Following an account:
  //
  //   POST /api/v1/accounts/:id/follow
  //
  //   Returns the target account's Relationship.

  async followAccount (id: number): Promise<R<{relationship: Relationship}> | R<{}>> {
    const url = this._getApi('accounts', id, 'follow')
    const method = 'POST'

    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const relationship = new Relationship(response.value.json)
    return response.copy({relationship})
  }

  //   Unfollowing an account:
  //
  //   POST /api/v1/accounts/:id/unfollow
  //
  //   Returns the target account's Relationship.

  async unfollowAccount (id: number): Promise<R<{relationship: Relationship}> | R<{}>> {
    const url = this._getApi('accounts', id, 'unfollow')
    const method = 'POST'

    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const relationship = new Relationship(response.value.json)
    return response.copy({relationship})
  }

  //   Blocking an account:
  //
  //   POST /api/v1/accounts/:id/block
  //
  //   Returns the target account's Relationship.

  async blockAccount (id: number): Promise<R<{relationship: Relationship}> | R<{}>> {
    const url = this._getApi('accounts', id, 'block')
    const method = 'POST'

    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const relationship = new Relationship(response.value.json)
    return response.copy({relationship})
  }

  //   Unblocking an account:
  //
  //   POST /api/v1/accounts/:id/unblock
  //
  //   Returns the target account's Relationship.

  async unblockAccount (id: number): Promise<R<{relationship: Relationship}> | R<{}>> {
    const url = this._getApi('accounts', id, 'unblock')
    const method = 'POST'

    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const relationship = new Relationship(response.value.json)
    return response.copy({relationship})
  }

  //   Muting an account:
  //
  //   POST /api/v1/accounts/:id/mute
  //
  //   Returns the target account's Relationship.

  async muteAccount (id: number): Promise<R<{relationship: Relationship}> | R<{}>> {
    const url = this._getApi('accounts', id, 'mute')
    const method = 'POST'

    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const relationship = new Relationship(response.value.json)
    return response.copy({relationship})
  }

  //   Unmuting an account:
  //
  //   POST /api/v1/accounts/:id/unmute
  //
  //   Returns the target account's Relationship.
  async unmuteAccount (id: number): Promise<R<{relationship: Relationship}> | R<{}>> {
    const url = this._getApi('accounts', id, 'unmute')
    const method = 'POST'

    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const relationship = new Relationship(response.value.json)
    return response.copy({relationship})
  }

  //   Getting an account's relationships:
  //
  //   GET /api/v1/accounts/relationships
  //
  //   Query parameters:
  //
  //   id (can be array): Account IDs
  //
  //   Returns an array of Relationships of the current user to a list of given accounts.
  async getRelationships (ids: any): Promise<R<{relationships: [Relationship]}> | R<{}>> {
    const targets = Array.isArray(ids) ? ids : [ids]
    const url = this._getApi('accounts', null, 'relationships', {id: targets})
    // const url = this._getApi('accounts', null, `relationships?id[]=${ids[0]}&id[]=${ids[1]}`, {id: targets})
    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const relationships = response.value.json.map(v => new Relationship(v))
    return response.copy({relationships})
  }

  //   Searching for accounts:
  //
  //   GET /api/v1/accounts/search
  //
  //   Query parameters:
  //
  //   q: What to search for
  //   limit: Maximum number of matching accounts to return (default: 40)
  //
  //   Returns an array of matching Accounts.
  //   Will lookup an account remotely if the search term is
  //   in the username@domain format and not yet in the database.
  async searchAccounts (query: string, limit = 40): Promise<R<{accounts: [Account]}> | R<{}>> {
    const url = this._getApi('accounts', null, 'search', {q: query, limit})
    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const accounts = response.value.json.map(v => new Account(v))
    return response.copy({accounts})
  }

  /*
  * Blocks
  */

  //   Fetching a user's blocks:
  //
  //   GET /api/v1/blocks
  //
  //   Returns an array of Accounts blocked by the authenticated user.

  async getBlocks (): Promise<R<{link: Link, accounts: [Account]}> | R<{}>> {
    const url = this._getApi('blocks')
    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const accounts = response.value.json.map(v => new Account(v))
    const link = response.link(Link.Type.accounts)
    return response.copy({link, accounts})
  }

  /*
   * Favourites
   */

  //   Fetching a user's favourites:
  //
  //   GET /api/v1/favourites
  //
  //   Returns an array of Statuses favourited by the authenticated user.

  async getFavorites (): Promise<R<{link: Link, statuses: [Status]}> | R<{}>> {
    const url = this._getApi('favourites')
    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const statuses = response.value.json.map(v => new Status(v))
    const link = response.link(Link.Type.statuses)
    return response.copy({link, statuses})
  }

  /*
   * Follow Requests
   */

  //   Fetching a list of follow requests:
  //
  //   GET /api/v1/follow_requests
  //
  //   Returns an array of Accounts which have requested to follow the authenticated user.

  async getFollowRequests (): Promise<R<{link: Link, accounts: [Account]}> | R<{}>> {
    const url = this._getApi('follow_requests')
    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const accounts = response.value.json.map(v => new Account(v))
    const link = response.link(Link.Type.accounts)
    return response.copy({link, accounts})
  }

  //   Authorizing follow requests:
  //
  //   POST /api/v1/follow_requests/:id/authorize
  //
  //   Form data:
  //
  //   id: The id of the account to authorize or reject
  //
  //   Returns an empty object.

  async authorizeFollowRequest (id: number): Promise<R<{}>> {
    const url = this._getApi('follow_requests', id, 'authorize')
    const method = 'POST'
    const body = new FormData()
    body.append('id', String(id))

    return await this._authFetch(url, {method, body})
  }

  //   Rejecting follow requests:
  //
  //   POST /api/v1/follow_requests/:id/reject
  //
  //   Form data:
  //
  //   id: The id of the account to authorize or reject
  //
  //   Returns an empty object.

  async rejectFollowRequest (id: number): Promise<R<{}>> {
    const url = this._getApi('follow_requests', id, 'reject')
    const method = 'POST'
    const body = new FormData()
    body.append('id', String(id))

    return await this._authFetch(url, {method, body})
  }

  /*
   * Follows
   */

  //   Following a remote user:
  //
  //   POST /api/v1/follows
  //
  //   Form data:
  //
  //   uri: username@domain of the person you want to follow
  //
  //   Returns the local representation of the followed account, as an Account.

  async followRemoteUser (uri: string): Promise<R<{account: Account}> | R<{}>> {
    const url = this._getApi('follows')
    const method = 'POST'
    const body = new FormData()
    body.append('uri', uri)

    const response = await this._authFetchJson(url, {method, body})
    if (response.isFailure()) return response

    const account = new Account(response.value.json)
    return response.copy({account})
  }

  /*
   * Instances
   */

  //   Getting instance information:
  //
  //   GET /api/v1/instance
  //
  //   Returns the current Instance. Does not require authentication.

  async getInstanceInfo (): Promise<R<{instance: Instance}> | R<{}>> {
    const url = this._getApi('instance')

    const response = await this._fetchJson(url)
    if (response.isFailure()) return response

    const instance = new Instance(response.value.json)
    return response.copy({instance})
  }

   /*
   * Media
   */

  //   Uploading a media attachment:
  //
  //   POST /api/v1/media
  //
  //   Form data:
  //
  //   file: Media to be uploaded
  //
  //   Returns an Attachment that can be used when creating a statuses.

  async uploadMediaAttachment (file: any): Promise<R<{attachment: Attachment}> | R<{}>> {
    const url = this._getApi('media')
    const method = 'POST'
    const body = new FormData()
    body.append('file', file)

    const response = await this._authFetchJson(url, {method, body})
    if (response.isFailure()) return response

    const attachment = new Attachment(response.value.json)
    return response.copy({attachment})
  }

  /*
   * Mutes
   */

  //   Fetching a user's mutes:
  //
  //   GET /api/v1/mutes
  //
  //   Returns an array of Accounts muted by the authenticated user.

  async getMutes (): Promise<R<{accounts: [Account]}> | R<{}>> {
    const url = this._getApi('mutes')

    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const accounts = response.value.json.map(v => new Account(v))
    return response.copy({accounts})
  }

  /*
   * Notifications
   */

  //   Fetching a user's notifications:
  //
  //   GET /api/v1/notifications
  //
  //   Returns a list of Notifications for the authenticated user.

  async getNotifications (): Promise<R<{link: Link, notifications: [Notification]}> | R<{}>> {
    const url = this._getApi('notifications')

    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const notifications = response.value.json.map(v => new Notification(v))
    const link = response.link(Link.Type.notifications)
    return response.copy({link, notifications})
  }

  //   Getting a single notification:
  //
  //   GET /api/v1/notifications/:id
  //
  //   Returns the Notification.

  async getNotification (id: number): Promise<R<{notification: Notification}> | R<{}>> {
    const url = this._getApi('notifications', id)

    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const notification = new Notification(response.value.json)
    return response.copy({notification})
  }

  //   Clearing notifications:
  //
  //   POST /api/v1/notifications/clear
  //
  //   Deletes all notifications from the Mastodon server for the authenticated user. Returns an empty object.

  async clearNotifications (): Promise<R<{}>> {
    const url = this._getApi('notifications', 'clear')
    const method = 'POST'
    return await this._authFetch(url, {method})
  }

  /*
   * Reports
   */

  //   Fetching a user's reports:
  //
  //   GET /api/v1/reports
  //
  //   Returns a list of Reports made by the authenticated user.

  async fetchReports (): Promise<R<{reports: [Report]}> | R<{}>> {
    const url = this._getApi('reports')

    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const reports = response.value.json.map(j => new Report(j))
    return response.copy({reports})
  }

  //   Reporting a user:
  //
  //   POST /api/v1/reports
  //
  //   Form data:
  //
  //   account_id: The ID of the account to report
  //   status_ids: The IDs of statuses to report (can be an array)
  //   comment: A comment to associate with the report.
  //
  //   Returns the finished Report.

  async reportUser ({accountId = null, statusIds = [], comment = null} = {}): Promise<R<{report: Report}> | R<{}>> {
    const url = this._getApi('reports')
    const method = 'POST'

    const body = new FormData()

    body.append('account_id', accountId)
    for (const id of statusIds) {
      body.append('status_ids[]', id)
    }
    body.append('comment', comment)

    const response = await this._authFetchJson(url, {method, body})
    if (response.isFailure()) return response

    const report = new Report(response.value.json)
    return response.copy({report})
  }

  /*
  * Search
  */

  //   Searching for content:
  //
  //   GET /api/v1/search
  //
  //   Form data:
  //
  //   q: The search query
  //   resolve: Whether to resolve non-local accounts
  //
  //   Returns Results. If q is a URL, Mastodon will attempt to fetch the provided account
  // or statuses. Otherwise, it will do a local account and hashtag search.

  async search (query: string, resolveRemoteAccounts = false): Promise<R<{result: Result}> | R<{}>> {
    const url = this._getApi('search', null, null, {q: query, resolve: resolveRemoteAccounts})
    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const result = new Result(response.value.json)
    return response.copy({result})
  }

  /*
  * Statuses
  */

  //   Fetching a status:
  //
  //   GET /api/v1/statuses/:id
  //
  //   Returns a Status.

  async fetchStatus (id: number): Promise<R<{status: Status}> | R<{}>> {
    const url = this._getApi('statuses', id)
    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const status = new Status(response.value.json)
    return response.copy({status})
  }

  //   Getting statuses context:
  //
  //   GET /api/v1/statuses/:id/context
  //
  //   Returns a Context.

  async getStatusContext (id: number): Promise<R<{context: Context}> | R<{}>> {
    const url = this._getApi('statuses', id, 'context')
    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const context = new Context(response.value.json)
    return response.copy({context})
  }

  //   Getting a card associated with a statuses:
  //
  //   GET /api/v1/statuses/:id/card
  //
  //   Returns a Card.

  async getStatusCard (id: number): Promise<R<{card: Card}> | R<{}>> {
    const url = this._getApi('statuses', id, 'card')
    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const card = new Card(response.value.json)
    return response.copy({card})
  }

  //   Getting who reblogged a statuses:
  //
  //   GET /api/v1/statuses/:id/reblogged_by
  //
  //   Returns an array of Accounts.

  async getRebloggingAccounts (id: number): Promise<R<{accounts: [Account]}> | R<{}>> {
    const url = this._getApi('statuses', id, 'reblogged_by')

    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const accounts = response.value.json.map(j => new Account(j))
    return response.copy({accounts})
  }

  //   Getting who favourited a statuses:
  //
  //   GET /api/v1/statuses/:id/favourited_by
  //
  //   Returns an array of Accounts.

  async getFavoritingAccounts (id: number): Promise<R<{accounts: [Account]}> | R<{}>> {
    const url = this._getApi('statuses', id, 'favourited_by')

    const response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const accounts = response.value.json.map(j => new Account(j))
    return response.copy({accounts})
  }

  //   Posting a new statuses:
  //
  //   POST /api/v1/statuses
  //
  //   Form data:
  //
  //   status: The text of the statuses
  //   in_reply_to_id (optional): local ID of the statuses you want to reply to
  //   media_ids (optional): array of media IDs to attach to the statuses (maximum 4)
  //   sensitive (optional): set this to mark the media of the statuses as NSFW
  //   spoiler_text (optional): text to be shown as a warning before the actual content
  //   visibility (optional): either "direct", "private", "unlisted" or "public"
  //
  //   Returns the new Status.

  async postStatus (text: string, {
    replyTo = null,
    mediaIds = [],
    sensitive = false,
    spoilerText = null,
    visibility = 'public',
  } = {}): Promise<R<{status: Status}> | R<{}>> {
    const url = this._getApi('statuses', null, null)
    const method = 'POST'

    const body = new FormData()
    body.append('status', text)
    if (replyTo) body.append('in_reply_to_id', replyTo)
    for (const id of mediaIds) {
      body.append('media_ids[]', id)
    }
    if (sensitive) body.append('sensitive', 'true')
    if (spoilerText) body.append('spoiler_text', spoilerText)
    body.append('visibility', visibility)

    const response = await this._authFetchJson(url, {method, body})
    if (response.isFailure()) return response

    const status = new Status(response.value.json)
    return response.copy({status})
  }

  //   Deleting a statuses:
  //
  //   DELETE /api/v1/statuses/:id
  //
  //   Returns an empty object.

  async deleteStatus (id: number): Promise<R<{}>> {
    const url = this._getApi('statuses', id)
    const method = 'DELETE'
    return await this._authFetch(url, {method})
  }

  //   Reblogging a statuses:
  //
  //   POST /api/v1/statuses/:id/reblog
  //
  //   Returns the target Status.

  async reblogStatus (id: number): Promise<R<{status: Status}> | R<{}>> {
    const url = this._getApi('statuses', id, 'reblog')
    const method = 'POST'
    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const status = new Status(response.value.json)
    return response.copy({status})
  }

  //   Unreblogging a statuses:
  //
  //   POST /api/v1/statuses/:id/unreblog
  //
  //   Returns the target Status.

  async unreblogStatus (id: number): Promise<R<{status: Status}> | R<{}>> {
    const url = this._getApi('statuses', id, 'unreblog')
    const method = 'POST'
    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const status = new Status(response.value.json)
    return response.copy({status})
  }

  //   Favouriting a statuses:
  //
  //   POST /api/v1/statuses/:id/favourite
  //
  //   Returns the target Status.

  async favoriteStatus (id: number): Promise<R<{status: Status}> | R<{}>> {
    const url = this._getApi('statuses', id, 'favourite')
    const method = 'POST'
    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const status = new Status(response.value.json)
    return response.copy({status})
  }

  //   Unfavouriting a statuses:
  //
  //   POST /api/v1/statuses/:id/unfavourite
  //
  //   Returns the target Status.

  async unfavoriteStatus (id: number): Promise<R<{status: Status}> | R<{}>> {
    const url = this._getApi('statuses', id, 'unfavourite')
    const method = 'POST'
    const response = await this._authFetchJson(url, {method})
    if (response.isFailure()) return response

    const status = new Status(response.value.json)
    return response.copy({status})
  }

  /*
   * Timelines
   */

  //   Retrieving a home timeline:
  //
  //   GET /api/v1/timelines/home

  async getHomeTimeLine (): Promise<R<{link: Link, statuses: [Status]}> | R<{}>> {
    const url = this._getApi('timelines', null, 'home')
    const response = await this._authFetchJson(url)

    if (response.isFailure()) return response

    const statuses = response.value.json.map(v => new Status(v))
    const link = response.link(Link.Type.statuses)
    return response.copy({link, statuses})
  }

  //   Retrieving a public timeline:
  //
  //   GET /api/v1/timelines/public
  //
  //   Query parameters:
  //   local: Only return statuses originating from this instance

  async getTimeLine (local = false): Promise<R<{link: Link, statuses: [Status]}> | R<{}>> {
    const url = this._getApi('timelines', null, 'public', {local})
    const response = await this._authFetchJson(url)

    if (response.isFailure()) return response

    const statuses = response.value.json.map(v => new Status(v))
    const link = response.link(Link.Type.statuses)
    return response.copy({link, statuses})
  }

  //   Retrieving a hashtag timeline:
  //
  //   GET /api/v1/timelines/tag/:hashtag
  //
  //   Query parameters:
  //   local: Only return statuses originating from this instance
  async getHashTagTimeLine (hashtag: string, local = false): Promise<R<{link: Link, statuses: [Status]}> | R<{}>>  {
    const url = this._getApi('timelines', 'tag', hashtag, {local})
    const response = await this._authFetchJson(url)

    if (response.isFailure()) return response

    const statuses = response.value.json.map(v => new Status(v))
    const link = response.link(Link.Type.statuses)
    return response.copy({link, statuses})
  }

  /*
  * Link
  */

  async getNext (link: Link) {
    if (!link || !link.next || !link.next.url) return R.create(R.Type.nullLink)
    return await this._loadLinkUrl(link.next.url, link)
  }

  async getPrev (link: Link) {
    if (!link || !link.prev || !link.prev.url) return R.create(R.Type.nullLink)
    return await this._loadLinkUrl(link.prev.url, link)
  }

  /*
   * Stream
   */

  createUserStream (handler: Mastodon.StreamHandler) {
    const stream = 'user'
    return this.createStream({stream}, handler)
  }

  createPublicStream (local: boolean, handler: Mastodon.StreamHandler) {
    const stream = local ? 'public:local' : 'public'
    return this.createStream({stream}, handler)
  }

  createHashTagStream (tag, local: boolean, handler: Mastodon.StreamHandler) {
    const stream = local ? 'hashtag:local' : 'public'
    return this.createStream({stream, tag}, handler)
  }

  createStream (target, handler: Mastodon.StreamHandler) {
    target.access_token = this.credential.accessToken
    const socket = new WebSocket(this._getStream(target))
    socket.onopen = () => handler('open')
    socket.onclose = () => handler('close')
    socket.onmessage = msg => {
      const data = JSON.parse(msg.data)
      if (!data.payload) return

      switch (data.event) {
        case 'update': {
          const status = new Status(JSON.parse(data.payload))
          handler('message', 'update', status)
          break
        }
        case 'notification': {
          const notification = new Notification(JSON.parse(data.payload))
          handler('message', 'notification', notification)
          break
        }
        case 'delete': {
          const id = data.payload
          handler('message', 'delete', id)
          break
        }
        default:
          break
      }
    }
    return socket
  }

  /*
   * util
   */

  _getHTTPS () {
    const protocol = this.useSecureProtocol ? 'https' : 'http'
    return `${protocol}://${this.url.api}`
  }

  _getWWS () {
    const protocol = this.useSecureProtocol ? 'wss' : 'ws'
    return `${protocol}://${this.url.stream}`
  }

  _getApi (resource: string, id: string | number = null, action: string = null, params: object = null) {
    let r = (resource !== null) ? ('/' + resource) : ''
    let i = (id !== null) ? ('/' + id) : ''
    let a = (action !== null) ? ('/' + action) : ''
    let url = `${this._getHTTPS()}/api/v1${r}${i}${a}`

    if (!params) return url
    return `${url}?${this._queryParams(params)}`
  }

  _getStream (params: object = null) {
    const url = `${this._getWWS()}/api/v1/streaming`

    if (!params) return url
    return `${url}?${this._queryParams(params)}`
  }

  _queryParams(params: object): string {
    if (!params) return ''

    const urlParams = new URLSearchParams()

    for (const [key, value] of Object.entries(params)) {
      if (!value) continue

      if (Array.isArray(value)) {
        const k = `${key}[]`
        for (const v of value) {
          urlParams.append(k, v)
        }
      } else {
        urlParams.set(key, value)
      }
    }

    return urlParams.toString()
  }

  async _fetch (
    url: string, {
      body = null,
      method = 'GET',
      mode = 'CORS' as RequestMode,
      headers = {}
    }: RequestInit = {}): Promise<R<{response?: Response, status?: number, statusText?: string}>> {
    try {
      const response = await fetch(url, {
        body,
        method,
        mode: mode as RequestMode,
        headers,
      })

      if (!response.ok) {
        const info = {status: response.status, statusText: response.statusText, response}
        return R.create(R.Type.invalidStatus, info)
      }

      return R.success({response})
    } catch (error) {
      return R.create(R.Type.networkError, error)
    }
  }

  async _fetchJson (
    url, {
      body = null,
      method = 'GET',
      mode = 'CORS' as RequestMode,
      headers = {}
    }: RequestInit = {}): Promise<R<{json?: any}>> {
    const h = {}
    Object.assign(h, headers)
    try {
      const response = await this._fetch(url, {body, method, mode, headers: h})
      if (response.isFailure()) return response

      const json = await response.value.response.json()
      const result: R<{json?: any}> = R.success({json})
      result.headers = response.value.response.headers
      return result
    } catch (error) {
      return R.create(R.Type.networkError, error)
    }
  }

  async _authFetch (url, {body = null, method = 'GET', mode = 'CORS' as RequestMode, headers = {}}: RequestInit = {}) {
    return await this._fetch(url, {
      body,
      method,
      mode,
      headers: {
        Authorization: `Bearer ${this.credential.accessToken}`,
        ...headers,
      },
    })
  }

  async _authFetchJson (url, {body = null, method = 'GET', mode = 'CORS' as RequestMode, headers = {}}: RequestInit = {}) {
    return await this._fetchJson(url, {
      body,
      method,
      mode,
      headers: {
        Authorization: `Bearer ${this.credential.accessToken}`,
        ...headers,
      },
    })
  }

  async _loadLinkUrl (url: string, link: Link): Promise<any> {
    let response = await this._authFetchJson(url)
    if (response.isFailure()) return response

    const json = response.value.json
    let newLink = response.link(link.type)

    switch (link.type) {
      case Link.Type.statuses: {
        const statuses = json.map(j => new Status(j))
        return response.copy({link: newLink || link, statuses})
      }

      case Link.Type.accounts: {
        const statuses = json.map(j => new Account(j))
        return response.copy({link: newLink || link, statuses})
      }

      case Link.Type.notifications: {
        const notifications = json.map(j => new Notification(j))
        return response.copy({link: newLink || link, notifications})
      }

      default:
        throw new Error('not impl. link')
    }
  }
}

module Mastodon {
  export interface Credentials {
    clientID?: string
    clientSecret?: string,
    accessToken?: string,
    refreshToken?: string,
  }
  export interface URLs {
    base?: string
    api?: string,
    stream?: string,
  }
  export type StreamHandler = (mode: string, event?: string, payload?: any) => void
}

export default Mastodon
