/**
 * Created by tottokotkd on 17/04/27.
 */

import Mastodon from '../../lib/index'

import token from './token.json'
import urls from './urls.json'

export default () => {
  const client = new Mastodon()
  client.setClientName('themisto test')
  client.setURLs(urls)
  client.setCredentials(token)
  return client
}
