/**
 * Created by tottokotkd on 17/05/01.
 */

import Mastodon from '../../lib/index'

import token from './token2.json'
import urls from './urls.json'

export default () => {
  const client = new Mastodon()
  client.setClientName('themisto test')
  client.setURLs(urls)
  client.setCredentials(token)
  return client
}
