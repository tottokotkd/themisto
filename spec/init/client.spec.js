/**
 * Created by tottokotkd on 17/04/25.
 */

import Mastodon from '../../lib/index'

describe('init', function () {
  this.timeout(20000)

  it('should construct a instance, with dummy callback', () => {
    const client = new Mastodon()
    expect(client.callback).to.be.equals('urn:ietf:wg:oauth:2.0:oob')
  })

  describe('set / get', () => {
    it('should set / get credential', () => {
      const client = new Mastodon()
      let {clientID, clientSecret, accessToken, refreshToken} = client.exportConfig().credential
      expect(clientID).to.be.null
      expect(clientSecret).to.be.null
      expect(accessToken).to.be.null
      expect(refreshToken).to.be.null

      const value = {
        clientID: 'a',
        clientSecret: 'b',
        accessToken: 'c',
        refreshToken: 'd',
      }
      client.setCredentials(value)
      expect(client.exportConfig().credential).to.eql(value)
    })
    it('should set / get client name', () => {
      const client = new Mastodon()
      let name = client.exportConfig().name
      expect(name).to.be.equals('themisto-lib')

      const value = 'new name'
      client.setClientName(value)
      expect(client.exportConfig().name).to.equals(value)
    })
    it('should set / get URLs', () => {
      const client = new Mastodon()
      let {stream, api, base} = client.exportConfig().url
      expect(stream).to.be.null
      expect(api).to.be.null
      expect(base).to.be.null

      const value = {
        stream: 'a',
        api: 'b',
        base: 'c',
      }
      client.setURLs(value)
      expect(client.exportConfig().url).to.eql(value)
    })
    it('should set / get scopes', () => {
      const client = new Mastodon()
      expect(client.exportConfig().scopes).to.be.eql(['read', 'write', 'follow'])

      const value1 = ['read', 'follow']
      client.setScopes(value1)
      expect(client.exportConfig().scopes).to.eql(value1)

      const value2 = ['write']
      client.setScopes(value2)
      expect(client.exportConfig().scopes).to.eql(['write'])
    })
  })
})
