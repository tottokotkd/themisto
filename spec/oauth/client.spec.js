/**
 * Created by tottokotkd on 17/04/28.
 */

import testClient from '../testClient/lockedClient'

describe('client', function () {
  this.timeout(20000)

  describe('oauth', () => {
    it('should get auth URL', async () => {
      const client = testClient()
      const url = client.getAuthorizeURL()
      const redirect = encodeURIComponent('urn:ietf:wg:oauth:2.0:oob')
      const scopes = encodeURIComponent(client.scopes.join(' '))

      const base = client.url.base
      const id = client.exportConfig().credential.clientID
      const expected = `https://${base}/oauth/authorize?redirect_uri=${redirect}&response_type=code&scope=${scopes}&client_id=${id}`
      expect(url).to.be.equals(expected)
    })
    xit('should send auth token', async () => {
      const client = testClient()
      const token = '2. replace this :)' // only this to change :)

      if (token === '2. replace this :)') {
        const url = client.getAuthorizeURL()
        expect(url).to.be.equals('1. access this url :)')
      } else {
        const result = await client.authenticate(token)
        expect(result.isSuccess()).to.be.true

        expect(result.value).to.have.property('accessToken')
        expect(result.value).to.have.property('refreshToken')
        expect(`access token: ${result.value.accessToken}, refresh token: ${result.value.refreshToken}`)
          .to.be.eql('you should turn off this spec :)')
      }
    })
  })
})
