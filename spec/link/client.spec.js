/**
 * Created by tottokotkd on 17/04/27.
 */

import every from 'lodash/every'
import includes from 'lodash/includes'
import testClient from '../testClient/unlockedClient'

describe('link', function () {
  this.timeout(20000)
  it('should load prev statuses by link', async () => {
    let statuses = []
    const client = testClient()

    const response1 = await client.getTimeLine()
    expect(every(response1.value.statuses, s => !includes(statuses, ss => ss.id() === s.id())))
    statuses = statuses.concat(response1.value.statuses)

    const response2 = await client.getPrev(response1.value.link)
    if (response2.value.statuses.length) {
      expect(response2.value.link.prev.url).not.to.be.equals(response1.value.link.prev.url)
      expect(every(response2.value.statuses, s => !includes(statuses, ss => ss.id() === s.id())))
      statuses = statuses.concat(response2.value.statuses)
    } else {
      expect(response2.value.link).to.be.eql(response1.value.link)
    }

    const response3 = await client.getPrev(response2.value.link)
    if (response3.value.statuses.length) {
      expect(response3.value.link.prev.url).not.to.be.equals(response2.value.link.prev.url)
      expect(every(response3.value.statuses, s => !includes(statuses, ss => ss.id() === s.id())))
      statuses = statuses.concat(response3.value.statuses)
    } else {
      expect(response3.value.link).to.be.eql(response2.value.link)
    }

    const response4 = await client.getPrev(response3.value.link)
    if (response4.value.statuses.length) {
      expect(response4.value.link.prev.url).not.to.be.equals(response3.value.link.prev.url)
      expect(every(response4.value.statuses, s => !includes(statuses, ss => ss.id() === s.id())))
      statuses = statuses.concat(response4.value.statuses)
    } else {
      expect(response4.value.link).to.be.eql(response3.value.link)
    }
  })
  it('should load next statuses by link', async () => {
    let statuses = []
    const client = testClient()

    const response1 = await client.getTimeLine()
    expect(every(response1.value.statuses, s => !includes(statuses, ss => ss.id() === s.id())))
    statuses = statuses.concat(response1.value.statuses)

    const response2 = await client.getNext(response1.value.link)
    if (response2.value.statuses.length) {
      expect(response2.value.link.next.url).not.to.be.equals(response1.value.link.next.url)
      expect(every(response2.value.statuses, s => !includes(statuses, ss => ss.id() === s.id())))
      statuses = statuses.concat(response2.value.statuses)
    } else {
      expect(response2.value.link).to.be.eql(response1.value.link)
    }

    const response3 = await client.getNext(response2.value.link)
    if (response3.value.statuses.length) {
      expect(response3.value.link.next.url).not.to.be.equals(response2.value.link.next.url)
      expect(every(response3.value.statuses, s => !includes(statuses, ss => ss.id() === s.id())))
      statuses = statuses.concat(response3.value.statuses)
    } else {
      expect(response3.value.link).to.be.eql(response2.value.link)
    }

    const response4 = await client.getNext(response3.value.link)
    if (response4.value.statuses.length) {
      expect(response4.value.link.next.url).not.to.be.equals(response3.value.link.next.url)
      expect(every(response4.value.statuses, s => !includes(statuses, ss => ss.id() === s.id())))
      statuses = statuses.concat(response4.value.statuses)
    } else {
      expect(response4.value.link).to.be.eql(response3.value.link)
    }

    // expect(statuses).to.be.length(80)
  })
  xit('should load prev accounts by link', async () => {
    let accounts = []
    const client = testClient()

    const response1 = await client.getTimeLine()
    expect(every(response1.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
    accounts = accounts.concat(response1.value.accounts)

    const response2 = await client.getPrev(response1.value.link)
    if (response2.value.accounts.length) {
      expect(response2.value.link.prev.url).not.to.be.equals(response1.value.link.prev.url)
      expect(every(response2.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response2.value.accounts)
    } else {
      expect(response2.value.link).to.be.eql(response1.value.link)
    }

    const response3 = await client.getPrev(response2.value.link)
    if (response3.value.accounts.length) {
      expect(response3.value.link.prev.url).not.to.be.equals(response2.value.link.prev.url)
      expect(every(response3.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response3.value.accounts)
    } else {
      expect(response3.value.link).to.be.eql(response2.value.link)
    }

    const response4 = await client.getPrev(response3.value.link)
    if (response4.value.accounts.length) {
      expect(response4.value.link.prev.url).not.to.be.equals(response3.value.link.prev.url)
      expect(every(response4.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response4.value.accounts)
    } else {
      expect(response4.value.link).to.be.eql(response3.value.link)
    }
  })
  xit('should load next accounts by link', async () => {
    let accounts = []
    const client = testClient()

    const response1 = await client.getTimeLine()
    expect(every(response1.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
    accounts = accounts.concat(response1.value.accounts)

    const response2 = await client.getNext(response1.value.link)
    if (response2.value.accounts.length) {
      expect(response2.value.link.next.url).not.to.be.equals(response1.value.link.next.url)
      expect(every(response2.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response2.value.accounts)
    } else {
      expect(response2.value.link).to.be.eql(response1.value.link)
    }

    const response3 = await client.getNext(response2.value.link)
    if (response3.value.accounts.length) {
      expect(response3.value.link.next.url).not.to.be.equals(response2.value.link.next.url)
      expect(every(response3.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response3.value.accounts)
    } else {
      expect(response3.value.link).to.be.eql(response2.value.link)
    }

    const response4 = await client.getNext(response3.value.link)
    if (response4.value.accounts.length) {
      expect(response4.value.link.next.url).not.to.be.equals(response3.value.link.next.url)
      expect(every(response4.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response4.value.accounts)
    } else {
      expect(response4.value.link).to.be.eql(response3.value.link)
    }

    // expect(accounts).to.be.length(80)
  })
  xit('should load prev notifications by link', async () => {
    let accounts = []
    const client = testClient()

    const response1 = await client.getTimeLine()
    expect(every(response1.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
    accounts = accounts.concat(response1.value.accounts)

    const response2 = await client.getPrev(response1.value.link)
    if (response2.value.accounts.length) {
      expect(response2.value.link.prev.url).not.to.be.equals(response1.value.link.prev.url)
      expect(every(response2.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response2.value.accounts)
    } else {
      expect(response2.value.link).to.be.eql(response1.value.link)
    }

    const response3 = await client.getPrev(response2.value.link)
    if (response3.value.accounts.length) {
      expect(response3.value.link.prev.url).not.to.be.equals(response2.value.link.prev.url)
      expect(every(response3.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response3.value.accounts)
    } else {
      expect(response3.value.link).to.be.eql(response2.value.link)
    }

    const response4 = await client.getPrev(response3.value.link)
    if (response4.value.accounts.length) {
      expect(response4.value.link.prev.url).not.to.be.equals(response3.value.link.prev.url)
      expect(every(response4.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response4.value.accounts)
    } else {
      expect(response4.value.link).to.be.eql(response3.value.link)
    }
  })
  xit('should load next notifications by link', async () => {
    let accounts = []
    const client = testClient()

    const response1 = await client.getTimeLine()
    expect(every(response1.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
    accounts = accounts.concat(response1.value.accounts)

    const response2 = await client.getNext(response1.value.link)
    if (response2.value.accounts.length) {
      expect(response2.value.link.next.url).not.to.be.equals(response1.value.link.next.url)
      expect(every(response2.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response2.value.accounts)
    } else {
      expect(response2.value.link).to.be.eql(response1.value.link)
    }

    const response3 = await client.getNext(response2.value.link)
    if (response3.value.accounts.length) {
      expect(response3.value.link.next.url).not.to.be.equals(response2.value.link.next.url)
      expect(every(response3.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response3.value.accounts)
    } else {
      expect(response3.value.link).to.be.eql(response2.value.link)
    }

    const response4 = await client.getNext(response3.value.link)
    if (response4.value.accounts.length) {
      expect(response4.value.link.next.url).not.to.be.equals(response3.value.link.next.url)
      expect(every(response4.value.accounts, s => !includes(accounts, ss => ss.id() === s.id())))
      accounts = accounts.concat(response4.value.accounts)
    } else {
      expect(response4.value.link).to.be.eql(response3.value.link)
    }

    // expect(accounts).to.be.length(80)
  })
})
