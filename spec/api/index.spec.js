/**
 * Created by tottokotkd on 17/05/04.
 */

import forEach from 'lodash/forEach'

import Mastodon, {
  Response, Link,
  Account, Attachment, Application, Card, Context, Error, Instance, Mention, Notification, Relationship, Report, Result, Status, Tag
} from '../../lib/index'

import unlockedClient from '../testClient/unlockedClient'
import lockedClient from '../testClient/lockedClient'
import base64toBlob from './base64toBlob'
import img from './1.jpg'

xdescribe('api', function () {
  this.timeout(20000)
  describe('accounts', () => {
    it('should fetch an account', async () => {
      const client = unlockedClient()
      const response = await client.getAccount(1)

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('account')

        const {account} = response.value
        expect(account).to.be.instanceOf(Account)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
    it('should get the current user', async () => {
      const client = unlockedClient()
      const response = await client.getCurrentAccount()

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('account')

        const {account} = response.value
        expect(account).to.be.instanceOf(Account)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
    it('should update the current user', async () => {
      const client = unlockedClient()

      const displayName = 'noop'
      const note = 'hey look this guy testing with mstdn.jp... ' + (new Date())
      const response = await client.updateCurrentAccount({displayName, note})

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('account')

        const {account} = response.value
        expect(account).to.be.instanceOf(Account)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
    it('should get an account_s followers', async () => {
      const client = unlockedClient()
      const {account} = (await client.getCurrentAccount()).value
      const response = await client.getFollowers(account.id())

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('accounts')

        const {accounts} = response.value
        expect(accounts).to.be.instanceOf(Array)
        forEach(accounts, a => expect(a).to.be.instanceOf(Account))

        expect(response.value).to.have.property('link')
        expect(response.value.link).to.be.instanceOf(Link)
      } else {
        throw response
      }
    })
    it('should get who account is following', async () => {
      const client = unlockedClient()
      const {account} = (await client.getCurrentAccount()).value
      const response = await client.getFollowings(account.id())

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('accounts')

        const {accounts} = response.value
        expect(accounts).to.be.instanceOf(Array)
        forEach(accounts, a => expect(a).to.be.instanceOf(Account))

        expect(response.value).to.have.property('link')
        expect(response.value.link).to.be.instanceOf(Link)
      } else {
        throw response
      }
    })
    it('should get an account_s statuses', async () => {
      const client = unlockedClient()
      const {account} = (await client.getCurrentAccount()).value
      const response = await client.getStatusesOfAccount(account.id())

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('statuses')

        const {statuses} = response.value
        expect(statuses).to.be.instanceOf(Array)
        forEach(statuses, a => expect(a).to.be.instanceOf(Status))

        expect(response.value).to.have.property('link')
        expect(response.value.link).to.be.instanceOf(Link)
      } else {
        throw response
      }
    })
    it('should follow / unfollow an account:', async () => {
      const client = lockedClient()
      const target = (await unlockedClient().getCurrentAccount()).value.account

      const response1 = await client.followAccount(target.id())
      expect(response1).to.be.instanceOf(Response)
      if (response1.isSuccess()) {
        expect(response1.value).to.have.property('relationship')

        const {relationship} = response1.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.following()).to.be.true

        expect(response1.value).not.to.have.property('link')
      } else {
        throw response1
      }

      const response2 = await client.unfollowAccount(target.id())
      expect(response2).to.be.instanceOf(Response)
      if (response2.isSuccess()) {
        expect(response2.value).to.have.property('relationship')

        const {relationship} = response2.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.following()).to.be.false

        expect(response2.value).not.to.have.property('link')
      } else {
        throw response2
      }

      const response3 = await client.followAccount(target.id())
      expect(response3).to.be.instanceOf(Response)
      if (response3.isSuccess()) {
        expect(response3.value).to.have.property('relationship')

        const {relationship} = response3.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.following()).to.be.true

        expect(response3.value).not.to.have.property('link')
      } else {
        throw response3
      }

      const response4 = await client.unfollowAccount(target.id())
      expect(response4).to.be.instanceOf(Response)
      if (response4.isSuccess()) {
        expect(response4.value).to.have.property('relationship')

        const {relationship} = response4.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.following()).to.be.false

        expect(response4.value).not.to.have.property('link')
      } else {
        throw response4
      }
    })
    it('should block / unblock an account', async () => {
      const client = unlockedClient()
      const target = (await client.searchAccounts('themisto@mstdn.jp', 1)).value.accounts[0]

      const response1 = await client.blockAccount(target.id())
      expect(response1).to.be.instanceOf(Response)
      if (response1.isSuccess()) {
        expect(response1.value).to.have.property('relationship')

        const {relationship} = response1.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.blocking()).to.be.true

        expect(response1.value).not.to.have.property('link')
      } else {
        throw response1
      }

      const response2 = await client.unblockAccount(target.id())
      expect(response2).to.be.instanceOf(Response)
      if (response2.isSuccess()) {
        expect(response2.value).to.have.property('relationship')

        const {relationship} = response2.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.blocking()).to.be.false

        expect(response2.value).not.to.have.property('link')
      } else {
        throw response2
      }

      const response3 = await client.blockAccount(target.id())
      expect(response3).to.be.instanceOf(Response)
      if (response3.isSuccess()) {
        expect(response3.value).to.have.property('relationship')

        const {relationship} = response3.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.blocking()).to.be.true

        expect(response3.value).not.to.have.property('link')
      } else {
        throw response3
      }

      const response4 = await client.unblockAccount(target.id())
      expect(response4).to.be.instanceOf(Response)
      if (response4.isSuccess()) {
        expect(response4.value).to.have.property('relationship')

        const {relationship} = response4.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.blocking()).to.be.false

        expect(response4.value).not.to.have.property('link')
      } else {
        throw response4
      }
    })
    it('should mute / unmute an account', async () => {
      const client = unlockedClient()
      const target = (await client.searchAccounts('themisto@mstdn.jp', 1)).value.accounts[0]

      const response1 = await client.muteAccount(target.id())
      expect(response1).to.be.instanceOf(Response)
      if (response1.isSuccess()) {
        expect(response1.value).to.have.property('relationship')

        const {relationship} = response1.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.muting()).to.be.true

        expect(response1.value).not.to.have.property('link')
      } else {
        throw response1
      }

      const response2 = await client.unmuteAccount(target.id())
      expect(response2).to.be.instanceOf(Response)
      if (response2.isSuccess()) {
        expect(response2.value).to.have.property('relationship')

        const {relationship} = response2.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.muting()).to.be.false

        expect(response2.value).not.to.have.property('link')
      } else {
        throw response2
      }

      const response3 = await client.muteAccount(target.id())
      expect(response3).to.be.instanceOf(Response)
      if (response3.isSuccess()) {
        expect(response3.value).to.have.property('relationship')

        const {relationship} = response3.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.muting()).to.be.true

        expect(response3.value).not.to.have.property('link')
      } else {
        throw response3
      }

      const response4 = await client.unmuteAccount(target.id())
      expect(response4).to.be.instanceOf(Response)
      if (response4.isSuccess()) {
        expect(response4.value).to.have.property('relationship')

        const {relationship} = response4.value
        expect(relationship).to.be.instanceOf(Relationship)
        expect(relationship.muting()).to.be.false

        expect(response4.value).not.to.have.property('link')
      } else {
        throw response4
      }
    })
    it('should get an account_s relationships', async () => {
      const client = unlockedClient()
      const {account} = (await client.getCurrentAccount()).value
      const ids = (await client.getFollowings(account.id())).value.accounts.map(a => a.id())
      const response = await client.getRelationships(ids)

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('relationships')

        const {relationships} = response.value
        expect(relationships).to.be.instanceOf(Array)
        expect(relationships).to.be.length(ids.length)
        forEach(relationships, r => expect(r).to.be.instanceOf(Relationship))

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
    it('should search for accounts', async () => {
      const client = unlockedClient()
      const response = await client.searchAccounts('themisto@mstdn.jp', 50)

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('accounts')
        for (const account of response.value.accounts) {
          expect(account).to.be.instanceOf(Account)
        }
        expect(response.value.accounts.length).to.be.lessThan(51)
        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
  })
  describe('apps', () => {
    xit('should register an application:', async () => {
      const client = new Mastodon()
      client.setURLs({base: 'mstdn.jp'})
      await client.registerApp()

      const result = client.getCredentials()
      expect(result).to.have.property('clientID')
      expect(result).to.have.property('clientSecret')
      expect(`id: ${result.clientID}, secret: ${result.clientSecret}`)
        .to.be.eql('you should turn off this spec :)')
    })
    it('should fail with invalid url', async () => {
      const client = new Mastodon()
      client.setURLs({base: 'invalid.example.com'})
      const response = await client.registerApp()
      expect(response.type).to.be.equals(Response.Type.networkError)
    })
  })
  describe('blocks', () => {
    it('should fetch a user_s blocks:', async () => {
      const client = unlockedClient()
      const response = await client.getBlocks()

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('accounts')
        for (const account of response.value.accounts) {
          expect(account).to.be.instanceOf(Account)
        }

        expect(response.value).to.have.property('link')
      } else {
        throw response
      }
    })
  })
  describe('favorites', () => {
    it('should fetch a user_s favourites', async () => {
      const client = unlockedClient()
      const response = await client.getFavorites()

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('statuses')

        const {statuses} = response.value
        expect(statuses).to.be.instanceOf(Array)
        for (const status of statuses) {
          expect(status).to.be.instanceOf(Status)
        }

        expect(response.value).to.have.property('link')
      } else {
        throw response
      }
    })
  })
  describe('follows', () => {
    it('should Following a remote user', async () => {
      const target = (await unlockedClient().getCurrentAccount()).value.account
      const uri = `${target.username()}@${unlockedClient().exportConfig().url.base}`

      const client = lockedClient()
      const response = await client.followRemoteUser(uri)

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('account')
        const {account} = response.value
        expect(account).to.be.instanceOf(Account)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
  })
  describe('follow requests', () => {
    it('should fetch a list of follow requests:', async () => {
      const client = lockedClient()
      const client2 = unlockedClient()

      const target = (await client.getCurrentAccount()).value.account.id()
      const follower = (await client2.getCurrentAccount()).value.account.id()
      await client2.unfollowAccount(target)
      await client2.followAccount(target)

      const response1 = await client.getFollowRequests()

      expect(response1).to.be.instanceOf(Response)
      if (response1.isSuccess()) {
        expect(response1.value).to.have.property('accounts')

        const {accounts} = response1.value

        expect(accounts).to.be.instanceOf(Array)
        for (const account of accounts) {
          expect(account).to.be.instanceOf(Account)
        }
        expect(accounts.map(a => a.id())).to.include(follower)

        expect(response1.value).to.have.property('link')
      } else {
        throw response1
      }
    })
    it('should authorize or rejecting follow requests', async () => {
      const client = lockedClient()
      const client2 = unlockedClient()

      const target = (await client.getCurrentAccount()).value.account.id()
      const follower = (await client2.getCurrentAccount()).value.account.id()
      await client2.unfollowAccount(target)

      // request -> reject
      await client2.followAccount(target)

      const a1 = (await client2.getFollowings(follower)).value.accounts
      expect(a1.map(a => a.id())).not.to.include(target)

      const r1 = (await client.getFollowRequests()).value.accounts
      expect(r1.map(r => r.id())).to.include(follower)

      const response1 = await client.rejectFollowRequest(follower)
      expect(response1.isSuccess()).to.be.true

      const a2 = (await client2.getFollowings(follower)).value.accounts
      expect(a2.map(a => a.id())).not.to.include(target)

      // request -> authorize
      await client2.followAccount(target)

      const a3 = (await client2.getFollowings(follower)).value.accounts
      expect(a3.map(a => a.id())).not.to.include(target)

      const r2 = (await client.getFollowRequests()).value.accounts
      expect(r2.map(r => r.id())).to.include(follower)

      const a4 = (await client2.getFollowings(follower)).value.accounts
      expect(a4.map(a => a.id())).not.to.include(target)

      const response2 = await client.authorizeFollowRequest(follower)
      expect(response2.isSuccess()).to.be.true

      const a5 = (await client2.getFollowings(follower)).value.accounts
      expect(a5.map(a => a.id())).to.include(target)
    })
  })
  describe('instances', () => {
    it('should get instance information:', async () => {
      const client = unlockedClient()
      const response = await client.getInstanceInfo()

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('instance')
        const {instance} = response.value
        expect(instance).to.be.instanceOf(Instance)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
  })
  describe('media', () => {
    const blob = () => {
      const matched = img.match(/^(\w+):([^;]+);(.*),([^,]+)$/)
      const base64 = matched[4]
      const type = matched[2]

      return base64toBlob(base64, type)
    }

    it('should upload a media attachment:', async () => {
      const client = unlockedClient()
      const response = await client.uploadMediaAttachment(blob())

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('attachment')

        const {attachment} = response.value
        expect(attachment).to.be.instanceOf(Attachment)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }

      const response2 = await client.uploadMediaAttachment(blob())
      const response3 = await client.uploadMediaAttachment(blob())
      const response4 = await client.uploadMediaAttachment(blob())

      const mediaIds = [
        response.value.attachment.id(),
        response2.value.attachment.id(),
        response3.value.attachment.id(),
        response4.value.attachment.id(),
      ]
      const status = await client.postStatus(`media test ${mediaIds.length}`, {mediaIds, visibility: 'unlisted'})
      expect(status.isSuccess()).to.be.true
    })
  })
  describe('mutes', () => {
    it('should fetch a user_s mutes', async () => {
      const client = unlockedClient()
      const response = await client.getMutes()

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('accounts')

        const {accounts} = response.value
        expect(accounts).to.be.instanceOf(Array)
        for (const account of accounts) {
          expect(account).to.be.instanceOf(Account)
        }

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
  })
  describe('notifications', () => {
    it('should fetch a user_s notifications', async () => {
      const client = unlockedClient()
      const other = lockedClient()

      // make notifications
      const target = (await client.getCurrentAccount()).value.account.id()
      await other.unfollowAccount(target)
      await other.followAccount(target)

      const response = await client.getNotifications()
      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('notifications')

        const {notifications} = response.value
        expect(notifications).to.be.instanceOf(Array)
        expect(notifications.length).to.be.greaterThan(0)
        for (const notification of notifications) {
          expect(notification).to.be.instanceOf(Notification)
        }

        expect(response.value).to.have.property('link')
        expect(response.value.link).to.be.instanceOf(Link)
      } else {
        throw response
      }
    })
    it('should get a single notification:', async () => {
      const client = unlockedClient()
      const other = lockedClient()

      // make notifications
      const target = (await client.getCurrentAccount()).value.account.id()
      await other.unfollowAccount(target)
      await other.followAccount(target)
      const {notifications} = (await client.getNotifications()).value

      const response = await client.getNotification(notifications[0].id())
      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('notification')

        const {notification} = response.value
        expect(notification).to.be.instanceOf(Notification)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
    it('should clear notifications', async () => {
      const client = unlockedClient()
      const other = lockedClient()

      // make notifications
      const target = (await client.getCurrentAccount()).value.account.id()
      await other.unfollowAccount(target)
      await other.followAccount(target)

      await client.clearNotifications()
      const response = await client.getNotifications()
      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        const {notifications} = response.value
        expect(notifications).to.be.length(0)
      } else {
        throw response
      }
    })
  })
  describe('reports', () => {
    xit('should fetch a user_s reports:', async () => {})
    xit('should report a user', async () => {})
  })
  describe('search', () => {
    it('should search for content', async () => {
      const client = unlockedClient()
      const response = await client.search('test', true)

      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('result')

        const {result} = response.value
        expect(result).to.be.instanceOf(Result)

        const accounts = result.accounts()
        expect(accounts).to.be.instanceOf(Array)
        for (const account of accounts) {
          expect(account).to.be.instanceOf(Account)
        }

        const hashtags = result.hashtags()
        expect(hashtags).to.be.instanceOf(Array)
        for (const hashtag of hashtags) {
          expect(hashtag).to.be.string
        }

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
  })
  describe('statuses', () => {
    it('should fetch a status', async () => {
      const client = unlockedClient()
      const targetStatus = (await client.postStatus('fetch test status')).value.status

      const response = await client.fetchStatus(targetStatus.id())
      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('status')
        const {status} = response.value
        expect(status).to.be.instanceOf(Status)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
    it('should get status context', async () => {
      const client = unlockedClient()
      const targetStatus = (await client.postStatus('fetch context test')).value.status

      const response = await client.getStatusContext(targetStatus.id())
      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('context')
        const {context} = response.value
        expect(context).to.be.instanceOf(Context)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
    it('should get a card associated with a status', async () => {
      const client = unlockedClient()
      const targetStatus = (await client.postStatus('fetch card test')).value.status

      const response = await client.getStatusCard(targetStatus.id())
      expect(response).to.be.instanceOf(Response)
      if (response.isSuccess()) {
        expect(response.value).to.have.property('card')
        const {card} = response.value
        expect(card).to.be.instanceOf(Card)

        expect(response.value).not.to.have.property('link')
      } else {
        throw response
      }
    })
    it('should get who reblogged a status', async () => {
      const client = unlockedClient()
      const {account} = (await client.getCurrentAccount()).value
      const targetStatus = (await client.postStatus('fetch reblogged accounts test')).value.status

      const response1 = await client.getRebloggingAccounts(targetStatus.id())
      expect(response1).to.be.instanceOf(Response)
      if (response1.isSuccess()) {
        expect(response1.value).to.have.property('accounts')
        const {accounts} = response1.value
        expect(accounts).to.be.instanceOf(Array)
        expect(accounts).to.be.length(0)

        expect(response1.value).not.to.have.property('link')
      } else {
        throw response1
      }

      await client.reblogStatus(targetStatus.id())
      const response2 = await client.getRebloggingAccounts(targetStatus.id())
      expect(response2).to.be.instanceOf(Response)
      if (response2.isSuccess()) {
        expect(response2.value).to.have.property('accounts')
        const {accounts} = response2.value
        expect(accounts).to.be.instanceOf(Array)
        expect(accounts).to.be.length(1)
        expect(accounts[0].id()).to.be.equals(account.id())

        expect(response2.value).not.to.have.property('link')
      } else {
        throw response2
      }
    })
    it('should get who favorited a status', async () => {
      const client = unlockedClient()
      const {account} = (await client.getCurrentAccount()).value
      const targetStatus = (await client.postStatus('fetch favorited accounts test')).value.status

      const response1 = await client.getFavoritingAccounts(targetStatus.id())
      expect(response1).to.be.instanceOf(Response)
      if (response1.isSuccess()) {
        expect(response1.value).to.have.property('accounts')
        const {accounts} = response1.value
        expect(accounts).to.be.instanceOf(Array)
        expect(accounts).to.be.length(0)

        expect(response1.value).not.to.have.property('link')
      } else {
        throw response1
      }

      await client.favoriteStatus(targetStatus.id())
      const response2 = await client.getFavoritingAccounts(targetStatus.id())
      expect(response2).to.be.instanceOf(Response)
      if (response2.isSuccess()) {
        expect(response2.value).to.have.property('accounts')
        const {accounts} = response2.value
        expect(accounts).to.be.instanceOf(Array)
        expect(accounts).to.be.length(1)
        expect(accounts[0].id()).to.be.equals(account.id())

        expect(response2.value).not.to.have.property('link')
      } else {
        throw response2
      }
    })
    it('should post a new status', async () => {
      const client = unlockedClient()
      const {account} = (await client.getCurrentAccount()).value

      const response1 = await client.postStatus(`test 1 @${account.acct()}`, {
        replyTo: null,
        mediaIds: [],
        sensitive: false,
        spoilerText: 'test spoiler',
        visibility: 'direct',
      })

      expect(response1).to.be.instanceOf(Response)
      let status1 = null
      if (response1.isSuccess()) {
        expect(response1.value).to.have.property('status')

        status1 = response1.value.status
        expect(status1).to.be.instanceOf(Status)

        expect(status1.content()).to.include('mention_link')
        expect(status1.content()).to.include(`@${account.username()}`)
        expect(status1.inReplyToId()).to.be.null
        expect(status1.inReplyToAccountId()).to.be.null
        expect(status1.sensitive()).to.be.false
        expect(status1.visibility()).to.be.equals(Status.Visibility.direct)
        expect(status1.spoilerText()).to.be.equals('test spoiler')

        expect(response1.value).not.to.have.property('link')
      } else {
        throw response1
      }

      const response2 = await client.postStatus('test 2', {
        replyTo: status1.id(),
        mediaIds: [],
        sensitive: true,
        spoilerText: null,
        visibility: 'unlisted',
      })

      expect(response2).to.be.instanceOf(Response)
      if (response2.isSuccess()) {
        expect(response2.value).to.have.property('status')

        const status2 = response2.value.status
        expect(status2).to.be.instanceOf(Status)

        expect(status2.inReplyToId()).to.be.equals(status1.id())
        expect(status2.inReplyToAccountId()).to.be.equals(account.id())
        expect(status2.sensitive()).to.be.true
        expect(status2.visibility()).to.be.equals(Status.Visibility.unlisted)
        expect(status2.spoilerText()).to.be.equals('')

        expect(response2.value).not.to.have.property('link')
      } else {
        throw response2
      }
    })
    it('should delete a status', async () => {
      const client = unlockedClient()
      const targetStatus = (await client.postStatus('delete test status')).value.status
      await client.deleteStatus(targetStatus.id())

      const response = await client.deleteStatus(targetStatus.id())
      expect(response).to.be.instanceOf(Response)
      expect(response.isSuccess()).to.be.true
    })
    it('should reblog / unreblog a status', async () => {
      const client = unlockedClient()
      const targetStatus = (await client.postStatus('reblog / unreblog test status')).value.status

      const response1 = await client.reblogStatus(targetStatus.id())
      expect(response1).to.be.instanceOf(Response)
      if (response1.isSuccess()) {
        expect(response1.value).to.have.property('status')
        const {status} = response1.value
        expect(status).to.be.instanceOf(Status)
        expect(status.reblogged()).to.be.true

        expect(response1.value).not.to.have.property('link')
      } else {
        throw response1
      }

      const response2 = await client.unreblogStatus(targetStatus.id())
      expect(response2).to.be.instanceOf(Response)
      if (response2.isSuccess()) {
        expect(response2.value).to.have.property('status')
        const {status} = response2.value
        expect(status).to.be.instanceOf(Status)
        expect(status.reblogged()).to.be.false

        expect(response2.value).not.to.have.property('link')
      } else {
        throw response2
      }
    })
    it('should favorite / unfavorite a status', async () => {
      const client = unlockedClient()
      const targetStatus = (await client.postStatus('favorite / unfavorite test status')).value.status

      const response1 = await client.favoriteStatus(targetStatus.id())
      expect(response1).to.be.instanceOf(Response)
      if (response1.isSuccess()) {
        expect(response1.value).to.have.property('status')
        const {status} = response1.value
        expect(status).to.be.instanceOf(Status)
        expect(status.favorited()).to.be.true

        expect(response1.value).not.to.have.property('link')
      } else {
        throw response1
      }

      const response2 = await client.unfavoriteStatus(targetStatus.id())
      expect(response2).to.be.instanceOf(Response)
      if (response2.isSuccess()) {
        expect(response2.value).to.have.property('status')
        const {status} = response2.value
        expect(status).to.be.instanceOf(Status)
        expect(status.favorited()).to.be.false

        expect(response2.value).not.to.have.property('link')
      } else {
        throw response2
      }
    })
  })
  describe('timelines', () => {
    it('should retrieve home timeline', async () => {
      const client = unlockedClient()
      const response = await client.getHomeTimeLine()

      expect(response).to.be.instanceOf(Response)
      expect(response.type).to.be.equals(Response.Type.success)

      expect(response.value).to.have.property('statuses')
      expect(response.value.statuses).to.be.instanceOf(Array)
      forEach(response.value.statuses, status => {
        expect(status).to.be.instanceOf(Status)
      })

      expect(response.value).to.have.property('link')
      expect(response.value.link).to.be.instanceOf(Link)
    })
    it('should retrieve public timeline', async () => {
      const client = unlockedClient()
      const response = await client.getTimeLine()

      expect(response).to.be.instanceOf(Response)
      expect(response.type).to.be.equals(Response.Type.success)

      expect(response.value).to.have.property('statuses')
      expect(response.value.statuses).to.be.instanceOf(Array)
      forEach(response.value.statuses, status => {
        expect(status).to.be.instanceOf(Status)
      })

      expect(response.value).to.have.property('link')
      expect(response.value.link).to.be.instanceOf(Link)
    })
    it('should retrieve hashtag timeline', async () => {
      const client = unlockedClient()
      const response = await client.getHashTagTimeLine('test')

      expect(response).to.be.instanceOf(Response)
      expect(response.type).to.be.equals(Response.Type.success)

      expect(response.value).to.have.property('statuses')
      expect(response.value.statuses).to.be.instanceOf(Array)
      forEach(response.value.statuses, status => {
        expect(status).to.be.instanceOf(Status)
      })

      expect(response.value).to.have.property('link')
      expect(response.value.link).to.be.instanceOf(Link)
    })
  })
})
