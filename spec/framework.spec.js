import assert from 'assert';
import lib from '../lib/index';

describe('(Framework)', () => {
  describe('Karma Plugins', () => {
    it('Should expose "expect" globally.', () => {
      assert.ok(expect);
    });

    it('Should expose "should" globally.', () => {
      assert.ok(should);
    });

    it('Should expose "sinon" globally.', () => {
      assert.ok(sinon);
    });

    it('Should expose a "__Rewire__" function on every import', () => {
      assert.ok(lib.__Rewire__);
    });

    it('Should have chai-as-promised helpers.', () => {
      const pass = new Promise((resolve) => resolve('test'));
      const fail = new Promise((resolve, reject) => reject());

      return Promise.all([
        expect(pass).to.be.fulfilled,
        expect(fail).to.not.be.fulfilled,
      ]);
    });
  });
});
